<?php
session_start();
include_once 'system/config.php';
include_once 'system/controller.php';
include_once 'system/connector.php';
include_once 'system/system.php';
include_once 'system/view.php';
include_once 'system/lang.php';

$startData = array(
	'instanceName' => 'bbDefault',
	'pageType' => null,
	'localPath' => null,
	'pageModeType' => 'frontend',
	'modulesPath' => 'modules/',
	'loginRequire' => true,
	'langList' => array('pl'),
	'langDefault' => 'pl'
);

$system = new system($startData);
$system->setPage();