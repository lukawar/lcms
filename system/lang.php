<?php

//cart
$lang['cart_is_empty'] = 'koszyk pusty';
$lang['cart_in'] = 'w koszyku';
$lang['cart_delete'] = 'usunięto z koszyka';
$lang['cart_add'] = 'dodano do koszyka';
$lang['cart_change'] = 'zmieniono zawartość koszyka';
$lang['cart_add_to'] = 'dodaj do koszyka';
$lang['cart_too_little'] = 'do zakupu brakuje ';
$lang['cart_calculate'] = 'przelicz koszyk';
$lang['cart_list_all'] = 'RAZEM: ';
$lang['cart_back'] = 'POWRÓT DO KOSZYKA';
$lang['cart_edit'] = 'edytuj zawartość koszyka';
$lang['return_to_category'] = 'kontynuuj zakupy';

//orders
$lang['order_go_to'] = 'przejdź do zamówienia';
$lang['add_send_address'] = 'dodaj adres wysyłki';
$lang['edit_send_address'] = 'zmień adres wysyłki';
$lang['order_confirm'] = 'ZATWIERDŹ';
$lang['order_save'] = 'zakończ zamówienie';

//other
$lang['delete_button'] = 'usuń';
$lang['too_little'] = 'za mało punktów';
$lang['count_too_little'] = 'wybrana ilość jest zbyt mała';
$lang['position_is_required'] = '* pozycje obowiązkowe';
$lang['zlotowka'] = ' + 1 zł';

//send
$lang['send_from_two'] = '<p>Zamówienie zostanie dostarczone w 2 przesyłkach, o nadaniu przesyłek poinformujemy mailowo.</p>';
$lang['send_from_one'] = '<p>O nadaniu przesyłki poinformujemy mailowo.</p>';

//schowek, przechowalnia, lista życzeń
$lang['chest'] = 'przechowalnia';
$lang['chest_add'] = 'dodaj do przechowalni';
$lang['chest_in'] = 'w przechowalni';
$lang['chest_del'] = 'usuń z przechowalni';
$lang['chest_to_cart'] = 'przenieś do koszyka';
$lang['chest_is_empty'] = '<p>przechowalnia jest pusta</p>';

//product
$lang['search_title'] = 'Wyniki wyszukiwania';