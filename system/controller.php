<?php
class Controller {
	public $data;
	
	public function __construct($type) {
		$this->typeSite = $type;
		$this->session = $_SESSION;	
		$this->post = $_POST;
		$this->get = $_GET;
		$this->data = new StdClass;
		$this->data = (object)array_merge((array)$this->session,(array)$this->post,(array)$this->get);
		$this->data->redirect = new StdClass;
		if(isset($_GET['action'])) $this->data->redirect->action = $_GET['action'];
		if(isset($_GET['option'])) $this->data->redirect->option = $_GET['option'];
		if(isset($_GET['cat'])) $this->data->redirect->cat = $_GET['cat'];
		if(isset($_GET['id'])) $this->data->redirect->id = $_GET['id'];
		$this->setControllerActions();
	}
	
	public function setControllerActions() {
		if(!isset($this->data->action) or empty($this->data->action))
			if($this->typeSite=='admin_')
				$this->data->action = 'index';
			else
				$this->data->action = 'homepage';
		if($this->data->action=='index' and $this->typeSite==null)
			$this->data->action=='homepage';
	}
	
	public function getData() {
		//var_dump($this->data);
		return $this->data; //return without filtering
	}
	
	public function trimData() {
		$data = $this->getData();
		$trim = array('lang', 'pl', 'en');
	
		if(in_array($data->option, $trim))
			$data->option = null;
		if(in_array($data->cat, $trim))
			$data->cat = null;
		if(in_array($data->id, $trim))
			$data->id = null;
			
		return (object)$data;
	}
	
	public function getMesData() {
		return mysql_escape_string((object)$this->data);
	}
	
	public function getHscData() {
		return htmlspecialchars((object)$this->data, ENT_QUOTES);
	}
	
	public function displayData() {
		var_dump($this->data);
	}
	
	public function displayDataArray() {
		$line = null;
		if(count($this->data)>0)
			foreach($this->data as $key=>$value)
				if(is_array($value)) {
					//$line.='<p>'.$key.'=>'.print_r($value).'</p>';
					foreach($value as $p_key=>$p_value)
						$line.='<p>'.$key.'=>'.$p_key.'['.$p_value.']</p>';
				} else $line.='<p>'.$key.'=>'.$value.'</p>';
		return $line;
	}
}
?>