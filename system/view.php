<?php
class View {
	public $db, $var, $lang;
	
	public function __construct() {
		global $db, $var, $data, $lang, $jslib, $csslib;
		$this->var = $var;
		$this->db = $db;
		$this->data = $data;
		$this->lang = (object)$lang;
		//var_dump($this->data);
	}
	
	//check admin privileges
	public function checkAdminRole($id, $action, $option) {
		if($this->var->admin_role=='admin')
			return true;
		else {
			//$this->db->queryString('select CAR_ADM_ID from centra_admin_roles where CAR_ADM_ID='.$id.' and CAR_ROLE_ID="'.$action.'" and CAR_ROLE_OPTION="'.$option.'" limit 1');
			$this->db->queryString('select ADR_ADM_ID from '.__BP.'admin_roles where ADR_ADM_ID='.$id.' and ADR_ROLE_ID="'.$action.'"');
			//$this->db->query();
			if($this->db->getRow())
				return true;
			else return false;
		}
	}
	
	public function setRedirectLink() {
		//debugger::logToConsole('mode: '.$this->data->pageModeType);
		if($this->data->pageModeType=='backend')
			$mode = true;
		else $mode = false;
		return helper::href(array($this->var->action, $this->var->option, $this->var->cat, $this->var->id), null, $mode);
	}
	
	public function parseContent($content) {
		$module_content = array();
		$module_list = array();
	
		preg_match_all("|{/[^>]+/}|U",  $content, $out, PREG_PATTERN_ORDER);
		if(count($out[0])>0) {
			$data_module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			foreach($out[0] as $module_str) {
				$module = str_replace(array('{/','/}'), array('',''), $module_str);
				$module = explode(':', $module);
				if(!isset($module[1])) 
			  	$module[1] = null;

				$module_content[] = $data_module->load($module[0],$module[1]);
				$module_list[] = $module_str;
			}

			return str_replace($module_list, $module_content, $content);
		} else return null;
	}
	
	public function pageError() {
		header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak strony do wyświetlenia</div>";
	}

	public function moduleError($module, $option) {
		return "<div class='errorInfo'>Can't load module - ".get_class($module)." / ".$option."</div>";
	}

	public function noProduct() {
		header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak produktów</div>";
	}
	
	public function noCategory() {
		header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak kategorii</div>";
	}

	public function userNotLogged() {
		return "<div class='warningInfo'>zaloguj się, by uzyskać dostęp do strony</div>";
	}

	public function noData() {
		return "<p align='center'> - brak danych - </p>";
	}

	public function goAway() {
		return "<div class='warningInfo'>brak uprawnień</div>";
	}

	public function error() {
		return "<div class='warningInfo'>wystąpił błąd</div>";
	}
}

interface IModule {
	public function showModule();
}

interface IModuleHomepage {
	public function showModule();
	public function homepage();
}