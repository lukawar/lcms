<?php
class Connector extends PDO {//connecting with PDO
	public $pdo, $pdoConnection, $queryString, $queryWhere, $queryGroupBy, $queryOrderBy, $queryJoin, $tableName;
	
  public function __construct($queryString=null) {
    $this->queryString = $queryString;
  }

	public function queryString($queryString) {
		unset($this->queryWhere);
		unset($this->queryOrderBy);
		unset($this->queryGroupBy);
		unset($this->queryJoin);
		$this->tableName = null;
		$this->queryString = $queryString;
	}
	
	//for insert loops - initialise table
	public function tableName($queryString) {
		$this->tableName = $queryString;
	}
	
	public function prepareQuery($limit = null) {
		if(isset($this->queryJoin)) {
			if(count($this->queryJoin)>0) {
				$join_string = implode(' ',$this->queryJoin);
				$this->queryString.= $join_string;
			}
		}
		
		if(isset($this->queryWhere)) {
			if(count($this->queryWhere)>0) {
				$where_string = implode(' ',$this->queryWhere);
				$this->queryString.= $where_string;
			}
		}
		
		if(isset($this->queryGroupBy)) {
			if(count($this->queryGroupBy)>0) {
				$group_string = implode(' ',$this->queryGroupBy);
				$this->queryString.= $group_string;
			}
		}
		
		if(isset($this->queryOrderBy)) {
			if(count($this->queryOrderBy)>0) {
				$order_string = implode(' ',$this->queryOrderBy);
				$this->queryString.= $order_string;
			}
		}
		
		if(isset($limit)) {
			if(isset($this->limit)) {
				$this->queryString.= $this->limit;
			}
		}
		unset($this->queryWhere);
		unset($this->queryOrderBy);
		unset($this->queryGroupBy);
		unset($this->queryJoin);
	}
	
  public function BaseConnection() { //create PDO object 
		$this->pdo = new PDO("mysql:host=".__HOST.";dbname=".__BASE, __USER, __PASS);
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->pdo->query('SET NAMES utf8');
		//$this->pdo->query("SET GLOBAL log_output = 'TABLE'");
		//$this->pdo->query("SET GLOBAL general_log = 'ON'");
		//$this->pdo->query('SET CHARACTER_SET utf8_unicode_ci');
		return $this->pdo;
  }

  public function insertQuery($istring) {
    foreach($istring as $key=>$is) {
      $label[] = $key;
      $value[] = "'".$is."'";
    }

    $this->queryString = "insert into ".$this->queryString." (".implode(",",$label).") values (".implode(",",$value).")";
    return $this->ExecQuery();
  }
  
  public function insertRow($istring) {
  	if(isset($this->tableName) and !is_null($this->tableName)) {
	    foreach($istring as $key=>$is) {
	      $label[] = $key;
	      $value[] = "'".$is."'";
	    }

	    $this->queryString = "insert into ".$this->tableName." (".implode(",",$label).") values (".implode(",",$value).")";
	    return $this->ExecQuery();
	  }
  }
  
  public function prepareInsertQuery($istring) {
    foreach($istring as $key=>$is) {
      $label[] = $key;
      $value[] = "'".$is."'";
    }

    $this->queryString = "insert into ".$this->queryString." (".implode(",",$label).") values (".implode(",",$value).")";
  }
	
  /* istring - array with elements
     id - identyficator
     id_value - value of identyficator */
  public function updateQuery($istring, $id, $id_value) {
    foreach($istring as $key=>$is)
      $value[] =$key.'="'.$is.'"';
    $this->queryString = "update ".$this->queryString." set ".implode(",",$value)." where $id='$id_value'";
    //$this->query();
    $this->ExecQuery();
  }
  
  /**
	* string - parameters in aray
	* query->where
	*/
  public function update($string) {
  	/*if(count($this->queryWhere)>0) {
			$where_string = implode(' ',$this->queryWhere);
			$this->where_string.= $where_string;
		}*/
		
		foreach($string as $key=>$is)
      $value[] =$key.'="'.$is.'"';
    $this->queryString = "update ".$this->queryString." set ".implode(",",$value);
    //unset($this->queryWhere);
    $this->ExecQuery();
	}
	
	/* id - identyficator
     id_value - value of identyficator */
	public function delQuery($id, $id_value) {
		$this->queryString = "delete from ".$this->queryString." where $id='$id_value'";
    $this->ExecQuery();
	}

  public function GetData() { //get query from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();

		try {
			$this->prepareQuery('with_limit');
				
   		$this->cell = $this->pdoConnection->query($this->queryString);
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 
    return $this->cell;
  }
  
  public function GetDataFetch() { //get query from base
  	global $sql_data;
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
			$this->prepareQuery('with_limit');
			
	  	$this->cell = $this->pdoConnection->query($this->queryString);
	  } catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
	  
	  $this->getCount = $this->cell->rowCount();
    
    $sql_data.= '<p><b>sql: </b>'.$this->queryString.'</p>';
    
    if($this->getCount>0)
    	return $this->cell;
    else return false;
  }
  
  public function getFoundRows() {
		if(isset($this->queryString)) {
			$this->cell = $this->pdoConnection->query('SELECT FOUND_ROWS() AS countRows');
			$c = $this->cell->fetch();
			return $c['countRows'];
		}
	}

  public function ExecQuery() {
  	global $sql_data;
		if(null === $this->pdoConnection) 
    	$this->pdoConnection = $this->BaseConnection();
			
		try {
			$this->prepareQuery();
    	$this->cell = $this->pdoConnection->exec($this->queryString);
    } catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
	  
	  $sql_data.= '<p><b>sql: </b>'.$this->queryString.'</p>';
    return $this->pdo->lastInsertId();
  }

  public function RunQuery() {
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		try {
			$this->prepareQuery();
			
			$this->cell = $this->pdoConnection->query($this->queryString);
		} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
    return $this->pdo->lastInsertId();
  }

  public function GetRow() { //get one row from base
  	global $sql_data;
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
			$this->prepareQuery();
			
   		$this->cell = $this->pdoConnection->query($this->queryString." limit 1");
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 		
 		$sql_data.= '<p><b>sql: </b>'.$this->queryString.' limit 1</p>';
    return $this->cell->fetch();
  }
  
  public function dict($dict, $lang='pl') { //get dict value from from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
   		$this->cell = $this->pdoConnection->query("select * from ".__BP."dict where DIC_KEY='".$dict."' and DIC_LNG='".$lang."' limit 1");
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 
    return $this->cell->fetch();
  }
  
  /* id - DIC_KEY value
     value - data value 
     row - row name */
  public function setDict($id, $value, $row='DIC_VALUE') {
    $this->queryString = "update ".__BP."dict set ".$row."='".$value."' where DIC_KEY='$id'";
    $this->ExecQuery();
  }
  
  public function limit($l, $count=null) {
  	if(!is_null($count))
  		$count = ','.$count;
		$this->limit= ' limit '.$l.$count;
	}
	
	public function where($glue, $where) {
		if(!isset($this->queryWhere)) 
			$this->queryWhere = null;
		if(count($this->queryWhere)>0)
			$this->queryWhere[] = $glue.' '.$where;
		else $this->queryWhere[] = ' where '.$where;
	}
	
	public function join($type, $table, $on1, $on2, $onAnd=null) {
		if(!isset($this->queryJoin)) 
			$this->queryJoin = null;
		if($on1==$on2)
			$this->queryJoin[] = ' '.$type.' join '.$table.' using ('.$on1.')';
		else {
			if($onAnd!=null)
				$this->queryJoin[] = ' '.$type.' join '.$table.' on ('.$on1.'='.$on2.' and '.$onAnd.')';
			else $this->queryJoin[] = ' '.$type.' join '.$table.' on '.$on1.'='.$on2;
		}
	}
	
	public function orderBy($order) {
		if(!isset($this->queryOrderBy)) 
			$this->queryOrderBy = null;
		if(count($this->queryOrderBy)>0)
			$this->queryOrderBy[] = ','.$order;		
		else $this->queryOrderBy[] = ' order by '.$order;
	}
	
	public function groupBy($group) {
		if(!isset($this->queryGroupBy)) 
			$this->queryGroupBy = null;
		if(count($this->queryGroupBy)>0)
			$this->queryGroupBy[] = ','.$group;
		else $this->queryGroupBy[] = ' group by '.$group;
	}
  
  public function getCount() { //
  	return $this->getCount;
  }
  
  public function query() { //show query
  	echo "<p>".$this->queryString."</p>";
  }
  
  public function showQuery() { //return query 
  	return $this->queryString;
  }
	
  public function close() {
	$this->pdo = null;
  }

  function __destruct() {
    //$this->cell->closeCursor();
  }
}