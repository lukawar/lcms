<?php
//get clint ip
class clientIP {
	static function get_old() {
		$ip = 0;
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
			
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipList = explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
			
			if($ip) {
				array_unshift($ipList, $ip);
				$ip = 0;
			}
			
			foreach($ipList as $v)
				if(!ereg('^(192\.168|172\.16|10|224|240|127|0)\.', $v))
					return $v;
					
		return $ip ? $ip : $_SERVER['REMOTE_ADDR'];
	}
	
	static function get() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
	}
	
	static function get_env() {
		/*$ip = getenv('HTTP_CLIENT_IP')?:
			getenv('HTTP_X_FORWARDED_FOR')?:
			getenv('HTTP_X_FORWARDED')?:
			getenv('HTTP_FORWARDED_FOR')?:
			getenv('HTTP_FORWARDED')?:
			getenv('REMOTE_ADDR');
			
		return $ip;*/
	}
}