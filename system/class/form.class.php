<?php
class form {
	public function __construct($action, $option, $title=null, $template='form.tpl.php') {
		global $data;
		$this->data = $data;	
		$this->form_action = $action;
		$this->form_option = $option;
		$this->form_template = $template;
		$this->form_title = $title;
		$this->elements = null;
	}
	//input type=text, password
	public function addText($label, $name, $required=null, $value=null, $template='input.tpl.php') {
		if($value)
			$val = ' value="'.$value.'"';
		else $val = null;
		
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$template)) 
			$input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$template);
		else $input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/input.tpl.php');
		$elements1 = array('__LABEL', '__NAME', '__VALUE', '__REQUIRED');
		$elements2 = array($label, $name, $val, $required);
		$this->elements.= str_replace($elements1, $elements2, $input);
	}
	
	//input - textarea, file
	public function addInput($label, $name, $value=null, $template='input.tpl.php', $require=null, $class=null) {
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$template)) 
			$input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$template);
		else $input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/input.tpl.php');
		$elements = array(
			'__LABEL' => $label, 
			'__NAME' => $name,
			'__VALUE' => $value,
			'__REQUIRE' => $require,
			'__CLASS' => $class
		);
		
		$this->elements.= Template::parse($elements, $input);
	}
	
	//checkbox
	public function addCheckbox($label, $name, $checked=null, $require=null, $template='checkbox.tpl.php') {
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$template)) 
			$input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$template);
		else $input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/checkbox.tpl.php');
		$elements = array(
			'__LABEL' => $label, 
			'__NAME' => $name,
			'__CHECKED' => $checked,
			'__REQUIRE' => $require
		);
		
		$this->elements.= Template::parse($elements, $input);
	}
	
	//select
	public function addSelect($label, $name, $positions, $value=null, $template='select.tpl.php', $require=null) {
		if(is_array($positions)) {
			if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$template)) 
				$input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$template);
			else $input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/ipnut.tpl.php');
			
			$select = new select($name, $name, 'form-control');
			foreach($positions as $pos_name=>$pos_value) {
				if($pos_name==$value)
					$select->addNode($pos_value, $pos_name, 'selected');
				else $select->addNode($pos_value, $pos_name);
			}
			
			$elements = array(
				'__LABEL' => $label, 
				'__NAME' => $name,
				'__SELECT' => $select->create()
			);
			
			$this->elements.= Template::parse($elements, $input);
		}
	}
	
	//input type=hidden
	public function addHidden($name, $value) {
		$this->elements.= '<input type="hidden" name="'.$name.'" value="'.$value.'">';
	}
	
	//various elements
	public function addElement($element) {
		$this->elements.= $element;
	}
	
	//input type submit
	public function addSubmit($value='zapisz', $template='submit.tpl.php', $element=null) {
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$template) and $template!=null) 
			$input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$template);
		else $input = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/submit.tpl.php');
		$elements = array('__VALUE' => $value, '__ELEMENT'=>$element);
		
		$this->elements.= Template::parse($elements, $input);
	}
	
	//create form
	public function setForm() {
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$this->form_template)) 
			$form_template = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$this->form_template);
		else $form_template = file_get_contents($this->data->localPath.$this->data->templatePath.'forms/form.tpl.php');
		
		$elements = array(
			'__BODY' => $this->elements,
			'__FORM_ACTION' => $this->form_action, 
			'__FORM_OPTION' => $this->form_option, 
			'__FORM_TITLE' => $this->form_title
		);
		
		return Template::parse($elements, $form_template);
	}
}