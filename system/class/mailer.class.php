<?php
//mailer library
class mailer {
	public $db, $var, $lang, $data;
	public $mailTemplatePath = 'templates/mail/';
	
	public function __construct() {
		global $db, $var, $data, $lang;
		$this->var = $var;
		$this->db = $db;
		$this->data = $data;
		$this->lang = (object)$lang;
		
		$this->subject = null;
		$this->template = 'default.tpl.php';
		
		require_once $this->data->localPath."system/lib/phpmailer/vendor/autoload.php";	
		
		$this->mail = new PHPMailer;
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();                         
		$this->mail->Host = __M_HOST;
		$this->mail->SMTPAuth = __M_SMTP_AUTH;     
		$this->mail->Username = __M_USER;                 
		$this->mail->Password = __M_PASS;
		$this->mail->Port = __M_PORT;
		$this->mail->CharSet = __M_CHARSET;

		$this->mail->From = __M_FROM_EMAIL;
		$this->mail->FromName = __M_FROM_NAME;
	}
	
	public function sendMail($mail_to, $name_to) {
		$this->mail->Subject = $this->subject;
		$this->mail->Body = str_replace('__CONTENT', $this->mailText, file_get_contents($this->data->localPath.$this->mailTemplatePath.$this->template));
		$this->mail->AltBody = $this->subject;
		
		//TODO: dynamic img linker
		$this->mail->addEmbeddedImage('templates/frontend/bb/img/logo_beabonus.png', 'logo.png', 'logo.png');
		$this->mail->isHTML(true);

		$this->mail->clearAddresses();
		$this->mail->addAddress($mail_to, $name_to);
		
		if(!$this->mail->send()) 
			$status =  "status: Mailer Error - " . $this->mail->ErrorInfo;
		else  $status = 'status: send';
		
		if(isset($this->var->admin_id) and !empty($this->var->admin_id))
			$insertId = $this->var->admin_id;
		elseif(isset($this->var->user_id) and !empty($this->var->user_id))
			$insertId = $this->var->user_id;
		else $insertId = 0;
		
		//save backup and status
		eventSaver::setBackup($insertId, $this->data->typeSite, 'mailer', 0, 'mailer', $status, 'to: '.$mail_to, 'title: '.$this->subject, 'content: '.addslashes($this->mailText));
	}
	
}