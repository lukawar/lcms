<?php
class helper {
  static function addhttp($url) {
    if(!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
  }
  
  static function href($parameters, $hash=null, $short=null) {
		$p = array('action', 'option', 'id', 'idp');
		$link = null;
		$counter = 0;
		$link__temp = array();
		if(is_array($parameters)) {
			foreach($parameters as $par) {
				if($par!=null) {
					$link_temp[$p[$counter]] = $par;
					$counter++;
				}
			}
			
			if(__URL_SHORT_LINK and $short==null) {
					$link = implode('-', $link_temp);
					$link.='.html';
				} else {
					foreach($link_temp as $key=>$value)
						$link.= '&'.$key.'='.$value;
					$link[0] = '?';
				}
			if($hash)
				$link.='#'.$hash;
				
			//die('link: '.$link);
			return $link;
		} else return '#';
	}
	
	public function saveCookie($name, $data) {
		setcookie($name, $data, time()+(3600*24));
	}
	
	public function langText($lang, $key) {
    global $langText;
		return $langText[$lang][$key];
	}
	
	public function languages() {
		$controller = new Controller;
		$this->var = $controller->trimData();
		//var_dump($this->var);
		if($this->var->s_lang=='pl') {
			return '<img src="img/icons/pl_a.png">&nbsp;&nbsp;&nbsp;<a href="'.helper::href(array($this->var->action, $this->var->option, $this->var->cat, $this->var->id, 'lang', 'en')).'"><img src="img/icons/en.png"></a>';
		} elseif($this->var->s_lang=='en') {
			return '<a href="'.helper::href(array($this->var->action, $this->var->option, $this->var->cat, $this->var->id, 'lang', 'pl')).'"><img src="img/icons/pl.png"></a>&nbsp;&nbsp;&nbsp;<img src="img/icons/en_a.png">';
		}
	}
	
	static function setQInfo($container, $info) {
		//TODO: create infobox
		$infoBox =  '<script>$("'.$container.'").append("'.$info.'");</script>';
	}
	
}