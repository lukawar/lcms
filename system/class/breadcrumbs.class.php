<?php
//create breadcrumb with paths & separator
class breadcrumbs {
	public $separator = '&nbsp;&raquo&nbsp;';
	
	public function __construct() {
		$this->crumbs = array();
	}
	
	public function addCrumb($name, $link=null, $class=null) {
		if($class)
			$class_name = " class='$class'";
		if($link)
			$this->crumbs[] = "<a href='$link'$class_name>$name</a>";
		else $this->crumbs[] = $name;
	}
	
	public function create() {
		$i = 0;
		foreach($this->crumbs as $crumb) { 
			if($i>0)
				$breadCrumb.= $this->separator.$crumb;
			else $breadCrumb.= $crumb;
			$i++;
		}
		return "<div id='breadcrumb'>".$breadCrumb."</div>";
	}
}