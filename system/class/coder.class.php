<?php
class coder {
	static function hashString($string) {
		$salt = 'f8j2S5jX8at2Do0saV_ansPw4w';
		$result = hash('sha256', $string.$salt);
		return $result;
	}
	
	static function mixString($string1, $string2) {
		$result = hash('sha256', $string1.'_'.$string);
		return $result;
	}
	
	static function stripPL_a($str, $replace = '_') {
	  $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	  $charsArr =  array('^', "'", '"', '`', '~');
	  $str = str_replace($charsArr, '', $str );
	  $return = trim(ereg_replace(' +',' ',preg_replace('/[^a-zA-Z0-9s]/','',strtolower($str))));
	  return str_replace(' ', $replace, $return);
  }
  
  static function stripPL($string){
  	//$trips = array'Ę'=>'e','ę'=>'e', 'Ó'=>'o', 'ó'=>'o','Ą'=>'a', 'ą'=>'a', 'Ś'=>'s', 'ś'=>'s', 'Ł'=>'l','ł'=>'l','Ź'=>'z', 'ź'=>'z', 'Ż'=>'z', 'ż'=>'z', 'Ć'=>'c','ć'=>'c', 'Ń'=>'n', 'ń'=>'n', ' '=>'_'
  	$strip1 = array('ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń', 'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', ' ', '&', '!', '-', '?');
  	$strip2 = array('e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', 'e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', '_', '_and_', '', '', '');
		
		return strtolower(str_replace($strip1, $strip2, $string));
	}
	
	static function generatePassword(){
    $pattern='qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    for($i=0; $i<8; $i++) {
    	$key.=$pattern{rand(0,strlen($pattern)-1)};
    }
    
    $key = ucfirst($key).rand(0, 99);
    return $key;
	}
  
  static function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '-_,');
	}

	static function base64_url_decode($input){
	  return base64_decode(strtr($input, '-_,', '+/='));
	}
  
  static function decryptString($key, $client_id, $secret_key) {
		//$key = 'sepultura';
		$iv = base64_decode($secret_key);
		$encrypted_string['client_id'] = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($client_id), MCRYPT_MODE_CFB, $iv);
		
		return $encrypted_string;
	}
	
	static function encryptString($string, $key) {
		//$id_string = 'jordanMatcon_d8dfjsdfj8';
		//$key = 'sepultura';
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB), MCRYPT_RAND);
		
		$encrypted_string = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_CFB, $iv));
		//$encrypted_string['key'] = base64_encode($iv);
		
		return $encrypted_string;
	}
	
	static function nip($input) {
		return preg_replace("/[^0-9]/", "",$input);
	}
}