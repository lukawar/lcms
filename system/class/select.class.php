<?php
//create select
class select {
	public function __construct($id, $name=null, $class=null, $multiple=null, $attr=null) {
		$this->id = $id;
		$this->name = $name;
		$this->class = $class;
		$this->attr = $attr;
		$this->node = null;
		
		if($multiple=='multiple')
			$multiple = " multiple='multiple'";
		
		$this->html = "<select id='".$this->id."' name='".$this->name."' class='".$this->class."'".$multiple.$attr.">__NODES\n\r</select>";
	}
	
	public function addNode($name, $value, $selected=null) {
		if($selected=='selected')
			$selected = " selected='selected'";
		else $selected = '';
			
		$this->node.="<option value='".$value."'".$selected.">".$name."</option>\n\r";
	}
	
	public function create() {
		return str_ireplace('__NODES',$this->node,$this->html);
	}
}