<?php
//create tables with rows, cells
class table {
	public $html, $td, $th;
	public function __construct($id=null, $class=null) {
		if($class!='') $class = "class='$class'";
		if($id!='') $id = "id='$id'";
		$this->htmlHead = "<table $class $id>";
	}
	
	public function createHeader($th) {
		foreach($th as $h) {
			if($h=='')
				$this->td.="<th width='30px'>&nbsp;</th>";
			else $this->td.="<th>".$h."</th>";
		}
		$this->html.=$this->addHeaderRow();
	}
	
	public function createBody() {
	  $this->html = $this->htmlHead."<tbody>".$this->html."</tbody></table>";
    return $this->html;
	}

	public function addLink($href, $link, $class=null, $tdClass=null, $title=null, $option=null) {
		if($class!='') $class = " class='$class'";
		if($tdClass!='') $tdClass = " class='$tdClass'";
		$this->td.="<td$tdClass><a href='$href'$class title='$title' $option>$link</a></td>";
	}
	
	public function addButton($type, $href, $class='primary', $target='_self') {
		$typeButton = array(
			'edit' => array('glyphicon glyphicon-pencil', 'Edycja rekordu', 'Edycja'),
			'delete' => array('glyphicon glyphicon-delete', 'Usuń rekord', 'Usuń'),
			'download' => array('glyphicon glyphicon-arrow-down', 'Pobierz', 'Pobierz'),
		);
		
		$classButton = array(
			'primary' => 'btn btn-primary btn-sm',
			'danger' => 'btn btn-danger btn-sm',
			'info' => 'btn btn-info btn-sm',
		);

		$this->td.="<td class='edit'><a href='$href'  title='".$typeButton[$type][1]."' class='".$classButton[$class]."' target='".$target."'><span class='".$typeButton[$type][0]."' aria-hidden='true'></span> ".$typeButton[$type][2]."</a></td>";
	}

	public function getBody() {
		return $this->html;
	}
	
	public function setCellColspan($data, $colspan, $class=null) {
		$this->td.= "<td colspan='".$colspan."' class='".$class."'>".$data."</td>";
	}

	public function addCell($td, $class=null) {
		$cl = null;
		if($class) $cl = " class='$class'";
		$this->td.="<td$cl>".$td."</td>";
	}

	public function addRow($class=null) {
		if($class) $cl = " class='$class'";
			else $cl = null;
		$this->html.="<tr$cl>".$this->td.'</tr>';
		$this->td = '';
	}
	
	private function addHeaderRow() {
		$this->htmlHead.='<thead><tr>'.$this->td.'</tr></thead>';
		$this->td = '';
	}
	
	private function createFoot() {
		$this->html.="</table>";
	}
}