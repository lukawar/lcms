<?php
class template {
	public function __construct($loginRequire) {
		global $db, $var, $data;
		$this->db = $db;	
		$this->var = $var;
		$this->data = $data;	
		$this->loginRequire = $loginRequire;
	}
	
	public function setLayout() {		
		///var_dump($this->var);
		//$layout_path = $this->getTemplatePath();
		$layout_path = $this->data->templatePath;
		
		if($this->data->loginRequire) {
			include_once $layout_path."config.php";
			
			if(is_array($login_require)) {
				$login_data = (object)$login_require;
				$login = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
				$checkLogin = $login->load($login_data->action, $login_data->option);
				
				if($checkLogin['logged']=='false' or !$checkLogin['logged']) {
					//if($checkLogin['info'])
					//	helper::setQInfo($checkLogin['container'], $checkLogin['info']);
					return $layout_path.$login_data->login;
				}
			} else return false;
		} 
		
		//continue 
		if($this->data->agent=='mobile') {
			include_once $layout_path."config.php";
			if(array_key_exists($this->var->action,$config_template))
				return $layout_path.$config_template[$this->var->action];
			else return $layout_path.'index.tpl.php';
		} else {	
			include_once $layout_path."config.php";
			if(array_key_exists($this->var->action,$config_template))
				return $layout_path.$config_template[$this->var->action];
			else return $layout_path.'index.tpl.php';
		}
	}
	
	public function getTemplatePath() {
		if(isset($_GET[V_TEST_TEMPLATE]))
			$_SESSION[V_TEST_TEMPLATE] = $_GET[V_TEST_TEMPLATE];

		if(isset($_SESSION[V_TEST_TEMPLATE])) {
			return $this->data->localPath.'templates/'.$this->data->pageModeType.'/'.$_SESSION[V_TEST_TEMPLATE];
			
		} else {
			$defaultTemplate = $this->getDefaultTemplate();
			return $this->data->localPath.'templates/'.$this->data->pageModeType.'/'.$defaultTemplate;
		}
	}

	private function getDefaultTemplate() {
		if(U_TYPE=='file') {
			return V_TEMPLATE;	
		} else {
			$skin = $this->db->dict('SKIN_'.$this->data->instanceName.'_'.$this->data->pageModeType);
			return $skin['DIC_VALUE'].'/';
		}
	}
	
	//parse templates & add data to placeholders
	static function parse($data, $file) {
		if(is_array($data) and $file!=null) {
			//$file = preg_replace("|/*[^>]+*/|U",  '', $file);
			$first = array();
			$second = array();
			foreach($data as $placeholder=>$value) {
				$first[] = $placeholder;
				$second[] = $value;
			}
			return str_replace($first, $second, $file);
		} else return View::pageError();
	}
	
	//parse file & add data to placeholders
	static function parseFile($data, $filePath) {
		if(is_array($data) and file_exists($filePath)) {
			//$file = preg_replace("|/*[^>]+*/|U",  '', $file);
			$first = array();
			$second = array();
			foreach($data as $placeholder=>$value) {
				$first[] = $placeholder;
				$second[] = $value;
			}
			return str_replace($first, $second, file_get_contents($filePath));
		} else return View::pageError();
	}
	
}