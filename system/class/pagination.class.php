<?php
class pagination {
	static function create($all, $page, $elements, $module, $pageSize=null) {
		$pageSelect = array(10, 20, 50, 100); //paging elements
		$pageSelectText = 'liczba elementów na stronie: ';
		
		if($all>0) {
			$pagination_elements = ceil($all/$elements);
			if($page>0)
				$result.='<div class="pagin active"><a href="'.$module.'&page='.$page.'&elements='.$elements.'">&laquo;</a></div>';

			if($pagination_elements<=14) {
				for($i = 1; $i <= $pagination_elements; $i++) {
					$i_p = $i;
					
					if($i==($page+1))
						$result.='<div class="pagin current">'.$i_p.'</div>';
					else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
				}
			} else {
				if($page<9) {
					for($i = 1; $i <= 10; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-3; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				} elseif($page>=($pagination_elements-10)) {
					for($i = 1; $i <= 3; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-10; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				} else {
					for($i = 1; $i <= 3; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					
					for($i = $page-5; $i <= $page+5; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
					
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-3; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				}
				
			}
			
			if($page<$pagination_elements-1) {
				$next = $page+2;
				$result.='<div class="pagin active"><a href="'.$module.'&page='.$next.'&elements='.$elements.'">&raquo;</a></div>';
			}
			
			$result ='<div id="pagin_container">'.$result.'</div>';
		} else $result = null;
		
		if($pageSize==true) {
			$pageSelectBody = new select('pageSize', 'pageSize');
			foreach($pageSelect as $ps) {
				if($ps==$elements)
					$pageSelectBody->addNode($ps, $ps, 'selected');
				else $pageSelectBody->addNode($ps, $ps);
			}
			return $pageSelectText.$pageSelectBody->create().$result;
		} else return $result;
	}
}