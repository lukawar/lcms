<?php
class page {
	public $pageBody = null;
	//parse templates & add data to placeholders and generate page
	public function __construct() {
		global $db, $var, $data;
		$this->db = $db;	
		$this->var = $var;
		$this->data = $data;	
		$this->toolbar = null;
		$this->buttons = null;
	}
	
	public function setButton($button) {
		$this->buttons.= $button;
	}
	
	public function setSpacer() {
		$this->buttons.= '&nbsp;';
	}
	
	public function setSubmit($prop=null, $value='zapisz') {
		$this->buttons.= '<button type="submit" class="btn btn-success submitBtn"'.$prop.'><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> '.$value.'</button>';
	}
	
	public function setCancel($action) {
		$this->buttons.= '<button type="button" url="?action='.$action.'" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>';
	}
	
	public function setToolbar($toolbar = 'toolbar.tpl.php') {
		if(file_exists($this->data->localPath.$this->data->templatePath.'forms/'.$toolbar)) {
			$this->toolbar = str_replace('__TOOLBAR', $this->buttons, file_get_contents($this->data->localPath.$this->data->templatePath.'forms/'.$toolbar));	
		}
	}
	
	public function setPage($data, $page = 'default.tpl.php') {
		if(is_array($data) and $page!=null) {
			if(!isset($data['__GLYPHICON']))
				$data['__GLYPHICON'] = 'home';
				
			$data['__TOOLBAR'] = $this->toolbar;
				
			$first = array();
			$second = array();
			foreach($data as $placeholder=>$value) {
				$first[] = $placeholder;
				$second[] = $value;
			}
			return str_replace($first, $second, $this->setPageTemplate($page));
		} else return View::pageError();
	}
	
	private function setPageTemplate($page) {
		if(file_exists($this->data->localPath.$this->data->templatePath.'pages/'.$page))
			return file_get_contents($this->data->localPath.$this->data->templatePath.'pages/'.$page);
		else return file_get_contents($this->data->localPath.$this->data->templatePath.'pages/'.'default.tpl.php');
	}
}