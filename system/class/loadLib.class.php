<?php
class loadLib extends View {
	private $js_template = '<script type="text/javascript" src="%s"></script>';
	private $css_template = '<link href="%s" rel="stylesheet">';
	
	public function addjslib($js) {
		global $jslib;
		$jslib[] = sprintf($this->js_template, $js);
	}
	
	public function addcsslib($css) {
		global $csslib;
		$csslib[] = sprintf($this->jcss_template, $css);
	}
}