<?php
class getJSON {
	public function __construct() {
		$this->path = SC_API_URL;
	}
	
	public function getData($param=null) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $this->path.$param);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
		//return json_decode($result);
	}
}