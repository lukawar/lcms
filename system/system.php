<?php
//core system class
class system {
	public $db, $var, $data;
	
	public function __construct($startData) {
		global $db, $var, $data;
		
		if(is_array($startData)) {
			$starter = (object)$startData;

			$data = $this->setPageType($starter);
					
			$controller = new Controller($this->data->typeSite);
			$var = $controller->getData();
			
			$db = new Connector;
			$db->BaseConnection();
		} else die('Config corrupted.<br/>Check configuration files.');
	}
	
	public function setPage() {
		$this->layout = new template($this->data->loginRequire);
		$this->data->templatePath = $this->layout->getTemplatePath();
		$layout = $this->layout->setLayout();
		$this->site = file_get_contents($layout);
		$this->parse_content();
	}
	
	public function displayData() {
		global $var;
		$this->layout = new template($this->data->loginRequire);
		$this->data->templatePath = $this->layout->getTemplatePath();
		$module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		die($module->load($var->action, $var->option));
	}
	
	public function parse_content() {
		$module_content = array();
		$module_list = array();

		//older version
		//$data_module = new Module($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
	
		$data_module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		preg_match_all("|{/[^>]+/}|U",  $this->site, $out, PREG_PATTERN_ORDER);
		foreach($out[0] as $module_str) {
			$module = str_replace(array('{/','/}'), array('',''), $module_str);
			$module = explode(':', $module);
			if(!isset($module[1])) 
		  	$module[1] = null;

			$module_content[] = $data_module->load($module[0],$module[1]);
			$module_list[] = $module_str;
		}

		$this->site = str_replace($module_list, $module_content, $this->site);
	}
	
	private function setPageType($starter) {
		$configOptions = new config_options();
		$this->data = new StdClass;
		$this->data->configOptions = $configOptions;
		$this->data->instanceName = $starter->instanceName;
		$this->data->typeSite = $starter->pageType;
		$this->data->pageModeType = $starter->pageModeType;
		$this->data->localPath = $starter->localPath;
		$this->data->modulesPath = $starter->modulesPath;
		$this->data->loginRequire = $starter->loginRequire;
		$this->setPageMode();
		
		if($configOptions->mobileAgentDetect[$starter->pageModeType]==true) {
			$agent_detect = new Mobile_Detect();
			if($agent_detect->isMobile() or $agent_detect->isTablet())
				$this->data->agent = 'mobile';
			else $this->data->agent = 'desktop';
		} else $this->data->agent = 'desktop';
		
		return (object)$this->data;
	}
	
	private function setPageMode() {
		//show errors
		if($this->data->configOptions->errorSite[$this->data->pageModeType]) {
			error_reporting(E_ALL);
			$this->data->pageMode[] = 'errorSite';
		} else error_reporting(0);
		
		//display test data
		if($this->data->configOptions->testingSite[$this->data->pageModeType]) {
			error_reporting(E_ALL);
			$this->data->pageMode[] = 'testingSite';
		}
		//TODO: display mainentance site	
	}
	
	public function __destruct() {
		if(isset($this->site))
			echo $this->site;
	}
}

class Module {
	public function __construct($type=null, $localPath=null, $modulesPath='modules/') {
		$this->typeSite = $type;
		$this->modulePath = $localPath.$modulesPath;
	}

	public function load($module, $option=null, $id=null) {
		if(defined($module)) {
			return constant($module);
		} else if(!file_exists($this->modulePath.$module."/".$this->typeSite.$module.".php")) {
			//return "<div class='errorInfo'>can't find module - ".$module." (type=".$this->typeSite.") ".$_SERVER['REQUEST_URI']." file: ".$this->modulePath.$module."/".$this->typeSite.$module.".php"."</div>";
			header('location: homepage.html');
		}
		require_once($this->modulePath.$module."/".$this->typeSite.$module.".php");
		if($option!=null)
			return $module($option);
		else return $module();
	}
}

class Model {
	public function __construct($type=null, $localPath=null, $modulesPath='modules/') {
		$this->typeSite = $type;
		$this->modelPath = $localPath.$modulesPath;
		//return (object)$this->getModel();
	}
	
	public function load($name, $option=null, $id=null) {		
		if(file_exists($this->modelPath.$name.'/'.$this->typeSite.$name.'.php')) {
			include_once($this->modelPath.$name.'/'.$this->typeSite.$name.'.php');
			if(class_exists($name)) {
				$model =  new	$name;
				if($option and method_exists($model, $option))
					return $model->$option($id);
				else {
					$model->optionValue = $option;
					return $model->showModule();
				}
			} else return null;
		} else 
			//return "<div class='errorInfo'>can't find module - ".$name." (type=".$this->typeSite.") ".$_SERVER['REQUEST_URI']." file: ".$this->modelPath.$name."/".$this->typeSite.$name.".php"."</div>";
			header('location: homepage.html');
	}
	
	public function get($function, $param=null) {
		return $this->model->$function();
	}
}


class eventSaver {
	//user_id - user or admin ID
	//event - event string
	//from - admin/user
	//type - module type
	//type_id - module id
	public static function add($user_id, $event, $from='admin', $module=null, $type=null, $type_id=null)	{
		global $db;
		$insert = array(
			'LOE_USER' => $user_id,
			'LOE_EVENT' => $event,
			'LOE_FROM' => $from,
			'LOE_MODULE' => $module,
			'LOE_TYPE' => $type,
			'LOE_TYPE_ID' => $type_id,
			'LOE_IP' => clientIP::get(),
			);
		$db->queryString(__BP.'log_events');
		$db->insertQuery($insert);
	}
	
	//user_id - user or admin ID
	//event - event string
	//from - admin/user
	//type - module type
	//type_id - module id
	public static function setBackup($user_id, $from, $module, $module_id, $type, $name, $data1=null, $data2=null, $data3=null, $data4=null, $data5=null, $data6=null)	{
		global $db;
		$insert = array(
			'TEB_ADM_ID' => $user_id,
			'TEB_FROM' => $from,
			'TEB_MODULE' => $module,
			'TEB_MODULE_ID' => $module_id,
			'TEB_TYPE' => $type,
			'TEB_NAME' => $name,
			'TEB_DATA_1' => $data1,
			'TEB_DATA_2' => $data2,
			'TEB_DATA_3' => $data3,
			'TEB_DATA_4' => $data4,
			'TEB_DATA_5' => $data5,
			'TEB_DATA_6' => $data6,
			'TEB_IP' => clientIP::get(),
			);
		$db->queryString(__BP.'text_backup');
		$db->insertQuery($insert);
	}
}

function __autoload($classname) {
  $filename = 'system/class/'. $classname .'.class.php';
  include_once($filename);
}