-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 27 Wrz 2016, 14:50
-- Wersja serwera: 5.5.27-29.0
-- Wersja PHP: 5.5.35-ogc0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `db214672`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__administrator`
--

CREATE TABLE IF NOT EXISTS `bb__administrator` (
`ADM_ID` int(11) NOT NULL,
  `ADM_ALIAS` varchar(50) NOT NULL,
  `ADM_NAME` varchar(200) NOT NULL,
  `ADM_SURNAME` varchar(200) NOT NULL,
  `ADM_LOGIN` varchar(200) NOT NULL,
  `ADM_PASS` varchar(200) NOT NULL,
  `ADM_MAIL` varchar(200) NOT NULL,
  `ADM_ROLE` enum('admin','user','sysadmin') NOT NULL DEFAULT 'user',
  `ADM_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADM_AGENCY` varchar(2) NOT NULL,
  `ADM_SEND_MAIL` enum('nie','tak') NOT NULL DEFAULT 'nie',
  `ADM_ACTIVE` enum('active','deleted') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Zrzut danych tabeli `bb__administrator`
--

INSERT INTO `bb__administrator` (`ADM_ID`, `ADM_ALIAS`, `ADM_NAME`, `ADM_SURNAME`, `ADM_LOGIN`, `ADM_PASS`, `ADM_MAIL`, `ADM_ROLE`, `ADM_DATE_ADD`, `ADM_AGENCY`, `ADM_SEND_MAIL`, `ADM_ACTIVE`) VALUES
(1, 'jssdihsdgohdihsd', 'Łukasz', 'Warguła', 'lukawar', 'abb7dcbcf57aed9ba61726ddf0846ca4fa521a9f1f9732b763be239994e7777e', 'lukasz@lightpoint.pl', 'admin', '0000-00-00 00:00:00', 'A', 'tak', 'active'),
(5, '316859d20a64fd7d843bfaec87da506a897cbf7b8da79644e7', 'Wojtek', 'Lewandowski', 'wojtek', '4e2ed56229f338792dda6b318375113bf037652816b2dd8f6bdc3362da36497a', 'wojtek@expansja.pl', 'user', '2016-09-14 12:01:56', '', 'tak', 'active'),
(6, 'd1d21615c31b7346b9847f98292937c87b2f2061c88312952f', 'Iza', 'Suliga', 'iza', '0b72edb38100a83a5534b1f404f54c82c2eb27db82f0653458d536dbaf3d3a59', 'iza@expansja.pl', 'admin', '2016-09-15 13:33:03', '', 'tak', 'active'),
(7, 'b02c43613a904e49bc4fe86a8c0677a9508750f6225b5805ad', 'Ewa', 'Gimzicka', 'ewa', 'cf519bda176fce917d62e593cf966a42d62a1653417aac75732c398682c84997', 'loyalty@expansja.pl', 'admin', '2016-09-15 13:33:51', '', 'tak', 'active'),
(8, '9cb0a22243663abf603f25c8a187938231d24d212eb14a6a76', 'Bartosz', 'Kamiński', 'bartek', '841cfe946b6214267c1e5a0f9c8b6b3d0f750c622cde83964439b9c1e17c5909', 'bartek@expansja.pl', 'admin', '2016-09-15 13:34:34', '', 'tak', 'active'),
(9, 'e279934104d41c14f2ceacd53fb3b12ae72d2394cf1d1d836f', 'Jan', 'Kowalski', 'jan', 'cdfff1872b68a503fe4b6f8d23e98c3e9acd034baff352eaca5c7c63270d2596', 'lukasz@expansja.pl', 'admin', '2016-09-15 13:35:10', '', 'tak', 'active'),
(10, '99247287fe8431cb69c5fe3ac1e66b20e53052627094dd9002', 'Bea', 'Bonus', 'beabeleza', '293c259ef274eed7e71ca49b554eda988b9f7fe655394ee2300d368dc7d0d4e5', 'iza@expansja.pl', 'user', '2016-09-27 11:46:08', '', 'tak', 'active');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__admin_dashboard`
--

CREATE TABLE IF NOT EXISTS `bb__admin_dashboard` (
`ADD_ID` int(11) NOT NULL,
  `ADD_ADM_ID` int(11) NOT NULL,
  `ADD_ACTION` varchar(50) NOT NULL,
  `ADD_OPTION` varchar(50) NOT NULL,
  `ADD_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__admin_roles`
--

CREATE TABLE IF NOT EXISTS `bb__admin_roles` (
  `ADR_ADM_ID` int(11) NOT NULL,
  `ADR_NAME` varchar(50) NOT NULL,
  `ADR_ROLE_ID` varchar(30) NOT NULL,
  `ADR_ROLE_OPTION` varchar(30) NOT NULL,
  `ADR_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADR_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

--
-- Zrzut danych tabeli `bb__admin_roles`
--

INSERT INTO `bb__admin_roles` (`ADR_ADM_ID`, `ADR_NAME`, `ADR_ROLE_ID`, `ADR_ROLE_OPTION`, `ADR_DATE`, `ADR_ORDER`) VALUES
(5, 'Artykuły', 'articles', '', '2016-09-27 10:38:41', 0),
(5, 'Aktualności', 'news', '', '2016-09-27 10:38:41', 0),
(10, 'Artykuły', 'articles', '', '2016-09-27 11:46:27', 0),
(10, 'Aktualności', 'news', '', '2016-09-27 11:46:27', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__articles`
--

CREATE TABLE IF NOT EXISTS `bb__articles` (
`ART_ID` int(11) NOT NULL,
  `ART_PARENT` int(11) NOT NULL DEFAULT '0',
  `ART_LEVEL` tinyint(4) NOT NULL DEFAULT '0',
  `ART_NAME` varchar(200) NOT NULL,
  `ART_ALIAS` varchar(200) NOT NULL,
  `ART_CONTENT` longtext NOT NULL,
  `ART_ORDER` int(11) NOT NULL,
  `ART_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ART_ADM_ID` int(11) NOT NULL,
  `ART_STATUS` enum('nowy','opublikowany','ukryty','deleted') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `bb__articles`
--

INSERT INTO `bb__articles` (`ART_ID`, `ART_PARENT`, `ART_LEVEL`, `ART_NAME`, `ART_ALIAS`, `ART_CONTENT`, `ART_ORDER`, `ART_DATE_ADD`, `ART_ADM_ID`, `ART_STATUS`) VALUES
(1, 0, 0, 'Regulamin', 'regulamin', '<p>Regulamin programu</p>', 0, '2016-09-20 10:27:27', 1, 'opublikowany');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__category`
--

CREATE TABLE IF NOT EXISTS `bb__category` (
`CAT_ID` int(11) NOT NULL,
  `CAT_NAME` varchar(150) NOT NULL,
  `CAT_ALIAS` varchar(150) NOT NULL,
  `CAT_PARENT` int(11) NOT NULL DEFAULT '0',
  `CAT_COUNT` int(11) NOT NULL DEFAULT '0',
  `CAT_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active',
  `CAT_ORDER` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `bb__category`
--

INSERT INTO `bb__category` (`CAT_ID`, `CAT_NAME`, `CAT_ALIAS`, `CAT_PARENT`, `CAT_COUNT`, `CAT_STATUS`, `CAT_ORDER`) VALUES
(1, 'WARSZTAT PRACY', 'warsztat_pracy', 0, 5, 'active', 0),
(2, 'MAŁA CZARNA', 'mala_czarna', 0, 0, 'active', 0),
(3, 'CHWILA RELAKSU', 'chwila_relaksu', 0, 1, 'active', 0),
(4, 'PRZYDATNE', 'przydatne', 0, 0, 'active', 0),
(5, 'ZAWSZE CZYSTO', 'zawsze_czysto', 0, 1, 'active', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__dict`
--

CREATE TABLE IF NOT EXISTS `bb__dict` (
`DIC_ID` int(11) NOT NULL,
  `DIC_KEY` varchar(100) NOT NULL,
  `DIC_VALUE` varchar(100) NOT NULL,
  `DIC_INT` int(11) DEFAULT NULL,
  `DIC_LNG` varchar(3) NOT NULL DEFAULT 'pl',
  `DIC_DESCRIPTION` varchar(30) NOT NULL,
  `DIC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `bb__dict`
--

INSERT INTO `bb__dict` (`DIC_ID`, `DIC_KEY`, `DIC_VALUE`, `DIC_INT`, `DIC_LNG`, `DIC_DESCRIPTION`, `DIC_DATE`) VALUES
(1, 'SKIN_bbDefault_frontend', 'bb', 0, 'pl', 'Skin folder name', '2016-08-08 09:39:58'),
(3, 'SKIN_bbDefault_backend', 'default', 0, 'pl', 'backend for BB', '2016-08-22 14:29:13');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__import_info`
--

CREATE TABLE IF NOT EXISTS `bb__import_info` (
`IIN_ID` int(11) NOT NULL,
  `IIN_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IIN_DATE_FROM` date NOT NULL,
  `IIN_DATE_TO` date NOT NULL,
  `IIN_IMPORT_ID` varchar(50) NOT NULL,
  `IIN_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__log_events`
--

CREATE TABLE IF NOT EXISTS `bb__log_events` (
`LOE_ID` int(11) NOT NULL,
  `LOE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOE_USER` int(11) NOT NULL,
  `LOE_EVENT` mediumtext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `LOE_FROM` enum('admin','user') NOT NULL,
  `LOE_MODULE` varchar(30) NOT NULL,
  `LOE_TYPE` varchar(20) NOT NULL,
  `LOE_TYPE_ID` int(11) DEFAULT NULL,
  `LOE_IP` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Zrzut danych tabeli `bb__log_events`
--

INSERT INTO `bb__log_events` (`LOE_ID`, `LOE_DATE`, `LOE_USER`, `LOE_EVENT`, `LOE_FROM`, `LOE_MODULE`, `LOE_TYPE`, `LOE_TYPE_ID`, `LOE_IP`) VALUES
(1, '2016-09-20 10:27:27', 1, 'new article - Regulamin', '', '', '1', 0, '::1'),
(2, '2016-09-20 12:01:35', 1, 'edit article - Regulamin', '', 'edit', '', 0, '::1'),
(3, '2016-09-20 12:03:07', 1, 'edit article - Regulamin', '', 'edit', '', 0, '::1'),
(4, '2016-09-20 12:04:30', 1, 'edit article - Regulamin', '', 'edit', '1', 0, '::1'),
(5, '2016-09-20 12:06:48', 1, 'edit article - Regulamin', 'admin', 'articles', 'edit', 1, '::1'),
(6, '2016-09-26 10:34:37', 1, 'add news - Wpis testowy', 'admin', 'news', 'add', 2, '::1'),
(7, '2016-09-26 10:47:15', 1, 'add news - Wpis tekstowy 3', 'admin', 'news', 'add', 3, '::1'),
(8, '2016-09-26 10:47:40', 1, 'edit news - Wpis tekstowy 3', 'admin', 'news', 'edit', 3, '::1'),
(9, '2016-09-26 10:48:01', 1, 'edit news - Wpis testowy', 'admin', 'news', 'edit', 2, '::1'),
(10, '2016-09-26 11:03:12', 1, 'edit news - Wpis testowy', 'admin', 'news', 'edit', 1, '::1'),
(11, '2016-09-26 11:03:54', 1, 'edit news - Wpis testowy', 'admin', 'news', 'edit', 1, '::1'),
(12, '2016-09-26 11:04:34', 1, 'edit news - Wpis testowy', 'admin', 'news', 'edit', 1, '::1'),
(13, '2016-09-26 14:55:26', 1, 'add user - Expansja', 'admin', 'user', 'add', 4, '::1'),
(14, '2016-09-27 12:15:03', 10, 'edit article - Regulamin', 'admin', 'articles', 'edit', 1, '79.186.6.214');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__menus`
--

CREATE TABLE IF NOT EXISTS `bb__menus` (
`MEN_ID` int(11) NOT NULL,
  `MEN_PARENT` int(11) NOT NULL,
  `MEN_TYPE` enum('left','right','top','bottom','center') COLLATE latin1_general_ci NOT NULL DEFAULT 'left',
  `MEN_NAME` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORD` int(11) NOT NULL,
  `MEN_MODULE` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_ID` int(11) NOT NULL,
  `MEN_MODULE_ALIAS` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_TITLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_HREF` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `MEN_STYLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORDER` int(11) NOT NULL,
  `MEN_STATUS` enum('aktywny','nieaktywny','usuni?ty') COLLATE latin1_general_ci NOT NULL DEFAULT 'aktywny'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__news`
--

CREATE TABLE IF NOT EXISTS `bb__news` (
`NEW_ID` int(11) NOT NULL,
  `NEW_NAME` varchar(250) NOT NULL,
  `NEW_ALIAS` varchar(250) NOT NULL,
  `NEW_DESC_SHORT` mediumtext NOT NULL,
  `NEW_DESC_LONG` mediumtext NOT NULL,
  `NEW_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NEW_DATE_DISPLAY` date NOT NULL,
  `NEW_DATE_START` date NOT NULL,
  `NEW_DATE_END` date NOT NULL,
  `NEW_BACKGROUND` varchar(100) DEFAULT NULL,
  `NEW_ADM_ID` int(11) NOT NULL,
  `NEW_STATUS` enum('nowy','aktywny','nieaktywny','usunięty') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `bb__news`
--

INSERT INTO `bb__news` (`NEW_ID`, `NEW_NAME`, `NEW_ALIAS`, `NEW_DESC_SHORT`, `NEW_DESC_LONG`, `NEW_DATE_ADD`, `NEW_DATE_DISPLAY`, `NEW_DATE_START`, `NEW_DATE_END`, `NEW_BACKGROUND`, `NEW_ADM_ID`, `NEW_STATUS`) VALUES
(1, 'Wpis testowy', 'wpis_testowy', '<p>Opis skr&oacute;cony</p>', '<p>Lorem ipsum lorem ipsum</p>', '2016-09-20 08:08:33', '2016-09-13', '2016-09-14', '2016-09-26', 'fe2765ae3f2a64c5f6f6b2f2a03025e9.jpg', 1, 'aktywny'),
(2, 'Wpis testowy', 'wpis_testowy', '<p>Skr&oacute;t newsa.</p>', '<p>Lorem ipsum lorem ipsum&nbsp; lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum<br /> lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum</p>', '2016-09-26 10:34:37', '2016-09-25', '2016-09-18', '2016-10-08', '746711ae875104eff3eb9ea3a7674425.jpg', 1, 'aktywny'),
(3, 'Wpis tekstowy 3', 'wpis_tekstowy_3', '<p>Tekst</p>', '<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p><strong> lorem ipsum lorem ipsum lorem ipsum lorem ipsum</strong></p>\r\n<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>', '2016-09-26 10:47:15', '2016-09-22', '2016-09-07', '2016-10-07', '9eeb33b4839e2e150971de3a8c8724a1.jpg', 1, 'aktywny');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__orders`
--

CREATE TABLE IF NOT EXISTS `bb__orders` (
`ORD_ID` int(11) NOT NULL,
  `ORD_USR_ID` int(11) NOT NULL,
  `ORD_UAS_ID` int(11) NOT NULL,
  `ORD_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ORD_POINTS` int(11) NOT NULL,
  `ORD_POSITIONS` tinyint(4) NOT NULL,
  `ORD_ADDRESS` mediumtext NOT NULL,
  `ORD_SEND_CODE` varchar(100) NOT NULL,
  `ORD_SEND_PATH` varchar(255) NOT NULL,
  `ORD_STATUS` enum('new','confirmed','sended','closed','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `bb__orders`
--

INSERT INTO `bb__orders` (`ORD_ID`, `ORD_USR_ID`, `ORD_UAS_ID`, `ORD_DATE_ADD`, `ORD_POINTS`, `ORD_POSITIONS`, `ORD_ADDRESS`, `ORD_SEND_CODE`, `ORD_SEND_PATH`, `ORD_STATUS`) VALUES
(1, 1, 4, '2016-09-15 12:32:55', 401, 2, 'Joanna Kowalska<br/>Głogowska 36<br/>64-123 Poznań<br/>tel: 23234234', '', '', 'new'),
(2, 2, 7, '2016-09-15 12:46:09', 364, 2, 'iza aco<br/>Bukowska 67/5 33<br/>60_567 Poznań<br/>tel: 667342068', '', '', 'new'),
(3, 1, 0, '2016-09-20 09:55:58', 214, 1, '', '', '', 'new'),
(4, 1, 2, '2016-09-21 07:49:58', 578, 3, 'Zdzisław Zdzisław<br/>Kordeckiego 3/4<br/>43-567 Poznań<br/>tel: 34535435', '', '', 'new');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__orders_status_change`
--

CREATE TABLE IF NOT EXISTS `bb__orders_status_change` (
`OSC_ID` int(11) NOT NULL,
  `OSC_ORD_ID` int(11) NOT NULL,
  `OSC_ADM_ID` int(11) NOT NULL,
  `OSC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OSC_STATUS` enum('new','confirmed','sended','closed','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__order_positions`
--

CREATE TABLE IF NOT EXISTS `bb__order_positions` (
`OPO_ID` int(11) NOT NULL,
  `OPO_ORD_ID` int(11) NOT NULL,
  `OPO_USR_ID` int(11) NOT NULL,
  `OPO_PRO_ID` int(11) NOT NULL,
  `OPO_PRO_ALIAS` varchar(100) NOT NULL,
  `OPO_PRO_NAME` varchar(250) NOT NULL,
  `OPO_CAT_ID` int(11) NOT NULL,
  `OPO_COUNT` tinyint(4) NOT NULL,
  `OPO_POINTS` int(11) NOT NULL,
  `OPO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OPO_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `bb__order_positions`
--

INSERT INTO `bb__order_positions` (`OPO_ID`, `OPO_ORD_ID`, `OPO_USR_ID`, `OPO_PRO_ID`, `OPO_PRO_ALIAS`, `OPO_PRO_NAME`, `OPO_CAT_ID`, `OPO_COUNT`, `OPO_POINTS`, `OPO_DATE_ADD`, `OPO_STATUS`) VALUES
(1, 1, 1, 2, 'hh_simonsen_360_boss_hair_dryer_limited_edition_mint_set', 'HH SIMONSEN 360° Boss Hair Dryer Limited Edition Mint Set', 1, 1, 214, '2016-09-15 12:32:55', 'active'),
(2, 1, 1, 3, 'hh_simonsen_360_straightening_iron_limited_edition_mintt', 'HH SIMONSEN 360 Straightening Iron Limited Edition Mint', 1, 1, 187, '2016-09-15 12:32:55', 'active'),
(3, 2, 2, 1, 'tigi_professional_xlarge', 'TIGI PROFESSIONAL X-Large  Round Brush', 1, 1, 150, '2016-09-15 12:46:09', 'active'),
(4, 2, 2, 2, 'hh_simonsen_360_boss_hair_dryer_limited_edition_mint_set', 'HH SIMONSEN 360° Boss Hair Dryer Limited Edition Mint Set', 1, 1, 214, '2016-09-15 12:46:09', 'active'),
(5, 3, 1, 2, 'hh_simonsen_360_boss_hair_dryer_limited_edition_mint_set', 'HH SIMONSEN 360° Boss Hair Dryer Limited Edition Mint Set', 1, 1, 214, '2016-09-20 09:55:58', 'active'),
(6, 4, 1, 1, 'tigi_professional_xlarge', 'TIGI PROFESSIONAL X-Large  Round Brush', 1, 1, 150, '2016-09-21 07:49:58', 'active'),
(7, 4, 1, 2, 'hh_simonsen_360_boss_hair_dryer_limited_edition_mint_set', 'HH SIMONSEN 360° Boss Hair Dryer Limited Edition Mint Set', 1, 2, 214, '2016-09-21 07:49:58', 'active');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pages`
--

CREATE TABLE IF NOT EXISTS `bb__pages` (
`PAG_ID` int(11) NOT NULL,
  `PAG_NAME` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pages_meta`
--

CREATE TABLE IF NOT EXISTS `bb__pages_meta` (
`PAM_ID` int(11) NOT NULL,
  `PAM_PAG_ID` int(11) NOT NULL,
  `PAM_TITLE` tinytext NOT NULL,
  `PAM_DESCRIPTION` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products`
--

CREATE TABLE IF NOT EXISTS `bb__products` (
`PRO_ID` int(11) NOT NULL,
  `PRO_NAME` varchar(100) NOT NULL,
  `PRO_ALIAS` varchar(100) NOT NULL,
  `PRO_CODE` varchar(50) NOT NULL,
  `PRO_EAN` varchar(20) NOT NULL,
  `PRO_DESC_LONG` longtext NOT NULL,
  `PRO_DESC_SHORT` mediumtext NOT NULL,
  `PRO_PRICE_OUR` double(12,2) NOT NULL,
  `PRO_PRICE_CLIENT` double(12,2) NOT NULL,
  `PRO_FOTO` varchar(250) DEFAULT NULL,
  `PRO_POINTS` int(11) NOT NULL,
  `PRO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PRO_STATUS` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `bb__products`
--

INSERT INTO `bb__products` (`PRO_ID`, `PRO_NAME`, `PRO_ALIAS`, `PRO_CODE`, `PRO_EAN`, `PRO_DESC_LONG`, `PRO_DESC_SHORT`, `PRO_PRICE_OUR`, `PRO_PRICE_CLIENT`, `PRO_FOTO`, `PRO_POINTS`, `PRO_DATE_ADD`, `PRO_STATUS`) VALUES
(1, 'TIGI PROFESSIONAL X-Large  Round Brush', 'tigi_professional_xlarge', 'TIGI_PROFESSIONAL', '', '<p><strong>TIGI PROFESSIONAL X-LARGE ROUND BRUSH</strong> to profesjonalna szczotka do modelowania w rozmiarze XL. Zapewnia szybkie i komfortowe modelowanie, oraz zdrowsze, bardziej lśniące włosy. Korpus szczotki pokryty jest aluminium, dzięki temu szybciej się nagrzewa i dłużej utrzymuje temperaturę. Nowe, większe otwory przyspieszają czas suszenia oraz zwiększają przepływ powietrza a przy okazji skracają czas modelowania włos&oacute;w. Wł&oacute;kna szczotki wykonane są z czarnego nylonu, dzięki temu są bardziej odporne na ciepło suszarki i bezpieczne dla sk&oacute;ry głowy i włos&oacute;w.Szczotka TIGI X-LARGE ROUND BRUSH jest bardzo lekka i trwała, a jej ergonomiczny uchwyt z tworzywa sztucznego sprawi, że z łatwością ułożysz piękną i wyjątkowo trwałą fryzurę.</p>\r\n<p><strong>Cechy produktu:</strong></p>\r\n<ul>\r\n<li>Korpus wykonany z aluminium</li>\r\n<li>Odporna na działanie wysokich temperatur</li>\r\n<li>Ergonomiczny uchwyt z tworzywa sztucznego</li>\r\n<li>Średnica korpusu bez szczeciny: 55 mm.</li>\r\n</ul>', '<p>Profesjonalna szczotka do suszenia i modelowania w rozmiarze XL</p>', 0.00, 1569.99, 'tigi.jpg', 150, '2016-09-01 08:50:17', 'active'),
(2, 'HH SIMONSEN 360° Boss Hair Dryer Limited Edition Mint Set', 'hh_simonsen_360_boss_hair_dryer_limited_edition_mint_set', 'HH_SIMONSEN_360', '', '<h6><strong>1) HH SIMONSEN 360o Boss Hair Dryer Limited Edition Mint - Suszarka</strong></h6>\r\n<p>Suszarka do włos&oacute;w HH Simonsen BOSS zaprojektowana została w oparciu o szerokie doświadczenia, kt&oacute;re zostały zebrane od profesjonalnych fryzjer&oacute;w z całej Europy. Boss wyr&oacute;żnia potężny i wydajny silnik 2000W AC, zar&oacute;wno szybki jak i mocny. Suszarka wykonana jest z najwyższej jakości materiał&oacute;w, jej projektanci postawili na niską wagę i ergonomię. Potężny silnik zapewni długi okres trwałości. Suszarka Boss jest wyposażona w jonizację nowej technologii, co pozytywnie wpływa na tworzenia połysku na włosach i efekt wygładzenia.</p>\r\n<p>Seria limitowana Mint!</p>\r\n<p><strong>Cechy produktu:</strong></p>\r\n<p>- Mocny i oszczędny silnik AC (2000 W)<br />- Wysokiej jakości materiały<br />- Ergonomiczna konstrukcja<br />- Najnowsza technologia jonizacji<br />- Powierzchnia uchwytu pokryta gumą<br />- Wskaźnik LED poziomu ustawień prędkości i ciepła<br />- 3-metrowy kabel</p>\r\n<h6><strong>2) HH SIMONSEN Boar Brush - Szczotka nabłyszczająca z włosia dzika</strong></h6>\r\n<p>Najnowsza szczotka HH Simonsen&reg;, została zaprojektowana w celu pobudzenia wzrostu włos&oacute;w oraz poprawy jakości sk&oacute;ry głowy. Szczotka wykonana jest z włos&oacute;w japońskiego dzika, dzięki czemu idealnie masuję sk&oacute;rę głowy, przez co zwiększa przepływ krwi do mieszk&oacute;w włosowych. Krążenie, pobudza produkcję sebum, dzięki czemu włosy stają się błyszczące na całej swej długości.</p>\r\n<h6><strong>3) HH SIMONSEN Hair Bobbles Mint - Gumka do włos&oacute;w 3 sztuki</strong></h6>\r\n<p>Gumka do włos&oacute;w Hair Bobbles o miękkim i elastycznym kształcie pozwala bez problemu wiązać włosy i tworzyć wszechstronne fryzury. Dzięki temu, że jest delikatna, nie wyrywa ani nie ciągnie włos&oacute;w oraz nie pozostawia odciśnięcia się śladu na powierzchni włosa. Gumka przeznaczona jest do każdego rodzaju włos&oacute;w. Dodatkowo, pełni r&oacute;wnież funkcję bransoletki i stanowi stylowy dodatek do każdej stylizacji. Gumka występuje w kilku wersjach kolorystycznych. Nie czekaj i wypr&oacute;buj Hair Bobbles już dziś!</p>\r\n<p><strong>Właściwości:</strong></p>\r\n<p>- Nie pozostawia ślad&oacute;w na włosach<br />- Nie wyrywa ani nie niszczy włos&oacute;w<br />- Funkcja bransoletki<br />- Przeznaczona do każdego typu włos&oacute;w</p>\r\n<p><strong>Całość zapakowana w eleganckie pudełko.</strong></p>', '<p>Suszarka 360&deg; Boss Mint</p>', 0.00, 999.99, '70016.jpg', 214, '2016-09-01 08:27:30', 'active'),
(3, 'HH SIMONSEN 360 Straightening Iron Limited Edition Mint', 'hh_simonsen_360_straightening_iron_limited_edition_mintt', 'HH_SIMONSEN', '', '<p>Nowa seria profesjonalnych sprzęt&oacute;w do stylizacji HH Simonsen 360o zmienia się jak wszystko dookoła. Dzięki tej linii produkt&oacute;w można uzyskać efekt zar&oacute;wno wygładzonych włos&oacute;w, jak i wspaniałych lok&oacute;w. Design produkt&oacute;w zmienia się cyklicznie. Nigdy nie wiemy jakie ciekawe rozwiązania będą przedstawione w następnej edycji. Produkty są nie tylko profesjonalne technicznie, ale r&oacute;wnież cieszą oko swoim wyglądem.</p>\r\n<p>360o Styler HH Simonsen - seria limitowana Mint.</p>\r\n<p>Powierzchnie grzewcze są podw&oacute;jnie powlekane teflonem, dodatkowo olejowane, ponadto dwukrotnie polerowane. Sprawiają, że sprzęt jest łatwy w użyciu, nawet jeśli włosy są długie i grube. Pozostawia efekt lśniących i gładkich włos&oacute;w.</p>\r\n<p>Wyświetlacz temperatury LED sprawia, że łatwo jest dostosować temperaturę do potrzeb włos&oacute;w w zakresie od 120 do 210 stopni. Prostownica jest gotowa do pracy w mniej niż 10 sekund. Automatyczny tryb uśpienia włączy się po 60 minutach bezczynności.System dwufunkcyjny pozwala używać sprzętu niezależnie od mocy zasilania (110V lub 240V). Przew&oacute;d o długości 3 metr&oacute;w został dodatkowo pokryty tkaniną.360o oferuje także najnowsze systemy bezpieczeństwa, kt&oacute;re obejmują podw&oacute;jne zabezpieczenie przed uszkodzeniem. Prostownica pakowana jest w ekskluzywny kartonik, więc będzie idealnym prezentem dla Ciebie lub Twoich bliskich :)</p>\r\n<p>Prostownica dostępna w kilku odcieniach - już dziś wybierz sw&oacute;j kolor i ciesz się piękną fryzurą!</p>\r\n<p><strong>Cechy produktu:</strong></p>\r\n<ul>\r\n<li>Wyświetlacz LED, zakres temperatury 120-210 stopni</li>\r\n<li>Płytki powlekane teflonowe</li>\r\n<li>Gotowa w mniej niż 10 sekund - Dwufunkcyjna - działa w zasilaniu 110V lub 240V (europejskie oraz amerykańskie)</li>\r\n<li>3-metrowy tekstylny kabel</li>\r\n<li>Podw&oacute;jne zabezpieczenie przed uszkodzeniem</li>\r\n<li>Atrakcyjne opakowanie</li>\r\n</ul>', '<p>Prostownica 360 Mint.</p>', 0.00, 463.00, 'hh-simonsen.jpg', 187, '2016-09-06 13:06:42', 'active'),
(4, 'HH SIMONSEN Boss Hair Dryer Black', 'hh_simonsen_boss_hair_dryer_black', 'HH_SIMONSEN', '', '<p>Suszarka do włos&oacute;w HH Simonsen BOSS zaprojektowana została w oparciu o szerokie doświadczenia, kt&oacute;re zostały zebrane od profesjonalnych fryzjer&oacute;w z całej Europy. Boss wyr&oacute;żnia potężny i wydajny silnik 2000W AC, zar&oacute;wno szybki jak i mocny. Suszarka wykonana jest z najwyższej jakości materiał&oacute;w, jej projektanci postawili na ni-ską wagę i ergonomię. Potężny silnik zapewni długi okres trwałości. Suszarka Boss jest wyposażona w jonizację nowej technologii, co pozytywnie wpływa na tworzenia połysku na włosach i efekt wygładzenia.</p>\r\n<p><strong>Cechy produktu:</strong></p>\r\n<ul>\r\n<li>Mocny i oszczędny silnik AC (2000 W</li>\r\n<li>Wysokiej jakości materiały</li>\r\n<li>Ergonomiczna konstrukcja</li>\r\n<li>Najnowsza technologia jonizacji</li>\r\n<li>Powierzchnia uchwytu pokryta gumą</li>\r\n<li>Wskaźnik LED poziomu ustawień prędkości i ciepła</li>\r\n<li>3-metrowy kabel</li>\r\n</ul>', '<p>Suszarka Boss Black</p>', 0.00, 477.00, 'hh-simonsen2.jpg', 178, '2016-09-06 13:09:07', 'active'),
(5, 'TANGLE ANGEL Cherub', 'tangle_angel_cherub', 'TANGLE ANGEL', '', '<p>Szczotka TANGLE ANGEL CHERUB to idelany wyb&oacute;r dla włos&oacute;w normalnych, cienkich lub delikatnych.</p>\r\n<p>Ułatwia rozczesywanie włos&oacute;w od nasady, jednocześnie masując sk&oacute;rę głowy.</p>\r\n<p>Dzięki wyprofilowanej rączce o płaskiej podstawie świetnie leży w głoni.</p>\r\n<p>Tłoczenie w anielskie skrzydła wyr&oacute;żnia ją na tle innych szczotek.</p>\r\n<p><br /><strong>Wysokość szczotki: 148 mm</strong></p>', '<p>Szczotka do włos&oacute;w Turkusowa</p>', 0.00, 120.00, 'tangle-angel.jpg', 17990, '2016-09-14 09:12:27', 'active'),
(6, 'OROFLUIDO Asia Zen Control Elixir 50 ml', 'orofluido_asia_zen_control_elixir_50_ml', 'OROFLUIDO', '', '<p>Orofluido sięga w swoich inspiracjach do rytuał&oacute;w piękna pochodzących z antycznych tradycji. Dzięki znakomitej mieszance składnik&oacute;w naturalnych przynosi doskonałe efekty. Olejek Asia Zen to ekskluzywny, profesjonalny kosmetyk na puszące się włosy, kt&oacute;re wymagają wygładzenia i nabłyszczenia. Zawarte w nim cenne olejki naturalne dodają włosom blasku, zapobiegają puszeniu się i ułatwiają układanie. Ponadto chronią kosmyki przed utratą wilgoci oraz niekorzystnym działaniem czynnik&oacute;w zewnętrznych. Olejek Tsubaki regeneruje, nawilża i przywraca włosom elastyczność. Olejek Ryżowy wzmacnia je i nabłyszcza. Ekstrakt z bambusa zmiękcza kosmyki i chroni je przed szkodliwym działaniem czynnik&oacute;w zewnętrznych. Ponadto kosmetyk zawiera filtr UV dzięki kt&oacute;remu zabezpiecza włosy przed przedwczesnym starzeniem się oraz wysoką temperaturą z urządzeń stylizacyjnych. Doskonała receptura olejku sprawi, że Twoje włosy staną się miękkie, gładkie i lśniące. Produkt ma przepiękny słodki, kwiatowy zapach kt&oacute;ry zachwyci Cię od pierwszego użycia.</p>\r\n<p>Spos&oacute;b użycia: Rozetrzyj niewielką ilość olejku w dłoniach, nanieś na wilgotne lub suche włosy. Nie spłukuj. Produkt może być stosowany codziennie, przed lub po stylizacji. Dla najlepszych rezultat&oacute;w stosuj olejek razem z całą linią kosmetyk&oacute;w z linii Orofluido Asia Zen Control.</p>\r\n<p><strong>Formuły i technologie zawarte w produkcie:</strong></p>\r\n<ul>\r\n<li><strong>Ekstrakt z bambusa</strong>- zmiękcza włosy, jednocześnie wzmacnia ich odporność na szkodliwe działanie czynnik&oacute;w środowiska</li>\r\n<li>Zapobiega utracie wilgoci, działa antyoksydacyjnie i zwiększa elastyczność włos&oacute;w.</li>\r\n<li><strong>Oleje Tsubaki</strong> - ma właściwości regenerujące, antyoksydacyjne i ochronne.</li>\r\n<li>Intensywnie nawilża włosy oraz nadaje im wspaniały blask i elastyczność. Przeciwdziała puszeniu się włos&oacute;w, pozostawia je miękkie i łatwe do rozczesywania.</li>\r\n</ul>\r\n<p><strong>Rezultat:</strong></p>\r\n<ul>\r\n<li>Włosy miękkie i gładkie</li>\r\n<li>Naturalny blask i zdrowy wygląd</li>\r\n<li>Włosy stają się elastyczne i sprężyste</li>\r\n<li>Kosmyki odporne na puszenie i elektryzowanie się</li>\r\n<li>Ochrona przed promieniowaniem słonecznym</li>\r\n<li>Ochrona koloru</li>\r\n</ul>', '<p>Olejek wygładzajacy do włos&oacute;w puszących się</p>', 0.00, 50.00, 'fluidoasia.jpg', 50, '2016-09-14 08:17:05', 'active'),
(7, 'Odkurzacz', 'odkurzacz', '1031', '', '<p>Wysoka skuteczność w poręcznym formacie gwarantuje szybkie i łatwe odkurzanie</p>\r\n<p><br />&bull;Imponująca skuteczność odkurzania - por&oacute;wnywalna z odkurzaczami o mocy 2500W* - przy znacznie niższym poborze energii elektrycznej</p>\r\n<p><br />&bull;Zmywalny filtr HEPA: idealny dla alergik&oacute;w. Filtr jest na dodatek zmywalny co nie powoduj dodatkowych koszt&oacute;w. Umożliwia osiągnięcie reemisji kurzu w klasie A</p>\r\n<p><br />&bull;System PowerProtect: gwarantuje długotrwałą skuteczność odkurzania nawet przy zapełniającym się worku</p>\r\n<p><br />&bull;Rzadsza wymiana i mniejsze koszty dzięki dużej pojemności worka na kurz 4l</p>\r\n<p><br />&bull;Szczotka do twardych podł&oacute;g - odpowiednia do odkurzania np. delikatnych parkiet&oacute;w</p>', '<p>Wysoka skuteczność w poręcznym formacie gwarantuje szybkie i łatwe odkurzanie <br /><br /></p>', 0.00, 365.00, 'MCSA00862036_BO_T_11_TD3_BGL3_BGL3A230_picture_KF1_front_kalbel_ENG_270814_def.jpg', 12170, '2016-09-26 12:05:00', 'active');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pro_cat`
--

CREATE TABLE IF NOT EXISTS `bb__pro_cat` (
  `PRO_ID` int(11) NOT NULL,
  `CAT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `bb__pro_cat`
--

INSERT INTO `bb__pro_cat` (`PRO_ID`, `CAT_ID`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 3),
(7, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pro_foto`
--

CREATE TABLE IF NOT EXISTS `bb__pro_foto` (
`PFO_ID` int(11) NOT NULL,
  `PFO_PRO_ID` int(11) NOT NULL,
  `PFO_ALIAS` varchar(100) NOT NULL,
  `PFO_FILE` varchar(100) NOT NULL,
  `PFO_TYPE` enum('thumb1','thumb2','full') NOT NULL,
  `PFO_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__text_backup`
--

CREATE TABLE IF NOT EXISTS `bb__text_backup` (
`TEB_ID` int(11) NOT NULL,
  `TEB_ADM_ID` int(11) NOT NULL,
  `TEB_FROM` varchar(10) NOT NULL,
  `TEB_MODULE` varchar(15) NOT NULL,
  `TEB_MODULE_ID` int(11) NOT NULL,
  `TEB_TYPE` varchar(30) NOT NULL,
  `TEB_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TEB_NAME` varchar(200) DEFAULT NULL,
  `TEB_DATA_1` longtext,
  `TEB_DATA_2` longtext,
  `TEB_DATA_3` longtext NOT NULL,
  `TEB_DATA_4` longtext,
  `TEB_IP` varchar(30) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Zrzut danych tabeli `bb__text_backup`
--

INSERT INTO `bb__text_backup` (`TEB_ID`, `TEB_ADM_ID`, `TEB_FROM`, `TEB_MODULE`, `TEB_MODULE_ID`, `TEB_TYPE`, `TEB_DATE`, `TEB_NAME`, `TEB_DATA_1`, `TEB_DATA_2`, `TEB_DATA_3`, `TEB_DATA_4`, `TEB_IP`) VALUES
(1, 1, 'admin_', 'news', 2, 'add', '2016-09-26 10:34:37', 'Wpis testowy', '<p>Lorem ipsum lorem ipsum&nbsp; lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum<br /> lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum</p>', '<p>Skr&oacute;t newsa.</p>', '2016-09-25;2016-09-18;2016-10-08', '', '::1'),
(2, 1, 'admin_', 'news', 3, 'add', '2016-09-26 10:47:15', 'Wpis tekstowy 3', '<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p><strong> lorem ipsum lorem ipsum lorem ipsum lorem ipsum</strong></p>\r\n<p><strong></strong> lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>', '<p>Tekst</p>', '2016-09-22;2016-09-07;2016-10-07', '', '::1'),
(3, 1, 'admin_', 'news', 3, 'edit', '2016-09-26 10:47:40', 'Wpis tekstowy 3', '<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p><strong> lorem ipsum lorem ipsum lorem ipsum lorem ipsum</strong></p>\r\n<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>', '<p>Tekst</p>', '2016-09-22;2016-09-07;2016-10-07', '', '::1'),
(4, 1, 'admin_', 'news', 2, 'edit', '2016-09-26 10:48:01', 'Wpis testowy', '<p>Lorem ipsum lorem ipsum&nbsp; lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum<br /> lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>\r\n<p>lorem ipsum</p>', '<p>Skr&oacute;t newsa.</p>', '2016-09-25;2016-09-18;2016-10-08', '', '::1'),
(5, 1, 'admin_', 'news', 1, 'edit', '2016-09-26 11:03:12', 'Wpis testowy', '<p>Lorem ipsum lorem ipsum</p>', '<p>Opis skr&oacute;cony</p>', '2016-09-13;2016-09-21;2016-09-07', '', '::1'),
(6, 1, 'admin_', 'news', 1, 'edit', '2016-09-26 11:03:54', 'Wpis testowy', '<p>Lorem ipsum lorem ipsum</p>', '<p>Opis skr&oacute;cony</p>', '2016-09-13;2016-09-14;2016-09-25', '', '::1'),
(7, 1, 'admin_', 'news', 1, 'edit', '2016-09-26 11:04:34', 'Wpis testowy', '<p>Lorem ipsum lorem ipsum</p>', '<p>Opis skr&oacute;cony</p>', '2016-09-13;2016-09-14;2016-09-26', '', '::1'),
(8, 1, 'admin_', 'user', 4, 'add', '2016-09-26 14:55:26', 'Expansja', 'Jan Kowalski, Kordeckiego 47, 61-144 Poznań; wojtek@expansja.pl; 123123123; 123123123; wojtek@expansja.pl', '', '', '', '::1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__users`
--

CREATE TABLE IF NOT EXISTS `bb__users` (
`USR_ID` int(11) NOT NULL,
  `USR_ALIAS` varchar(50) NOT NULL,
  `USR_CODE` varchar(20) NOT NULL,
  `USR_BB_CODE` varchar(20) NOT NULL,
  `USR_LOGIN` varchar(50) NOT NULL,
  `USR_PASS` varchar(100) NOT NULL,
  `USR_EMAIL` varchar(70) NOT NULL,
  `USR_PHONE` varchar(20) NOT NULL,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_PERSON_NAME` varchar(70) NOT NULL,
  `USR_PERSON_SURNAME` varchar(70) NOT NULL,
  `USR_GROUP` int(11) NOT NULL,
  `USR_NIP` varchar(13) NOT NULL,
  `USR_STREET` varchar(80) NOT NULL,
  `USR_NUMBER` varchar(10) NOT NULL,
  `USR_POSTCODE` varchar(6) NOT NULL,
  `USR_CITY` varchar(70) NOT NULL,
  `USR_POINTS` int(11) NOT NULL,
  `USR_DATE_ADD` date DEFAULT NULL,
  `USR_STATUS` enum('new','active','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `bb__users`
--

INSERT INTO `bb__users` (`USR_ID`, `USR_ALIAS`, `USR_CODE`, `USR_BB_CODE`, `USR_LOGIN`, `USR_PASS`, `USR_EMAIL`, `USR_PHONE`, `USR_NAME`, `USR_PERSON_NAME`, `USR_PERSON_SURNAME`, `USR_GROUP`, `USR_NIP`, `USR_STREET`, `USR_NUMBER`, `USR_POSTCODE`, `USR_CITY`, `USR_POINTS`, `USR_DATE_ADD`, `USR_STATUS`) VALUES
(1, 'zaklad_ogolnouslugowy_wojtek', 'admin', 'admin', 'admin', 'dfc4c26a10daebacb0fe122423d4232c6e370621c24cb18f1d1ba9eb110176c7', 'wojtek@expansja.pl', '123 123', 'Zakład Ogólnousługowy WOJTEK', 'Wojciech', 'Kowalski', 1, '787-23-234-33', 'Kordeckiego', '47', '61-144', 'Poznań', 4807, '2016-08-28', 'active'),
(2, 'fryzjer', '', '', 'user1', 'b1d5004c3506f49fe063c23f6f5068e46403ba5120c66aaa3f8c690f1a80f336', 'iza@expansja.pl', '34345', 'Fryzjer', 'Jan', 'Kowalski', 0, '234-23434', 'Grobla', '3/2', '63-123', 'Poznań', 5326, NULL, 'new'),
(3, 'expansja', '', '', 'user5', '8dae73b42ebd6867cb81eadea6b75b6eaadca9a50ff9b99ae50059ae6767b5da', 'loyalty@expansja.pl', '', 'expansja', 'ewa', 'g', 0, '781090009', 'kordka', '1', '60-144', 'poznań', 5000, NULL, 'new'),
(4, 'expansja', 'jankowalski', '', 'expansja', '886d871377f66302cf94ba3831e98b19b9c080ed1c24f6a2fedb90893ea4a11b', 'wojtek@expansja.pl', '123123123', 'Expansja', 'Jan', 'Kowalski', 0, '123123123', 'Kordeckiego', '47', '61-144', 'Poznań', 0, NULL, 'new');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__users_temp`
--

CREATE TABLE IF NOT EXISTS `bb__users_temp` (
`UTE_ID` int(11) NOT NULL,
  `UTE_NAME` varchar(150) NOT NULL,
  `UTE_LOGIN` varchar(8) NOT NULL,
  `UTE_PASS` varchar(8) NOT NULL,
  `UTE_NIP` varchar(13) NOT NULL,
  `UTE_PH` varchar(50) NOT NULL,
  `UTE_STATUS` varchar(3) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=401 ;

--
-- Zrzut danych tabeli `bb__users_temp`
--

INSERT INTO `bb__users_temp` (`UTE_ID`, `UTE_NAME`, `UTE_LOGIN`, `UTE_PASS`, `UTE_NIP`, `UTE_PH`, `UTE_STATUS`) VALUES
(100, 'Zakład Fryzjerski Zofia Wujec', '322475', '2a6fbe', '7821407232', 'Sara Wesołowska', 'bb'),
(101, 'Gabinet Kosmetyczny Aleksandra Cholewa', '17eb10', '8be94d', '6381510616', 'Damian Dziadek', 'bb'),
(102, 'SALON FRYZJERSKI DAMSKO-MĘSKI AGNIESZKA HOFFMANN', '8844fa', '6c98e7', '7861351288', 'Agnieszka Skalska', 'bb'),
(103, 'Pracownia Fryzjerska Maciej Fabijański', '89666f', '9a0817', '7842440766', 'Sara Wesołowska', 'bb'),
(104, 'Kameleon Pietrzak Damian', '2e3574', '15b1aa', '5741503205', 'Damian Dziadek', 'bb'),
(105, 'Robert Palma', 'e26c47', '8dca38', '5971482961', 'Agnieszka Skalska', 'bb'),
(106, 'Przedsiebiorstwo Wielobranżowe Tanita ', '3d400b', '287f5d', '5921575442', 'Artur Miluk', 'bb'),
(107, 'Studio Atelier Paulina Latusek', 'af99a9', '1c081c', '7772681647', 'Sara Wesołowska', 'bb'),
(108, 'Salon fryzjerski Styl Stachowiak Paweł', '8ae3f8', '4f80b4', '9950154952', 'Agnieszka Skalska', 'bb'),
(109, 'M.K. Studio Fryzur Sabina Mikołajczyk', '54997b', '98e8d7', '6191901874', 'Sara Wesołowska', 'bb'),
(110, 'Anita Szwarc FIRMA HANDLOWO-USŁUGOWA AS ', '85fd8d', '48de6d', '6981660726', 'Agnieszka Skalska', 'bb'),
(111, 'CEDAR-POL SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ', '5f3fb8', 'd2feb2', '5732750648', 'Damian Dziadek', 'bb'),
(112, 'Anami Style Anna Łężniak', '7e273f', '02af91', '5512356547', 'Justyna Bosak', 'bb'),
(113, 'STUDIO FRYZUR BARTOSZ WOLAK, INSTYTUT PIĘKNA', 'f40af3', '9f7504', '6312537486', 'Damian Dziadek', 'bb'),
(114, 'Salon Fryzjerski Agnieszka   ', 'c84e60', 'a16117', '9131508413', 'Ewa Horbal', 'bb'),
(115, 'GABINET KOSMETYCZNY PICHO AGNIESZKA', '80da25', 'ddc306', '8512587573', 'Agnieszka Skalska', 'bb'),
(116, 'Gabinet Urody Agnieszka Zielińska', '7a687d', '4628db', '9521993014', 'Ewa Zielińska', 'bb'),
(117, 'Atelier Fryzur Lukasz Milewski', 'fb0bfc', '107ef4', '9542657623', 'Damian Dziadek', 'bb'),
(118, 'Salon Fryzjerski Radosław Wysocki', 'c06ac7', 'b8082b', '6651168459', 'Sara Wesołowska', 'bb'),
(119, 'PHU HONORATA Aldona i Marian Szastok s.c.', '2ea697', 'e1b1c6', '6182143049', 'Sara Wesołowska', 'bb'),
(120, 'STUDIO INSPIRE ZYCH ŁUKASZ', 'b6f803', '6160d7', '5971482978', 'Agnieszka Skalska', 'bb'),
(121, 'Salon Urody Lidia Derdziak', '285389', '1b6df9', '7772777100', 'Sara Wesołowska', 'bb'),
(122, 'Studio Urody Lady Paulina Szymańska', '30945c', '07c427', '7772717144', 'Sara Wesołowska', 'bb'),
(123, 'Studio Fryzjerskie Colette Zofia Grabowska', 'f962f8', '322501', '9451324933', 'Justyna Bosak', 'bb'),
(124, 'TEN Łukasz Mazolewski', '5d816e', 'aa0ebc', '9570802178', 'Warszawa', 'bb'),
(125, 'OBSESSION PAULINA KOMAR ', '2bb58b', '7ad3c4', '8513015560', 'Agnieszka Skalska', 'bb'),
(126, 'MX Salon Fryzjerski Elżbieta Kwaśny', 'ddca47', 'c6e8f6', '7821187799', 'Agnieszka Skalska', 'bb'),
(127, 'Firma Handlowa Margo', '19aa8e', '6f82a9', '6970003580', 'Agnieszka Skalska', 'bb'),
(128, 'Studio Fryzur Ewa Fryc', 'fe9d23', '8f5dc0', '9451538700', 'Justyna Bosak', 'bb'),
(129, 'Gwiazda Iwona Gwiazda', 'ffdac8', '7e7a30', '6111021416', 'Ewa Horbal', 'bb'),
(130, 'Klinika Fryzjerstwa i Urody Agnieszka Wdowiak', 'a01392', 'e01094', '7292025038', 'Damian Dziadek', 'bb'),
(131, 'Atelier Fryzur Piotr Sierpiński', '70fccf', '88691b', '7822260328', 'Agnieszka Skalska', 'bb'),
(132, 'Hair Design Karol Olejnik', '13579e', 'b113f8', '6411862835', 'Justyna Bosak', 'bb'),
(133, 'SALON FRYZJERSKI E & I ILONA NAPIERAŁA', 'b4f181', '1e6f8a', '7781075716', 'Agnieszka Skalska', 'bb'),
(134, 'PRZEDSIĘBIORSTWO WIELOBRANŻOWE KATKA -', '512b72', '0b9041', '8950002176', 'Sara Wesołowska', 'bb'),
(135, 'Usługi Fryzjersko-Kosmetyczne Ula ', 'c0109f', 'db1e09', '7881832447', 'Agnieszka Skalska', 'bb'),
(136, 'ATHRU Marzena Kordek ', '194f48', 'c6712e', '6971862121', 'Sara Wesołowska', 'bb'),
(137, 'I.S.TROY Sp.z o.o.', 'cd79aa', 'f9ce72', '8522547629', 'Agnieszka Skalska', 'bb'),
(138, 'HAIR - SPA SALON FRYZUR SOBCZAK AGNIESZKA', 'a6f7c9', '7ac216', '7771786172', 'Sara Wesołowska', 'bb'),
(139, 'CLUB PIĄTKA CZAJKOWSKA MAŁGORZATA', '056d89', '4d78ec', '9231009773', 'Agnieszka Skalska', 'bb'),
(140, 'Salon Mody Valentino Danuta Banska', '59f1f0', 'bef90e', '7842107995', 'Sara Wesołowska', 'bb'),
(141, 'STUDIO FRYZUR OLA OLGA MICHALCZAK', '8fc00f', 'bf604a', '7881374039', 'Agnieszka Skalska', 'bb'),
(142, 'STUDIO FRYZJERSKIE EDYTA ZAGOZDA', 'e84af8', 'dfd701', '6681797384', 'Sara Wesołowska', 'bb'),
(143, 'Galeria Fryzur Żaneta Waszak', 'e1987b', '3ed981', '9680668290', 'Sara Wesołowska', 'bb'),
(144, 'VEGA-STUDIO KAMILA SIERADZAN', 'ca40ff', '863ab6', '8511805789', 'Agnieszka Skalska', 'bb'),
(145, 'Salon Fryzjerski Ewa Kowalska', 'c15999', 'e69e69', '7791028322', 'Agnieszka Skalska', 'bb'),
(146, 'STUDIO K.B. KATARZYNA BORCZYŃSKA', '7f9446', 'b7da1b', '7291033780', 'Damian Dziadek', 'bb'),
(147, 'Illusion Studio Melon Elżbieta', '011c9f', '12a780', '6661472561', 'Sara Wesołowska', 'bb'),
(148, 'PRO-SAN M.Jagiełka i M.Wiszniewski Sp.J.', 'c3a01d', '2ca5c1', '6711689074', 'Agnieszka Skalska', 'bb'),
(149, 'Novak Agnieszka Nowak ', 'fa0995', '508bb6', '6652920675', 'Agnieszka Skalska', 'bb'),
(150, 'Pracownia Fryzur', 'a705ce', '6540c8', '7772310827', 'Sara Wesołowska', 'bb'),
(151, 'Waldemar Ziemirski Hair Concept', '8ed88a', 'bb1f4d', '8731892678', 'Justyna Bosak', 'bb'),
(152, 'HOUSE OF BEAUTY RAFAŁ WIDOK', '198268', '2a2b45', '6422621006', 'Damian Dziadek', 'bb'),
(153, 'A&P Przemysław Skowroński ', '683a61', '90a322', '8792316733', 'Artur Miluk', 'bb'),
(154, 'AJ STUDIO Alicja Jelska-Symonds', '37cc21', 'a70027', '9581431679', 'Artur Miluk', 'bb'),
(155, 'BONA DEA KLINIKA MEDYCYNY ESTETYCZNEJ SPÓŁKA Z', 'acf459', '462f60', '6772382364', 'Justyna Bosak', 'bb'),
(156, 'FUZZ HAIR DESIGN', 'a547c9', '12672d', '7891516245', 'Agnieszka Skalska', 'bb'),
(157, 'STUDIO PIĘKNA  INCANTO SABINA MOSKAŁA', 'c88317', 'a78188', '5512375444', 'Justyna Bosak', 'bb'),
(158, 'SAM SEBASTIAN STYLE', 'd4c4fb', '53b614', '8512421915', 'Agnieszka Skalska', 'bb'),
(159, 'HOTEL ARŁAMÓW SPÓŁKA AKCYJNA ', '4f2601', '54854a', '6891093258', 'Justyna Bosak', 'bb'),
(160, 'PETPOLONIA MAŁGORZATA KOCZOT, ADAM TOMA SPÓŁKA', '94ad02', '6a6b08', '5521417260', 'Justyna Bosak', 'bb'),
(161, '4 mentis  Brukalski Remigiusz', '7e0631', 'a20ed4', '9551255315', 'Agnieszka Skalska', 'bb'),
(162, 'STACHOWICZ BEATA ZAKŁAD FRYZJERSKI DAMSKO-MĘSKI', 'f6505d', '521a4b', '6711351949', 'Agnieszka Skalska', 'bb'),
(163, 'FIRMA HANDLOWA MARGO MAŁGORZATA PYTLIK', '5b9da1', 'd9855f', '6970003580', 'Agnieszka Skalska', 'bb'),
(164, 'MAJSTER BUT Karol Woźniak', 'd56cb6', 'b7ac49', '7822140452', 'Agnieszka Skalska', 'bb'),
(165, 'Kolorowe Czuprynki Sp. z o.o.', 'dc1eea', '442ad6', '5842742495', 'Artur Miluk', 'bb'),
(166, 'Nail Spa Marzena Kanclerska-Gryz', '078194', 'efc7fd', '1130518440', 'Ewa Zielińska', 'bb'),
(167, 'Przedsiębiorstwo Prywatne Monika Lisewska', '167251', 'b20e16', '7821673034', 'Agnieszka Skalska', 'bb'),
(168, 'Studio Fryzur Zosia Zofia Keplin', '55ebaa', 'd544ee', '5551836665', 'Artur Miluk', 'bb'),
(169, 'KOPA HONORATA SALON FRYZUR i URODY', 'a3085e', 'fe9aaa', '9880146685', 'Ewa Horbal', 'bb'),
(170, 'IMPERIUM AGNIESZKA KARPIŃSKA ', '684018', 'ae1b78', '7822350362', 'Sara Wesołowska', 'bb'),
(171, 'Emas Agnieszka Marciniak ', '00f0ab', 'e83004', '7851191050', 'Sara Wesołowska', 'bb'),
(172, 'Salon Fryzjerski Diva', '0e3444', '6ebe69', '6381063991', 'Damian Dziadek', 'bb'),
(173, 'Exit Beata Tobuch', '599909', 'e6d917', '9161194296', 'Sara Wesołowska', 'bb'),
(174, 'SOBULA DOROTA SALON FRYZJERSKI DORIS', '812fb4', '745d8c', '7352029302', 'Justyna Bosak', 'bb'),
(175, 'Studio 3/3 Fryzjerstwo i Kosmetyka s.c.', 'fe7f40', 'c9203b', '6793112598', 'Justyna Bosak', 'bb'),
(176, 'Atelier Fryzur Łukasz Milewski', '52ed02', '5e011e', '9542657623', 'Damian Dziadek', 'bb'),
(177, 'Salon Fryzjerski Agnieszka Andrzej Pająk', 'd0e83f', 'a02ba5', '9131500419', 'Sara Wesołowska', 'bb'),
(178, 'STUDIO KOSMETYKI OLIVIA OLIWIA PLEWKA', 'c07bd0', '12a054', '6452303624', 'Damian Dziadek', 'bb'),
(179, 'WS Future Daniel Schmidt', '50f0b3', '2979af', '7811713944', 'Agnieszka Skalska', 'bb'),
(180, 'SALON URODY DOTYK PIĘKNA JOANNA STAŃCO', '0fca63', '278ba1', '5512301635', 'Justyna Bosak', 'bb'),
(181, 'b7 NOWA JAKOŚĆ FRYZJERSTWA, BARTŁOMIEJ KUREK', 'afc6bc', '5399c7', '6431669314', 'Damian Dziadek', 'bb'),
(182, 'MARCIN PAW  Studio Fryzjerskie Marcin Paw', 'f04ae3', '148a90', '5491206592', 'Damian Dziadek', 'bb'),
(183, 'GRABOWSKA-ROMANKO MAGDALENA SALON FRYZJERSKI', '4466f2', 'a7fc57', '6381063991', 'Damian Dziadek', 'bb'),
(184, 'Formosa Agnieszka Lula', 'd0b594', '416f6f', '7822045050', 'Agnieszka Skalska', 'bb'),
(185, 'PRACOWNIA FRYZJERSKO-KOSMETYCZNA KAMELEON', '6b4b5d', '2f5669', '7772293106', 'Agnieszka Skalska', 'bb'),
(186, 'Salon Fryzjerski Vivian Mariola Szpyrka', '88c57e', 'bb7f30', '6521017377', 'Damian Dziadek', 'bb'),
(187, 'Studio Fryzur A.N. Aleksandra Nycz ', 'df4143', '75f3c0', '5492147130', 'Justyna Bosak', 'bb'),
(188, 'Zakład Fryzjerski Janina Machulak', '0cdc87', '800ebf', '7541164283', 'Ewa Horbal', 'bb'),
(189, 'Salon Fryzjerski U Ewki Ewa Gąsienica-Makowska', 'f0ad8c', '1d9e7a', '7361548527', 'Justyna Bosak', 'bb'),
(190, 'Studio Viva Kamila Kasprzak', '80a8e6', 'f87e16', '6972055552', 'Sara Wesołowska', 'bb'),
(191, 'Studio METAMORFOZA Krystian Przybylski', 'f0e553', 'd7ffea', '7861237859', 'Sara Wesołowska', 'bb'),
(192, 'Dako Lokum Danuta Kostera', '7789f1', 'cfa8dd', '6180061330', 'Sara Wesołowska', 'bb'),
(193, 'beauty Coctail Olga Koza', '68df52', 'a4856b', '5731690559', 'Damian Dziadek', 'bb'),
(194, 'Salon Fryzjerski Esencja ', 'f0015d', '535a36', '9720981893', 'Agnieszka Skalska', 'bb'),
(195, 'CZESANKA ADAM ŻOLIK', 'd59a7a', '5bcad6', '6721854385', 'Agnieszka Skalska', 'bb'),
(196, 'Ewelina Zarychta', '137f39', '64593c', '8522483651', 'Agnieszka Skalska', 'bb'),
(197, 'Urszula Kilikowska', 'e3e9ad', 'ff5a14', '8521352438', 'Agnieszka Skalska', 'bb'),
(198, 'ZAKŁAD FRYZJERSKI MARIOLA MARIA TELESZ', '5f8c94', 'e2fde6', '9231082178', 'Agnieszka Skalska', 'bb'),
(199, 'Studio Fryzur Styl', '6bf208', '05cae8', '7792002224', 'Agnieszka Skalska', 'bb'),
(200, 'Le Grand Partners Sp. z o.o.', '3dee92', 'a58edf', '7771024692', 'Agnieszka Skalska', 'bb'),
(201, 'ALEXANDRA Aleksandra Puchalska', '850b47', 'bc7fd4', '7441797391', 'Artur Miluk', 'bb'),
(202, 'Anita Szwarc FIRMA HANDLOWO-USŁUGOWA AS', 'c150e8', '21660a', '6981660726', 'Agnieszka Skalska', 'bb'),
(203, 'ADARA S.C.SALON MODNEJ FRYZURY I.LESNOW, A.LESNOW', 'd0048b', 'f0bbc7', '5851271138', 'Artur Miluk', 'bb'),
(204, 'Galeria Fryzur Sylwia Matulis', '207361', '350b11', '5992314803', 'Agnieszka Skalska', 'bb'),
(205, 'Salon Fryzjerski,,Irokez', 'ff8a79', '8287c6', '5941295450', 'Agnieszka Skalska', 'bb'),
(206, 'BioSkin - gabinet kosmetyczny, gabinet', 'f20882', '4b554b', '6181833757', 'Sara Wesołowska', 'bb'),
(207, 'PEPETA NIERUCHOMOŚCI DANUTA PEPETA PRACOWNIA', 'fa4151', 'f3d7cc', '7772837003', 'Agnieszka Skalska', 'bb'),
(208, 'Studio Fryzur Visage', 'c3a847', 'a9c7cf', '6652053818', 'Sara Wesołowska', 'bb'),
(209, 'SALONY STYLU AGNIESZKA MISIURNA', 'c6084e', '875425', '9730737597', 'Agnieszka Skalska', 'bb'),
(210, 'Hair Styling Team Urszula Kowalska', 'c32b11', 'ced505', '8231480085', 'Justyna Bosak', 'bb'),
(211, 'ZAKŁAD USŁUGOWY CHABER Grażyna Bernat', 'a50816', '9dc955', '6441477390', 'Damian Dziadek', 'bb'),
(212, 'APRIM S.C. ADRIANA GAWROŃSKA,PIOTR GAWROŃSKI ', 'fdd669', 'ab9b26', '6181006238', 'Sara Wesołowska', 'bb'),
(213, 'Damian Kozierski Salon Fryzjerski', '66fe68', 'bf5de8', '9950058119', 'Agnieszka Skalska', 'bb'),
(214, 'HURT-DETAL HALINA OGONOWSKA', '1b8697', '8af4bb', '5920009663', 'Artur Miluk', 'bb'),
(215, 'AM STUDIO FRYZUR ANNA KAŹMIEROWSKA, MARTA SZAJ ', '83e152', '18a699', '7773250659', 'Agnieszka Skalska', 'bb'),
(216, 'ATELIER SZTUKI FRYZJERSKIEJ IZABELA JADWIGA KRAUSE', 'c30006', '8c8cf9', '5931256738', 'Agnieszka Skalska', 'bb'),
(217, 'SALON FRYZJERSKI WIOLETTA Wioletta Dębicka', '1b8228', '4bb10b', '7841132034', 'Agnieszka Skalska', 'bb'),
(218, 'PAWEŁ KRAKOWIAK DORADZTWO GOSPODARCZE', '0060c8', 'd92321', '7271016146', 'Agnieszka Skalska', 'bb'),
(219, 'FIRMA USŁUGOWO SZKOLENIOWA Tomasz Szymański', '04a505', '7fe922', '8571685432', 'Ewa Zielińska', 'bb'),
(220, 'SALON FRYZJERSKO - KOSMETYCZNY EUGENIUSZ NAWROT', 'e02ee7', '644923', '7771721965', 'Agnieszka Skalska', 'bb'),
(221, 'SALON FRYZJERSKO-KOSMETYCZNY GALAXY NATANEK', '395eca', '813e50', '6482703848', 'Damian Dziadek', 'bb'),
(222, 'Euromed Małgorzata Missal - Cegłowska', 'd7b6af', '827930', '7771393550', 'Agnieszka Skalska', 'bb'),
(223, 'Essenza Magdalena Czyrek', '17d584', 'c366bd', '8992596269', 'Ewa Horbal', 'bb'),
(224, 'CHRIS CASTLEFORD SALON  FRYZJERSKI KRZYSZTOF ', 'c1af14', '657043', '8691880461', 'Justyna Bosak', 'bb'),
(225, 'Studio I Biuro Anna Czernecka', '1cc471', 'def43a', '6381000654', 'Damian Dziadek', 'bb'),
(226, 'Hair Care Clinic Karolina Golema', '59b741', '1ad9ab', '8133681948', 'Justyna Bosak', 'bb'),
(227, 'FIRMA HANDLOWO-USŁUGOWA Monika Buko', '413e00', 'c1948b', '8981814567', 'Ewa Horbal', 'bb'),
(228, 'Victoria''s Beauty  Salon  Beata Zając ', '368949', 'b458d4', '8133567000', '', 'bb'),
(229, 'Agnes Style Agnieszka Wojdyła Fryzjerstwo ', 'ba9003', '27d8f4', '7352251153', 'Justyna Bosak', 'bb'),
(230, 'Zbigniew Zochorek ZBYSZEK ZOCHOREK HAIR DESIGN', 'a292d1', 'c05ec6', '6381673755', 'Damian Dziadek', 'bb'),
(231, 'Mariusz ROGOWSKI', 'd0098c', '58a5f1', '7342614855', 'Justyna Bosak', 'bb'),
(232, 'STUDIO FRYZJERSKI DOROTA WACHOŃSKA', 'f6c9a7', '1814e9', '9271033571', 'Agnieszka Skalska', 'bb'),
(233, 'BEAUTY HAIR KSYMENA WOJTKIEWICZ ', 'e4e990', 'ab75e3', '5941452518', 'Agnieszka Skalska', 'bb'),
(234, 'Salon Fryzjersko Kosmetyczny Agness', '6f1b93', 'deff6a', '7771220967', 'Sara Wesołowska', 'bb'),
(235, 'ZAKŁAD FRYZJERSKI DOROTA BASTER', 'af6b1b', '7a1620', '7941158464', 'Ewa Horbal', 'bb'),
(236, 'Karolina Czajka ', 'bfa067', '6b8867', '6751313837', 'Justyna Bosak', 'bb'),
(237, 'NASKAR Norbert Karbownik', '79e4f4', '5a551a', '8522353776', 'Agnieszka Skalska', 'bb'),
(238, 'Fryzjer dla dzieci Kolorowa Czuprynka Agnieszka ', '0e5cbb', '1a1151', '9581287308', 'Artur Miluk', 'bb'),
(239, 'STUDIO URODY A&D SPÓŁKA Z OGRANICZONĄ ', '2c91cd', '85c368', '8522617347', 'Agnieszka Skalska', 'bb'),
(240, 'Madame Salon Fryzur Anna Bielińska', '24619c', '5c0eac', '5922009118', 'Artur Miluk', 'bb'),
(241, 'Studio Stylizacji Fryzur Agnieszka Marcinkowska', 'f3dc7f', '2fc495', '8581431532', 'Agnieszka Skalska', 'bb'),
(242, 'SALON FRYZJERSKI EWELINA KOZŁOWSKA-KOLANEK ', '665535', '6cf19f', '5941506668', 'Agnieszka Skalska', 'bb'),
(243, 'Studio Agnieszka Kasprowicz ', '9d2177', '0f1583', '9231533899', 'Agnieszka Skalska', 'bb'),
(244, 'Salon Fryzjersko-kosmetyczny FRYZJERNIA AWANGARDA ', '69d9e2', '8cea9f', '6711445225', 'Agnieszka Skalska', 'bb'),
(245, 'USŁUGI FRYZJERSKIE RAFAŁ KONARZEWSKI', '678c63', 'c5f50d', '9551846915', 'Agnieszka Skalska', 'bb'),
(246, 'F.H.U. Marilyn Danuta Biesek', 'd6b8d8', 'a754c3', '5921168145', 'Artur Miluk', 'bb'),
(247, 'Tomasz Fryzjerstwo Damskie', 'e1c48a', 'a4ab88', '7271269410', 'Damian Dziadek', 'bb'),
(248, 'EWA NOWICKA ZAKŁAD FRYZJERSKI', '6b393c', 'c90fb7', '7881030601', 'Justyna Bosak', 'bb'),
(249, 'Jakub Hrycyniak', '8d3177', '5c3e09', '8672089329', 'Justyna Bosak', 'bb'),
(250, 'Studio Fryzury Maria Jędrzejczak', '22b064', '8bcacd', '7851071446', 'Sara Wesołowska', 'bb'),
(251, 'Instytut zdrowia i Urody Gałązka Oliwna Cecylia ', 'f5110d', 'ed5b96', '8411521147', 'Agnieszka Skalska', 'bb'),
(252, 'MB Salon Fryzjerski Professional Mariola Baran ', '92c7f2', '507a19', '6351804678', 'Damian Dziadek', 'bb'),
(253, 'STUDIO VIVA', 'ke4ra5', 'pw3bav', '', 'Sara Wesołowska', 'bb'),
(254, 'Mirosława Hebel-Bojczuk STUDIO', '783701', 'bfa9cd', '5861778844', 'Artur Miluk', 'bb'),
(255, 'Warsztat fryzur', '5320b4', 'fc8c02', '6342505479', 'Damian Dziadek', 'bb'),
(256, 'ICON SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ', 'af84cf', 'cf6be3', '6762478453', 'Justyna Bosak', 'bb'),
(257, 'Studio Urody s.c.', '78628d', '84eaa6', '8542370074', 'Justyna Bosak', 'bb'),
(258, 'F.H.U ,,PATRYCJA Patrycja Czauderna-Kuder', '23gi0a', 'snfw81', '', 'Justyna Bosak', 'bb'),
(259, 'MONIKA SZCZEPANEK STUDIO MAJORKA', 'd34f14', '2949d9', '8541740681', 'Agnieszka Skalska', 'bb'),
(260, '', 'b7177f', '97b518', '', '', 'new'),
(261, '', '30fb88', 'cd02b7', '', '', 'new'),
(262, '', 'fb58e8', '48775b', '', '', 'new'),
(263, '', '2c8bd6', '827b18', '', '', 'new'),
(264, '', '17c7b4', '8503fd', '', '', 'new'),
(265, '', 'ae717d', 'ee853d', '', '', 'new'),
(266, '', '1537fe', 'ca44ea', '', '', 'new'),
(267, '', 'bc7f1b', 'a89d90', '', '', 'new'),
(268, '', '0a7e31', 'cf090d', '', '', 'new'),
(269, '', '537091', '184bb1', '', '', 'new'),
(270, '', 'e1c0b0', 'fb0eac', '', '', 'new'),
(271, '', 'd71b2e', 'c8bc0c', '', '', 'new'),
(272, '', 'a378be', 'a2eb94', '', '', 'new'),
(273, '', '6b549d', 'ead6d1', '', '', 'new'),
(274, '', 'b0427a', '8fc437', '', '', 'new'),
(275, '', 'dcaba3', '912ed5', '', '', 'new'),
(276, '', '71e45b', '9566e5', '', '', 'new'),
(277, '', 'e4648b', '4c1b59', '', '', 'new'),
(278, '', '3d9c48', 'a8b6d0', '', '', 'new'),
(279, '', '725408', '181933', '', '', 'new'),
(280, '', '7815ea', '09a1b3', '', '', 'new'),
(281, '', '1ff760', '7e3e9b', '', '', 'new'),
(282, '', '2a2ce9', '1f46fe', '', '', 'new'),
(283, '', 'ca0e06', '86018b', '', '', 'new'),
(284, '', 'f12af4', 'dd63d8', '', '', 'new'),
(285, '', '2eab2a', '16b46c', '', '', 'new'),
(286, '', 'aa32ef', '86c966', '', '', 'new'),
(287, '', 'de5f4b', '9bc50b', '', '', 'new'),
(288, '', 'f91c2b', '7a584d', '', '', 'new'),
(289, '', '535076', '29e8d6', '', '', 'new'),
(290, '', '947008', 'be4f67', '', '', 'new'),
(291, '', '68054c', 'b364f3', '', '', 'new'),
(292, '', 'd9b200', 'a26c37', '', '', 'new'),
(293, '', '7ba3d1', 'ae95b7', '', '', 'new'),
(294, '', 'c754ec', 'c5b104', '', '', 'new'),
(295, '', '358d83', '0c5f70', '', '', 'new'),
(296, '', '01514b', 'bbc7dc', '', '', 'new'),
(297, '', '531766', 'cabf37', '', '', 'new'),
(298, '', '655457', 'd2bc29', '', '', 'new'),
(299, '', 'bdf5c2', '84f8a7', '', '', 'new'),
(300, '', 'fde3a2', 'c652f0', '', '', 'new'),
(301, '', '45adcc', '29c010', '', '', 'new'),
(302, '', 'fd367b', 'f41c07', '', '', 'new'),
(303, '', '47ca45', '83ed7a', '', '', 'new'),
(304, '', '1dcfd2', '5b44fa', '', '', 'new'),
(305, '', 'c89785', 'e03f05', '', '', 'new'),
(306, '', '1943c2', '890e75', '', '', 'new'),
(307, '', 'ba5b68', '868f6b', '', '', 'new'),
(308, '', '1e74e9', 'a0c132', '', '', 'new'),
(309, '', '5be5e2', 'dc81e6', '', '', 'new'),
(310, '', 'b2f621', 'e0a67b', '', '', 'new'),
(311, '', 'b3d0ab', '0258a6', '', '', 'new'),
(312, '', '06baee', 'c230af', '', '', 'new'),
(313, '', 'cd3516', '9bf565', '', '', 'new'),
(314, '', 'd74743', '13eba5', '', '', 'new'),
(315, '', '527704', '29394d', '', '', 'new'),
(316, '', 'bdab06', 'ed2e02', '', '', 'new'),
(317, '', 'fb747e', 'fed9e2', '', '', 'new'),
(318, '', '0dc30e', 'd94956', '', '', 'new'),
(319, '', '39ecac', '7b6ce8', '', '', 'new'),
(320, '', '019f27', '3b6144', '', '', 'new'),
(321, '', '89bd80', '7b8fe2', '', '', 'new'),
(322, '', '4e63d8', 'acb59c', '', '', 'new'),
(323, '', 'e57698', 'bdbc4d', '', '', 'new'),
(324, '', '91e5f1', 'e171bb', '', '', 'new'),
(325, '', '7847fd', 'b774c1', '', '', 'new'),
(326, '', 'cc11e1', 'fe1121', '', '', 'new'),
(327, '', '924d66', 'ce531b', '', '', 'new'),
(328, '', '69205b', 'b95eb0', '', '', 'new'),
(329, '', '048cdf', '330ece', '', '', 'new'),
(330, '', 'c31975', '846e8f', '', '', 'new'),
(331, '', 'e540f8', 'dc4d04', '', '', 'new'),
(332, '', 'f2d851', 'e50608', '', '', 'new'),
(333, '', '1cbf76', 'b3ed48', '', '', 'new'),
(334, '', '895079', '0a77ac', '', '', 'new'),
(335, '', '0072f9', '135b5e', '', '', 'new'),
(336, '', 'c14550', 'fc9014', '', '', 'new'),
(337, '', 'bee51d', '0bdfe9', '', '', 'new'),
(338, '', '1c346e', '23718d', '', '', 'new'),
(339, '', 'c53503', '52dd88', '', '', 'new'),
(340, '', '6af05b', '948e00', '', '', 'new'),
(341, '', '0192dd', '372e68', '', '', 'new'),
(342, '', '1a7bb5', '9ad7d7', '', '', 'new'),
(343, '', '3a22f6', '1d3b06', '', '', 'new'),
(344, '', '4b114d', 'b68a2e', '', '', 'new'),
(345, '', '0bc10f', 'ce7c7a', '', '', 'new'),
(346, '', 'beb4a3', '43ee20', '', '', 'new'),
(347, '', '51abaa', '457003', '', '', 'new'),
(348, '', '3d0ab6', 'fa205c', '', '', 'new'),
(349, '', 'f02b88', '94c232', '', '', 'new'),
(350, '', '706b7f', '619f0e', '', '', 'new'),
(351, '', '9bf85b', 'ca21c5', '', '', 'new'),
(352, '', '0bbf2e', '28675d', '', '', 'new'),
(353, '', 'cc69c7', 'ce1050', '', '', 'new'),
(354, '', '7d73ee', '00beba', '', '', 'new'),
(355, '', '3d3de9', '830307', '', '', 'new'),
(356, '', '387cba', 'a4b0b7', '', '', 'new'),
(357, '', '73e402', 'b8d2fe', '', '', 'new'),
(358, '', 'd7b7b9', 'fdfeba', '', '', 'new'),
(359, '', '4243d7', 'b27a24', '', '', 'new'),
(360, '', '542367', '9179c3', '', '', 'new'),
(361, '', '55a06f', 'aeff78', '', '', 'new'),
(362, '', '98551c', '0387c4', '', '', 'new'),
(363, '', 'ecf548', 'c2c012', '', '', 'new'),
(364, '', 'c4f80a', '6749ef', '', '', 'new'),
(365, '', '12b995', '181ed8', '', '', 'new'),
(366, '', 'ffb1f8', '743200', '', '', 'new'),
(367, '', 'c12390', '723b7b', '', '', 'new'),
(368, '', 'fac679', '7d13d0', '', '', 'new'),
(369, '', '8e0e56', 'c09b56', '', '', 'new'),
(370, '', 'a77583', 'a5b5d6', '', '', 'new'),
(371, '', '7c09d6', 'ed2ca5', '', '', 'new'),
(372, '', '75dbf2', '840791', '', '', 'new'),
(373, '', '564f08', 'a08c54', '', '', 'new'),
(374, '', '9be26b', '5f156a', '', '', 'new'),
(375, '', 'b0f30d', '9e717d', '', '', 'new'),
(376, '', 'ce46bd', '2d5da1', '', '', 'new'),
(377, '', '99e6f2', '60d35f', '', '', 'new'),
(378, '', '39ce25', 'a44516', '', '', 'new'),
(379, '', 'e02c5f', '89d634', '', '', 'new'),
(380, '', 'e7578b', 'b9d277', '', '', 'new'),
(381, '', '18928d', '889e33', '', '', 'new'),
(382, '', 'd30b47', 'd0f535', '', '', 'new'),
(383, '', '73ea47', 'cb8045', '', '', 'new'),
(384, '', '11d14a', 'b9e230', '', '', 'new'),
(385, '', 'cf619f', '88415f', '', '', 'new'),
(386, '', '86f396', '96ccef', '', '', 'new'),
(387, '', '9ae910', '2d73cb', '', '', 'new'),
(388, '', '523e6f', '9179bf', '', '', 'new'),
(389, '', 'd0fb47', '25261e', '', '', 'new'),
(390, '', '28b02a', 'dbaa87', '', '', 'new'),
(391, '', '21e68a', '6c1c8c', '', '', 'new'),
(392, '', 'af0570', '56853f', '', '', 'new'),
(393, '', '5628a5', '8d2d18', '', '', 'new'),
(394, '', '33c3f5', '33622b', '', '', 'new'),
(395, '', '052ac1', 'ee4b9e', '', '', 'new'),
(396, '', '120483', 'd6f767', '', '', 'new'),
(397, '', '1dac16', 'f32582', '', '', 'new'),
(398, '', 'b3ff26', '17ec35', '', '', 'new'),
(399, '', 'fc69ce', '64426b', '', '', 'new'),
(400, '', '0ed5e3', 'fd7389', '', '', 'new');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_address_send`
--

CREATE TABLE IF NOT EXISTS `bb__user_address_send` (
`UAS_ID` int(11) NOT NULL,
  `UAS_USR_ID` int(11) NOT NULL,
  `UAS_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UAS_PERSON_NAME` varchar(250) NOT NULL,
  `UAS_PERSON_SURNAME` varchar(250) NOT NULL,
  `UAS_STREET` varchar(250) NOT NULL,
  `UAS_NUMBER` varchar(10) NOT NULL,
  `UAS_POSTCODE` varchar(6) NOT NULL,
  `UAS_CITY` varchar(250) NOT NULL,
  `UAS_PHONE` varchar(16) NOT NULL,
  `UAS_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Zrzut danych tabeli `bb__user_address_send`
--

INSERT INTO `bb__user_address_send` (`UAS_ID`, `UAS_USR_ID`, `UAS_DATE_ADD`, `UAS_PERSON_NAME`, `UAS_PERSON_SURNAME`, `UAS_STREET`, `UAS_NUMBER`, `UAS_POSTCODE`, `UAS_CITY`, `UAS_PHONE`, `UAS_STATUS`) VALUES
(1, 1, '2016-09-08 12:23:34', 'lkj', 'lkj', '', 'jlk', 'jlk', 'jlkj', 'lkj', 'deleted'),
(2, 1, '2016-09-08 12:24:54', 'Zdzisław', 'Zdzisław', 'Kordeckiego', '3/4', '43-567', 'Poznań', '34535435', 'active'),
(3, 1, '2016-09-09 11:49:10', 'fasdfa', 'asdfadsf', 'j', 'kk', 'k', '', 'kjk', 'deleted'),
(4, 1, '2016-09-12 11:25:49', 'Joanna', 'Kowalska', 'Głogowska', '36', '64-123', 'Poznań', '23234234', 'active'),
(5, 1, '2016-09-13 12:13:14', 'ii', 'ii', '4', '4', '', '', 'ui', 'deleted'),
(6, 1, '2016-09-14 15:20:08', 'Kornelia', 'Kochanowska', 'piękna', '33', '', 'Olaboga', '0001115855', 'active'),
(7, 2, '2016-09-15 12:45:47', 'iza', 'aco', 'Bukowska 67/5', '33', '60_567', 'Poznań', '667342068', 'active'),
(8, 1, '2016-09-23 12:44:53', 'Adam', 'Słodowy', 'Grunwaldzka', '44', '43-345', 'POoznań', '345345', 'active'),
(9, 3, '2016-09-23 15:42:53', 'kinka', 'dddd', 'dddddd', 'ssss', 'dddddd', 'ddddd', '790087', 'active');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_func`
--

CREATE TABLE IF NOT EXISTS `bb__user_func` (
`UFU_ID` int(11) NOT NULL,
  `UFU_USR_ID` int(11) NOT NULL,
  `UFU_CODE` varchar(60) NOT NULL,
  `UFU_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UFU_TYPE` varchar(100) NOT NULL,
  `UFU_DATA` tinytext NOT NULL,
  `UFU_INFO` tinytext NOT NULL,
  `UFU_RESULT` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `bb__user_func`
--

INSERT INTO `bb__user_func` (`UFU_ID`, `UFU_USR_ID`, `UFU_CODE`, `UFU_DATE`, `UFU_TYPE`, `UFU_DATA`, `UFU_INFO`, `UFU_RESULT`) VALUES
(1, 1, 'c68382097b25f52fbde56117a555c97a63f5b9d205bc80f4b7d72acbf885', '2016-09-22 11:57:32', 'user data change', 'Zakład Ogólnousługowy WOJTEK<br/>Wojciech Kowalski,<br/>Kordeckiego 47,<br/>61-144 Poznań,<br/>nip: 787-23-234-33,<br/>tel: 123 123,<br/>email: wojtek@expansja.pl', '', ''),
(2, 3, '8a6ad3f42747e679d54b08c70f79bd99bb9d12cc07c2d8a89dd3ebd89e3d', '2016-09-23 15:41:51', 'user data change', 'expansja<br/>ewa g,<br/>kordka 1,<br/>60-144 poznań,<br/>nip: 781090009,<br/>tel: ,<br/>email: loyalty@expansja.pl', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_login`
--

CREATE TABLE IF NOT EXISTS `bb__user_login` (
`ULO_ID` bigint(20) NOT NULL,
  `ULO_USR_ID` int(11) NOT NULL,
  `ULO_SESSION` varchar(60) NOT NULL,
  `ULO_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ULO_IP` varchar(15) NOT NULL,
  `ULO_INFO` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Zrzut danych tabeli `bb__user_login`
--

INSERT INTO `bb__user_login` (`ULO_ID`, `ULO_USR_ID`, `ULO_SESSION`, `ULO_DATE`, `ULO_IP`, `ULO_INFO`) VALUES
(1, 1, '6d35d35e8497205e7594fbf5fa863e1d', '2016-09-15 09:33:35', '::1', NULL),
(2, 1, 'e75562b20282cf5b6c43d9b1d8c194f0', '2016-09-15 10:02:22', '::1', NULL),
(3, 2, '9b0cf3f39d4d5c91617f60a6833bbe4c', '2016-09-15 10:02:39', '::1', NULL),
(4, 2, '861d2e28224baa6d30f2395886140fe8', '2016-09-15 11:20:46', '::1', NULL),
(5, 1, 'a13059d759b35cbd60beadb45b12e19d', '2016-09-15 11:22:04', '::1', NULL),
(6, 2, 'f969e6b9ae50efdee7461ce288cf74f4', '2016-09-15 11:22:19', '::1', NULL),
(7, 1, '174c925748701d7dd3ed9b7578e48bcd', '2016-09-15 11:33:50', '79.184.132.203', NULL),
(8, 1, '6ca8980afbb89f66d0f216b7ef0d8129', '2016-09-15 11:50:27', '79.184.132.203', NULL),
(9, 2, 'bf62541e1c4d2779424655618bba6049', '2016-09-15 12:43:27', '79.184.132.203', NULL),
(10, 1, '20518bd33cc0789bfd3cb6711867c002', '2016-09-15 13:00:31', '79.184.132.203', NULL),
(11, 1, '0dc5b1bdcb7b357c397677c8780246d6', '2016-09-15 13:00:33', '79.184.132.203', NULL),
(12, 1, '66370b04e903a0f89bbf2e3d3cc4f9a6', '2016-09-15 13:04:08', '79.184.132.203', NULL),
(13, 1, '2b89b600c53968b55dd72cd3c5837ce3', '2016-09-15 13:04:18', '79.184.132.203', NULL),
(14, 1, '286c8a88d1b8a39228fb67a8aeaa066d', '2016-09-15 13:04:36', '79.184.132.203', NULL),
(15, 1, '5e58fcc723857bed7bbfe5385d12bb4e', '2016-09-15 13:05:21', '79.184.132.203', NULL),
(16, 1, 'e1f8cfc9e0105606f0c9e5c0a02f4249', '2016-09-15 13:06:50', '79.184.132.203', NULL),
(17, 1, '5324d52aa3b0ad00149936d2e6865a6f', '2016-09-15 13:06:51', '::1', NULL),
(18, 1, 'c4c043d1cbbcab7dd9c479dd6147a71f', '2016-09-15 13:07:12', '::1', NULL),
(19, 1, 'a98c423f889004f0803a2621b569cd7d', '2016-09-15 13:07:27', '::1', NULL),
(20, 1, '52ad86b1a9015b95b6d79db541f58d65', '2016-09-15 13:09:55', '79.184.132.203', NULL),
(21, 1, 'd81a5b1ebe9bc92de82a5d141e14ed2e', '2016-09-15 13:10:57', '79.184.132.203', NULL),
(22, 1, 'd6cbce542dabcdc9fbffb557909a64b2', '2016-09-15 15:04:15', '79.184.132.203', NULL),
(23, 1, '028c701df9fae5688bf72513c6c7e51d', '2016-09-15 15:04:35', '79.184.132.203', NULL),
(24, 1, '5b164bfdb02c3bd008c15be497d702b7', '2016-09-15 15:07:26', '79.184.132.203', NULL),
(25, 1, 'fd55e42f0f8e66ae3bbff6e11e792faa', '2016-09-15 15:07:44', '79.184.132.203', NULL),
(26, 1, '73b68800b439add05b954fbe01f011e8', '2016-09-15 15:08:19', '79.184.132.203', NULL),
(27, 1, 'ee248942e738d4fd5628e8763bda7e2f', '2016-09-15 15:16:45', '79.184.132.203', NULL),
(28, 1, '909d54560e5bbfd6d3563d3d26a8c5df', '2016-09-15 15:49:03', '79.184.132.203', NULL),
(29, 1, '97e9b9abab3e88608cae59f0e9d18a34', '2016-09-16 07:20:46', '83.23.63.116', NULL),
(30, 1, '4a25d5a94fe124fcbc5ce665b41118a0', '2016-09-16 16:47:45', '37.47.187.224', NULL),
(31, 1, '3af5dc24ca3d3495a3e30f3d1fc4f3db', '2016-09-18 16:20:05', '::1', NULL),
(32, 1, 'c9dff2872eb1ceaca245ad11e3c3ba41', '2016-09-19 13:47:06', '79.186.11.50', NULL),
(33, 1, 'f31100e9a3d54693bff07959cba26ce3', '2016-09-20 09:53:55', '83.23.64.45', NULL),
(34, 1, '6985cd11a362979f54e1f08e11390190', '2016-09-20 09:54:26', '83.23.64.45', NULL),
(35, 1, '61222e040566332c2f56764b491b3262', '2016-09-22 08:40:20', '79.186.93.162', NULL),
(36, 1, '9dffd6599a69d46e90b57e68fbe9039a', '2016-09-22 11:35:06', '::1', NULL),
(37, 1, '6b6fd29c73bf0c6ce90c3d9d2860254f', '2016-09-22 14:16:03', '79.186.93.162', NULL),
(38, 1, '4d187a955e375c35186ae0c036bbb701', '2016-09-23 10:05:32', '79.184.138.144', NULL),
(39, 1, '4c2446f1f6015ea1431117df1026812f', '2016-09-23 13:21:35', '79.184.138.144', NULL),
(40, 1, '2ca16a50a3fba06cda362c98786c5586', '2016-09-23 13:55:48', '79.184.138.144', NULL),
(41, 1, '8426e4780731d9c8e407307d013a75f4', '2016-09-23 14:24:16', '79.184.138.144', NULL),
(42, 1, 'd2ef4dca82ade96e593c7355f6b3a1d1', '2016-09-23 15:09:02', '::1', NULL),
(43, 3, 'd3ea5e32ee3b8c0722a88fb87b496933', '2016-09-23 15:18:08', '::1', NULL),
(44, 3, '841e09c02e8274c744452e8aa6413e06', '2016-09-23 15:19:26', '::1', NULL),
(45, 3, '60c5f57758568c3528f6f3ca8f2a93f3', '2016-09-23 15:36:48', '79.184.138.144', NULL),
(46, 1, '54d81ec23ff506c92eb880ca20f16675', '2016-09-23 20:47:40', '::1', NULL),
(47, 1, '19fa8dbcd07afef1cef0ac26e921bb7c', '2016-09-23 21:50:36', '::1', NULL),
(48, 1, 'ab6f5e1298023155029f9b23893931d9', '2016-09-23 21:52:46', '::1', NULL),
(49, 1, '12ffcc08fca0a7c48b15d653c17784fd', '2016-09-23 21:52:51', '::1', NULL),
(50, 1, '61c2a0f474763afe2ec042f90abd1819', '2016-09-23 21:52:51', '::1', NULL),
(51, 1, '3fe433fb4af8733c009006a62b88bbf2', '2016-09-23 21:52:52', '::1', NULL),
(52, 1, '73f5cf3831174bc03d11c099d0988ee6', '2016-09-23 21:52:52', '::1', NULL),
(53, 1, 'cbaf1a1dfe9bc77d2cf81f958453c7ee', '2016-09-26 10:45:27', '79.186.83.37', NULL),
(54, 1, '78f714313f239c66420c281b2af9294c', '2016-09-26 10:45:38', '79.186.83.37', NULL),
(55, 3, '96e3f07fafe4f8d7fbddb97d43d0a982', '2016-09-26 11:38:51', '79.186.83.37', NULL),
(56, 1, 'd1d719ea91c2482d1c3ea989389277f6', '2016-09-26 11:45:44', '79.186.83.37', NULL),
(57, 1, '5ef7b95f56a66d5dd4a09a284e814ada', '2016-09-27 07:25:38', '79.184.129.236', NULL),
(58, 3, '2914e300dc0f73886b907f3866eed1ad', '2016-09-27 07:55:02', '79.184.129.236', NULL),
(59, 1, '05fedc269cef8f441a49888091a767d4', '2016-09-27 10:25:03', '79.186.6.214', NULL),
(60, 1, 'c8970b0133bf0f6bc2a2acfc41e50171', '2016-09-27 10:34:36', '::1', NULL),
(61, 1, '29179d24296d41f74b503d0c88be627c', '2016-09-27 12:15:35', '79.186.6.214', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_points`
--

CREATE TABLE IF NOT EXISTS `bb__user_points` (
`UPO_ID` bigint(20) NOT NULL,
  `UPO_USR_ID` int(11) NOT NULL,
  `UPO_USR_UID` int(11) NOT NULL,
  `UPO_IMPORT_ID` int(11) NOT NULL,
  `UPO_IMPORT_DATE` date NOT NULL,
  `UPO_PRO_ID` int(11) NOT NULL,
  `UPO_PRO_VALUE` int(11) NOT NULL,
  `UPO_PRO_COUNT` int(11) NOT NULL,
  `UPO_STATUS` enum('active','inative') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `bb__user_points`
--

INSERT INTO `bb__user_points` (`UPO_ID`, `UPO_USR_ID`, `UPO_USR_UID`, `UPO_IMPORT_ID`, `UPO_IMPORT_DATE`, `UPO_PRO_ID`, `UPO_PRO_VALUE`, `UPO_PRO_COUNT`, `UPO_STATUS`) VALUES
(1, 1, 1, 1, '2016-09-14', 1, 6000, 32, 'active'),
(2, 2, 2, 34, '2016-09-14', 1, 5690, 34, 'active'),
(3, 3, 3, 1, '2016-09-23', 3, 5000, 3, 'active');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_recalculate`
--

CREATE TABLE IF NOT EXISTS `bb__user_recalculate` (
`URE_ID` int(11) NOT NULL,
  `URE_USR_ID` int(11) NOT NULL,
  `URE_POINTS_IMPORT` int(11) NOT NULL,
  `URE_POINTS_ORDERS` int(11) NOT NULL,
  `URE_POINTS_PROMOTIONS` int(11) NOT NULL,
  `URE_POINTS` int(11) NOT NULL,
  `URE_SESSION` varchar(60) NOT NULL,
  `URE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `URE_TYPE` enum('auto','login','order') NOT NULL,
  `URE_COMMENT` varchar(250) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Zrzut danych tabeli `bb__user_recalculate`
--

INSERT INTO `bb__user_recalculate` (`URE_ID`, `URE_USR_ID`, `URE_POINTS_IMPORT`, `URE_POINTS_ORDERS`, `URE_POINTS_PROMOTIONS`, `URE_POINTS`, `URE_SESSION`, `URE_DATE`, `URE_TYPE`, `URE_COMMENT`) VALUES
(1, 1, 6000, 0, 0, 6000, '6d35d35e8497205e7594fbf5fa863e1d', '2016-09-15 09:33:35', 'login', NULL),
(2, 1, 6000, 0, 0, 6000, 'e75562b20282cf5b6c43d9b1d8c194f0', '2016-09-15 10:02:22', 'login', NULL),
(3, 2, 0, 0, 0, 0, '9b0cf3f39d4d5c91617f60a6833bbe4c', '2016-09-15 10:02:39', 'login', NULL),
(4, 2, 0, 0, 0, 0, '861d2e28224baa6d30f2395886140fe8', '2016-09-15 11:20:46', 'login', NULL),
(5, 1, 6000, 0, 0, 6000, 'a13059d759b35cbd60beadb45b12e19d', '2016-09-15 11:22:04', 'login', NULL),
(6, 2, 0, 0, 0, 0, 'f969e6b9ae50efdee7461ce288cf74f4', '2016-09-15 11:22:19', 'login', NULL),
(7, 1, 6000, 0, 0, 6000, '174c925748701d7dd3ed9b7578e48bcd', '2016-09-15 11:33:50', 'login', NULL),
(8, 1, 6000, 0, 0, 6000, '6ca8980afbb89f66d0f216b7ef0d8129', '2016-09-15 11:50:27', 'login', NULL),
(9, 1, 6000, 401, 0, 5599, '6ca8980afbb89f66d0f216b7ef0d8129', '2016-09-15 12:32:55', 'order', NULL),
(10, 2, 5690, 0, 0, 5690, 'bf62541e1c4d2779424655618bba6049', '2016-09-15 12:43:27', 'login', NULL),
(11, 2, 5690, 364, 0, 5326, 'bf62541e1c4d2779424655618bba6049', '2016-09-15 12:46:09', 'order', NULL),
(12, 1, 6000, 401, 0, 5599, '20518bd33cc0789bfd3cb6711867c002', '2016-09-15 13:00:32', 'login', NULL),
(13, 1, 6000, 401, 0, 5599, '0dc5b1bdcb7b357c397677c8780246d6', '2016-09-15 13:00:33', 'login', NULL),
(14, 1, 6000, 401, 0, 5599, '66370b04e903a0f89bbf2e3d3cc4f9a6', '2016-09-15 13:04:08', 'login', NULL),
(15, 1, 6000, 401, 0, 5599, '2b89b600c53968b55dd72cd3c5837ce3', '2016-09-15 13:04:18', 'login', NULL),
(16, 1, 6000, 401, 0, 5599, '286c8a88d1b8a39228fb67a8aeaa066d', '2016-09-15 13:04:36', 'login', NULL),
(17, 1, 6000, 401, 0, 5599, '5e58fcc723857bed7bbfe5385d12bb4e', '2016-09-15 13:05:21', 'login', NULL),
(18, 1, 6000, 401, 0, 5599, 'e1f8cfc9e0105606f0c9e5c0a02f4249', '2016-09-15 13:06:50', 'login', NULL),
(19, 1, 6000, 401, 0, 5599, '5324d52aa3b0ad00149936d2e6865a6f', '2016-09-15 13:06:51', 'login', NULL),
(20, 1, 6000, 401, 0, 5599, 'c4c043d1cbbcab7dd9c479dd6147a71f', '2016-09-15 13:07:12', 'login', NULL),
(21, 1, 6000, 401, 0, 5599, 'a98c423f889004f0803a2621b569cd7d', '2016-09-15 13:07:27', 'login', NULL),
(22, 1, 6000, 401, 0, 5599, '52ad86b1a9015b95b6d79db541f58d65', '2016-09-15 13:09:55', 'login', NULL),
(23, 1, 6000, 401, 0, 5599, 'd81a5b1ebe9bc92de82a5d141e14ed2e', '2016-09-15 13:10:57', 'login', NULL),
(24, 1, 6000, 401, 0, 5599, 'd6cbce542dabcdc9fbffb557909a64b2', '2016-09-15 15:04:15', 'login', NULL),
(25, 1, 6000, 401, 0, 5599, '028c701df9fae5688bf72513c6c7e51d', '2016-09-15 15:04:35', 'login', NULL),
(26, 1, 6000, 401, 0, 5599, '5b164bfdb02c3bd008c15be497d702b7', '2016-09-15 15:07:26', 'login', NULL),
(27, 1, 6000, 401, 0, 5599, 'fd55e42f0f8e66ae3bbff6e11e792faa', '2016-09-15 15:07:44', 'login', NULL),
(28, 1, 6000, 401, 0, 5599, '73b68800b439add05b954fbe01f011e8', '2016-09-15 15:08:19', 'login', NULL),
(29, 1, 6000, 401, 0, 5599, 'ee248942e738d4fd5628e8763bda7e2f', '2016-09-15 15:16:45', 'login', NULL),
(30, 1, 6000, 401, 0, 5599, '909d54560e5bbfd6d3563d3d26a8c5df', '2016-09-15 15:49:03', 'login', NULL),
(31, 1, 6000, 401, 0, 5599, '97e9b9abab3e88608cae59f0e9d18a34', '2016-09-16 07:20:46', 'login', NULL),
(32, 1, 6000, 401, 0, 5599, '4a25d5a94fe124fcbc5ce665b41118a0', '2016-09-16 16:47:45', 'login', NULL),
(33, 1, 6000, 401, 0, 5599, '3af5dc24ca3d3495a3e30f3d1fc4f3db', '2016-09-18 16:20:05', 'login', NULL),
(34, 1, 6000, 401, 0, 5599, 'c9dff2872eb1ceaca245ad11e3c3ba41', '2016-09-19 13:47:06', 'login', NULL),
(35, 1, 6000, 401, 0, 5599, 'f31100e9a3d54693bff07959cba26ce3', '2016-09-20 09:53:55', 'login', NULL),
(36, 1, 6000, 401, 0, 5599, '6985cd11a362979f54e1f08e11390190', '2016-09-20 09:54:26', 'login', NULL),
(37, 1, 6000, 615, 0, 5385, '6985cd11a362979f54e1f08e11390190', '2016-09-20 09:55:58', 'order', NULL),
(38, 1, 6000, 1193, 0, 4807, 'a98c423f889004f0803a2621b569cd7d', '2016-09-21 07:49:58', 'order', NULL),
(39, 1, 6000, 1193, 0, 4807, '61222e040566332c2f56764b491b3262', '2016-09-22 08:40:20', 'login', NULL),
(40, 1, 6000, 1193, 0, 4807, '9dffd6599a69d46e90b57e68fbe9039a', '2016-09-22 11:35:06', 'login', NULL),
(41, 1, 6000, 1193, 0, 4807, '6b6fd29c73bf0c6ce90c3d9d2860254f', '2016-09-22 14:16:03', 'login', NULL),
(42, 1, 6000, 1193, 0, 4807, '4d187a955e375c35186ae0c036bbb701', '2016-09-23 10:05:32', 'login', NULL),
(43, 1, 6000, 1193, 0, 4807, '4c2446f1f6015ea1431117df1026812f', '2016-09-23 13:21:35', 'login', NULL),
(44, 1, 6000, 1193, 0, 4807, '2ca16a50a3fba06cda362c98786c5586', '2016-09-23 13:55:48', 'login', NULL),
(45, 1, 6000, 1193, 0, 4807, '8426e4780731d9c8e407307d013a75f4', '2016-09-23 14:24:16', 'login', NULL),
(46, 1, 6000, 1193, 0, 4807, 'd2ef4dca82ade96e593c7355f6b3a1d1', '2016-09-23 15:09:02', 'login', NULL),
(47, 3, 0, 0, 0, 0, 'd3ea5e32ee3b8c0722a88fb87b496933', '2016-09-23 15:18:08', 'login', NULL),
(48, 3, 5000, 0, 0, 5000, '841e09c02e8274c744452e8aa6413e06', '2016-09-23 15:19:27', 'login', NULL),
(49, 3, 5000, 0, 0, 5000, '60c5f57758568c3528f6f3ca8f2a93f3', '2016-09-23 15:36:48', 'login', NULL),
(50, 1, 6000, 1193, 0, 4807, '54d81ec23ff506c92eb880ca20f16675', '2016-09-23 20:47:40', 'login', NULL),
(51, 1, 6000, 1193, 0, 4807, '19fa8dbcd07afef1cef0ac26e921bb7c', '2016-09-23 21:50:36', 'login', NULL),
(52, 1, 6000, 1193, 0, 4807, 'ab6f5e1298023155029f9b23893931d9', '2016-09-23 21:52:47', 'login', NULL),
(53, 1, 6000, 1193, 0, 4807, '12ffcc08fca0a7c48b15d653c17784fd', '2016-09-23 21:52:51', 'login', NULL),
(54, 1, 6000, 1193, 0, 4807, '61c2a0f474763afe2ec042f90abd1819', '2016-09-23 21:52:51', 'login', NULL),
(55, 1, 6000, 1193, 0, 4807, '3fe433fb4af8733c009006a62b88bbf2', '2016-09-23 21:52:52', 'login', NULL),
(56, 1, 6000, 1193, 0, 4807, '73f5cf3831174bc03d11c099d0988ee6', '2016-09-23 21:52:52', 'login', NULL),
(57, 1, 6000, 1193, 0, 4807, 'cbaf1a1dfe9bc77d2cf81f958453c7ee', '2016-09-26 10:45:27', 'login', NULL),
(58, 1, 6000, 1193, 0, 4807, '78f714313f239c66420c281b2af9294c', '2016-09-26 10:45:38', 'login', NULL),
(59, 3, 5000, 0, 0, 5000, '96e3f07fafe4f8d7fbddb97d43d0a982', '2016-09-26 11:38:51', 'login', NULL),
(60, 1, 6000, 1193, 0, 4807, 'd1d719ea91c2482d1c3ea989389277f6', '2016-09-26 11:45:44', 'login', NULL),
(61, 1, 6000, 1193, 0, 4807, '5ef7b95f56a66d5dd4a09a284e814ada', '2016-09-27 07:25:38', 'login', NULL),
(62, 3, 5000, 0, 0, 5000, '2914e300dc0f73886b907f3866eed1ad', '2016-09-27 07:55:02', 'login', NULL),
(63, 1, 6000, 1193, 0, 4807, '05fedc269cef8f441a49888091a767d4', '2016-09-27 10:25:03', 'login', NULL),
(64, 1, 6000, 1193, 0, 4807, 'c8970b0133bf0f6bc2a2acfc41e50171', '2016-09-27 10:34:36', 'login', NULL),
(65, 1, 6000, 1193, 0, 4807, '29179d24296d41f74b503d0c88be627c', '2016-09-27 12:15:35', 'login', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `bb__administrator`
--
ALTER TABLE `bb__administrator`
 ADD PRIMARY KEY (`ADM_ID`), ADD KEY `ADM_AGENCY` (`ADM_AGENCY`), ADD KEY `ADM_SEND_MAIL` (`ADM_SEND_MAIL`), ADD KEY `ADM_ALIAS` (`ADM_ALIAS`);

--
-- Indexes for table `bb__admin_dashboard`
--
ALTER TABLE `bb__admin_dashboard`
 ADD PRIMARY KEY (`ADD_ID`), ADD KEY `ADD_ADM_ID` (`ADD_ADM_ID`), ADD KEY `ADD_ACTION` (`ADD_ACTION`), ADD KEY `ADD_ORDER` (`ADD_ORDER`);

--
-- Indexes for table `bb__admin_roles`
--
ALTER TABLE `bb__admin_roles`
 ADD KEY `ADR_ADM_ID` (`ADR_ADM_ID`,`ADR_ROLE_ID`), ADD KEY `ADR_ORDER` (`ADR_ORDER`);

--
-- Indexes for table `bb__articles`
--
ALTER TABLE `bb__articles`
 ADD PRIMARY KEY (`ART_ID`), ADD KEY `ART_PARENT` (`ART_PARENT`,`ART_ALIAS`,`ART_ORDER`,`ART_STATUS`), ADD KEY `ART_LEVEL` (`ART_LEVEL`), ADD KEY `ART_ADM_ID` (`ART_ADM_ID`), ADD KEY `ART_DATE_ADD` (`ART_DATE_ADD`);

--
-- Indexes for table `bb__category`
--
ALTER TABLE `bb__category`
 ADD PRIMARY KEY (`CAT_ID`), ADD KEY `CAT_ALIAS` (`CAT_ALIAS`,`CAT_PARENT`,`CAT_STATUS`,`CAT_ORDER`), ADD KEY `CAT_COUNT` (`CAT_COUNT`);

--
-- Indexes for table `bb__dict`
--
ALTER TABLE `bb__dict`
 ADD PRIMARY KEY (`DIC_ID`);

--
-- Indexes for table `bb__import_info`
--
ALTER TABLE `bb__import_info`
 ADD PRIMARY KEY (`IIN_ID`), ADD KEY `IIN_DATE_ADD` (`IIN_DATE_ADD`,`IIN_DATE_FROM`,`IIN_DATE_TO`,`IIN_IMPORT_ID`,`IIN_STATUS`);

--
-- Indexes for table `bb__log_events`
--
ALTER TABLE `bb__log_events`
 ADD PRIMARY KEY (`LOE_ID`), ADD KEY `LOE_USER` (`LOE_USER`), ADD KEY `LOE_FROM` (`LOE_FROM`), ADD KEY `LOA_TYPE` (`LOE_TYPE`), ADD KEY `LOE_TYPE_ID` (`LOE_TYPE_ID`), ADD KEY `LOE_MODULE` (`LOE_MODULE`);

--
-- Indexes for table `bb__menus`
--
ALTER TABLE `bb__menus`
 ADD PRIMARY KEY (`MEN_ID`), ADD KEY `MEN_TYPE` (`MEN_TYPE`,`MEN_ORD`,`MEN_MODULE`,`MEN_MODULE_ID`), ADD KEY `MEN_PARENT` (`MEN_PARENT`), ADD KEY `MEN_ORDER` (`MEN_ORDER`), ADD KEY `MEN_MODULE_ALIAS` (`MEN_MODULE_ALIAS`);

--
-- Indexes for table `bb__news`
--
ALTER TABLE `bb__news`
 ADD PRIMARY KEY (`NEW_ID`), ADD KEY `NEW_ADM_ID` (`NEW_ADM_ID`), ADD KEY `NEW_ALIAS` (`NEW_ALIAS`), ADD KEY `NEW_DATE_START` (`NEW_DATE_START`), ADD KEY `NEW_DATE_END` (`NEW_DATE_END`), ADD KEY `NEW_STATUS` (`NEW_STATUS`), ADD KEY `NEW_ADM_ID_2` (`NEW_ADM_ID`);

--
-- Indexes for table `bb__orders`
--
ALTER TABLE `bb__orders`
 ADD PRIMARY KEY (`ORD_ID`), ADD KEY `ORD_USR_ID` (`ORD_USR_ID`,`ORD_STATUS`), ADD KEY `ORD_UAS_ID` (`ORD_UAS_ID`);

--
-- Indexes for table `bb__orders_status_change`
--
ALTER TABLE `bb__orders_status_change`
 ADD PRIMARY KEY (`OSC_ID`), ADD KEY `OSC_ORD_ID` (`OSC_ORD_ID`,`OSC_ADM_ID`,`OSC_DATE`,`OSC_STATUS`);

--
-- Indexes for table `bb__order_positions`
--
ALTER TABLE `bb__order_positions`
 ADD PRIMARY KEY (`OPO_ID`), ADD KEY `OPO_ORD_ID` (`OPO_ORD_ID`,`OPO_USR_ID`,`OPO_PRO_ID`,`OPO_DATE_ADD`,`OPO_STATUS`);

--
-- Indexes for table `bb__pages`
--
ALTER TABLE `bb__pages`
 ADD PRIMARY KEY (`PAG_ID`);

--
-- Indexes for table `bb__pages_meta`
--
ALTER TABLE `bb__pages_meta`
 ADD PRIMARY KEY (`PAM_ID`), ADD KEY `PAM_PAG_ID` (`PAM_PAG_ID`);

--
-- Indexes for table `bb__products`
--
ALTER TABLE `bb__products`
 ADD PRIMARY KEY (`PRO_ID`), ADD KEY `PRO_ALIAS` (`PRO_ALIAS`,`PRO_CODE`,`PRO_POINTS`,`PRO_DATE_ADD`,`PRO_STATUS`), ADD KEY `PRO_EAN` (`PRO_EAN`);

--
-- Indexes for table `bb__pro_cat`
--
ALTER TABLE `bb__pro_cat`
 ADD KEY `PRO_ID` (`PRO_ID`,`CAT_ID`);

--
-- Indexes for table `bb__pro_foto`
--
ALTER TABLE `bb__pro_foto`
 ADD PRIMARY KEY (`PFO_ID`), ADD KEY `PFO_PRO_ID` (`PFO_PRO_ID`,`PFO_ALIAS`,`PFO_TYPE`,`PFO_STATUS`);

--
-- Indexes for table `bb__text_backup`
--
ALTER TABLE `bb__text_backup`
 ADD PRIMARY KEY (`TEB_ID`), ADD KEY `TEB_ADM_ID` (`TEB_ADM_ID`,`TEB_MODULE`,`TEB_MODULE_ID`,`TEB_TYPE`,`TEB_DATE`), ADD KEY `TEB_FROM` (`TEB_FROM`);

--
-- Indexes for table `bb__users`
--
ALTER TABLE `bb__users`
 ADD PRIMARY KEY (`USR_ID`), ADD KEY `USR_ALIAS` (`USR_ALIAS`,`USR_CODE`,`USR_BB_CODE`,`USR_LOGIN`,`USR_PASS`,`USR_GROUP`,`USR_NIP`,`USR_STATUS`), ADD KEY `USR_EMAIL` (`USR_EMAIL`);

--
-- Indexes for table `bb__users_temp`
--
ALTER TABLE `bb__users_temp`
 ADD PRIMARY KEY (`UTE_ID`), ADD KEY `UTE_STATUS` (`UTE_STATUS`);

--
-- Indexes for table `bb__user_address_send`
--
ALTER TABLE `bb__user_address_send`
 ADD PRIMARY KEY (`UAS_ID`), ADD KEY `UAS_USR_ID` (`UAS_USR_ID`,`UAS_STATUS`), ADD KEY `UAS_DATE_ADD` (`UAS_DATE_ADD`), ADD KEY `UAS_STATUS` (`UAS_STATUS`);

--
-- Indexes for table `bb__user_func`
--
ALTER TABLE `bb__user_func`
 ADD PRIMARY KEY (`UFU_ID`), ADD KEY `UFU_USR_ID` (`UFU_USR_ID`,`UFU_CODE`);

--
-- Indexes for table `bb__user_login`
--
ALTER TABLE `bb__user_login`
 ADD PRIMARY KEY (`ULO_ID`), ADD KEY `ULO_USR_ID` (`ULO_USR_ID`,`ULO_SESSION`,`ULO_DATE`);

--
-- Indexes for table `bb__user_points`
--
ALTER TABLE `bb__user_points`
 ADD PRIMARY KEY (`UPO_ID`), ADD KEY `UPO_USR_ID` (`UPO_USR_ID`,`UPO_USR_UID`,`UPO_IMPORT_ID`,`UPO_IMPORT_DATE`,`UPO_PRO_ID`);

--
-- Indexes for table `bb__user_recalculate`
--
ALTER TABLE `bb__user_recalculate`
 ADD PRIMARY KEY (`URE_ID`), ADD KEY `URE_USR_ID` (`URE_USR_ID`,`URE_SESSION`,`URE_DATE`,`URE_TYPE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `bb__administrator`
--
ALTER TABLE `bb__administrator`
MODIFY `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `bb__admin_dashboard`
--
ALTER TABLE `bb__admin_dashboard`
MODIFY `ADD_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__articles`
--
ALTER TABLE `bb__articles`
MODIFY `ART_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `bb__category`
--
ALTER TABLE `bb__category`
MODIFY `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `bb__dict`
--
ALTER TABLE `bb__dict`
MODIFY `DIC_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `bb__import_info`
--
ALTER TABLE `bb__import_info`
MODIFY `IIN_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__log_events`
--
ALTER TABLE `bb__log_events`
MODIFY `LOE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT dla tabeli `bb__menus`
--
ALTER TABLE `bb__menus`
MODIFY `MEN_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__news`
--
ALTER TABLE `bb__news`
MODIFY `NEW_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `bb__orders`
--
ALTER TABLE `bb__orders`
MODIFY `ORD_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `bb__orders_status_change`
--
ALTER TABLE `bb__orders_status_change`
MODIFY `OSC_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__order_positions`
--
ALTER TABLE `bb__order_positions`
MODIFY `OPO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `bb__pages`
--
ALTER TABLE `bb__pages`
MODIFY `PAG_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__pages_meta`
--
ALTER TABLE `bb__pages_meta`
MODIFY `PAM_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__products`
--
ALTER TABLE `bb__products`
MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `bb__pro_foto`
--
ALTER TABLE `bb__pro_foto`
MODIFY `PFO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__text_backup`
--
ALTER TABLE `bb__text_backup`
MODIFY `TEB_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `bb__users`
--
ALTER TABLE `bb__users`
MODIFY `USR_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `bb__users_temp`
--
ALTER TABLE `bb__users_temp`
MODIFY `UTE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=401;
--
-- AUTO_INCREMENT dla tabeli `bb__user_address_send`
--
ALTER TABLE `bb__user_address_send`
MODIFY `UAS_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `bb__user_func`
--
ALTER TABLE `bb__user_func`
MODIFY `UFU_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `bb__user_login`
--
ALTER TABLE `bb__user_login`
MODIFY `ULO_ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT dla tabeli `bb__user_points`
--
ALTER TABLE `bb__user_points`
MODIFY `UPO_ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `bb__user_recalculate`
--
ALTER TABLE `bb__user_recalculate`
MODIFY `URE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
