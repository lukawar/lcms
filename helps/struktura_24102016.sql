-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 24 Paź 2016, 11:36
-- Wersja serwera: 5.5.27-29.0
-- Wersja PHP: 5.5.35-ogc0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `db214672`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__administrator`
--

CREATE TABLE IF NOT EXISTS `bb__administrator` (
`ADM_ID` int(11) NOT NULL,
  `ADM_ALIAS` varchar(50) NOT NULL,
  `ADM_NAME` varchar(200) NOT NULL,
  `ADM_SURNAME` varchar(200) NOT NULL,
  `ADM_LOGIN` varchar(200) NOT NULL,
  `ADM_PASS` varchar(200) NOT NULL,
  `ADM_MAIL` varchar(200) NOT NULL,
  `ADM_ROLE` enum('admin','user','sysadmin') NOT NULL DEFAULT 'user',
  `ADM_FROM` enum('bb','ex') NOT NULL DEFAULT 'bb',
  `ADM_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADM_AGENCY` varchar(2) NOT NULL,
  `ADM_SEND_MAIL` enum('nie','tak') NOT NULL DEFAULT 'nie',
  `ADM_ACTIVE` enum('active','deleted') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__admin_dashboard`
--

CREATE TABLE IF NOT EXISTS `bb__admin_dashboard` (
`ADD_ID` int(11) NOT NULL,
  `ADD_ADM_ID` int(11) NOT NULL,
  `ADD_ACTION` varchar(50) NOT NULL,
  `ADD_OPTION` varchar(50) NOT NULL,
  `ADD_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__admin_roles`
--

CREATE TABLE IF NOT EXISTS `bb__admin_roles` (
  `ADR_ADM_ID` int(11) NOT NULL,
  `ADR_NAME` varchar(50) NOT NULL,
  `ADR_ROLE_ID` varchar(30) NOT NULL,
  `ADR_ROLE_OPTION` varchar(30) NOT NULL,
  `ADR_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADR_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__articles`
--

CREATE TABLE IF NOT EXISTS `bb__articles` (
`ART_ID` int(11) NOT NULL,
  `ART_PARENT` int(11) NOT NULL DEFAULT '0',
  `ART_LEVEL` tinyint(4) NOT NULL DEFAULT '0',
  `ART_NAME` varchar(200) NOT NULL,
  `ART_ALIAS` varchar(200) NOT NULL,
  `ART_CONTENT` longtext NOT NULL,
  `ART_ORDER` int(11) NOT NULL,
  `ART_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ART_ADM_ID` int(11) NOT NULL,
  `ART_STATUS` enum('nowy','opublikowany','ukryty','deleted') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__category`
--

CREATE TABLE IF NOT EXISTS `bb__category` (
`CAT_ID` int(11) NOT NULL,
  `CAT_NAME` varchar(150) NOT NULL,
  `CAT_ALIAS` varchar(150) NOT NULL,
  `CAT_PARENT` int(11) NOT NULL DEFAULT '0',
  `CAT_COUNT` int(11) NOT NULL DEFAULT '0',
  `CAT_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active',
  `CAT_ORDER` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__chest`
--

CREATE TABLE IF NOT EXISTS `bb__chest` (
`CHE_ID` int(11) NOT NULL,
  `CHE_USR_ID` int(11) NOT NULL,
  `CHE_PRO_ID` int(11) NOT NULL,
  `CHE_PRO_NAME` varchar(100) NOT NULL,
  `CHE_PRO_ALIAS` varchar(100) NOT NULL,
  `CHE_PRO_CAT` int(11) NOT NULL,
  `CHE_PRO_PRICE` int(11) NOT NULL,
  `CHE_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CHE_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__dict`
--

CREATE TABLE IF NOT EXISTS `bb__dict` (
`DIC_ID` int(11) NOT NULL,
  `DIC_KEY` varchar(100) NOT NULL,
  `DIC_VALUE` varchar(100) NOT NULL,
  `DIC_INT` int(11) DEFAULT NULL,
  `DIC_LNG` varchar(3) NOT NULL DEFAULT 'pl',
  `DIC_DESCRIPTION` varchar(30) NOT NULL,
  `DIC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__import_info`
--

CREATE TABLE IF NOT EXISTS `bb__import_info` (
`IIN_ID` int(11) NOT NULL,
  `IIN_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IIN_DATE_FROM` date NOT NULL,
  `IIN_DATE_TO` date NOT NULL,
  `IIN_IMPORT_ID` varchar(50) NOT NULL,
  `IIN_FILE` varchar(50) DEFAULT NULL,
  `IIN_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__log_events`
--

CREATE TABLE IF NOT EXISTS `bb__log_events` (
`LOE_ID` int(11) NOT NULL,
  `LOE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOE_USER` int(11) NOT NULL,
  `LOE_EVENT` mediumtext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `LOE_FROM` enum('admin','user') NOT NULL,
  `LOE_MODULE` varchar(30) NOT NULL,
  `LOE_TYPE` varchar(20) NOT NULL,
  `LOE_TYPE_ID` int(11) DEFAULT NULL,
  `LOE_IP` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=958 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__menus`
--

CREATE TABLE IF NOT EXISTS `bb__menus` (
`MEN_ID` int(11) NOT NULL,
  `MEN_PARENT` int(11) NOT NULL,
  `MEN_TYPE` enum('left','right','top','bottom','center') COLLATE latin1_general_ci NOT NULL DEFAULT 'left',
  `MEN_NAME` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORD` int(11) NOT NULL,
  `MEN_MODULE` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_ID` int(11) NOT NULL,
  `MEN_MODULE_ALIAS` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_TITLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_HREF` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `MEN_STYLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORDER` int(11) NOT NULL,
  `MEN_STATUS` enum('aktywny','nieaktywny','usuni?ty') COLLATE latin1_general_ci NOT NULL DEFAULT 'aktywny'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__news`
--

CREATE TABLE IF NOT EXISTS `bb__news` (
`NEW_ID` int(11) NOT NULL,
  `NEW_NAME` varchar(250) NOT NULL,
  `NEW_ALIAS` varchar(250) NOT NULL,
  `NEW_DESC_SHORT` mediumtext NOT NULL,
  `NEW_DESC_LONG` mediumtext NOT NULL,
  `NEW_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NEW_DATE_DISPLAY` date NOT NULL,
  `NEW_DATE_START` date NOT NULL,
  `NEW_DATE_END` date NOT NULL,
  `NEW_BACKGROUND` varchar(100) DEFAULT NULL,
  `NEW_ADM_ID` int(11) NOT NULL,
  `NEW_STATUS` enum('nowy','aktywny','nieaktywny','usunięty') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__orders`
--

CREATE TABLE IF NOT EXISTS `bb__orders` (
`ORD_ID` int(11) NOT NULL,
  `ORD_USR_ID` int(11) NOT NULL,
  `ORD_UAS_ID` int(11) NOT NULL,
  `ORD_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ORD_POINTS` int(11) NOT NULL,
  `ORD_POSITIONS` tinyint(4) NOT NULL,
  `ORD_ADDRESS` mediumtext NOT NULL,
  `ORD_SEND_CODE` varchar(100) DEFAULT NULL,
  `ORD_SEND_PATH` varchar(255) DEFAULT NULL,
  `ORD_SEND_FIRM` varchar(100) DEFAULT NULL,
  `ORD_SEND_DATE` date DEFAULT NULL,
  `ORD_STATUS` enum('new','confirmed','sended','closed','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__orders_status`
--

CREATE TABLE IF NOT EXISTS `bb__orders_status` (
`OST_ID` int(11) NOT NULL,
  `OST_ORD_ID` int(11) NOT NULL,
  `OST_FROM` enum('ex','bb') NOT NULL,
  `OST_USR_ID` int(11) NOT NULL,
  `OST_POINTS` int(11) NOT NULL,
  `OST_POSITIONS` tinyint(4) NOT NULL,
  `OST_SEND_DATE` date DEFAULT NULL,
  `OST_SEND_PATH` varchar(200) DEFAULT NULL,
  `OST_SEND_CODE` varchar(20) DEFAULT NULL,
  `OST_SEND_FIRM` varchar(50) DEFAULT NULL,
  `OST_STATUS` enum('new','confirmed','sended','closed','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__orders_status_change`
--

CREATE TABLE IF NOT EXISTS `bb__orders_status_change` (
`OSC_ID` int(11) NOT NULL,
  `OSC_OST_ID` int(11) NOT NULL,
  `OSC_ADM_ID` int(11) DEFAULT NULL,
  `OSC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OSC_STATUS` enum('new','confirmed','sended','closed','deleted') NOT NULL DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__order_positions`
--

CREATE TABLE IF NOT EXISTS `bb__order_positions` (
`OPO_ID` int(11) NOT NULL,
  `OPO_ORD_ID` int(11) NOT NULL,
  `OPO_USR_ID` int(11) NOT NULL,
  `OPO_PRO_ID` int(11) NOT NULL,
  `OPO_PRO_ALIAS` varchar(100) NOT NULL,
  `OPO_PRO_CODE` varchar(50) DEFAULT NULL,
  `OPO_PRO_EAN` varchar(50) DEFAULT NULL,
  `OPO_PRO_NAME` varchar(250) NOT NULL,
  `OPO_CAT_ID` int(11) NOT NULL,
  `OPO_COUNT` tinyint(4) NOT NULL,
  `OPO_POINTS` int(11) NOT NULL,
  `OPO_POINTS_TRUE` int(11) NOT NULL,
  `OPO_PROMOTION` varchar(120) DEFAULT NULL,
  `OPO_FROM` enum('ex','bb') NOT NULL,
  `OPO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OPO_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pages`
--

CREATE TABLE IF NOT EXISTS `bb__pages` (
`PAG_ID` int(11) NOT NULL,
  `PAG_NAME` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pages_meta`
--

CREATE TABLE IF NOT EXISTS `bb__pages_meta` (
`PAM_ID` int(11) NOT NULL,
  `PAM_PAG_ID` int(11) NOT NULL,
  `PAM_TITLE` tinytext NOT NULL,
  `PAM_DESCRIPTION` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products`
--

CREATE TABLE IF NOT EXISTS `bb__products` (
`PRO_ID` int(11) NOT NULL,
  `PRO_NAME` varchar(100) NOT NULL,
  `PRO_NAME_CATALOG` varchar(200) NOT NULL,
  `PRO_ALIAS` varchar(100) NOT NULL,
  `PRO_CODE` varchar(50) NOT NULL,
  `PRO_EAN` varchar(20) NOT NULL,
  `PRO_DESC_LONG` longtext NOT NULL,
  `PRO_DESC_SHORT` mediumtext NOT NULL,
  `PRO_VENDOR` varchar(50) DEFAULT NULL,
  `PRO_PRICE` float(12,2) NOT NULL,
  `PRO_FOTO` varchar(250) DEFAULT NULL,
  `PRO_POINTS` int(11) NOT NULL,
  `PRO_FROM` enum('ex','bb') NOT NULL DEFAULT 'bb',
  `PRO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PRO_STATUS` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `PRO_CHECK` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=238 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products_import_prom`
--

CREATE TABLE IF NOT EXISTS `bb__products_import_prom` (
`PIP_ID` int(11) NOT NULL,
  `PIP_PRODUCT` int(11) NOT NULL,
  `PIP_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PIP_DATE_FROM` date NOT NULL,
  `PIP_DATE_TO` date NOT NULL,
  `PIP_VALUE` float(12,2) NOT NULL DEFAULT '0.00',
  `PIP_TYPE` enum('percent','plus','value','inactive') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products_news`
--

CREATE TABLE IF NOT EXISTS `bb__products_news` (
`PNE_ID` int(11) NOT NULL,
  `PNE_PRO_ID` int(11) NOT NULL,
  `PNE_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PNE_DATE_FROM` date DEFAULT NULL,
  `PNE_DATE_TO` date DEFAULT NULL,
  `PNE_STATUS` enum('all','bydate','inactive') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products_promotions`
--

CREATE TABLE IF NOT EXISTS `bb__products_promotions` (
`PPR_ID` int(11) NOT NULL,
  `PPR_PRO_ID` int(11) NOT NULL,
  `PPR_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PPR_VALUE` float(12,2) NOT NULL,
  `PPR_STATUS` enum('percent','minus','value','inactive') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__products_temp`
--

CREATE TABLE IF NOT EXISTS `bb__products_temp` (
`PRO_ID` int(11) NOT NULL,
  `PRO_NAME` varchar(100) NOT NULL,
  `PRO_NAME_CATALOG` varchar(200) NOT NULL,
  `PRO_ALIAS` varchar(100) NOT NULL,
  `PRO_CODE` varchar(50) NOT NULL,
  `PRO_EAN` varchar(20) NOT NULL,
  `PRO_DESC_LONG` longtext NOT NULL,
  `PRO_DESC_SHORT` mediumtext NOT NULL,
  `PRO_VENDOR` varchar(50) DEFAULT NULL,
  `PRO_PRICE` float(12,2) NOT NULL,
  `PRO_FOTO` varchar(250) DEFAULT NULL,
  `PRO_POINTS` int(11) NOT NULL,
  `PRO_FROM` enum('ex','bb') NOT NULL DEFAULT 'bb',
  `PRO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PRO_STATUS` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `PRO_CHECK` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=99 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pro_cat`
--

CREATE TABLE IF NOT EXISTS `bb__pro_cat` (
  `PRO_ID` int(11) NOT NULL,
  `CAT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__pro_foto`
--

CREATE TABLE IF NOT EXISTS `bb__pro_foto` (
`PFO_ID` int(11) NOT NULL,
  `PFO_PRO_ID` int(11) NOT NULL,
  `PFO_ALIAS` varchar(100) NOT NULL,
  `PFO_FILE` varchar(200) NOT NULL,
  `PFO_TYPE` enum('thumb1','thumb2','full') DEFAULT NULL,
  `PFO_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=124 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__text_backup`
--

CREATE TABLE IF NOT EXISTS `bb__text_backup` (
`TEB_ID` int(11) NOT NULL,
  `TEB_ADM_ID` int(11) NOT NULL,
  `TEB_FROM` varchar(10) NOT NULL,
  `TEB_MODULE` varchar(15) NOT NULL,
  `TEB_MODULE_ID` int(11) NOT NULL,
  `TEB_TYPE` varchar(30) NOT NULL,
  `TEB_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TEB_NAME` varchar(200) DEFAULT NULL,
  `TEB_DATA_1` longtext,
  `TEB_DATA_2` longtext,
  `TEB_DATA_3` longtext NOT NULL,
  `TEB_DATA_4` longtext,
  `TEB_DATA_5` varchar(100) DEFAULT NULL,
  `TEB_DATA_6` varchar(100) DEFAULT NULL,
  `TEB_IP` varchar(30) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=857 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__users`
--

CREATE TABLE IF NOT EXISTS `bb__users` (
`USR_ID` int(11) NOT NULL,
  `USR_ALIAS` varchar(50) NOT NULL,
  `USR_CODE` varchar(20) NOT NULL,
  `USR_BB_CODE` varchar(20) NOT NULL,
  `USR_LOGIN` varchar(50) NOT NULL,
  `USR_PASS` varchar(100) NOT NULL,
  `USR_EMAIL` varchar(70) NOT NULL,
  `USR_PHONE` varchar(20) NOT NULL,
  `USR_NAME` varchar(100) NOT NULL,
  `USR_PERSON_NAME` varchar(70) NOT NULL,
  `USR_PERSON_SURNAME` varchar(70) NOT NULL,
  `USR_POSITION` varchar(100) DEFAULT NULL,
  `USR_GROUP` int(11) NOT NULL,
  `USR_NIP` varchar(13) NOT NULL,
  `USR_STREET` varchar(80) NOT NULL,
  `USR_NUMBER` varchar(10) NOT NULL,
  `USR_POSTCODE` varchar(6) NOT NULL,
  `USR_CITY` varchar(70) NOT NULL,
  `USR_POINTS` int(11) NOT NULL,
  `USR_AGREEMENT` varchar(200) DEFAULT NULL,
  `USR_DATE_ADD` date DEFAULT NULL,
  `USR_PH` int(11) DEFAULT NULL,
  `USR_SESSION` varchar(60) NOT NULL,
  `USR_STATUS` enum('new','active','deleted') NOT NULL DEFAULT 'new',
  `USR_REPORT` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=404 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__users_temp`
--

CREATE TABLE IF NOT EXISTS `bb__users_temp` (
`UTE_ID` int(11) NOT NULL,
  `UTE_NAME` varchar(150) NOT NULL,
  `UTE_LOGIN` varchar(8) NOT NULL,
  `UTE_PASS` varchar(8) NOT NULL,
  `UTE_NIP` varchar(13) NOT NULL,
  `UTE_PH` varchar(50) NOT NULL,
  `UTE_PH_ID` int(11) NOT NULL,
  `UTE_STATUS` varchar(3) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=401 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_address_send`
--

CREATE TABLE IF NOT EXISTS `bb__user_address_send` (
`UAS_ID` int(11) NOT NULL,
  `UAS_USR_ID` int(11) NOT NULL,
  `UAS_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UAS_PERSON_NAME` varchar(250) NOT NULL,
  `UAS_PERSON_SURNAME` varchar(250) NOT NULL,
  `UAS_STREET` varchar(250) NOT NULL,
  `UAS_NUMBER` varchar(10) NOT NULL,
  `UAS_POSTCODE` varchar(6) NOT NULL,
  `UAS_CITY` varchar(250) NOT NULL,
  `UAS_PHONE` varchar(16) NOT NULL,
  `UAS_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_func`
--

CREATE TABLE IF NOT EXISTS `bb__user_func` (
`UFU_ID` int(11) NOT NULL,
  `UFU_USR_ID` int(11) NOT NULL,
  `UFU_CODE` varchar(160) NOT NULL,
  `UFU_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UFU_TYPE` varchar(100) NOT NULL,
  `UFU_DATA` tinytext NOT NULL,
  `UFU_INFO` tinytext NOT NULL,
  `UFU_RESULT` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_login`
--

CREATE TABLE IF NOT EXISTS `bb__user_login` (
`ULO_ID` bigint(20) NOT NULL,
  `ULO_USR_ID` int(11) NOT NULL,
  `ULO_SESSION` varchar(60) NOT NULL,
  `ULO_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ULO_IP` varchar(15) NOT NULL,
  `ULO_INFO` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=616 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_points`
--

CREATE TABLE IF NOT EXISTS `bb__user_points` (
`UPO_ID` bigint(20) NOT NULL,
  `UPO_USR_ID` int(11) NOT NULL,
  `UPO_USR_UID` int(11) NOT NULL,
  `UPO_USR_NIP` varchar(12) NOT NULL,
  `UPO_IMPORT_ID` int(11) NOT NULL,
  `UPO_IMPORT_DATE` date NOT NULL,
  `UPO_PRO_ID` int(11) NOT NULL,
  `UPO_IMPORT_VALUE` float(12,2) NOT NULL DEFAULT '0.00',
  `UPO_VALUE` float(12,2) NOT NULL DEFAULT '0.00',
  `UPO_PRO_COUNT` int(11) NOT NULL,
  `UPO_ORDER_DATE` date DEFAULT NULL,
  `UPO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPO_STATUS` enum('active','calculated','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8517 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_points_promotions`
--

CREATE TABLE IF NOT EXISTS `bb__user_points_promotions` (
`UPP_ID` int(11) NOT NULL,
  `UPP_USR_ID` int(11) NOT NULL,
  `UPP_POINTS` int(11) NOT NULL,
  `UPP_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPP_UPI_ID` int(11) NOT NULL,
  `UPP_COMMENT` varchar(250) DEFAULT NULL,
  `UPP_STATUS` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bb__user_recalculate`
--

CREATE TABLE IF NOT EXISTS `bb__user_recalculate` (
`URE_ID` int(11) NOT NULL,
  `URE_USR_ID` int(11) NOT NULL,
  `URE_POINTS_IMPORT` int(11) NOT NULL,
  `URE_POINTS_ORDERS` int(11) NOT NULL,
  `URE_POINTS_PROMOTIONS` int(11) NOT NULL,
  `URE_POINTS` int(11) NOT NULL,
  `URE_SESSION` varchar(60) NOT NULL,
  `URE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `URE_TYPE` enum('auto','login','order') NOT NULL,
  `URE_COMMENT` varchar(250) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=680 ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `bb__administrator`
--
ALTER TABLE `bb__administrator`
 ADD PRIMARY KEY (`ADM_ID`), ADD KEY `ADM_AGENCY` (`ADM_AGENCY`), ADD KEY `ADM_SEND_MAIL` (`ADM_SEND_MAIL`), ADD KEY `ADM_ALIAS` (`ADM_ALIAS`), ADD KEY `ADM_FROM` (`ADM_FROM`);

--
-- Indexes for table `bb__admin_dashboard`
--
ALTER TABLE `bb__admin_dashboard`
 ADD PRIMARY KEY (`ADD_ID`), ADD KEY `ADD_ADM_ID` (`ADD_ADM_ID`), ADD KEY `ADD_ACTION` (`ADD_ACTION`), ADD KEY `ADD_ORDER` (`ADD_ORDER`);

--
-- Indexes for table `bb__admin_roles`
--
ALTER TABLE `bb__admin_roles`
 ADD KEY `ADR_ADM_ID` (`ADR_ADM_ID`,`ADR_ROLE_ID`), ADD KEY `ADR_ORDER` (`ADR_ORDER`);

--
-- Indexes for table `bb__articles`
--
ALTER TABLE `bb__articles`
 ADD PRIMARY KEY (`ART_ID`), ADD KEY `ART_PARENT` (`ART_PARENT`,`ART_ALIAS`,`ART_ORDER`,`ART_STATUS`), ADD KEY `ART_LEVEL` (`ART_LEVEL`), ADD KEY `ART_ADM_ID` (`ART_ADM_ID`), ADD KEY `ART_DATE_ADD` (`ART_DATE_ADD`);

--
-- Indexes for table `bb__category`
--
ALTER TABLE `bb__category`
 ADD PRIMARY KEY (`CAT_ID`), ADD KEY `CAT_ALIAS` (`CAT_ALIAS`,`CAT_PARENT`,`CAT_STATUS`,`CAT_ORDER`), ADD KEY `CAT_COUNT` (`CAT_COUNT`);

--
-- Indexes for table `bb__chest`
--
ALTER TABLE `bb__chest`
 ADD PRIMARY KEY (`CHE_ID`), ADD KEY `CHE_USR_ID` (`CHE_USR_ID`,`CHE_PRO_ID`,`CHE_PRO_CAT`,`CHE_DATE_ADD`,`CHE_STATUS`);

--
-- Indexes for table `bb__dict`
--
ALTER TABLE `bb__dict`
 ADD PRIMARY KEY (`DIC_ID`);

--
-- Indexes for table `bb__import_info`
--
ALTER TABLE `bb__import_info`
 ADD PRIMARY KEY (`IIN_ID`), ADD KEY `IIN_DATE_ADD` (`IIN_DATE_ADD`,`IIN_DATE_FROM`,`IIN_DATE_TO`,`IIN_IMPORT_ID`,`IIN_STATUS`);

--
-- Indexes for table `bb__log_events`
--
ALTER TABLE `bb__log_events`
 ADD PRIMARY KEY (`LOE_ID`), ADD KEY `LOE_USER` (`LOE_USER`), ADD KEY `LOE_FROM` (`LOE_FROM`), ADD KEY `LOA_TYPE` (`LOE_TYPE`), ADD KEY `LOE_TYPE_ID` (`LOE_TYPE_ID`), ADD KEY `LOE_MODULE` (`LOE_MODULE`);

--
-- Indexes for table `bb__menus`
--
ALTER TABLE `bb__menus`
 ADD PRIMARY KEY (`MEN_ID`), ADD KEY `MEN_TYPE` (`MEN_TYPE`,`MEN_ORD`,`MEN_MODULE`,`MEN_MODULE_ID`), ADD KEY `MEN_PARENT` (`MEN_PARENT`), ADD KEY `MEN_ORDER` (`MEN_ORDER`), ADD KEY `MEN_MODULE_ALIAS` (`MEN_MODULE_ALIAS`);

--
-- Indexes for table `bb__news`
--
ALTER TABLE `bb__news`
 ADD PRIMARY KEY (`NEW_ID`), ADD KEY `NEW_ADM_ID` (`NEW_ADM_ID`), ADD KEY `NEW_ALIAS` (`NEW_ALIAS`), ADD KEY `NEW_DATE_START` (`NEW_DATE_START`), ADD KEY `NEW_DATE_END` (`NEW_DATE_END`), ADD KEY `NEW_STATUS` (`NEW_STATUS`), ADD KEY `NEW_ADM_ID_2` (`NEW_ADM_ID`);

--
-- Indexes for table `bb__orders`
--
ALTER TABLE `bb__orders`
 ADD PRIMARY KEY (`ORD_ID`), ADD KEY `ORD_USR_ID` (`ORD_USR_ID`,`ORD_STATUS`), ADD KEY `ORD_UAS_ID` (`ORD_UAS_ID`), ADD KEY `ORD_SEND_DATE` (`ORD_SEND_DATE`);

--
-- Indexes for table `bb__orders_status`
--
ALTER TABLE `bb__orders_status`
 ADD PRIMARY KEY (`OST_ID`), ADD KEY `OST_ORD_ID` (`OST_ORD_ID`,`OST_FROM`,`OST_USR_ID`,`OST_STATUS`);

--
-- Indexes for table `bb__orders_status_change`
--
ALTER TABLE `bb__orders_status_change`
 ADD PRIMARY KEY (`OSC_ID`), ADD KEY `OSC_ORD_ID` (`OSC_OST_ID`,`OSC_ADM_ID`,`OSC_DATE`,`OSC_STATUS`);

--
-- Indexes for table `bb__order_positions`
--
ALTER TABLE `bb__order_positions`
 ADD PRIMARY KEY (`OPO_ID`), ADD KEY `OPO_ORD_ID` (`OPO_ORD_ID`,`OPO_USR_ID`,`OPO_PRO_ID`,`OPO_DATE_ADD`,`OPO_STATUS`), ADD KEY `OPO_PRO_CODE` (`OPO_PRO_CODE`,`OPO_PRO_EAN`), ADD KEY `OPO_FROM` (`OPO_FROM`);

--
-- Indexes for table `bb__pages`
--
ALTER TABLE `bb__pages`
 ADD PRIMARY KEY (`PAG_ID`);

--
-- Indexes for table `bb__pages_meta`
--
ALTER TABLE `bb__pages_meta`
 ADD PRIMARY KEY (`PAM_ID`), ADD KEY `PAM_PAG_ID` (`PAM_PAG_ID`);

--
-- Indexes for table `bb__products`
--
ALTER TABLE `bb__products`
 ADD PRIMARY KEY (`PRO_ID`), ADD KEY `PRO_ALIAS` (`PRO_ALIAS`,`PRO_CODE`,`PRO_POINTS`,`PRO_DATE_ADD`,`PRO_STATUS`), ADD KEY `PRO_EAN` (`PRO_EAN`), ADD KEY `PRO_FROM` (`PRO_FROM`), ADD KEY `PRO_CHECK` (`PRO_CHECK`);

--
-- Indexes for table `bb__products_import_prom`
--
ALTER TABLE `bb__products_import_prom`
 ADD PRIMARY KEY (`PIP_ID`), ADD KEY `PIP_PRODUCT` (`PIP_PRODUCT`,`PIP_DATE_FROM`,`PIP_DATE_TO`,`PIP_VALUE`);

--
-- Indexes for table `bb__products_news`
--
ALTER TABLE `bb__products_news`
 ADD PRIMARY KEY (`PNE_ID`), ADD KEY `PNE_PRO_ID` (`PNE_PRO_ID`,`PNE_DATE_ADD`,`PNE_DATE_FROM`,`PNE_DATE_TO`), ADD KEY `PNE_STATUS` (`PNE_STATUS`);

--
-- Indexes for table `bb__products_promotions`
--
ALTER TABLE `bb__products_promotions`
 ADD PRIMARY KEY (`PPR_ID`), ADD KEY `PPR_PRO_ID` (`PPR_PRO_ID`,`PPR_DATE_ADD`,`PPR_STATUS`);

--
-- Indexes for table `bb__products_temp`
--
ALTER TABLE `bb__products_temp`
 ADD PRIMARY KEY (`PRO_ID`), ADD KEY `PRO_ALIAS` (`PRO_ALIAS`,`PRO_CODE`,`PRO_POINTS`,`PRO_DATE_ADD`,`PRO_STATUS`), ADD KEY `PRO_EAN` (`PRO_EAN`), ADD KEY `PRO_FROM` (`PRO_FROM`), ADD KEY `PRO_CHECK` (`PRO_CHECK`);

--
-- Indexes for table `bb__pro_cat`
--
ALTER TABLE `bb__pro_cat`
 ADD KEY `PRO_ID` (`PRO_ID`,`CAT_ID`);

--
-- Indexes for table `bb__pro_foto`
--
ALTER TABLE `bb__pro_foto`
 ADD PRIMARY KEY (`PFO_ID`), ADD KEY `PFO_PRO_ID` (`PFO_PRO_ID`,`PFO_ALIAS`,`PFO_TYPE`,`PFO_STATUS`);

--
-- Indexes for table `bb__text_backup`
--
ALTER TABLE `bb__text_backup`
 ADD PRIMARY KEY (`TEB_ID`), ADD KEY `TEB_ADM_ID` (`TEB_ADM_ID`,`TEB_MODULE`,`TEB_MODULE_ID`,`TEB_TYPE`,`TEB_DATE`), ADD KEY `TEB_FROM` (`TEB_FROM`);

--
-- Indexes for table `bb__users`
--
ALTER TABLE `bb__users`
 ADD PRIMARY KEY (`USR_ID`), ADD KEY `USR_ALIAS` (`USR_ALIAS`,`USR_CODE`,`USR_BB_CODE`,`USR_LOGIN`,`USR_PASS`,`USR_GROUP`,`USR_NIP`,`USR_STATUS`), ADD KEY `USR_EMAIL` (`USR_EMAIL`), ADD KEY `USR_PH` (`USR_PH`), ADD KEY `USR_REPORT` (`USR_REPORT`), ADD KEY `USR_SESSION` (`USR_SESSION`);

--
-- Indexes for table `bb__users_temp`
--
ALTER TABLE `bb__users_temp`
 ADD PRIMARY KEY (`UTE_ID`), ADD KEY `UTE_STATUS` (`UTE_STATUS`), ADD KEY `UTE_PH_ID` (`UTE_PH_ID`);

--
-- Indexes for table `bb__user_address_send`
--
ALTER TABLE `bb__user_address_send`
 ADD PRIMARY KEY (`UAS_ID`), ADD KEY `UAS_USR_ID` (`UAS_USR_ID`,`UAS_STATUS`), ADD KEY `UAS_DATE_ADD` (`UAS_DATE_ADD`), ADD KEY `UAS_STATUS` (`UAS_STATUS`);

--
-- Indexes for table `bb__user_func`
--
ALTER TABLE `bb__user_func`
 ADD PRIMARY KEY (`UFU_ID`), ADD KEY `UFU_USR_ID` (`UFU_USR_ID`,`UFU_CODE`);

--
-- Indexes for table `bb__user_login`
--
ALTER TABLE `bb__user_login`
 ADD PRIMARY KEY (`ULO_ID`), ADD KEY `ULO_USR_ID` (`ULO_USR_ID`,`ULO_SESSION`,`ULO_DATE`);

--
-- Indexes for table `bb__user_points`
--
ALTER TABLE `bb__user_points`
 ADD PRIMARY KEY (`UPO_ID`), ADD KEY `UPO_USR_ID` (`UPO_USR_ID`,`UPO_USR_UID`,`UPO_IMPORT_ID`,`UPO_IMPORT_DATE`,`UPO_PRO_ID`), ADD KEY `UPO_IMPORT_VALUE` (`UPO_IMPORT_VALUE`), ADD KEY `UPO_USR_NIP` (`UPO_USR_NIP`), ADD KEY `UPO_ORDER_DATE` (`UPO_ORDER_DATE`), ADD KEY `UPO_DATE_ADD` (`UPO_DATE_ADD`);

--
-- Indexes for table `bb__user_points_promotions`
--
ALTER TABLE `bb__user_points_promotions`
 ADD PRIMARY KEY (`UPP_ID`), ADD KEY `UPP_USR_ID` (`UPP_USR_ID`,`UPP_DATE_ADD`,`UPP_UPI_ID`,`UPP_STATUS`);

--
-- Indexes for table `bb__user_recalculate`
--
ALTER TABLE `bb__user_recalculate`
 ADD PRIMARY KEY (`URE_ID`), ADD KEY `URE_USR_ID` (`URE_USR_ID`,`URE_SESSION`,`URE_DATE`,`URE_TYPE`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `bb__administrator`
--
ALTER TABLE `bb__administrator`
MODIFY `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `bb__admin_dashboard`
--
ALTER TABLE `bb__admin_dashboard`
MODIFY `ADD_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `bb__articles`
--
ALTER TABLE `bb__articles`
MODIFY `ART_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `bb__category`
--
ALTER TABLE `bb__category`
MODIFY `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `bb__chest`
--
ALTER TABLE `bb__chest`
MODIFY `CHE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `bb__dict`
--
ALTER TABLE `bb__dict`
MODIFY `DIC_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `bb__import_info`
--
ALTER TABLE `bb__import_info`
MODIFY `IIN_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `bb__log_events`
--
ALTER TABLE `bb__log_events`
MODIFY `LOE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=958;
--
-- AUTO_INCREMENT dla tabeli `bb__menus`
--
ALTER TABLE `bb__menus`
MODIFY `MEN_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__news`
--
ALTER TABLE `bb__news`
MODIFY `NEW_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `bb__orders`
--
ALTER TABLE `bb__orders`
MODIFY `ORD_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `bb__orders_status`
--
ALTER TABLE `bb__orders_status`
MODIFY `OST_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `bb__orders_status_change`
--
ALTER TABLE `bb__orders_status_change`
MODIFY `OSC_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__order_positions`
--
ALTER TABLE `bb__order_positions`
MODIFY `OPO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT dla tabeli `bb__pages`
--
ALTER TABLE `bb__pages`
MODIFY `PAG_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__pages_meta`
--
ALTER TABLE `bb__pages_meta`
MODIFY `PAM_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__products`
--
ALTER TABLE `bb__products`
MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=238;
--
-- AUTO_INCREMENT dla tabeli `bb__products_import_prom`
--
ALTER TABLE `bb__products_import_prom`
MODIFY `PIP_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `bb__products_news`
--
ALTER TABLE `bb__products_news`
MODIFY `PNE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `bb__products_promotions`
--
ALTER TABLE `bb__products_promotions`
MODIFY `PPR_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `bb__products_temp`
--
ALTER TABLE `bb__products_temp`
MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT dla tabeli `bb__pro_foto`
--
ALTER TABLE `bb__pro_foto`
MODIFY `PFO_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT dla tabeli `bb__text_backup`
--
ALTER TABLE `bb__text_backup`
MODIFY `TEB_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=857;
--
-- AUTO_INCREMENT dla tabeli `bb__users`
--
ALTER TABLE `bb__users`
MODIFY `USR_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT dla tabeli `bb__users_temp`
--
ALTER TABLE `bb__users_temp`
MODIFY `UTE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=401;
--
-- AUTO_INCREMENT dla tabeli `bb__user_address_send`
--
ALTER TABLE `bb__user_address_send`
MODIFY `UAS_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `bb__user_func`
--
ALTER TABLE `bb__user_func`
MODIFY `UFU_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT dla tabeli `bb__user_login`
--
ALTER TABLE `bb__user_login`
MODIFY `ULO_ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=616;
--
-- AUTO_INCREMENT dla tabeli `bb__user_points`
--
ALTER TABLE `bb__user_points`
MODIFY `UPO_ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8517;
--
-- AUTO_INCREMENT dla tabeli `bb__user_points_promotions`
--
ALTER TABLE `bb__user_points_promotions`
MODIFY `UPP_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `bb__user_recalculate`
--
ALTER TABLE `bb__user_recalculate`
MODIFY `URE_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=680;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
