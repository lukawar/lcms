<?php
class cart extends View {
	public $path = 'modules/cart/templates/';
	public function showModule() {
		return true;
	}
	
	public function check() {
		$list = array(
			'__CONTENT' => $this->cartPositions(true)
		);
		return Template::parse($list, file_get_contents($this->data->localPath.$this->path.'positions.tpl.php'));
	}
	
	public function cartPositions($buttons=false) {
		$line = null;
	
		if(isset($this->var->cart)) {
			$cart = (object)$this->var->cart;
			if(isset($cart->products) and count($cart->products)>0) {
				$table = new table('dataTable', 'table table-striped stripe hover');
				$i = 1;
				foreach($cart->products as $product) {
					$deleteButton = '<button type="button" class="btn btn-danger btn-sm cartPosDel" product="'.$product['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> '.$this->lang->delete_button.'</button>';
					
					$table->addCell($i.'.', 'lp');
					$table->addCell("<a href='".helper::href(array('produkty','view', $product['cat'], $product['alias']))."'>".$product['name']."</a>");
					
					if($buttons)
						$table->addCell('<input type="number" class="cart-count" value="'.$product['count'].'" min="1" product="'.$product['id'].'"> szt. ');
					else $table->addCell($product['count'].' szt.');
					
					$table->addCell((int)$product['price']*(int)$product['count']." pkt. +".$product['count']." zł ");
					
					if($buttons)
						$table->addCell($deleteButton);
					$table->addRow();
					$i++;
				}
				
				$table->addCell(null);
				$table->addCell('<p align="right">'.$this->lang->cart_list_all.'</p>', 'bold');
				$table->addCell($this->var->cart['countAll']." szt. ", 'bold');
				$table->addCell($this->var->cart['valueAll']." pkt. +".$this->var->cart['countAll']." zł ", 'bold');
				//$table->addCell($this->var->cart['countAll']." zł ", 'bold');
				$table->addCell(null);
				$table->addRow('trBordered');
				
				$line = $table->createBody();
				
				if($buttons) {
					$orderButton = '<div class="pull-right"><button type="button" class="btn btn-success buttonUrl continue" url="'.helper::href(array('category', 'view', $this->var->last_category)).'">'.$this->lang->return_to_category.'</button> <button type="button" class="btn btn-primary" id="calculateCartButton">'.$this->lang->cart_calculate.'</button> <button type="button" class="btn btn-success buttonUrl" url="'.helper::href(array('order')).'">'.$this->lang->order_go_to.'</button> </div>';
					$line.=$orderButton;
				}
			} else $line = $this->lang->cart_is_empty;
		} else $line = $this->lang->cart_is_empty;
		return $line;
	}
	
	public function productList() {
		
	}
	
	public function updateList() {
		return "lista w koszyku";
	}
	
	public function add() {
		$cart_temp = array();
		$count = floor((int)$this->var->count);
		
		if($count<=0)
			return json_encode(array('result' => false, 'responseText' => $this->lang->count_too_little));
		
		if(isset($this->var->cart)) {
			$cart = (object)$this->var->cart;
			if(isset($cart->products) and is_array($cart->products)) {
				foreach($cart->products as $pro_temp)
					$cart_temp[$pro_temp['id']] = $pro_temp['count'];
					
				//check if exist
				if(array_key_exists($this->var->id, $cart_temp))
					$cart_temp[$this->var->id] = $cart_temp[$this->var->id] + $count;
				else $cart_temp[$this->var->id] = $count;
			} else {
				//add new positions to temporary cart
				$cart_temp[$this->var->id] = $count;
			} 
		} else {
			//add new positions to temporary cart
			$cart_temp[$this->var->id] = $count;
		}
		
		$calculatedCart = $this->calculateCart($cart_temp);
		if($calculatedCart['valueAll']<=(int)$this->var->user_points_all) {
			$_SESSION['cart'] = $calculatedCart;
			$_SESSION['user_points'] = $this->var->user_points = (int)$this->var->user_points_all - (int)$calculatedCart['valueAll'];
			return json_encode(array('result' => true, 'responseText' => $this->lang->cart_add, 'valueAll'=>(int)$calculatedCart['valueAll'], 'countAll'=>(int)$calculatedCart['countAll'], 'pointsAll'=>(int)$this->var->user_points));
		} else {
			return json_encode(array('result' => false, 'responseText' => $this->lang->too_little));
		}
	}
	
	//delete position from cart list
	public function delete() {
		$cart_temp = array();
		
		if(isset($this->var->cart)) {
			$cart = (object)$this->var->cart;
			if(isset($cart->products) and is_array($cart->products)) {
				foreach($cart->products as $pro_temp)
					$cart_temp[$pro_temp['id']] = $pro_temp['count'];
					
				//delete from temporary cart
				unset($cart_temp[$this->var->id]);
			} else {
				//nothing
			} 
		} else {
			//nothing
		}
		
		$calculatedCart = $this->calculateCart($cart_temp);
		if($calculatedCart['valueAll']<=$this->var->user_points_all) {
			$_SESSION['cart'] = $this->var->cart = $calculatedCart;
			$_SESSION['user_points'] = $this->var->user_points = (int)$this->var->user_points_all - (int)$calculatedCart['valueAll'];
			return json_encode(array('response'=>true, 'responseText' => $this->lang->cart_delete, 'cartContent' => $this->cartPositions(true), 'valueAll'=>(int)$calculatedCart['valueAll'], 'countAll'=>(int)$calculatedCart['countAll'], 'pointsAll'=>(int)$this->var->user_points));
		} else {
			return json_encode(array('response'=>false, 'responseText' => $this->lang->too_little));
		}
	}
	
	//change count in cart position
	public function change() {
		$cart_temp = array();
		
		if(isset($this->var->cart)) {
			$cart = (object)$this->var->cart;
			if(isset($cart->products) and is_array($cart->products)) {
				foreach($cart->products as $pro_temp)
					$cart_temp[$pro_temp['id']] = floor((int)$pro_temp['count']);
					
				//change count
				$pCount = floor((int)$this->var->pCount);
				if($pCount<=0)
					$pCount = 1;
				
				$cart_temp[$this->var->id] = $pCount;
			} else {
				//nothing
			} 
		} else {
			//nothing
		}
		
		$calculatedCart = $this->calculateCart($cart_temp);
		if($calculatedCart['valueAll']<=$this->var->user_points_all) {
			$_SESSION['cart'] = $this->var->cart = $calculatedCart;
			$_SESSION['user_points'] = $this->var->user_points = (int)$this->var->user_points_all - (int)$calculatedCart['valueAll'];
			return json_encode(array('response'=>true, 'responseText' => $this->lang->cart_change, 'cartContent' => $this->cartPositions(true), 'valueAll'=>(int)$calculatedCart['valueAll'], 'countAll'=>(int)$calculatedCart['countAll'], 'pointsAll'=>(int)$this->var->user_points));
		} else {
			return json_encode(array('response'=>false, 'responseText' => $this->lang->too_little, 'cartContent' => $this->cartPositions(true)));
		}
	}
	
	public function recalculateOrder() {
		$cart_temp = array();
		
		if(isset($this->var->cart)) {
			$cart = (object)$this->var->cart;
			if(isset($cart->products) and is_array($cart->products)) {
				foreach($cart->products as $pro_temp)
					$cart_temp[$pro_temp['id']] = floor((int)$pro_temp['count']);
			}
		}
		$calculatedCart = $this->calculateCart($cart_temp);
		if($calculatedCart['valueAll']<=$this->var->user_points_all) 
			return true;
		else return false;
	}
	
	private function calculateCart($cart_temp) {
		if(is_array($cart_temp) and count($cart_temp)>0) {
			$products_id = array();
			foreach($cart_temp as $id=>$count) 
				$products_id[] = $id;
			$products = implode(',',$products_id);
			$this->db->queryString('select PRO_ID, CAT_ID, PRO_NAME, PRO_ALIAS, PRO_POINTS,PPR_STATUS,PPR_VALUE, PRO_FROM from '.__BP.'products');
			$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
			$this->db->where('and', 'PRO_ID in ('.$products.')');
			$this->db->where('and', 'PRO_STATUS=1');
			//$this->db->prepareQuery();die($this->db->query());
			$pro_data = $this->db->getDataFetch();
			
			//reset data
			$countAll = 0;
			$valueAll = 0;
			
			$cart_calculated = array();
			$cart_calculated_products = array();
			
			$oneFrom = true;
			$oneFromTemp = null;
			
			foreach($pro_data as $pro) {
				
				$price = (int)$pro['PRO_POINTS'];
				$promotion = null;
				//promotions
				if($pro['PPR_STATUS']!=null) {
					switch ($pro['PPR_STATUS']) {
				    case 'percent':$price = round($pro['PRO_POINTS'] - (($pro['PRO_POINTS']*$pro['PPR_VALUE'])/100));break;
				    case 'minus':$price = round($pro['PRO_POINTS'] - $pro['PPR_VALUE']);break;
				    case 'value':$price = round($pro['PPR_VALUE']);break;
					}
					$promotion = $pro['PPR_VALUE'].' '.$pro['PPR_STATUS'];
				}
				
				//check for 2 types from
				if(is_null($oneFromTemp)) {
					$oneFromTemp = $pro['PRO_FROM'];
				} else {
					if($oneFromTemp==$pro['PRO_FROM'])
						$oneFromTemp = $pro['PRO_FROM'];
					else $oneFrom = false;
				}
				
				$cart_calkulated[] = array(
					'id' => $pro['PRO_ID'],
					'name' => $pro['PRO_NAME'],
					'alias' => $pro['PRO_ALIAS'],
					'cat' => $pro['CAT_ID'],
					'price' => (int)$price,
					'promotion' => $promotion,
					'price_true' => (int)$pro['PRO_POINTS'],
					'count' => floor((int)$cart_temp[$pro['PRO_ID']]),
					'from' => $pro['PRO_FROM'],
				);
				$countAll = $countAll + (int)$cart_temp[$pro['PRO_ID']]; 													//suma wszystkich produktów w koszyku
				$valueAll = $valueAll + ((int)$cart_temp[$pro['PRO_ID']] * (int)$price);		//wartość koszyka
			}
			
			$cart_calculated = array(
				'oneFrom' => $oneFrom,
				'valueAll' => $valueAll,
				'countAll' => $countAll,
				'products' => $cart_calkulated
			);
		} else {
			$cart_calculated = array(
				'valueAll' => 0,
				'countAll' => 0,
			);
		}
		
		//var_dump($cart_calculated);
		return $cart_calculated;
	}
}