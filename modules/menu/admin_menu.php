<?php
class menu extends View {
	public $path = 'modules/menu/templates/';
	public $menuPath = 'files/menu/';
	//public $placeHolder = array('top', 'bottom', 'left', 'right', 'center');
	public $placeHolder = array('homepage', 'bottom','top');
	public $placeHolderTemplate = array(
		'top'=>"<li class='%s'><a href='%s'>%s</a>%s</li>",
		'bottom'=>"<li class='%s'><a href='%s'>%s</a>%s</li>",
		'homepage'=>"<div class='rightMenu' class='%s'><a href='%s'>%s</a>%s</div>",
	);
	public $placeEmptyTemplate = "<div class='topMenu emptyMenu'>%s";

	//return the list of menus with all data
	public function showModule() {
		$this->db->queryString('select * from '.__BP.'menus');
		$this->db->where('and', 'MEN_PARENT=0');
		$this->db->orderBy('MEN_ORDER, MEN_TYPE');
		$menus = $this->db->getDataFetch();
		
		$i = 1;
		if($menus) {
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Nazwa', 'Położenie','Moduł','Kolejność','','');
			$table->createHeader($tableHeader);
			
			foreach($menus as $menu) {
				if($i>1) 
					$menuup = '<a href="?action=menu&option=menuup&id='.$menu['MEN_ID'].'&ord='.$menu['MEN_ORDER'].'"><img src="templates/backend/default/img/au.png"></a>';
				else $menuup = '&nbsp;&nbsp;&nbsp;';
				if($i<$menu_data->getCount)
					$menudown = '<a href="?action=menu&option=menudown&id='.$menu['MEN_ID'].'&ord='.$menu['MEN_ORDER'].'"><img src="templates/backend/default/img/adown.png"></a>';
				else $menudown = '';
				
				$table->addCell($i, 'lp');
				$table->addLink('', $menu['MEN_NAME'], 'fancybox fancybox.ajax');
				$table->addLink('', $menu['MEN_TYPE'], 'fancybox fancybox.ajax');
				$table->addLink('', $menu['MEN_MODULE'], 'fancybox fancybox.ajax');
				$table->addCell($menuup.' '.$menudown);
				$table->addLink('?action=menu&option=addChild&id='.$menu['MEN_ID'], 'dodaj&nbsp;podstronę');
				$table->addLink('?action=menu&option=editMenu&id='.$menu['MEN_ID'].'&menu_module='.$menu['MEN_MODULE'].'&menu_option='.$menu['MEN_MODULE_ALIAS'], 'edycja');
				$table->addRow();
				
				//add child rows
				//NOPE
				/*
				$this->db->queryString("select * from ".__BP."menus where MEN_PARENT=".$menu['MEN_ID']." order by MEN_ORDER, MEN_TYPE");
				$menus_ch = $this->db->getDataFetch();
				$i_ch = 1;
				if($menus_ch!=null) {
					foreach($menus_ch as $m_ch) {
						$table->addCell('');
						$table->addLink('', $i_ch.". ".$m_ch['MEN_NAME'], 'fancybox fancybox.ajax');
						$table->addLink('', $m_ch['MEN_TYPE'], 'fancybox fancybox.ajax');
						$table->addLink('', $m_ch['MEN_MODULE'], 'fancybox fancybox.ajax');
						//$table->addCell($menuup.' '.$menudown);
						$table->addCell('');
						$table->addCell('');
						$table->addLink('?action=menu&option=editMenu&id='.$m_ch['MEN_ID'].'&menu_module='.$m_ch['MEN_MODULE'].'&menu_option='.$m_ch['MEN_MODULE_ALIAS'], 'edycja');
						$table->addRow();
						$i_ch++;	
					}	
				}*/
				
				$i++;
			}
			$tableBody = $table->createBody();
		} else $tableBody = $this->noData();
		
		$list = array(
			'__GLYPHICON' => 'list',
			'__HEAD' => 'MENU',
			'__CONTENT'=> $tableBody
		);
		
		$page = new Page;
		$page->setButton('<a href="'.helper::href(array('menu','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj pozycję</a>');
		$page->setToolbar();
		return $page->setPage($list);
	}
	
	//create window for adding the new one
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('menu', 'addSave', 'Wypełnij dane');
			$form->addText('Nazwa', 'MEN_NAME', 'required');
			$form->addElement($this->createMenuSelect());
			$form->addElement($this->getMenuList());
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Nowa pozycja w menu',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'list',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=menu" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
		
		/*$list = array(
			'__MENU_TYPE'	=>$this->createMenuSelect(),
			'__MENU_LIST'	=>$this->getMenuList(),
		);
		
		return Template::parse($list, file_get_contents($this->path.'addMenu.tpl.php'));*/
	}
	
	private function createMenuSelect($position=null) {
		$line = null;
		foreach($this->placeHolder as $ph) {
			if($position==$ph)
				$selected = " selected='selected'";
			else $selected = '';
			
			$line.= "<option value='".$ph."'".$selected.">".$ph."</option>";
		}
		return '<div class="form-group labeled"><label for="MEN_TYPE" class="col-sm-3 control-label">Położenie </label><div class="col-sm-9"><select name="MEN_TYPE" id="MEN_TYPE" class="form-control">'.$line."</select></div></div>";
	}
	
	private function getMenuList() {
		$handle = opendir('modules');
		$catalogs = '';
		$result = null;
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		
		foreach($catalogs as $module) {	
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if($menu['menu'][0]==true) {
					//$module = new Module('admin_');
					$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		    	$result.= "<h4>".$module_info['title']."</h4>";
					//$result.= $module->load($menu['action'][0],'getForMenu');
					$result.= $model->load($menu['action'][0],'getForMenu');
		    }
		  }
		}
		
		if($this->var->menu_module=='homepage')
		  $selected_h = " checked";
		if($this->var->menu_module=='none')
		  $selected_n = " checked";
		//else $selected = '';
		//echo $this->var->menu_module.' - '.$selected_h.' - '.$selected_n;
		$result.= "<br/><input type='radio' name='menu' value='homepage:none:1:none' $selected_h> - <strong>strona główna</strong><br/><input type='radio' name='menu' value='none:none:1:none' $selected_n> - <strong>brak modułu</strong>";
		
		return '<div class="form-group labeled"><label for="menu" class="col-sm-3 control-label">Menu </label><div class="col-sm-9">'.$result.'</div></div>';
	}
	
	public function saveMenu() {
		$this->db->queryString("select max(MEN_ORDER) as max_order from ".__BP."menus");
		$max = $this->db->getRow();
		
		$menu = explode(':', $this->var->menu);
		
		if($menu[1]=='none') {
			$href = '?action='.$menu[0];
			$menu[1] = null;
		} else $href = '?action='.$menu[0].'&option='.$menu[1];
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_NAME_EN'=>$this->var->MEN_NAME_EN,
			'MEN_NAME_FR'=>$this->var->MEN_NAME_FR,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_STYLE'=>$menu[4],
			'MEN_HREF'=>$href,
			'MEN_ORDER'=>(int)$max['max_order'] + 1,
			
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = 'none';
			
		/*if($menu[0]=='gallery')
			$insert_data['MEN_MODULE_ALIAS'] = 'gallery';*/
		
		$this->db->queryString(__BP.'menus');
		$this->db->insertQuery($insert_data);
		//$menu_data->query();
		$this->createMenu();
		header('location: ?action=menu');
	}
	
	private function createMenu() {
		foreach($this->placeHolder as $ph){
			$this->setMenu($ph);
		}
	}
	
	public function editMenu() {
		$this->db->queryString("select MEN_TYPE, MEN_NAME, MEN_NAME_EN, MEN_NAME_FR from ".__BP."menus where MEN_ID=".$this->var->id);
		$menu = $this->db->getRow();
		
		$list = array(
			'__MENU_TYPE'	=>$this->createMenuSelect($menu['MEN_TYPE']),
			'__MENU_LIST'	=>$this->getMenuList(),
			'__MEN_NAME'=>$menu['MEN_NAME'],
			'__MEN_EN_NAME'=>$menu['MEN_NAME_EN'],
			'__MEN_FR_NAME'=>$menu['MEN_NAME_FR'],
			'__MEN_ID'=>$this->var->id
		);
		
		return Template::parse($list, file_get_contents($this->path.'editMenu.tpl.php'));
	}
	
	public function saveEditMenu() {
		$menu = explode(':', $this->var->menu);
		
		if($menu[1]=='none') {
			$href = '?action='.$menu[0];
			$menu[1] = null;
		} else $href = '?action='.$menu[0].'&option='.$menu[1];
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_NAME_EN'=>$this->var->MEN_NAME_EN,
			'MEN_NAME_FR'=>$this->var->MEN_NAME_FR,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_STYLE'=>$menu[4],
			'MEN_HREF'=>$href,
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = null;
			
		/*if($menu[0]=='gallery')
			$insert_data['MEN_MODULE_ALIAS'] = 'gallery';*/
		
		$this->db->queryString(__BP.'menus');
		$this->db->updateQuery($insert_data,'MEN_ID',$this->var->MEN_ID);
		//$menu_data->query();
		$this->createMenu();
		header('location: ?action=menu');
	}
	
	public function addChild() {
		$this->db->queryString("select MEN_TYPE from ".__BP."menus where MEN_ID=".$this->var->id);
		$menu_type = $this->db->getRow();
		
		$list = array(
			'__MENU_TYPE'	=>$menu_type['MEN_TYPE'],
			'__MENU_LIST'	=>$this->getMenuList(),
			'__MENU_PARENT' =>$this->var->id, 
		);
		
		return Template::parse($list, file_get_contents($this->path.'addMenuChild.tpl.php'));		
	}
	
	public function saveMenuChild() {
		$this->db->queryString("select max(MEN_ORDER) as max_order from ".__BP."menus where MEN_PARENT=".$this->var->MEN_PARENT);
		$max = $this->db->getRow();
		
		$menu = explode(':', $this->var->menu);
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_HREF'=>'?action='.$menu[0].'&option='.$menu[1],
			'MEN_ORDER'=>(int)$max['max_order'] + 1,
			'MEN_PARENT'=>$this->var->MEN_PARENT,
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = 'none';
		
		$this->db->queryString(__BP.'menus');
		$this->db->insertQuery($insert_data);
		//$menu_data->query();
		header('location: ?action=menu');
	}
	
	public function menuup() {
		$this->db->queryString("SELECT MEN_ORDER as order_max, MEN_ID as order_id FROM ".__BP."menus WHERE MEN_ORDER<".$this->var->ord." order by MEN_ORDER DESC");
		$max = $this->db->getRow();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$this->var->ord." where MEN_ID=".$max['order_id']);
		$this->db->execQuery();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$max['order_max']." where MEN_ID=".$this->var->id);
		$this->db->execQuery();
		
		header('location: admin.php?action=menu');
	}
	
	public function menudown() {
		$this->db->queryString("SELECT MEN_ORDER as order_max, MEN_ID FROM ".__BP."menus WHERE MEN_ORDER>".$this->var->ord." order by MEN_ORDER ASC");
		$this->db->getRow();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$max['order_max']." where MEN_ID=".$this->var->id);
		$this->db->execQuery();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$this->var->ord." where MEN_ID=".$max['MEN_ID']);
		$this->db->execQuery();
		
		header('location: admin.php?action=menu');
	}
	
	public function generateMenuTop() {
		$this->setMenu('top');
	}
	
	public function setMenu($type) {
  	$helper = new helper();
		$this->db->queryString('select * from '.__BP.'menus where MEN_TYPE="'.$type.'" and MEN_STATUS=1 order by MEN_ORDER');
		$menus = $this->db->getDataFetch();
		$class = null; 
		$id = 1;
		foreach($menus as $menu) {
			$class = 'module_'.$id;
			if($menu['MEN_MODULE']=='produkty') {
				$pline = file_get_contents('files/products/menu/categories.php');
				$pline_en = file_get_contents('files/products/menu/categories_en.php');
				$pline_fr = file_get_contents('files/products/menu/categories_fr.php');
				$class = 'cat-module';
			} else {
				$pline = null;
				$pline_en = null;
				$pline_fr = null;
			}
			
				
			$line.=sprintf($this->placeHolderTemplate[$type], $class, helper::href(array($menu['MEN_MODULE'], $menu['MEN_MODULE_ALIAS'])), $menu['MEN_NAME'], $pline);	
			$line_en.=sprintf($this->placeHolderTemplate[$type], $class, helper::href(array($menu['MEN_MODULE'], $menu['MEN_MODULE_ALIAS'])), $menu['MEN_NAME_EN'], $pline_en);	
			$line_fr.=sprintf($this->placeHolderTemplate[$type], $class, helper::href(array($menu['MEN_MODULE'], $menu['MEN_MODULE_ALIAS'])), $menu['MEN_NAME_FR'], $pline_fr);	
		
			$id++;	
		}
		
		file_put_contents($this->menuPath.'menu_'.$type.'.php', $line);		
		file_put_contents($this->menuPath.'menu_'.$type.'_en.php', $line_en);		
		file_put_contents($this->menuPath.'menu_'.$type.'_fr.php', $line_fr);
	}
	
}

function menu($option=null) {
	$menu = new menu;
	if($option!=null and method_exists($menu, $option))
		return $menu->$option();
	else 
		return $menu->showModule();
}