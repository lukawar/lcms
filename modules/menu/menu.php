<?php
class AMenu extends View {
	public $path = 'modules/menu/templates/';
	public $menuPath = 'files/menu/';
	public $placeHolder = array('top', 'right');
	public $placeHolderTemplate = array(
		'top'=>"<li><a href='%s'>%s</a></li>",
		'right'=>"<div class='rightMenu'><a href='%s'>%s</a>",
	);
	public $placeEmptyTemplate = "<div class='topMenu emptyMenu'>%s";
  
  public function getTop_() {
    $helper = new helper();
    if($this->var->s_lang=='en')
    	$menu = '<li><a href="'.helper::href(array('woda_z_jury')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_JURA').'</a></li>
		<li><a href="'.helper::href(array('produkty')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_PRODUKTY').'</a></li>
		<li><a href="'.helper::href(array('ofirmie')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_OFIRMIE').'</a></li>
		<li><a href="'.helper::href(array('csr')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_CSR').'</a></li>
		<li><a href="'.helper::href(array('kontakt')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_KONTAKT').'</a></li>
		<li><a href="'.helper::href(array('media')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_MEDIA').'</a></li>';		
		else 
			$menu = '<li><a href="'.helper::href(array('csr')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_CSR').'</a></li>
		<li><a href="'.helper::href(array('produkty')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_PRODUKTY').'</a></li>
		<li><a href="'.helper::href(array('woda_z_jury')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_JURA').'</a></li>
		
		<li>&nbsp;</li>
		<li><a href="'.helper::href(array('ofirmie')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_OFIRMIE').'</a></li>	
		<li><a href="'.helper::href(array('kariera')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_KARIERA').'</a></li>	
		<li><a href="'.helper::href(array('news')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_NEWS').'</a></li>	
		<li><a href="'.helper::href(array('kontakt')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_KONTAKT').'</a></li>
		<li><a href="'.helper::href(array('media')).'">'.$helper->langText($this->var->s_lang, '__LANG_NAV_MEDIA').'</a></li>';
		return str_ireplace('__MENU', $menu, $this->getNav());
  }
  
  public function getTop() {
  	//return $this->getMenu('top');
  	return $this->getMenu('top');
	}
	
	public function getBottom() {
  	//return $this->getMenu('bottom');
  	//return $this->getMenu('bottom');
	}
	
	public function getMenu($type) {
		//die($this->menuPath.'menu_'.$type.$this->var->lang_sufix.'.php');
		return file_get_contents($this->menuPath.'menu_'.$type.$this->var->lang_sufix.'.php');
	}
	
	/*public function getMenu($type) {
  	$helper = new helper();
		$this->db->queryString('select * from '.__BP.'menus where MEN_TYPE="'.$type.'" and MEN_STATUS=1 order by MEN_ORDER');
		$menus = $this->db->getDataFetch();
		
		foreach($menus as $menu) {
			$line.=sprintf($this->placeHolderTemplate[$type],helper::href(array($menu['MEN_MODULE'], $menu['MEN_MODULE_ALIAS'])),$menu['MEN_NAME']);
		}
		
		return $line;
	}
	
	public function getList($option) {
		if(in_array($option,$this->placeHolder)) {
			$this->db->queryString("select * from ".__BP."menus where MEN_TYPE='".$option."' and MEN_STATUS=1 and MEN_PARENT=0 order by MEN_ORDER");
			$menu_d = $this->db->getDataFetch();
			$m_c = $this->db->getCount();
			$element_width = 100/$m_c;
			
			if(array_key_exists($option, $this->placeHolderTemplate)) {
				foreach($menu_d as $md) {
					if($md['MEN_STYLE']=='text' or $md['MEN_STYLE']=='gallery')
						$line.= sprintf($this->placeHolderTemplate[$option], $element_width.'%', $md['MEN_MODULE_ALIAS'], $md['MEN_HREF'], $md['MEN_NAME']);
					elseif($md['MEN_STYLE']=='none')
						$line.= sprintf($this->placeEmptyTemplate, $md['MEN_NAME']);
					elseif($md['MEN_STYLE']=='box') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= sprintf($result, '', $result);
					}
					
					//add child menu
					$this->db->queryString("select * from ".__BP."menus where MEN_TYPE='".$option."' and MEN_STATUS=1 and MEN_PARENT=".$md['MEN_ID']." order by MEN_ORDER");
					$menu_ch = $this->db->getDataFetch();
					
					if($menu_ch!=null) {
						$line.="<div class='menu_ch'>";
						foreach($menu_ch as $m_ch) {
							$line.="<div class='m_element'><a href='".$m_ch['MEN_HREF']."'>".$m_ch['MEN_NAME']."</a></div>";
						}
						
						$line.="</div>";
					}
					$line.="</div>";
				}
				return $line;
			} else {
				foreach($menu_d as $md) {
					if($md['MEN_STYLE']=='text')
						$line.= "<li><a href='".$md['MEN_HREF']."'>".$md['MEN_NAME']."</a></li>";
					elseif($md['MEN_STYLE']=='box') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= "<li>$result</li>";
					}
					elseif($md['MEN_STYLE']=='banner') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= "<li>$result</li>";
					}
				}
				return "<ul class='$option'>".$line."</ul>";
			}
		} else return $this->moduleError('menu', $option);
	}*/
	
	public function getClass() {
		if($this->var->action==gallery and $this->var->option) {
			$this->db->queryString("select GCA_FILE from ".__BP."gallery_categories where GCA_ALIAS='".$this->var->option."'");
			$class_temp = $this->db->getRow();
			$class = $class_temp['GCA_FILE'];
			if($class!=null and $class!='')
				return $class;
			else return "back1";
		} else return "back1";
	}
}

function menu($option=null) {
	$menu = new AMenu;
	if($option!=null and method_exists($menu, $option))
		return $menu->$option();
	else 
		return $menu->getList();
}
?>