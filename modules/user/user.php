<?php
class user extends View implements IModule {
	public $path = 'modules/user/templates/';
	
	public function showModule() {
		return $this->panel();
	}
	
	public function checkLogin() {
		if(!isset($this->var->s_u_login)) {
			//$this->var->s_u_login = new StdClass;
			//$this->var->s_u_password = new StdClass;
		}
		
		if(isset($this->var->u_login) and isset($this->var->u_password) and $this->var->loginForm=='login') {
			$_SESSION['s_u_login'] = $this->var->s_u_login = $this->var->u_login;
			$_SESSION['s_u_password'] = $this->var->s_u_password = coder::hashString($this->var->u_password);
		} //else return false;
		
		//get user data
		if(isset($this->var->s_u_login)) {
			$this->db->queryString('select * from '.__BP.'users');
			$this->db->where('and', 'USR_LOGIN="'.$this->var->s_u_login.'"');
			$this->db->where('and', 'USR_PASS="'.$this->var->s_u_password.'"');
			$this->db->where('and', 'USR_STATUS in (1,2)');
			$user = $this->db->getRow();
		} else $user = null;
	
		if($user) {
			//set usr data
			if(isset($this->var->loginForm) and $this->var->loginForm=='login') {
				//TODO: set points calculation function from orders and promotions
				$_SESSION['user_session'] = $this->var->user_session = md5($user['USR_ID'].microtime());
				$_SESSION['user_id'] = $this->var->user_id = $user['USR_ID'];
				$_SESSION['user_status'] = $this->var->user_status = $user['USR_STATUS'];
				$_SESSION['user_name'] = $this->var->user_name = $user['USR_NAME'];
				$_SESSION['user_alias'] = $this->var->user_alias = $user['USR_ALIAS'];
				$_SESSION['user_code'] = $this->var->user_code = $user['USR_CODE'];
				$_SESSION['user_bb_code'] = $this->var->user_bb_code = $user['USR_BB_CODE'];
				$_SESSION['user_email'] = $this->var->user_email = $user['USR_EMAIL'];
				$_SESSION['user_person_full_name'] = $this->var->user_person_name = $user['USR_PERSON_NAME'].'&nbsp;'.$user['USR_PERSON_SURNAME'];
				$_SESSION['user_person_name'] = $this->var->user_person_name = $user['USR_PERSON_NAME'];
				$_SESSION['last_category']	= $this->var->last_category = 'homepage';
				
				//add login info 
				$this->db->queryString(__BP.'user_login');
				$login = array(
					'ULO_USR_ID' => $user['USR_ID'],
					'ULO_SESSION' => $this->var->user_session,
					'ULO_IP' => clientIP::get(),
				);
				$this->db->insertQuery($login);
				
				//recalculate user data
				$this->recalculateUserPoints($user['USR_ID'], 'login');
			}
			$checkLogin['logged'] = 'true';		
			return $checkLogin;
		} else {
			$checkLogin['logged'] = 'false';
			$checkLogin['info'] = 'Błędne dane logowania';
			$checkLogin['container'] = '#loginInfo';
			//TODO: add bad login save info
			return $checkLogin;
		}

	}
	
	//set user box in nav
	public function usrBox() {
		$box = file_get_contents($this->data->localPath.$this->path.'usrBox.tpl.php');
		$list = array(
			'__USR_NAME' => $this->var->user_person_name,
			'__USR_POINTS' => $this->var->user_points,
			'__PANEL_LINK' => helper::href(array('user','panel')),
			'__CART_LINK' => helper::href(array('cart','check')),
			'__CART_DATA' => $this->getCartData(),
			'__LOGOUT_LINK' => 'logout.php',
		);
		return Template::parse($list, $box);
	}
	
	public function panel() {
		$this->db->queryString('select * from '.__BP.'users');
		$this->db->where('and', 'USR_STATUS in (1,2)');
		$this->db->where('and', 'USR_ID='.$this->var->user_id);
		$this->user_data = (object)$this->db->getRow();
		
		$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		
		$panel = file_get_contents($this->data->localPath.$this->path.'panel.tpl.php');
		$list = array(
			'__USER_DATA' => $this->getUserData(),
			'__USER_INFO' => $this->getUserInfo(),
			'__USER_ORDERS' => $model->load('order', 'getUserOrders'),
			'__USER_CHEST' => $model->load('chest', 'getUserChest')
		);
		return Template::parse($list, $panel);
	}
		
	private function getUserData() {
		//user address&data
		$form = new Form('user', 'userDataSave', 'Edycja danych');
		$form->addElement('<div class="col-sm-12"><p><input type="checkbox" id="registr_regulation" required="required" name="USR_REGULATIONS"> Oświadczam, że zapoznałem się z <a href="articles-regulamin.html"><em>regulaminem</em> i akceptuję wszystkie zawarte w nim warunki.</a></p></div>');
		$form->addElement('<h5>DANE FIRMOWE</h5><div class="row spacer"></div>');
		$form->addText('Nazwa*', 'USR_NAME', 'required', $this->user_data->USR_NAME);
		$form->addText('Imię*', 'USR_PERSON_NAME', 'required', $this->user_data->USR_PERSON_NAME);
		$form->addText('Nazwisko*', 'USR_PERSON_SURNAME', 'required', $this->user_data->USR_PERSON_SURNAME);
		$form->addText('Stanowisko*', 'USR_POSITION', 'required', $this->user_data->USR_POSITION);
		$form->addText('NIP*', 'USR_NIP', 'required', $this->user_data->USR_NIP);

		$form->addText('Ulica*', 'USR_STREET', 'required', $this->user_data->USR_STREET);
		$form->addText('Numer*', 'USR_NUMBER', 'required', $this->user_data->USR_NUMBER);
		$form->addText('Kod pocztowy*', 'USR_POSTCODE', 'required', $this->user_data->USR_POSTCODE);
		$form->addText('Miejscowość*', 'USR_CITY', 'required', $this->user_data->USR_CITY);
		
		$form->addText('E-mail*', 'USR_EMAIL', 'required', $this->user_data->USR_EMAIL);
		$form->addText('Telefon', 'USR_PHONE', null, $this->user_data->USR_PHONE);
		$form->addElement('<div class="row">');
		$form->addHidden('saveData', 'userData');
		//$form->addHidden('statusData', $this->user_data->USR_STATUS);
		if ($_SESSION['user_status'] == 'new') {
		$form->addElement('<div class="col-sm-12"><hr>Oświadczam, że:</div>');

		$form->addElement(Template::parseFile(array('__PATH'=>$this->data->localPath.$this->path), $this->data->localPath.$this->path.'agreementJs.tpl.php'));
		/************/

		//Sekcja 1
		$form->addElement('<div class="col-sm-12 base-agreement" id="reg_self_cont"><hr><p><span class="reg-statements"><input type="checkbox" required="required" id="reg_self" name="agr1"> 1 </span>jestem osobą fizyczną prowadzącą we własnym imieniu indywidualną działalność gospodarczą i we własnym imieniu, w charakterze przedsiębiorcy, dokonującą rejestracji w programie „Bea Bonus”;</p></div>');

		$form->addElement('<div class="col-sm-12 sub-agreement" id=""><h5>Podmiot podlegający rejestracji</h5></div>');

		$form->addElement('<div class="col-sm-12 sub-agreement" id="reg_self_fiz_cont"><p><span class="reg-statements"><input type="checkbox" id="reg_self_fiz" name="agr11"> 1.1</span>osoba fizyczna prowadząca we własnym imieniu indywidualną działalność gospodarczą </p></div>');
		//import agreementA


		$form->addElement('<div class="col-sm-12 sub-agreement" id="reg_self_org_cont"><p><span class="reg-statements"><input type="checkbox" id="reg_self_org" name="agr12"> 1.2 </span>jednostka organizacyjna (spółka cywilna, spółka jawna, spółka komandytowa, spółka komandytowo-akcyjna, spółka z ograniczoną odpowiedzialnością, spółka akcyjna)</p></div>');
		//import agreementB

		/************/
		// Sekcja 2
		$form->addElement('<div class="col-sm-12 base-agreement" id="reg_other_subj_cont"><hr><p><span class="reg-statements"><input type="checkbox" id="reg_other_subj"> 2 </span>jestem osobą fizyczną dokonującą rejestracji innego podmiotu w programie „Bea Bonus” i że jestem uprawniony/a do samodzielnego reprezentowania tego podmiotu</p><hr></div>');
		//import agreementC

		$form->addElement('<div class="col-sm-12" id="agreements_container"></div>');
		}	

		$form->addSubmit('Zapisz dane');

		$form->addElement('<div class="col-sm-12"><hr><span class="smallInfo">'.$this->lang->position_is_required.'</span><p>&nbsp;</p><p>&nbsp;</p></div>');		
		$form->addElement('</div>');

		$page = new Page;
		$list = array(
			'__HEAD' => 'Aktualizacja danych osobowych',
			'__CONTENT' => $form->setForm(),
		);
		
		return $page->setPage($list, 'pageForm.tpl.php');	
	}
	
	public function getUserInfo() {
		$line = "<p>Nazwa: <strong>".$this->user_data->USR_NAME."</strong></p>";
		$line.= "<p>W programie od: <strong>".$this->user_data->USR_DATE_ADD."</strong></p>";
		$line.= "<p>Liczba punktów: <strong>".$this->user_data->USR_POINTS."</strong></p>";
		

		//user login data
		$formLogin = new Form('user', 'userLoginSave', 'Edycja danych');
		$formLogin->addElement('<div class="col-sm-12"><hr><p>&nbsp;</p><p>&nbsp;</p></div>');
		$formLogin->addElement('<h5>DANE LOGOWANIA</h5><div class="row spacer"></div>');
		$formLogin->addText('Login*', 'USR_LOGIN', 'required', $this->user_data->USR_LOGIN);
		$formLogin->addInput('Hasło*', 'USR_PASS1', null, 'password.tpl.php');
		$formLogin->addInput('Hasło ponownie*', 'USR_PASS2', null, 'password.tpl.php');
		$formLogin->addHidden('saveData', 'userData');
		$formLogin->addSubmit('Zapisz dane');
		$formLogin->addElement('<div class="col-sm-12"><hr><span class="smallInfo">'.$this->lang->position_is_required.'</span><p>&nbsp;</p><p>&nbsp;</p></div>');
		
		//froms for send address
		$formsSend = null;
		$this->db->queryString('select * from '.__BP.'user_address_send');
		$this->db->where('and', 'UAS_USR_ID='.$this->var->user_id);
		$this->db->where('and', 'UAS_STATUS=1');
		$userSend = $this->db->getDataFetch();
		
		if($userSend){
			foreach($userSend as $us) {
				$formSend = new Form('user', 'userFormSendSave', 'Edycja danych');
				$formSend->addElement('<h5>DANE DO WYSYŁKI</h5><div class="row spacer"></div>');
				$formSend->addText('Imię*', 'UAS_PERSON_NAME', 'required', $us['UAS_PERSON_NAME']);
				$formSend->addText('Nazwisko*', 'UAS_PERSON_SURNAME', 'required', $us['UAS_PERSON_SURNAME']);
				$formSend->addText('Ulica*', 'UAS_STREET', 'required', $us['UAS_STREET']);
				$formSend->addText('Numer*', 'UAS_NUMBER', 'required', $us['UAS_NUMBER']);
				$formSend->addText('Kod pocztowy*', 'UAS_POSTCODE', 'required', $us['UAS_POSTCODE']);
				$formSend->addText('Miejscowość*', 'UAS_CITY', 'required', $us['UAS_CITY']);
				$formSend->addText('Telefon', 'UAS_PHONE', null, $us['UAS_PHONE']);
				$formSend->addHidden('UAS_ID', $us['UAS_ID']);
				$formSend->addSubmit('Zapisz dane', null, '<button type="button" class="btn btn-danger buttonUrlConfirm" url="index.php?action=user&option=addressDelete&id='.$us['UAS_ID'].'">USUŃ ADRES</button>');
				$formSend->addElement('<div class="col-sm-12"><hr><span class="smallInfo">'.$this->lang->position_is_required.'</span><p>&nbsp;</p><p>&nbsp;</p></div>');
				$formsSend.= $formSend->setForm();
			}
		}
		
		$page = new Page;
		$setButton = '<div class="col-sm-12"><button type="button" class="btn btn-success buttonUrl" url="'.helper::href(array('user', 'addAddress')).'"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> DODAJ NOWY ADRES</button></div>';
		$list = array(
			'__HEAD' => 'Dane dodatkowe',
			'__CONTENT' => $line.$formLogin->setForm().$formsSend.$setButton,
		);
		
		return $page->setPage($list, 'pageForm.tpl.php');
		
	}
	
	//delete send address
	public function addressDelete() {
		if(isset($this->var->id)) {
			$this->db->queryString('update '.__BP.'user_address_send set UAS_STATUS=2 where UAS_USR_ID='.$this->var->user_id.' and UAS_ID='.$this->var->id);
			$this->db->execQuery();
		}
		header('location: '.helper::href(array('user', 'panel')));
	}
	
	public function userDataSave() {

		if(isset($this->var->USR_NAME) and isset($this->var->saveData) and isset($this->var->USR_EMAIL)) {
			//save user data

			$user = array(
				'USR_ALIAS' => coder::stripPL($this->var->USR_NAME),
				//'USR_CODE' =>  unknown at this moment
				//'USR_BB_CODE'
				'USR_EMAIL' => $this->var->USR_EMAIL,
				'USR_PHONE' => $this->var->USR_PHONE,
				'USR_NAME' => $this->var->USR_NAME,
				'USR_PERSON_NAME' => $this->var->USR_PERSON_NAME,
				'USR_PERSON_SURNAME' => $this->var->USR_PERSON_SURNAME,
				'USR_POSITION' => $this->var->USR_POSITION,
				//'USR_GROUP' => $this->var->USR_GROUP,
				'USR_NIP' => coder::nip($this->var->USR_NIP),
				'USR_STREET' => $this->var->USR_STREET,
				'USR_NUMBER' => $this->var->USR_NUMBER,
				'USR_POSTCODE' => $this->var->USR_POSTCODE,
				'USR_CITY' => $this->var->USR_CITY,
				'USR_STATUS' => 2,
				'USR_DATE_ADD'=>date('Y-m-d h:i:s')
			);

			if ($_SESSION['user_status'] == 'new') {
				$agreements = array (
					'0:'.$this->var->USR_REGULATIONS,
					'1:'.$this->var->agr1,
					'11:'.$this->var->agr11,
					'111:'.$this->var->agr111,
					'112:'.$this->var->agr112,
					'113:'.$this->var->agr113,
					'114:'.$this->var->agr114,
					'115:'.$this->var->agr115,
					'12:'.$this->var->agr12,
					'121:'.$this->var->agr121,
					'122:'.$this->var->agr122,
					'123:'.$this->var->agr123,
					'124:'.$this->var->agr124,
					'21:'.$this->var->agr21
					);	
				$user['USR_AGREEMENT'] = implode(',', $agreements);

			}


			// var_dump(($_SESSION['user_status'] == 'new') ? 'USR_AGREEMENT' => implode(',', $agreements) : null);
				$_SESSION['user_status'] = 'active';
			
			$this->db->queryString(__BP.'users');
			$this->db->updateQuery($user, 'USR_ID', $this->var->user_id);			
			
			//change session data
			$_SESSION['user_name'] = $this->var->user_name = $this->var->USR_NAME;
			$_SESSION['user_email'] = $this->var->user_email = $this->var->USR_EMAIL;
			$_SESSION['user_person_full_name'] = $this->var->user_person_name = $this->var->USR_PERSON_NAME.'&nbsp;'.$this->var->USR_PERSON_SURNAME;
			$_SESSION['user_person_name'] = $this->var->user_person_name = $this->var->USR_PERSON_NAME;
			
			//save info to user_func and generate link to activate
			$code = coder::hashString(microtime());
			$userData = $this->var->USR_NAME.'<br/>'.$this->var->USR_PERSON_NAME.' '.$this->var->USR_PERSON_SURNAME.' '.$this->var->USR_POSITION.',<br/>'.$this->var->USR_STREET.' '.$this->var->USR_NUMBER.',<br/>'.$this->var->USR_POSTCODE.' '.$this->var->USR_CITY.',<br/>nip: '.coder::nip($this->var->USR_NIP).',<br/>tel: '.$this->var->USR_PHONE.',<br/>email: '.$this->var->USR_EMAIL;
			$ufuInsert = array(
				'UFU_USR_ID' => $this->var->user_id,
				'UFU_CODE' => $code,
				'UFU_TYPE' => 'user data change',
				'UFU_DATA' => $userData,
			);
			
			$this->db->queryString(__BP.'user_func');
			$this->db->insertQuery($ufuInsert);
			
			if($this->var->user_status=='new') {
				//send address
				$this->db->queryString('select UAS_ID from '.__BP.'user_address_send');
				$this->db->where('and', 'UAS_USR_ID='.$this->var->user_id);
				$this->db->where('and', 'UAS_STATUS=1');
				$address = $this->db->getRow();
				
				if(!$address) {
					//generate new
					$insert_address = array(
						'UAS_USR_ID' => $this->var->user_id,
						'UAS_PERSON_NAME' => $this->var->USR_PERSON_NAME,
						'UAS_PERSON_SURNAME' => $this->var->USR_PERSON_SURNAME,
						'UAS_POSITION' => $this->var->USR_POSITION,
						'UAS_STREET' => $this->var->USR_STREET,
						'UAS_NUMBER' => $this->var->USR_NUMBER,
						'UAS_POSTCODE' => $this->var->USR_POSTCODE,
						'UAS_CITY' => $this->var->USR_CITY,
						'UAS_PHONE' => $this->var->USR_PHONE
					);
					$this->db->queryString(__BP.'user_address_send');
					$this->db->insertQuery($insert_address);
				}
				
				//send request for activate account
				$list = array(
					'__LINK'	=> SC_ROOT_URL.helper::href(array('user','activate', $code)),
					'__USER_DATA' => $userData,
				);
				$text = Template::parseFile($list, $this->data->localPath.$this->path.'mailActivateAccount.tpl.php');
				$mailer = new Mailer;
				$mailer->subject = 'BeaBonus - potwierdź swoje konto w systemie';
				$mailer->mailText = $text;
				$mailer->sendMail($this->var->USR_EMAIL, $this->var->USR_PERSON_NAME.' '.$this->var->USR_PERSON_SURNAME);
				
				if($this->var->user_status=='new') {
					header('location: '.helper::href(array('user', 'send')));
				} else header('location: '.helper::href(array('user', 'panel')));
			}
		}
	}
	
	public function userFormSendSave() {
		if(isset($this->var->UAS_ID) and $this->var->UAS_ID!=null) {
			//null
		}
		header('location: '.helper::href(array('user', 'panel')));
	}
	
	private function getCartData() {
		if(isset($this->var->cart)) {
			$cart =  (object)$this->var->cart;
			if(isset($cart->products) and count($cart->products)>0)
				return $cart->countAll.' szt. / '.$cart->valueAll.' pkt.';
			else return $this->lang->cart_is_empty;
		} else return $this->lang->cart_is_empty;
	}
	
	public function formSendAddress($asUser=null) {
		$form = new Form('user', 'saveSendAddress', 'Dodaj adres wysyłki');
		$form->addText('Imię', 'UAS_PERSON_NAME', 'required');
		$form->addText('Nazwisko', 'UAS_PERSON_SURNAME', 'required');
		$form->addText('Telefon', 'UAS_PHONE', 'required');
		$form->addText('Ulica', 'UAS_STREET', 'required');
		$form->addText('Numer', 'UAS_NUMBER', 'required');
		$form->addText('Kod pocztowy', 'UAS_POSTCODE');
		$form->addText('Miejscowość', 'UAS_CITY');
		$form->addHidden('saveData', 'save');
		if($asUser!=null)
			$form->addHidden('asUser', $asUser);
		$form->addSubmit('Zapisz dane');
		
		return $form->setForm();
	}
	
	public function addAddress() {
		$page = new Page;
		$list = array(
			'__HEAD' => 'Dodaj nowy adres wysyłki',
			'__CONTENT' => $this->formSendAddress('panel'),
		);
		
		return $page->setPage($list, 'pageForm.tpl.php');
	}
	
	public function saveSendAddress() {
		if(isset($this->var->saveData) and $this->var->saveData=='save') {
			$insert = array(
				'UAS_USR_ID' => $this->var->user_id,
				'UAS_PERSON_NAME' => $this->var->UAS_PERSON_NAME,
				'UAS_PERSON_SURNAME' => $this->var->UAS_PERSON_SURNAME,
				'UAS_STREET' => $this->var->UAS_STREET,
				'UAS_NUMBER' => $this->var->UAS_NUMBER,
				'UAS_POSTCODE' => $this->var->UAS_POSTCODE,
				'UAS_CITY' => $this->var->UAS_CITY,
				'UAS_PHONE' => $this->var->UAS_PHONE,
			);
			$this->db->queryString(__BP.'user_address_send');
			$adres = $this->db->insertQuery($insert);
			
			if(isset($this->var->asUser) and $this->var->asUser!=null)
				header('location: '.helper::href(array('user', $this->var->asUser)));
			else header('location: '.helper::href(array('order', 'adres', $adres)));
		}
	}
	
	public function getSendAddress() {
		if($this->var->order['address']!='firm') {
			$this->db->queryString('select * from '.__BP.'user_address_send');
			$this->db->where('and', 'UAS_USR_ID='.$this->var->user_id);
			$this->db->where('and', 'UAS_ID='.$this->var->order['address']);
			$this->db->where('and', 'UAS_STATUS=1');
			$user = $this->db->getRow();
			
			if($user) {
				$line = $user['UAS_PERSON_NAME'].' '.$user['UAS_PERSON_SURNAME'].'<br/>'.$user['UAS_STREET'].' '.$user['UAS_NUMBER'].'<br/>'.$user['UAS_POSTCODE'].' '.$user['UAS_CITY'].'<br/>tel: '.$user['UAS_PHONE'];
				$_SESSION['order']['user_address'] = $line;
				return $line;
			} else {
				return $this->noData();
			}
			
		}
	}
	
	public function userLoginSave() {
		if($this->var->USR_PASS1==$this->var->USR_PASS2 and !empty($this->var->USR_PASS1)) {
			$this->db->queryString(__BP.'users');
			$update = array(
				'USR_LOGIN' => $this->var->USR_LOGIN,
				'USR_PASS' => coder::hashString($this->var->USR_PASS1),
			);
			$this->db->updateQuery($update, 'USR_ID', $this->var->user_id);
			header('location: logout.php');
		}
	}
	
	public function recalculateUserPoints($userId, $type=null) {
		//recalculate points and save
		
		//set recalcultions to 'stored'
		if(!is_null($type)) {
			$this->db->queryString('update '.__BP.'user_recalculate set URE_STATUS=2 where URE_USR_ID='.$this->var->user_id);
			$this->db->execQuery();
		}
		
		//points from imports
		$points = 0;
		$this->db->queryString('select sum(UPO_VALUE) as UPO_SUM from '.__BP.'user_points');
		$this->db->where('and', 'UPO_USR_ID='.$userId);
		$this->db->where('and', 'UPO_STATUS=2');
		$points_data = $this->db->getRow();
		if($points_data)
			$points = (int)floor($points_data['UPO_SUM']);
		
		//TODO: points from promotions
		$promotions = 0;
		$this->db->queryString('select sum(UPP_POINTS) as UPP_SUM from '.__BP.'user_points_promotions');
		$this->db->where('and', 'UPP_USR_ID='.$userId);
		$this->db->where('and', 'UPP_STATUS=1');
		$promotions_data = $this->db->getRow();
		if($promotions_data)
			$promotions = (int)floor($promotions_data['UPP_SUM']);
		
		//points from orders
		$orders = 0;
		$this->db->queryString('select sum(ORD_POINTS) as ORD_SUM from '.__BP.'orders');
		$this->db->where('and', 'ORD_USR_ID='.$userId);
		$this->db->where('and', 'ORD_STATUS<>5');
		$orders_data = $this->db->getRow();
		if($orders_data)
			$orders = (int)$orders_data['ORD_SUM'];
		
		//user all points
		$userPoints = $points + $promotions - $orders;
			
		if(!is_null($type)) {
			$_SESSION['user_points'] = $this->var->user_points = $userPoints;
			$_SESSION['user_points_all'] = $this->var->user_points = $userPoints;
		}
		
		if(is_null($type)) 
			$type = 'import';
			
		//save calculation
		$recalculate = array(
			'URE_USR_ID' => $userId,
			'URE_POINTS_IMPORT' => $points,
			'URE_POINTS_ORDERS' => $orders,
			'URE_POINTS_PROMOTIONS' => $promotions,
			'URE_POINTS' => $userPoints,
			'URE_SESSION' => $this->var->user_session,
			'URE_TYPE' => $type,
		);
		
		$this->db->queryString(__BP.'user_recalculate');
		$this->db->insertQuery($recalculate);
		
		//update user points in user table
		$userPointsUpdate = array('USR_POINTS' => $userPoints);
		$this->db->queryString(__BP.'users');
		$this->db->updateQuery($userPointsUpdate, 'USR_ID', $userId);
		
	}
	
	public function recalculateAfterOrder() {
		$this->recalculateUserPoints($this->var->user_id, 'order');
	}
	
	public function activate() {
		if(isset($this->var->id)) {
			$this->db->queryString('select * from '.__BP.'user_func'); 
			$this->db->where('and', 'UFU_CODE="'.$this->var->id.'"');
			$activate = $this->db->getRow();
			
			if($activate) {
				$this->db->queryString(__BP.'users');
				$this->db->updateQuery(array('USR_STATUS'=>2, 'USR_DATE_ADD'=>date('Y-m-d h:i:s')),'USR_ID', $activate['UFU_USR_ID']);
				
				$this->db->queryString(__BP.'user_func');
				$this->db->updateQuery(array('UFU_RESULT'=>'changed', 'UFU_INFO'=>'activation date: '.date('Y-m-d h:i:s')),'UFU_ID', $activate['UFU_ID']);
				
				$_SESSION['user_status'] = $this->var->user_status = 'active';
				
				$list = array(
					'__USER_DATA' => $activate['UFU_DATA'],
				);
				
				$text = Template::parseFile($list, $this->data->localPath.$this->path.'userActivateSendInfo.tpl.php');
				$mailer = new Mailer;
				$mailer->subject = 'BeaBonus - użytkownik aktywował konto w systemie';
				$mailer->mailText = $text;
				$mailer->sendMail(__ADMIN_MAIL, __ADMIN_NAME);
				
				header('location: '.helper::href(array('user', 'active')));
			} else return $this->noData();
		} else return $this->noData();
	}
	
	public function generateLogins() {
		$this->db->queryString("select * from bb__users_temp where UTE_LOGIN='' and UTE_PASS=''");
		$users = $this->db->getDataFetch();
		foreach($users as $u) {
			$pass = substr(md5('pass'.$u['UTE_ID']), 0, 6);
			$login = substr(md5('log'.$u['UTE_ID']), 0, 6);
			//echo $login.' '.$pass.'<br/>';
			$this->db->queryString('bb__users_temp');
			$this->db->updateQuery(array('UTE_LOGIN'=>$login, 'UTE_PASS'=>$pass), 'UTE_ID', $u['UTE_ID']);
		}
	}
	
	public function send() {
		return Template::parseFile(array(), $this->data->localPath.$this->path.'linkSend.tpl.php');
	}
	
	public function active() {
		return Template::parseFile(array(), $this->data->localPath.$this->path.'accountActive.tpl.php');
	}
}		

function user() {
	$user = new user;
	return $user->showModule();
}