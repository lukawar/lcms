<span class="smallInfo"><input type="checkbox" id="" name="agrr111" required="required"> 1.1.1</span><p>Oświadczam, że nie jestem (i) wspólnikiem, pracownikiem lub współpracownikiem Organizatora ani (ii) członkiem zarządu komplementariusza Organizatora ani (iii) członkiem najbliższej rodziny osób wskazanych w pkt (i) lub (ii)  (przez członków najbliższej rodziny rozumie się: wstępnych, zstępnych, rodzeństwo, małżonków, rodziców małżonków i osoby pozostające w stosunku przysposobienia). </p>


<span class="smallInfo"><input type="checkbox" id="" name="agr112" required="required"> 1.1.2</span><p>Wyrażam zgodę na przetwarzanie moich danych osobowych – jako przedsiębiorcy podlegającego rejestracji w programie „Bea Bonus” – przez <strong>Bea Beleza Polska spółka z ograniczoną odpowiedzialnością spółka komandytowa</strong> z siedzibą w Komornikach, ul. Wiśniowa 22 (62-052 Komorniki) w celu umożliwienia rejestracji w programie „Bea Bonus” i prawidłowego prowadzenia tego programu. Oświadczam, że zostałem poinformowany/a o prawie dostępu do treści podanych przeze mnie danych oraz możliwości ich zmiany i usunięcia. Oświadczam, że jestem świadomy/a, że udzielona przeze mnie zgoda może być odwołana w każdym czasie.</p>


<span class="smallInfo"><input type="checkbox" id="" name="agr113" required="required"> 1.1.3</span><p>Wyrażam zgodę na otrzymywanie przeze mnie drogą elektroniczną na wskazany przeze mnie adres poczty elektronicznej informacji handlowych związanych z programem „Bea Bonus” od Bea Beleza Polska spółka z ograniczoną odpowiedzialnością spółka komandytowa z siedzibą w Komornikach, przy ul. Wiśniowej 22 (62-052 Komorniki).</p>


<span class="smallInfo"><input type="checkbox" id="" name="agr114" required="required"> 1.1.4</span><p>Wyrażam zgodę na używanie przez Bea Beleza Polska spółka z ograniczoną odpowiedzialnością spółka komandytowa z siedzibą w Komornikach, przy ul. Wiśniowej 22 (62-052 Komorniki) telekomunikacyjnych urządzeń końcowych (w szczególności komputerów, telefonów komórkowych, tabletów), których jestem posiadaczem, dla celów marketingu bezpośredniego związanego z programem „Bea Bonus”.</p>


<span class="smallInfo"><input type="checkbox" id="" name="agr115" required="required"> 1.1.5</span><p>Oświadczam, że zapoznałem/am się z regulaminem Programu „Bea Bonus”, akceptuję jego treść oraz że przystępuję do programu „Bea Bonus”.</p>