<div class="boxed flexed">
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="form-group formTitle"><h4>Dane użytkownika systemowe</h4></div>
		__USER_DATA_SYSTEM
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="form-group formTitle"><h4>Dane do wysyłki</h4></div>
		__USER_DATA_SEND
	</div>
</div>

<div class="form-group labeled">
	<p>Ostatnie logowanie: <strong>__LAST_LOGIN</strong></p>
</div>

<div class="form-group formTitle"><h4>Punkty</h4></div>

<div class="form-group labeled">
	<p>Punkty z zakupów: <strong>__POINTS_IMPORT</strong></p>
</div>	

<div class="form-group labeled">
	<p>Posiadane: <strong>__POINTS_USER</strong></p>
</div>

<div class="form-group labeled">
	<p>Punkty z promocji: <strong>__POINTS_PROM</strong></p>
</div>

<div class="form-group formTitle"><h4>Schowek</h4></div>
<div class="form-group labeled">
	<div class="col-xs-12 col-md-12">__CHEST</div>
</div>


<p>&nbsp;</p>