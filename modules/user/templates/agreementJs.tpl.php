<script>
// (function(){
// 	var chb_reg_self = $('#reg_self');
// })();
$(document).ready(function() {
    // $("#txtAge").toggle(this.checked);
    var pathRel = '__PATH';
	var agreement_a = false;
	var agreement_a_a = false;
	var agreement_a_b = false;

	var reg_self_cont = $('#reg_self_cont');
	var reg_self = $('#reg_self');

	var reg_self_fiz_cont = $('#reg_self_fiz_cont');
	var reg_self_fiz = $('#reg_self_fiz');
	var reg_self_org_cont = $('#reg_self_org_cont');
	var reg_self_org = $('#reg_self_org');


	var agreement_b = false;
	var reg_oth_self = $('#reg_oth_self');
	var agr_container = $('#agreements_container');
	var sub_agr = $('.sub-agreement');
	var reg_other_subj_cont = $('#reg_other_subj_cont');
	var reg_other_subj = $('#reg_other_subj');




	// sekcja 1
	reg_self.on('click', function() {
		if(agreement_a === false) {
			$(this).attr("required", true);
			reg_self_fiz.prop( "required", true );
			reg_other_subj.prop( "required", false );

			//reset subagreement checked
			reg_self_fiz.removeAttr('checked');
			reg_self_org.removeAttr('checked');

			agreement_a = true;
			sub_agr.css('display','block');
			reg_other_subj_cont.css('display','none');

		} else {
			agreement_a = false;
			sub_agr.css('display','none');
			agr_container.html('');
			reg_oth_self.prop('checked', false);
			reg_other_subj_cont.css('display','block');
		};
		// $('#reg_self_org').css('display','block');

	});


	//1.1
	reg_self_fiz.on('click', function() {

		if(agreement_a_a === false) {
			$(this).attr("required", true);
			reg_self_org.prop( "required", false );

			//reset neighbour checked
			reg_self_org.removeAttr('checked');

			reg_self.removeAttr( "required" );
			reg_self_fiz_cont.css('display','block');
			reg_self_org_cont.css('display','none');
			var jqxhr = $.get( pathRel + "agreementA.tpl.php", function(data) {
						    	agr_container.html(data);
							})
							.done(function() {
							})
							.fail(function() {
								agr_container.html('nie udało się załadować pozostałej części formularza');
							});

			agreement_a_a = true;
		} else {
			reg_self_org_cont.css('display','block');
			agr_container.html('Aby kontunuować rejestrację wybierz jedną z powyższych opcji.');
			agreement_a_a = false;

		};



	});

	//1.2
	reg_self_org.on('click', function() {

		if(agreement_a_b === false) {
			$(this).attr("required", true);
			reg_self_fiz.prop( "required", false );

			//reset neighbour checked
			reg_self_fiz.removeAttr('checked');

			reg_self_org_cont.css('display','block');
			reg_self_fiz_cont.css('display','none');

			agreement_a_b = true;
		} else {
			reg_self_fiz_cont.css('display','block');
			agr_container.html('Aby kontunuować rejestrację wybierz jedną z powyższych opcji.');
			agreement_a_b = false;

		};

		var jqxhr = $.get( pathRel + "agreementB.tpl.php", function(data) {
					    	agr_container.html(data);
						})
						.done(function() {
						})
						.fail(function() {
							agr_container.html('nie udało się załadować pozostałej części formularza');
						});

	});



	// sekcja 2

	reg_other_subj.on('click', function() {
		if(agreement_b === false) {
			console.log(agreement_b);
			$(this).attr("required", true);
			reg_self.prop( "required", false);

			sub_agr.css('display','none');
			reg_self_cont.css('display','none');
			reg_other_subj_cont.css('display','block');

			var jqxhr = $.get( pathRel + "agreementC.tpl.php", function(data) {
			    	agr_container.html(data);
				})
				.done(function() {
				})
				.fail(function() {
					agr_container.html('nie udało się załadować pozostałej części formularza');
				});
				agreement_b = true;
		} else {

			sub_agr.css('display','block');
			reg_self_cont.css('display','block');

			agr_container.html('');
			sub_agr.css('display','none');
			agreement_b = false;
		}
	});	



});

$(function(){
	//change bootstrap tab
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop() || $('html').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });
});
</script>