<h3 class="block_title">panel użytkownika</h3>
<div class="container user-panel">
	<ul class="nav nav-tabs" id="userPanel">
		<li class="active"><a href="#dane" data-toggle="tab">Dane użytkownika</a></li>
	  <li><a href="#informacje" data-toggle="tab">Informacje</a></li>
	  <li><a href="#zamowienia" data-toggle="tab">Zamówienia</a></li>
	  <li><a href="#przechowalnia" data-toggle="tab">Przechowalnia</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in active" id="dane">__USER_DATA</div>
	  <div role="tabpanel" class="tab-pane fade" id="informacje">__USER_INFO</div>
	  <div role="tabpanel" class="tab-pane fade" id="zamowienia">__USER_ORDERS</div>
	  <div role="tabpanel" class="tab-pane fade" id="przechowalnia">__USER_CHEST</div>
	</div>
</div>

<div class="modal fade" id="modalWindow" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="modalLabel"></h5>
      </div>
      <div class="modal-body" id="modalBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>