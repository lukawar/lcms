<div class="top-status-box col-xs-12 col-sm-5 col-md-3">
	<header><strong>Witaj!</strong> <span class="status-user">__USR_NAME</span>
	</header>
	
	<div class="status-box-points">twoje punkty<span class="status-points" id="userPoints">__USR_POINTS</span></div>

	<div class="row white-links invert">
		<div class="col-xs-6 col-md-7"><a href="__CART_LINK" class="check-cart"><i class="material-icons">shopping_cart</i> <span id="cartData">__CART_DATA</span></a></div>
		<div class="col-xs-6 col-md-5 dropdown">
			<div class="status-more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">więcej</div>
				
		  <ul class="dropdown-menu" aria-labelledby="dLabel">
		    <li><a href="__PANEL_LINK">panel klienta</a></li>
		    <li role="separator" class="divider"></li>
		    <li><a href="__LOGOUT_LINK">wyloguj</a></li>
		  </ul>
		</div>
	</div>
</div>