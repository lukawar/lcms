<?php
class user extends View implements IModule {
	public $path = 'modules/user/templates/';
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'users');
			//report filter, hide test accounts
			$this->db->where('and', 'USR_REPORT=1');
			$this->db->where('and', 'USR_STATUS<>3');
			
			//ph filter
			if($this->var->admin_agency=='ph')
				$this->db->where('and', 'USR_PH='.$this->var->admin_id);
			
			$users = $this->db->getDataFetch();
			
			if($users)	{
				$table = new table('dataTable', 'table table-striped stripe hover');
				
				if($this->data->agent=='desktop') 
					$tableHeader = array('lp.', 'NIP', 'Nazwa', 'Dane', 'Miejscowość', 'Punkty','Data', 'Status');
				elseif($this->data->agent=='mobile')
					$tableHeader = array('lp.', 'Uczestnik', 'Punkty');
					
				if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
					$tableHeader[] = ' ';
					
				$table->createHeader($tableHeader);
				
				$i = 1;
				foreach($users as $user) {
					if($this->data->agent=='desktop') {
						$table->addCell($i, 'lp');
						$table->addLink($user['USR_ID'], $user['USR_NIP'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_NAME'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_PERSON_NAME'].' '.$user['USR_PERSON_SURNAME'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_CITY'].', '.$user['USR_POSTCODE'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_POINTS'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_DATE_ADD'], 'userDetails');
						$table->addLink($user['USR_ID'], $user['USR_STATUS'], 'userDetails');
						if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
							$table->addButton('edit', 'admin.php?action=user&option=edit&id='.$user['USR_ID'], 'primary');
					} elseif($this->data->agent=='mobile') {
						$line = '<strong>'.$user['USR_NIP'].' '.$user['USR_NAME'].'</strong><br/><span class="smallerTd">'.$user['USR_PERSON_NAME'].' '.$user['USR_PERSON_SURNAME'].' '.$user['USR_CITY'].', '.$user['USR_POSTCODE'].' ('.$user['USR_STATUS'].')</span>';
						$table->addCell($i, 'lp');
						$table->addLink($user['USR_ID'], $line, 'userDetails');
						$table->addCell($user['USR_POINTS']);
						if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
							$table->addButton('edit', 'admin.php?action=user&option=edit&id='.$user['USR_ID'], 'primary');
					}
					$table->addRow();
					$i++;
					
				}
				$table = $table->createBody();
			} else $table = $this->noData();
			
			$list = array(
				'__GLYPHICON' => 'group',
				'__HEAD' => 'LISTA UŻYTKOWNIKÓW',
				'__CONTENT'=> $table
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('user','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> nowy użytkownik</a>');
			$page->setButton('<a href="'.helper::href(array('user','report'), null, 'full').'" class="btn btn-success"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> raporty</a>');
			$page->setToolbar();
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('user', 'addSave', 'Wypełnij dane');
			$form->addElement('<div class="form-group labeled"><h5> Dane adresowe</h5></div>');
			$form->addText('Nazwa', 'USR_NAME');
			$form->addText('Kod', 'USR_CODE');
			$form->addText('NIP*', 'USR_NIP', 'required');
			$form->addText('Imię', 'USR_PERSON_NAME');
			$form->addText('Nazwisko', 'USR_PERSON_SURNAME');
			$form->addText('Ulica', 'USR_STREET');
			$form->addText('Numer', 'USR_NUMBER');
			$form->addText('Kod pocztowy', 'USR_POSTCODE');
			$form->addText('Miejscowość', 'USR_CITY');
			$form->addText('E-mail', 'USR_EMAIL');
			$form->addText('Telefon', 'USR_PHONE');
			$form->addElement('<div class="form-group labeled"><h5> Dane systemowe</h5></div>');
			$form->addText('Login*', 'USR_LOGIN', 'required');
			$form->addText('Hasło*', 'USR_PASS1', 'required');
			$form->addText('Hasło ponownie*', 'USR_PASS2', 'required');
			$form->addSelect('Status', 'USR_STATUS', $this->getStatus() , null);
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj nowego użytkownika',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'group',
			);
			
			$page->setSubmit();
			$page->setCancel($this->var->action);
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->USR_PASS1===$this->var->USR_PASS2) {
				$insert = array(
					'USR_NAME' => $this->var->USR_NAME,
					'USR_ALIAS' => coder::stripPL($this->var->USR_NAME),
					'USR_STATUS' => $this->var->USR_STATUS,
					'USR_CODE' => $this->var->USR_CODE,
					'USR_LOGIN' => $this->var->USR_LOGIN,
					'USR_PASS' => coder::hashString($this->var->USR_PASS1),
					'USR_EMAIL' => $this->var->USR_EMAIL,
					'USR_PHONE' => $this->var->USR_PHONE,
					'USR_PERSON_NAME' => $this->var->USR_PERSON_NAME,
					'USR_PERSON_SURNAME' => $this->var->USR_PERSON_SURNAME,
					'USR_NIP' => coder::nip($this->var->USR_NIP),
					'USR_STREET' => $this->var->USR_STREET,
					'USR_NUMBER' => $this->var->USR_NUMBER,
					'USR_POSTCODE' => $this->var->USR_POSTCODE,
					'USR_CITY' => $this->var->USR_CITY,
				);
				
				$this->db->queryString(__BP.'users');
				$id = $this->db->insertQuery($insert);
				
				//save event 
				eventSaver::add($this->var->admin_id, 'add user - '. $this->var->USR_NAME, 'admin', 'user', 'add', $id);
			
				//save backup
				eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'user', $id, 'add', $this->var->USR_NAME, $this->var->USR_PERSON_NAME.' '.$this->var->USR_PERSON_SURNAME.', '.$this->var->USR_STREET.' '.$this->var->USR_NUMBER.', '.$this->var->USR_POSTCODE.' '.$this->var->USR_CITY.'; '.$this->var->USR_EMAIL.'; '.$this->var->USR_PHONE.'; '.$this->var->USR_NIP.'; '.$this->var->USR_EMAIL);
				
				header('location: admin.php?action=user');
			}
			
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'users');
			$this->db->where('and', 'USR_ID='.(int)$this->var->id);
			
			//ph filter
			if($this->var->admin_agency=='ph')
				$this->db->where('and', 'USR_PH='.$this->var->admin_id);
			
			$user = $this->db->getRow();
			//$this->db->query();
			
			$form = new Form('user', 'editSave', 'Edycja danych użytkownika', 'multipleForm.tpl.php');
			//$form->addElement('<div class="form-group labeled"><h5> Dane adresowe</h5></div>');
			$form->addText('Nazwa', 'USR_NAME', null, $user['USR_NAME']);
			$form->addText('Kod', 'USR_CODE', null, $user['USR_CODE']);
			$form->addText('NIP*', 'USR_NIP', 'required', $user['USR_NIP']);
			$form->addText('Imię', 'USR_PERSON_NAME', null, $user['USR_PERSON_NAME']);
			$form->addText('Nazwisko', 'USR_PERSON_SURNAME', null, $user['USR_PERSON_SURNAME']);
			$form->addText('Ulica', 'USR_STREET', null, $user['USR_STREET']);
			$form->addText('Numer', 'USR_NUMBER', null, $user['USR_NUMBER']);
			$form->addText('Kod pocztowy', 'USR_POSTCODE', null, $user['USR_POSTCODE']);
			$form->addText('Miejscowość', 'USR_CITY', null, $user['USR_CITY']);
			$form->addText('E-mail', 'USR_EMAIL', null, $user['USR_EMAIL']);
			$form->addText('Telefon', 'USR_PHONE', null, $user['USR_PHONE']);
			$form->addSelect('Status', 'USR_STATUS', null, $this->getStatus(), $user['USR_STATUS']);
			$form->addHidden('USR_ID', $user['USR_ID']);
			$form->addSubmit('Zapisz dane');
			
			$formS = new Form('user', 'editSavePass', 'Edycja danych systemowych użytkownika', 'multipleForm.tpl.php');
			//$form->addElement('<div class="form-group labeled"><h5> Dane systemowe</h5></div>');
			$formS->addText('Login*', 'USR_LOGIN', 'required', $user['USR_LOGIN']);
			$formS->addText('Hasło*', 'USR_PASS1', 'required');
			$formS->addText('Hasło ponownie*', 'USR_PASS2', 'required');
			$formS->addHidden('USR_ID', $user['USR_ID']);
			$formS->addSubmit('Zapisz dane');
			
			$formPH = new Form('user', 'editSavePh', 'Dane dodatkowe', 'multipleForm.tpl.php');
			$formPH->addSelect('Status', 'USR_STATUS', $this->getStatus(), $user['USR_STATUS']);
			$formPH->addSelect('Przypisany PH', 'USR_PH', $this->getPh(), $user['USR_PH']);
			
			$formPH->addHidden('USR_ID', $user['USR_ID']);
			$formPH->addSubmit('Zapisz dane');
			
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Edycja danych uczestnika',
				'__CONTENT' => $form->setForm().$formS->setForm().$formPH->setForm(),
				'__GLYPHICON' => 'group',
			);
			
			//$page->setSubmit();
			//$page->setCancel($this->var->action);
			//$page->setToolbar();			
			return $page->setPage($list, 'default.tpl.php');
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'users');
			$list = array(
				'USR_NAME'	=> $this->var->USR_NAME,
				'USR_CODE' => $this->var->USR_CODE,
				'USR_NIP' => coder::nip($this->var->USR_NIP),
				'USR_PERSON_NAME' => $this->var->USR_PERSON_NAME,
				'USR_PERSON_SURNAME' => $this->var->USR_PERSON_SURNAME,
				'USR_STREET' => $this->var->USR_STREET,
				'USR_NUMBER' => $this->var->USR_NUMBER,
				'USR_POSTCODE' => $this->var->USR_POSTCODE,
				'USR_CITY' => $this->var->USR_CITY,
				'USR_EMAIL' => $this->var->USR_EMAIL,
				'USR_PHONE' => $this->var->USR_PHONE,
				'USR_STATUS' => $this->var->USR_STATUS,
			);
			
			$this->db->updateQuery($list, 'USR_ID', $this->var->USR_ID);
			
			//save event 
			eventSaver::add($this->var->admin_id, 'edit user - '. $this->var->USR_NAME, 'admin', 'user', 'edit', $this->var->USR_ID);
		
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'user', $this->var->USR_ID, 'edit', $this->var->USR_NAME, $this->var->USR_PERSON_NAME.' '.$this->var->USR_PERSON_SURNAME.', '.$this->var->USR_STREET.' '.$this->var->USR_NUMBER.', '.$this->var->USR_POSTCODE.' '.$this->var->USR_CITY.'; '.$this->var->USR_EMAIL.'; '.$this->var->USR_PHONE.'; '.$this->var->USR_NIP.'; '.$this->var->USR_EMAIL);
			
			header('location: admin.php?action=user');
		} else return $this->goAway();
	}
	
	public function editSavePass() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->USR_PASS1===$this->var->USR_PASS1) {
				$this->db->queryString(__BP.'users');
				$this->db->updateQuery(array('USR_LOGIN' => $this->var->USR_LOGIN, 'USR_PASS' => coder::hashString($this->var->USR_PASS1)), 'USR_ID', $this->var->USR_ID);
				
				//save event 
				eventSaver::add($this->var->admin_id, 'change pass user - '. $this->var->USR_NAME, 'admin', 'user', 'change pass', $this->var->USR_ID);
				
				header('location: admin.php?action=user');
			}
		} else return $this->goAway();
	}
	
	public function editSavePh() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if(!empty($this->var->USR_STATUS)) {
				$this->db->queryString(__BP.'users');
				$this->db->updateQuery(array('USR_STATUS'=>$this->var->USR_STATUS, 'USR_PH' => $this->var->USR_PH), 'USR_ID', $this->var->USR_ID);
				
				//save event 
				eventSaver::add($this->var->admin_id, 'change status/ph user - '. $this->var->USR_ID, 'admin', 'user', 'change STATUS', $this->var->USR_ID);
				
				//save backup
				eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'user', $this->var->USR_ID, 'status', 'status: '.$this->var->USR_STATUS, 'ph: '.$this->var->USR_PH);
				
				header('location: admin.php?action=user');
			}
		} else return $this->goAway();	
	}
	
	public function details() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'users');
			$this->db->where('and', 'USR_ID='.(int)$this->var->id);
			
			if($this->var->admin_agency=='ph')
				$this->db->where('and', 'USR_PH='.$this->var->admin_id);
				
			$user = $this->db->getRow();
			
			$this->db->queryString('select * from '.__BP.'user_address_send');
			$this->db->where('and', 'UAS_USR_ID='.(int)$this->var->id);
			$this->db->where('and', 'UAS_STATUS=1');
			
			$adr = null;
			
			//send address
			$addresses = $this->db->getDataFetch();
			if($addresses) {
				foreach($addresses as $address)
					$adr.=$address['UAS_PERSON_NAME'].' '.$address['UAS_PERSON_SURNAME'].'<br/>'.$address['UAS_STREET'].' '.$address['UAS_NUMBER'].'<br/>'.$address['UAS_POSTCODE'].' '.$address['UAS_CITY'].'<br/>Tel: '.$address['UAS_PHONE'].'<br/><br/>';
			} 
			
			//points import
			$points_import = 0;
			$this->db->queryString('select sum(UPO_VALUE) as points from '.__BP.'user_points');
			$this->db->where('and', 'UPO_USR_ID='.(int)$this->var->id);
			$this->db->where('and', 'UPO_STATUS=2');
			
			$user_points_import = $this->db->getRow();
			if($user_points_import)
				$points_import = $user_points_import['points'];
				
			if($points_import=='') $points_import = 0;
			
			//points prom
			$points_prom = 0;
			$this->db->queryString('select sum(UPP_POINTS) as points from '.__BP.'user_points_promotions');
			$this->db->where('and', 'UPP_USR_ID='.(int)$this->var->id);
			$this->db->where('and', 'UPP_STATUS=1');
			
			$user_points_prom = $this->db->getRow();
			if($user_points_prom)
				$points_prom = $user_points_prom['points'];
				
			if(empty($points_prom))
				$points_prom = 0;
				
			$points_all = $points_import + $points_prom;
			
			//last login
			$this->db->queryString('select ULO_DATE from '.__BP.'user_login');
			$this->db->where('and', 'ULO_USR_ID='.(int)$this->var->id);
			$this->db->orderBy('ULO_DATE desc');
			$login = $this->db->getRow();
			
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			
			$list = array(
				'__USER_DATA_SYSTEM' => $user['USR_NAME'].'<br/>'.$user['USR_PERSON_NAME'].' '.$user['USR_PERSON_SURNAME'].'<br/>'.$user['USR_STREET'].' '.$user['USR_NUMBER'].'<br/>'.$user['USR_POSTCODE'].' '.$user['USR_CITY'].'<br/>NIP: '.$user['USR_NIP'].'<br/>Tel: '.$user['USR_PHONE'].'<br/>Email: '.$user['USR_EMAIL'],
				'__USER_DATA_SEND' => $adr,
				'__POINTS_IMPORT' => $points_import,
				'__POINTS_PROM' => $points_prom,
				'__POINTS_USER' => $user['USR_POINTS'],
				'__LAST_LOGIN' => $login['ULO_DATE'],
				'__CHEST' => $model->load('chest', 'getUserChest', ['id' => (int)$this->var->id, 'points' => $user['USR_POINTS']]),
			);
			
			
			$data = Template::parseFile($list, $this->data->localPath.$this->path.'details.tpl.php');
			$details = array(
				'modalLabel' => 'DANE UCZESTNIKA',
				'responseText' => $data,
			);
			
			return json_encode($details);
		} else return $this->goAway();
	}
	
	//-report---------------------------------------------------------------------------------------
	public function report() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('report', 'user', 'Zakres danych');
			$form->addInput('Zarejestrowani od', 'REP_DATE_FROM', null, 'datepicker.tpl.php');
			$form->addInput('Zarejestrowani do', 'REP_DATE_TO', null, 'datepicker.tpl.php');
			$form->addSelect('Status', 'REP_STATUS', $this->getStatus(true) , 'all');
			if($this->var->admin_agency=='A' or $this->var->admin_agency=='u')
				$form->addSelect('Przypisany PH', 'REP_PH', $this->getPh(), null);
				
			$page = new Page;
			$list = array(
				'__HEAD' => 'RAPORT UCZESTNIKÓW',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'group',
			);
			
			$page->setSubmit(null, 'generuj');
			$page->setCancel($this->var->action);
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	//----------------------------------------------------------------------------------------
	
	public function getStatus($all=false) {
		$status = array(
			'new' => 'nowy',
			'active' => 'aktywny',
			'deleted' => 'usunięty',
		);
		if($all)
			$status['all'] = 'wszystkie konta';
		return $status;		
	}
	
	
	private function getPh() {
		$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		return $model->load('administrator', 'getPh');
	}
	
	//--DASHBOARD------------------------------------------------------------------------------------------
	
	public function dashboard() {
		$this->db->queryString('select USR_NIP, USR_NAME, USR_DATE_ADD, USR_ID from '.__BP.'users');
		$this->db->where('and', 'USR_STATUS=2');
		$this->db->orderBy('USR_DATE_ADD desc');
		$this->db->limit(10);
		
		$users = $this->db->getDataFetch();//$this->db->query();
		
		if($users) {
			//$table = new table('dataTable', 'table table-striped stripe hover');
			$table = new table('dataTableWidget', 'table table-striped stripe hover');
			$tableHeader = array('Nip','Nazwa', 'Data');
			$table->createHeader($tableHeader);
			foreach($users as $user) {
				$link = 'admin.php?action=users&option=edit&id='.$user['USR_ID'];
				$table->addLink($link, $user['USR_NIP'], null, null, 'Szczegóły');
				$table->addLink($link, $user['USR_NAME'], null, null, 'Szczegóły');
				$table->addLink($link, $user['USR_DATE_ADD'], null, null, 'Szczegóły');
				$table->addRow();
			}
			$tabList = $table->createBody().'<div class="box-footer"><a class="btn btn-success" href="?action=user">Pełna lista <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>';
		} else $tabList = $this->noData();
		$list = array(
			'__MODULE_TITLE'=>'Uczestnicy - 10 ostatnich',
			'__MODULE_ICON'=>'group',
			'__MODULE_CONTENT'=>$tabList,
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
	
}

function user($option=null) {
	$user = new user;
	if($option!=null and method_exists($user, $option))
		return $user->$option();
	else 
		return $user->showModule();
}