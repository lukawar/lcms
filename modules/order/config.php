<?php
$module_info['title'] = 'Zamówienia';
$module_info['description'] = 'Zarządzanie i obsługa zamówień';
$module_info['author'] = 'Expansja';

$menu['title'][0] = 'Zamówienia';
$menu['action'][0] = 'order';
$menu['icon'][0] = 'shopping_cart';
$menu['order'][0] = '6';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = true;