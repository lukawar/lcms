<?php
class order extends View implements iModule {
	public $path = 'modules/order/templates/'; //path to templates for admin
	public $status = array('new' => 'nowe', 'confirmed' => 'potwierdzone', 'sended' => 'wysłane', /*'closed' => 'zamknięte',*/ 'deleted' => 'usunięte');
	public $kurier = array('GLS' => 'https://gls-group.eu/PL/pl/sledzenie-paczek', 'DPD' => 'https://www.dpd.com.pl/');
		
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('dataTable', 'table table-striped stripe hover');
			if($this->data->agent=='desktop') 
				$tableHeader = array('lp.', 'Numer', 'Data zamówienia', 'Użytkownik', 'NIP', 'Kwota', 'Ilość', 'Status');
			elseif($this->data->agent=='mobile')
				$tableHeader = array('Data', 'Zamówienie', 'Status');
			
			if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
					$tableHeader[] = ' ';
					
			$table->createHeader($tableHeader);
			
			$i = 1;
							
			$this->db->queryString('select * from '.__BP.'orders');
			$this->db->join('left', __BP.'orders_status', 'ORD_ID', 'OST_ORD_ID');
			$this->db->join('left', __BP.'users', 'USR_ID', 'ORD_USR_ID');
			$this->db->where('and', 'ORD_STATUS<>5');
			$this->db->orderBy('ORD_DATE_ADD desc');
			
			//ph filter
			if($this->var->admin_agency=='ph')
				$this->db->where('and', 'USR_PH='.$this->var->admin_id);
			
			//from filter
			if($this->var->admin_agency!='A')
				$this->db->where('and', 'OST_FROM="'.$this->var->admin_from.'"');
			
			$orders = $this->db->getDataFetch();
			
			if($orders)	{
				foreach($orders as $order) {
					if($this->data->agent=='desktop') {
						$table->addCell($i, 'lp');
						$table->addCell($order['ORD_ID']); /// TODO: insert generated order id
						$table->addCell($order['ORD_DATE_ADD']);
						$table->addCell($order['USR_NAME'].' '.$order['USR_PERSON_NAME'].' '.$order['USR_PERSON_SURNAME']);
						$table->addCell($order['USR_NIP']);
						$table->addCell($order['ORD_POINTS']);
						$table->addCell($order['ORD_POSITIONS']);
						$table->addCell($this->status[$order['OST_STATUS']]);
						if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
								$table->addButton('edit', 'admin.php?action=order&option=edit&id='.$order['ORD_ID'], 'primary');
					} elseif($this->data->agent=='mobile') {
						$line = '<strong>Zamówienie: '.$order['ORD_ID'].', '.$order['USR_NIP'].'</strong><br/>'.$order['USR_NAME'].' '.$order['USR_PERSON_NAME'].' '.$order['USR_PERSON_SURNAME'];
						$table->addCell($order['ORD_DATE_ADD']);
						$table->addCell($line);
						$table->addCell($this->status[$order['ORD_STATUS']]);
						if($this->var->admin_agency=='u' or $this->var->admin_agency=='A')
								$table->addButton('edit', 'admin.php?action=order&option=edit&id='.$order['ORD_ID'], 'primary');
					}
					$table->addRow();
					$i++;
						
				}
				$tableBody = $table->createBody();		
			} else $tableBody = $this->noData();
			
			$list = array(
				'__GLYPHICON' => 'shopping_cart',
				'__HEAD' => 'ZAMÓWIENIA',
				'__CONTENT'=> $tableBody
			);
			
			$page = new Page;
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$orderData = $this->getOrderData($this->var->id);			
			
			if(!$orderData)
				return $this->noData();
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Zamówienie nr.',
				'__CONTENT' => $orderData,
				'__GLYPHICON' => 'shopping_cart',
			);
			
			$page->setSubmit(' id="saveBack"', 'zapisz');
			$page->setSubmit(' id="saveStay"', 'zapisz i zostań');
			$page->setCancel($this->var->action);
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function getOrderData($id) {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'orders');
			$this->db->join('left', __BP.'users', 'USR_ID', 'ORD_USR_ID');
			//$this->db->join('left', __BP.'orders_status', 'ORD_ID', 'OST_ORD_ID');
			$this->db->where('and', 'ORD_ID='.(int)$id);
			$this->db->where('and', 'ORD_STATUS<>5');
			
			//ph filter
			if($this->var->admin_agency=='ph')
				$this->db->where('and', 'USR_PH='.$this->var->admin_id);
			
			$order = $this->db->getRow();
			
			//order data			
			if($order) {
				//status data
				$this->db->queryString('select * from '.__BP.'orders_status');
				$this->db->where('and', 'OST_ORD_ID='.(int)$order['ORD_ID']);
				//from filter
				if($this->var->admin_agency!='A')
					$this->db->where('and', 'OST_FROM="'.$this->var->admin_from.'"');
				
				$orderStatus = $this->db->getData();
				
				$user = null;
				$statusList = null;
				
				if($orderStatus) {
					foreach($orderStatus as $oStatus) {
						//order positions
						$this->db->queryString('select * from '.__BP.'order_positions');
						$this->db->where('and', 'OPO_ORD_ID='.$order['ORD_ID']);
						$this->db->where('and', 'OPO_STATUS=1');
						$this->db->where('and', 'OPO_FROM="'.$oStatus['OST_FROM'].'"');
						
						$positions = $this->db->getDataFetch();
						
						if($positions)	{
							$table = new table('dataTableWhite', 'table table-striped stripe hover');
							$tableHeader = array('lp.', 'Produkt', 'Ilość', 'Kwota', 'Wartość');
							$table->createHeader($tableHeader);
						
							$i = 1;
							foreach($positions as $position) {
								$table->addCell($i, 'lp');
								$table->addCell($position['OPO_PRO_NAME']);
								//$table->addCell($position['OPO_PRO_CODE']);
								$table->addCell($position['OPO_COUNT'].' szt.');
								$table->addCell($position['OPO_POINTS'].' pkt');
								$table->addCell($position['OPO_POINTS']*$position['OPO_COUNT'].' pkt +'.$position['OPO_COUNT'].' zł');
								$table->addRow();
								$i++;
									
							}
							
							$table->addCell('');
							$table->addCell('RAZEM:');
							//$table->addCell($position['OPO_PRO_CODE']);
							$table->addCell($oStatus['OST_POSITIONS'].' szt.');
							$table->addCell('');
							$table->addCell($oStatus['OST_POINTS'].' pkt +'.$oStatus['OST_POSITIONS'].' zł');
							$table->addRow();
							
							$tablePositions = $table->createBody();		
						} else $tableBody = $this->noData();
						
						if($oStatus['OST_STATUS']=='sended')
							$required = 'required';
						else $required = null;					
						
						//get order positions & data for bb/ex
						$orderFrom = array(
							'__OST_ID' => $oStatus['OST_ID'],
							'__OST_FROM' => $oStatus['OST_FROM'],
							'__OST_SEND_CODE' => $oStatus['OST_SEND_CODE'],
							'__OST_SEND_PATH' => $oStatus['OST_SEND_PATH'],
							'__OST_SEND_FIRM' => $this->getKurier($oStatus['OST_SEND_FIRM'], $oStatus['OST_FROM']),
							'__OST_SEND_DATE' => $oStatus['OST_SEND_DATE'],
							'__OLD_OST_STATUS' => $oStatus['OST_STATUS'],
							'__ORDER_STATUS' => $this->orderStatus($oStatus['OST_STATUS'], $oStatus['OST_FROM']),
							'__REQUIRED' => $required,
							'__ORDER_POSITIONS' => $tablePositions,
						);
						$statusList.= Template::parseFile($orderFrom, $this->data->localPath.$this->path.'orderStatusBox.tpl.php');
					}
				}
				
				if(isset($this->var->type) and $this->var->type=='saved') {
					$type = '$("#modalWindow").modal();';
					$modalInfo = 'Zapisano dane';
				} else {
					$type = null;
					$modalInfo = null;
				}
				
				$userlist = array(
					'__USER_DATA_SYSTEM' => $order['USR_NAME'].'<br/>NIP: '.$order['USR_NIP'].'<br/>'.$order['USR_PERSON_NAME'].' '.$order['USR_PERSON_SURNAME'].'<br/>'.$order['USR_STREET'].' '.$order['USR_NUMBER'].'<br/>'.$order['USR_POSTCODE'].' '.$order['USR_CITY'].'<br/><br/>tel: '.$order['USR_PHONE'].'<br/>mail: '.$order['USR_EMAIL'],
					'__USER_DATA_SEND' => $order['ORD_ADDRESS'],
					'__ORDER_NUMBER' => $order['ORD_ID'],
					'__ORDER_DATE' => $order['ORD_DATE_ADD'],			
					'__MODAL_INFO' => $modalInfo,
					'__MODAL_SHOW' => $type,
					'__ORDER_BOX_STATUS' => $statusList,
					'__ORDER_VALUE' => $order['ORD_POSITIONS'].' szt. / '.$order['ORD_POINTS'].' pkt. +'.$order['ORD_POSITIONS'].' zł',
					'__ORD_ID' => $order['ORD_ID'],
					'__ORD_USR_ID' => $order['ORD_USR_ID'],
				);
				
				$user = Template::parseFile($userlist, $this->data->localPath.$this->path.'edit.tpl.php');
				
				return $user;
				
			} else return null;
		} else return $this->goAway();	
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			//order status for ex
			if(isset($this->var->TYPE_ex) and $this->var->TYPE_ex=='true')
				$this->saveAndSend('ex');
				
			//order status for bb
			if(isset($this->var->TYPE_bb) and $this->var->TYPE_bb=='true')
				$this->saveAndSend('bb');
			
			if($this->var->actionType=='saveStay')
				header('location: admin.php?action=order&option=edit&id='.$this->var->ORD_ID.'&type=saved');
			else
				header('location: admin.php?action=order');
		} else return $this->goAway();
	}
	
	private function saveAndSend($from) {
		$this->db->queryString(__BP.'orders_status');
		$list = array(
			'OST_STATUS' => $this->var->OST_STATUS[$from],
			'OST_SEND_CODE' => $this->var->OST_SEND_CODE[$from],
			'OST_SEND_PATH' => $this->var->OST_SEND_PATH[$from],
			'OST_SEND_FIRM' => $this->var->OST_SEND_FIRM[$from],
			'OST_SEND_DATE' => $this->var->OST_SEND_DATE[$from],
		);
		
		$this->db->updateQuery($list, 'OST_ID', $this->var->OST_ID[$from]);
		
		//save event 
		eventSaver::add($this->var->admin_id, 'edit order '.$from.' - '. $this->var->ORD_ID, 'admin', 'order', 'edit '.$from, $this->var->OST_ID[$from]);
	
		//save backup
		eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'order', $this->var->ORD_ID, 'edit', $from.' status: '.$this->var->OST_STATUS[$from], 'numer wysyłki: '.$this->var->OST_SEND_CODE[$from], 'link: '.$this->var->OST_SEND_PATH[$from], 'firma: '.$this->var->OST_SEND_FIRM[$from], 'data wysyłki: '.$this->var->OST_SEND_DATE[$from]);
		
		//send notification
		if($this->var->userNotification[$from]=='send') {
			$content = null;
			$this->db->queryString('select USR_EMAIL, USR_PERSON_NAME, USR_PERSON_SURNAME from '.__BP.'users');
			$this->db->where('and', 'USR_ID='.(int)$this->var->ORD_USR_ID);
			$this->db->where('and', 'USR_STATUS=2');
			$user = $this->db->getRow();
			
			if(!empty($this->var->OST_SEND_DATE[$from]))
				$content.='<p>Data wysyłki: <strong>'.$this->var->OST_SEND_DATE[$from].'</strong></p>';
			if(!empty($this->var->OST_SEND_CODE[$from]))
				$content.='<p>Numer wysyłki: <strong>'.$this->var->OST_SEND_CODE[$from].'</strong></p>';
			if(!empty($this->var->OST_SEND_FIRM[$from]))
				$content.='<p>Firma kurierska: <strong><a href="'.$this->kurier[$this->var->OST_SEND_FIRM[$from]].'">'.$this->var->OST_SEND_FIRM[$from].'</a></strong></p>';
			if(!empty($this->var->OST_SEND_PATH[$from]))
				$content.='<p>Link śledzenia wysyłki: <strong><a href="'.$this->var->OST_SEND_PATH[$from].'">'.$this->var->OST_SEND_PATH[$from].'</a></strong></p>';
				
			//positions
			if($this->var->OST_STATUS[$from]=='sended') {
				$this->db->queryString('select * from '.__BP.'order_positions');
				$this->db->where('and', "OPO_FROM='".$from."'");
				$this->db->where('and', 'OPO_ORD_ID='.$this->var->ORD_ID);
				
				$positions = $this->db->getDataFetch();
				
				if($positions) {
					$table = new table('dataTable', 'table table-striped stripe hover');
					$tableHeader = array('lp.', 'Nazwa', 'Ilość', 'Punkty');
					$table->createHeader($tableHeader);
					$i = 1;
					
					foreach($positions as $pos) {
						$value = (int)$pos['OPO_COUNT']*(int)$pos['OPO_POINTS'];
						$table->addCell($i.'.', 'lp');
						$table->addCell($pos['OPO_PRO_NAME']);
						$table->addCell($pos['OPO_COUNT'].' szt.');
						$table->addCell($value.' pkt +'.$pos['OPO_COUNT'].' zł');
						$table->addRow();
						$i++;
					}
					$content.= $table->createBody();
				}
			}
				
			$list = array(
				'__ORD_CONTENT' => $content,
				'__ORD_NUMER' => $this->var->ORD_ID,
				'__ORD_STATUS' => $this->status[$this->var->OST_STATUS[$from]],
			);
			
			$text = Template::parseFile($list, $this->data->localPath.$this->path.'statusChange.tpl.php');
			$this->sendInfo($user['USR_EMAIL'], $user['USR_PERSON_NAME'].' '.$user['USR_PERSON_SURNAME'], 'Zmiana statusu zamówienia nr '.$this->var->ORD_ID.' w programie BeaBonus', $text);
		}
		
		//generate wz document		
		if($from=='ex' and $this->var->OST_STATUS[$from]=='sended') {
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			$model->load('wz', 'generateDocument', $this->var->ORD_ID);
		}
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private function orderStatus($order=null, $from) {
		$select = new select('OST_STATUS_'.$from, 'OST_STATUS['.$from.']', 'form-control');
		foreach($this->status as $value=>$name) {
			if($order==$value)
				$select->addNode($name, $value, 'selected');
			else $select->addNode($name, $value);
		}
		return $select->create();		
	}
	
	private function getKurier($k, $from) {
		$select = new select('OST_SEND_FIRM_'.$from, 'OST_SEND_FIRM['.$from.']', 'form-control');
		$select->addNode('brak', null);
		foreach($this->kurier as $nazwa=>$link) {
			if($nazwa==$k)
				$select->addNode($nazwa, $nazwa, 'selected');
			else $select->addNode($nazwa, $nazwa);
		}
		return $select->create();		
	}
	
	public function sendInfo($mail, $name, $subject, $text) {
		$mailer = new Mailer;
		$mailer->subject = $subject;
		$mailer->mailText = $text;
		$mailer->sendMail($mail, $name);
	}
	
	//--DASHBOARD------------------------------------------------------------------------------------------
	
	public function dashboard() {
		$this->db->queryString('select ORD_ID, ORD_DATE_ADD, USR_NAME, ORD_POINTS from '.__BP.'orders');
		$this->db->join('left', __BP.'users', 'ORD_USR_ID', 'USR_ID');
		$this->db->where('and', 'ORD_STATUS=1');
		$this->db->orderBy('ORD_DATE_ADD desc');
		$this->db->limit(10);
		
		$orders = $this->db->getDataFetch();//$this->db->query();
		
		if($orders) {
			//$table = new table('dataTable', 'table table-striped stripe hover');
			$table = new table('dataTableWidget', 'table table-striped stripe hover');
			$tableHeader = array('Nazwa','Punkty', 'Data');
			$table->createHeader($tableHeader);
			foreach($orders as $order) {
				$link = 'admin.php?action=order&option=edit&id='.$order['ORD_ID'];
				$table->addLink($link, $order['USR_NAME'], null, null, 'Szczegóły');
				$table->addLink($link, $order['ORD_POINTS'], null, null, 'Szczegóły');
				$table->addLink($link, $order['ORD_DATE_ADD'], null, null, 'Szczegóły');
				$table->addRow();
			}
			$tabList = $table->createBody().'<div class="box-footer"><a class="btn btn-success" href="?action=orders">Pełna lista <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>';
		} else $tabList = $this->noData();
		$list = array(
			'__MODULE_TITLE'=>'Zamówienia - 10 ostatnich',
			'__MODULE_ICON'=>'shopping_cart',
			'__MODULE_CONTENT'=>$tabList,
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
}

function order($option=null) {
	$order = new order;
	if($option!=null and method_exists($order, $option))
		return $order->$option();
	else 
		return $order->showModule();
}