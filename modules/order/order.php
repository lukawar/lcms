<?php
class order extends View implements IModule {
	public $path = 'modules/order/templates/';
	public $statusList = array('new' => 'nowe', 'confirmed' => 'przyjęte', 'sended' => 'wysłane', 'closed' => 'zamknięte', 'deleted'=> 'usunięte');
	public $kurier = array('GLS' => 'https://gls-group.eu/PL/pl/sledzenie-paczek', 'DPD' => 'https://www.dpd.com.pl/');
	public function showModule() {
		return $this->sendAddress();
	}
	
	public function adres() {
		return $this->sendAddress();
	}
	
	public function sendAddress() {
		$list = array(
			'__ADDRESS_ADD' => $this->addLink(),
			'__ADDRESS_LIST' => $this->getSendAddressList(),
			'__CONTENT' => $this->getSendAddress(),
			'__CART_PATH' => helper::href(array('cart','check')),
			'__CART_TEXT' => $this->lang->cart_back, 
			'__ORDER_CONFIRM' => $this->lang->order_confirm,
		);
		return Template::parseFile($list, $this->data->localPath.$this->path.'address.tpl.php');
	}
	
	public function getSendAddress() {
		return null;
	}
	
	public function getFormSendAddress() {
		$sendAddress = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		return $sendAddress->load('user', 'formSendAddress');
	}
	
	private function getSendAddressList() {
		if(isset($this->var->id) and $this->var->id!=null)
			$addressId = $this->var->id;
		elseif(isset($this->var->order['address']) and $this->var->order['address']!=null)
			$addressId = $this->var->order['address'];
		else $addressId = null;
		
		$select = new select('userAddress', 'userAddress', 'form-controll');
		$this->db->queryString('select * from '.__BP.'user_address_send');
		$this->db->where('and', 'UAS_STATUS=1');
		$this->db->where('and', 'UAS_USR_ID='.$this->var->user_id);
		$user_address = $this->db->getDataFetch();
		if($user_address) 
			foreach($user_address as $ua)
				if($ua['UAS_ID']==$addressId)
					$select->addNode($ua['UAS_PERSON_NAME'].' '.$ua['UAS_PERSON_SURNAME'].', '.$ua['UAS_STREET'].' '.$ua['UAS_NUMBER'].', '.$ua['UAS_POSTCODE'].' '.$ua['UAS_CITY'], $ua['UAS_ID'], 'selected');
				else $select->addNode($ua['UAS_PERSON_NAME'].' '.$ua['UAS_PERSON_SURNAME'].', '.$ua['UAS_STREET'].' '.$ua['UAS_NUMBER'].', '.$ua['UAS_POSTCODE'].' '.$ua['UAS_CITY'], $ua['UAS_ID']);

		return $select->create();		
	}
	
	public function addressConfirm() {
		//$order = array('address' => $this->var->userAddress);
		$_SESSION['order']['address'] = $this->var->userAddress;
		
		header('location: '.helper::href(array('order', 'confirm')));		
	}
	
	public function confirm() {
		//cart data
		if(isset($this->var->cart) and !empty($this->var->cart)) {
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			
			$list = array(
				'__CART_POSITIONS' => $model->load('cart', 'cartPositions'),
				'__ORDER_SEND_ADDRESS' => $model->load('user', 'getSendAddress'),
			);
			$page = new Page;
			$page->setButton('<button type="button" class="btn btn-warning buttonUrl" url="'.helper::href(array('cart', 'check')).'"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> '.$this->lang->cart_edit.'</button>');
			$page->setSpacer();
			$page->setButton('<button type="button" class="btn btn-warning buttonUrl" url="'.helper::href(array('order', 'adres', $this->var->order['address'])).'"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> '.$this->lang->edit_send_address.'</button>');
			$page->setSpacer();
			$page->setSpacer();
			$page->setSpacer();
			$page->setSpacer();
			$page->setButton('<button type="button" class="btn btn-success buttonUrl" url="'.helper::href(array('order', 'save')).'">'.$this->lang->order_save.' <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>');
			$page->setToolbar();			
			return $page->setPage($list, 'cartConfirm.tpl.php');
		} else return $this->noData();
	}
	
	public function save() { 
		$recalculate = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		if($recalculate->load('cart', 'recalculateOrder')) {
			//save data order
			$orderData = (object)$this->var->order;
			$cartData = (object)$this->var->cart;
			$this->db->queryString(__BP.'orders');
			$insert = array(
				'ORD_USR_ID' => $this->var->user_id,
				'ORD_UAS_ID' => $orderData->address,
				'ORD_POINTS' => $cartData->valueAll,
				'ORD_POSITIONS' => $cartData->countAll,
				'ORD_ADDRESS' => $orderData->user_address,
			);
			
			$order = $this->db->insertQuery($insert);
			
			//save to orders_status_change
			//$this->db->queryString(__BP.'orders_status_change');
			//$this->db->insertQuery(array('OSC_ORD_ID' => $order));
			
			//table for mail
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Nazwa', 'Ilość', 'Punkty');
			$table->createHeader($tableHeader);
			$i = 1;
			
			$order_status = array();
			//save order positions
			$this->db->queryString(__BP.'order_positions');
			foreach($cartData->products as $position) {
				if(empty($order_status[$position['from']]['countAll']))
					$order_status[$position['from']]['countAll'] = $position['count'];
				else $order_status[$position['from']]['countAll']+= $position['count'];
				
				if(empty($order_status[$position['from']]['priceAll']))
					$order_status[$position['from']]['priceAll'] = $position['price'] * $position['count'];
				else $order_status[$position['from']]['priceAll']+= $position['price'] * $position['count'];
				
				$this->db->queryString(__BP.'order_positions');
				$insertPos = array(
					'OPO_ORD_ID' => $order,
					'OPO_USR_ID' => $this->var->user_id,
					'OPO_PRO_ID' => $position['id'],
					'OPO_PRO_ALIAS' => $position['alias'],
					'OPO_PRO_NAME' => $position['name'],
					'OPO_CAT_ID' => $position['cat'],
					'OPO_COUNT' => $position['count'],
					'OPO_POINTS' => $position['price'],
					'OPO_POINTS_TRUE' => $position['price_true'],
					'OPO_FROM' => $position['from'],
					'OPO_PROMOTION' => $position['promotion'],
				);
				
				$this->db->insertQuery($insertPos);
				
				$table->addCell($i.'.', 'lp');
				$table->addCell($position['name']);
				$table->addCell($position['count'].' szt.');
				$table->addCell($position['price'].' pkt +'.$position['count'].' zł');
				$table->addRow();
				$i++;
			}
			
			//save to order_status
			foreach($order_status as $from=>$ost) {
				$this->db->queryString(__BP.'orders_status');
				$orderp = array(
					'OST_ORD_ID' => $order,
					'OST_FROM' => $from,
					'OST_USR_ID' => $this->var->user_id,
					'OST_POINTS' => $ost['priceAll'],
					'OST_POSITIONS' => $ost['countAll'],
				);
				$this->db->insertQuery($orderp);
			}
			
			//recalculate user points
			$recalculate = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			$recalculate->load('user', 'recalculateAfterOrder');
			
			//resume
			$table->addCell(null);
			$table->addCell('Razem:', 'bolded tdright');
			$table->addCell($cartData->countAll.' szt.','bolded');
			$table->addCell($cartData->valueAll.' pkt +'.$cartData->countAll.' zł', 'bolded');
			$table->addRow();
			
			//send mail
			if($cartData->oneFrom)
				$fromInfo = $this->lang->send_from_one;
			else $fromInfo = $this->lang->send_from_two;
				
			$list = array(
				'__ORD_NUMER'	=> $order,
				'__ORD_DATE_ADD' => date('Y-m-d'),
				'__ORD_ADDRESS' => $orderData->user_address,
				'__ORDER' => $table->createBody(),
				'__FROM' => $fromInfo,
			);
			
			$text = Template::parseFile($list, $this->data->localPath.$this->path.'orderMail.tpl.php');
			$this->sendInfo($this->var->user_email, $this->var->user_person_full_name, 'Nowe zamówienie nr '.$order.' w programie BeaBonus', $text);
			
			//clear session
			unset($_SESSION['cart']);
			unset($_SESSION['order']);
			
			header('location: '.helper::href(array('order', 'saved')));
		} else header('location: '.helper::href(array('order', 'error')));
	}
	
	public function saved() {
		return Template::parseFile(array(), $this->data->localPath.$this->path.'save.tpl.php');
	}
	
	public function error() {
		return Template::parseFile(array(), $this->data->localPath.$this->path.'error.tpl.php');
	}
	
	public function getUserOrders() {
		$this->db->queryString('select * from '.__BP.'orders');
		$this->db->join('left', __BP.'orders_status', 'ORD_ID', 'OST_ORD_ID');
		$this->db->where('and', 'OST_STATUS<>5');
		$this->db->where('and', 'ORD_USR_ID='.$this->var->user_id);
		$orders = $this->db->getDataFetch();
		
		if($orders) {
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Numer', 'Punkty / Ilość', '&nbsp;', 'Data zamówienia', 'Status');
			$table->createHeader($tableHeader);
			$i = 1;
			foreach($orders as $order) {
				$table->addCell($i.'.', 'lp');
				$table->addLink($order['OST_ID'], $order['ORD_ID'], 'orderDetails');
				$table->addLink($order['OST_ID'], $order['OST_POINTS']." pkt. / ".$order['OST_POSITIONS']." szt.", 'orderDetails');
				$table->addLink($order['OST_ID'], '+&nbsp;'.$order['OST_POSITIONS']."&nbsp;zł", 'orderDetails');
				$table->addLink($order['OST_ID'], $order['ORD_DATE_ADD'], 'orderDetails');
				$table->addLink($order['OST_ID'], $this->statusList[$order['OST_STATUS']], 'orderDetails');
				$table->addRow();
				$i++;
			}

			return $table->createBody();
		} else {
			return Template::parseFile(array(), $this->data->localPath.$this->path.'noOrders.tpl.php');
		}
	}
	
	public function details() {
		//data from orders
		$this->db->queryString('select * from '.__BP.'orders');
		$this->db->join('left', __BP.'orders_status', 'ORD_ID', 'OST_ORD_ID');
		$this->db->where('and', 'ORD_USR_ID='.$this->var->user_id);
		$this->db->where('and', 'OST_ID='.(int)$this->var->id);
		$this->db->where('and', 'OST_STATUS!=5');
		
		$order = $this->db->getRow();
		
		//positions data
		$this->db->queryString('select * from '.__BP.'order_positions');
		$this->db->where('and', 'OPO_USR_ID='.$this->var->user_id);
		$this->db->where('and', 'OPO_ORD_ID='.$order['ORD_ID']);
		$this->db->where('and', 'OPO_FROM="'.$order['OST_FROM'].'"');
		$this->db->where('and', 'OPO_STATUS=1');
		
		//TODO: info o zmianach statusu
		
		$positions = $this->db->getDataFetch();
		
		if($order) {
			if($positions) {
				$table = new table('dataTable', 'table table-striped stripe hover');
				$tableHeader = array('lp.', 'Produkt', 'Punkty', 'Ilość', 'Wartość', '&nbsp;');
				$table->createHeader($tableHeader);
				$i = 1;
				foreach($positions as $position) {
					$table->addCell($i.'.', 'lp');
					$table->addCell($position['OPO_PRO_NAME']);
					$table->addCell($position['OPO_POINTS'].'&nbsp;pkt.');
					$table->addCell($position['OPO_COUNT'].'&nbsp;szt.');
					$table->addCell((int)$position['OPO_COUNT']*(int)$position['OPO_POINTS'].'&nbsp;pkt.');
					$table->addCell('+&nbsp;'.$position['OPO_COUNT'].'&nbsp;zł');
					$table->addRow();
					$i++;
				}
				//$data = $table->createBody();
				$ordData = null;
				
				if($order['OST_SEND_FIRM'])
					$ordData.='Kurier: <strong><a href="'.$this->kurier[$order['OST_SEND_FIRM']].'" target="_blank">'.$order['OST_SEND_FIRM'].'</a></strong><br/>';
				if($order['OST_SEND_CODE'])
					$ordData.='Numer wysyłki: <strong>'.$order['OST_SEND_CODE'].'</strong><br/>';
				if($order['OST_SEND_PATH'])
					$ordData.='Link śledzenia: <strong><a href="'.$order['OST_SEND_PATH'].'" target="_blank">link</a></strong><br/>';
				if($order['OST_SEND_DATE'])
					$ordData.='Data wysyłki: <strong>'.$order['OST_SEND_DATE'].'</strong><br/>';
				$list = array(
					'__POSITIONS' => $table->createBody(),
					'__ORD_ID' => $order['ORD_ID'],
					'__ORD_DATE_ADD' => $order['ORD_DATE_ADD'],
					'__ORD_STATUS' => $this->statusList[$order['OST_STATUS']],
					'__ORD_ADDRESS' => $order['ORD_ADDRESS'],
					'__ORD_DATA' => $ordData,
				);
				$data = Template::parseFile($list, $this->data->localPath.$this->path.'details.tpl.php');
			} else echo "position";//$data = $this->noData();
		} else echo "order";//$data = $this->noData();
		
		$details = array(
			'modalLabel' => 'ZAMÓWIENIE NR. '.$order['ORD_ID'],
			'responseText' => $data,
		);
		
		return json_encode($details);
	}
	
	public function sendInfo($mail, $name, $subject, $text) {
		$mailer = new Mailer;
		$mailer->subject = $subject;
		$mailer->mailText = $text;
		$mailer->sendMail($mail, $name);
	}
	
	private function addLink() {
		return Template::parseFile(['__TEXT'=>$this->lang->add_send_address], $this->data->localPath.$this->path.'addAddresButton.tpl.php');
	}
}