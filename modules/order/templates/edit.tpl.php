<div class="boxed flexed">
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="form-group formTitle"><h4>Dane użytkownika systemowe</h4></div>
		__USER_DATA_SYSTEM
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="form-group formTitle"><h4>Dane do wysyłki</h4></div>
		__USER_DATA_SEND
	</div>
</div>

<div class="form-group formTitle"><h4>Dane zamówienia</h4></div>
<div class="form-group labeled">
	<label for="ORD_SEND_CODE" class="col-xs-12 col-md-2 control-label">Numer zamówienia: </label>
	<div class="col-xs-12 col-md-4"><strong>__ORDER_NUMBER</strong></div>

	<label for="ORD_SEND_CODE" class="col-xs-12 col-md-2 control-label">Data zamówienia: </label>
	<div class="col-xs-12 col-md-4"><strong>__ORDER_DATE</strong></div>
</div>

<div class="form-group labeled">
	<label for="ORD_SEND_CODE" class="col-xs-12 col-md-2 control-label">Wartość: </label>
	<div class="col-xs-12 col-md-4"><strong>__ORDER_VALUE</strong></div>
	
	<label for="ORD_SEND_CODE" class="col-xs-12 col-md-2 control-label"> </label>
	<div class="col-xs-12 col-md-4"><strong></strong></div>
</div>

__ORDER_BOX_STATUS
<input type="hidden" name="action" value="order"/>
<input type="hidden" name="option" value="editSave"/>
<input type="hidden" name="ORD_ID" value="__ORD_ID"/>
<input type="hidden" name="ORD_USR_ID" value="__ORD_USR_ID"/>
<input type="hidden" name="userNotification[ex]" id="userNotification_ex" value="false"/>
<input type="hidden" name="userNotification[bb]" id="userNotification_bb" value="false"/>
<input type="hidden" name="actionType" id="actionType" value="saveBack"/>

<script>
$(document).ready(function() {
	$('#modalBody').html('__MODAL_INFO');
	__MODAL_SHOW
});

$(document).on('click','button',function(){
	var actionType = $(this).attr('id');
	$('#actionType').val(actionType);
});

$(document).on('submit','form',function(){
	if($('#OST_STATUS_bb').val()!=$('#OLD_OST_STATUS_bb').val() || $('#OST_STATUS_ex').val()!=$('#OLD_OST_STATUS_ex').val()) {
		if(confirm("Zmieniono status zamówienia - czy wysłać użytkownikowi powiadomienie?")) {
			if($('#OST_STATUS_bb').val()!=$('#OLD_OST_STATUS_bb').val()) {
				$('#userNotification_bb').val('send');
			}
			if($('#OST_STATUS_ex').val()!=$('#OLD_OST_STATUS_ex').val()) {
				$('#userNotification_ex').val('send');
			}
		}
	}
});
</script>