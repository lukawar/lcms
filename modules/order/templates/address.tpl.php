<h3 class="block_title">Zamówienie - Adres dostawy</h3>
<div class="container">
	<div class="row">
		<div class="col-md-12"><h3>Wybierz adres dostawy</h3></div>
	</div>
	<form name="setSendAddres" action="index.php" method="post">
		<div class="row">
			<div class="col-md-8">__ADDRESS_LIST</div>
			<div class="col-md-4">__ADDRESS_ADD</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-primary buttonUrl" url="__CART_PATH">__CART_TEXT</button>
				<input type="submit" class="btn btn-success" value="__ORDER_CONFIRM">
			</div>
		</div>
		<input type="hidden" name="action" value="order">
		<input type="hidden" name="option" value="addressConfirm">
	</form>
	<div class="row">
		<div class="col-md-12" id="orderContent">__CONTENT</div>
	</div>
</div>
