<div class="form-group formTitle"><h4>Pozycje zamówienia</h4></div>
<div class="form-group labeled">__ORDER_POSITIONS</div>

<div class="form-group formTitle"><h4>Informacje o wysyłce</h4></div>

<div class="form-group labeled">
	<label for="OST_SEND_CODE" class="col-xs-12 col-md-2 control-label">Status: </label>
	<div class="col-xs-12 col-md-4">__ORDER_STATUS</div>
</div>

<div class="form-group labeled">
	<label for="OST_SEND_CODE___OST_FROM" class="col-xs-12 col-md-2 control-label">Numer wysyłki: </label>
	<div class="col-xs-12 col-md-4"><input type="text" name="OST_SEND_CODE[__OST_FROM]" id="OST_SEND_CODE___OST_FROM" value="__OST_SEND_CODE" class="form-control" __REQUIRED></div>

	<label for="OST_SEND_PATH___OST_FROM" class="col-xs-12 col-md-2 control-label">Link śledzenia: </label>
	<div class="col-xs-12 col-md-4"><input type="text" name="OST_SEND_PATH[__OST_FROM]" id="OST_SEND_PATH___OST_FROM" value="__OST_SEND_PATH" class="form-control"></div>
</div>

<div class="form-group labeled">
	<label for="OST_SEND_FIRM___OST_FROM" class="col-xs-12 col-md-2 control-label">Kurier: </label>
	<div class="col-xs-12 col-md-4">__OST_SEND_FIRM</div>

	<label for="OST_SEND_DATE" class="col-xs-12 col-md-2 control-label">Data wysyłki: </label>
	<div class="col-xs-12 col-md-4"><input type="text" name="OST_SEND_DATE[__OST_FROM]" id="OST_SEND_DATE___OST_FROM" value="__OST_SEND_DATE" class="form-control" __REQUIRED></div>
</div>
<input type="hidden" name="TYPE___OST_FROM" value="true"/>
<input type="hidden" name="OST_ID[__OST_FROM]" value="__OST_ID"/>
<input type="hidden" name="OLD_OST_STATUS[__OST_FROM]" id="OLD_OST_STATUS___OST_FROM" value="__OLD_OST_STATUS"/>