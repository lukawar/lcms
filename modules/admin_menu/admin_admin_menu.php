<?php
class admin_menu extends View {
	public $path = 'modules/admin_menu/templates/';
	
	public function showModule() {
		$role = $this->var->admin_role;
		return $this->$role();
	}
	
	//menu structure for ph
	private function user() {
		if($this->var->admin_id) {
			$result = null;
			$this->db->queryString("select * from ".__BP."admin_roles where ADR_ADM_ID=".$this->var->admin_id);
			$admin_roles = $this->db->getDataFetch();
			
			if($admin_roles) {
				foreach($admin_roles as $ar) {
					if(file_exists($this->data->localPath.$this->data->modulesPath.$ar['ADR_ROLE_ID']."/config.php"))  {
		    		include($this->data->localPath.$this->data->modulesPath.$ar['ADR_ROLE_ID']."/config.php");
		    		$icon = $menu['icon'][0];
		    	} else $icon = 'forward';
		    	
					$list = array(
							'__LINK' => 'admin.php?action='.$ar['ADR_ROLE_ID'],
							'__CLASS_LINK' =>  $ar['ADR_ROLE_ID']==$this->var->action?'active':'normal',
							'__CLASS_ICON' => $icon,
							'__TEXT' => $ar['ADR_NAME'],
						);
					
					 $result.= Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
				}
				return '<ul class="adminMenu">'.$result."</ul>";;
			} else return null;
		}
	}
	
	//menu structure for admin
	private function admin() {
		//add admin options 
		$line = null;
	
		$list = array(
			'__LINK' => 'admin.php?action=options&option=system',
			'__CLASS_LINK' => 'normal',
			'__CLASS_ICON' => 'settings',
			'__TEXT' => 'Konfiguracja',
		);
	 
	 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
	 unset($list);
		
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu) and $menu['show'][0]==true) {
		    	$list = array(
							'__LINK' => 'admin.php?action='.$menu['action'][0],
							'__CLASS_LINK' => $menu['action'][0]==$this->var->action?'active':'normal',
							'__CLASS_ICON' => $menu['icon'][0],
							'__TEXT' => $menu['title'][0],
						);
					 
					 //$result[$menu['order'][0]] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
		    }
		  }
		}
		
		ksort($result);
		foreach($result as $key=>$value)
			$line.=$value;
		return '<ul class="adminMenu">'.$line."</ul>";
	}
	
	public function getForRole() {
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		
		$result = "<div class='list-group'>\n";
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu)) {
		    	$result.="<a href='?action=".$menu['action'][0]."' id='".$menu['action'][0]."' class='list-group-item'>".$menu['title'][0]."</a>\n";
		    }
		  }
		}
		$result.= "</div>";
		return $result;
	}
}

function admin_menu() {
	$admin_menu = new admin_menu;
	return $admin_menu->showModule();
}