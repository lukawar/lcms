<?php
class wz extends View implements iModule {
	public $path = 'modules/wz/templates/'; //path to templates for admin
	public $filesPath = 'files/wz/';
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'wz');
			$this->db->orderBy('WZD_DATE_ADD desc');
			
			$wzs = $this->db->getDataFetch();
			
			if($wzs) {
				$i = 1;
				$table = new table('dataTable', 'table table-striped stripe hover');
				$tableHeader = array('lp.', 'Zamówienie', 'Data wygenerowania', '');
				$table->createHeader($tableHeader);
				
				foreach($wzs as $wz) {
					$table->addCell($i, 'lp');
					$table->addCell($wz['WZD_ORD_ID']);
					$table->addCell($wz['WZD_DATE_ADD']);
					$table->addButton('download', SC_ROOT_URL.$wz['WZD_PATH'], 'info', '_blank');
					$table->addRow();
					$i++;
				}
					$tableBody = $table->createBody();
			} else $tableBody = $this->noData();
			
			$list = array(
				'__GLYPHICON' => 'description',
				'__HEAD' => 'DOKUMENTY WZ',
				'__CONTENT'=> $tableBody
			);
			
			$page = new Page;
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function generateDocument($order) {
		require_once($this->data->localPath.'system/lib/dompdf/vendor/autoload.php');
		
		
		$options = new Dompdf\Options;
		$options->set('defaultFont', 'dejavu sans');
		$options->set('fontHeightRatio', 1);
		$options->set('isRemoteEnabled', TRUE);
		
		$dompdf = new Dompdf\Dompdf($options);
		
		$contxt = stream_context_create([ 
    'ssl' => [ 
        'verify_peer' => FALSE, 
        'verify_peer_name' => FALSE,
        'allow_self_signed'=> TRUE
		    ] 
		]);
		$dompdf->setHttpContext($contxt);
		//$html = $this->prepareView();
		$dompdf->loadHtml($this->prepareView($order));

		$dompdf->setPaper('A4', 'portrait');

		$dompdf->render();
		
		//$dompdf->stream();
		$output = $dompdf->output();
		
		$catalog = $this->data->localPath.$this->filesPath.md5(microtime()).'/';
		$filename = 'wz_'.$order.'_'.date('Ymd').'.pdf';
		if(!file_exists($catalog))
				if (!mkdir($catalog, 0777, true))
	    		die('Failed to create folders...');
		
		file_put_contents($catalog.$filename, $output);
		
		$this->db->queryString(__BP.'wz');
		$this->db->insertQuery(['WZD_ORD_ID'=>$order,'WZD_PATH'=>$catalog.$filename]);
		
    //Mailer
    $list = array(
			'__ORDER_ID' => $order,
			'__FILE_PATH' => SC_ROOT_URL.$catalog.$filename,
		);
		
		$text = Template::parseFile($list, $this->data->localPath.$this->path.'wzInfo.tpl.php');
		$mailer = new Mailer;
		$mailer->subject = 'BeaBonus - wygenerowano dokument WZ';
		$mailer->mailText = $text;
		$mailer->sendMail(__ADMIN_MAIL, __ADMIN_NAME);
	}
	
	public function prepareView($id) {
		//order data
		$this->db->queryString('select * from '.__BP.'orders');
		$this->db->join('left', __BP.'orders_status', 'ORD_ID','OST_ORD_ID', "OST_FROM='ex'");
		$this->db->where('and', 'ORD_ID='.$id);
		//$this->db->where('and', 'ORD_STATUS=3');
		//$this->db->prepareQuery();$this->db->query();
		$order = $this->db->getRow();
		
		//order positions
		$this->db->queryString('select * from '.__BP.'order_positions');
		$this->db->join('left', __BP.'products', 'OPO_PRO_ID', 'PRO_ID');
		$this->db->where('and', 'OPO_ORD_ID='.$id);
		$this->db->where('and', "OPO_FROM='ex'");
		//$this->db->prepareQuery();$this->db->query();
		
		$positions = $this->db->getDataFetch();
		
		if($positions) {
			$line = null;
			$sum = 0;
			foreach($positions as $pos) {
				$value = (int)$pos['PRO_PRICE']*(int)$pos['OPO_COUNT'];
				$sum+=$value;
				$line.='<tr><td>'.$pos['PRO_EAN'].'</td><td>'.$pos['PRO_NAME'].'</td><td>'.$pos['OPO_COUNT'].'</td><td>szt.</td><td>'.$pos['OPO_COUNT'].'</td><td>'.$pos['PRO_PRICE'].'</td><td>'.$value.'</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
			}
			$line.='<tr><td colspan="6" style="text-align: right;font-weight: bold;">Razem:</td><td style="text-align: right;font-weight: bold;">'.$sum.'</td><td colspan="2">&nbsp;</td></tr>';
		}
		
		$list = array(
			'__POSITIONS' => $line,
			'__DATE_TODAY' => date('Y-m-d'),
			'__ORDER_FIRM' => $order['OST_SEND_FIRM'],
			'__DATE_SEND' => $order['OST_SEND_DATE'],
			'__ORDER_ID' => $order['ORD_ID'],
			'__ORDER_DATE' => $order['ORD_DATE_ADD'],
		);
		return Template::parseFile($list, $this->data->localPath.$this->path.'template.tpl.php');
	}
}

function wz($option=null) {
	$wz = new wz;
	if($option!=null and method_exists($wz, $option))
		return $wz->$option();
	else 
		return $wz->showModule();
}