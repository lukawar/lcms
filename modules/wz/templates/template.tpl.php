<html>
<head>   
<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>
<style>
	table {
		border-collapse: collapse;
		border: 0;
		margin: 0;
		padding: 0;
		width: 100%;
		font-size: 7pt;
	} 
	
	table td {
		padding: 0;
		margin: 0;
	}
	.bordered td {
		border-collapse: collapse;
		border: 1px solid #000;
		vertical-align: top;
    padding: 3px;
	}
	
	.lightHeader {
		background: #ececec;
		font-weight: bold;
		text-align: center;
	}
	
	.darkHeader {
		background: #c2c2c2;
		font-weight: bold;
		text-align: center;
	}
	
	.midHeader {
		background: #dfdfdf;
		text-align: center;
	}
	
	.midHeader span {font-size: 30pt;}
	
	.stamp {
		padding: 10px 0 0 1px;
	}
	
</style>
</head>
<body>
<table class="container">
	<tr><td><table class="tHeader bordered">
		<tr>
			<td colspan="2" rowspan="4">Pieczęć firmowa<div class="stamp"><img src="http://beabonus.beabeleza.pl/templates/default/img/expansja_stamp.png"/></div></td>
			<td colspan="2" rowspan="4">Odbiorca</td>
			<td rowspan="4" class="midHeader" width="10%"><span>WZ</span><br/>Wydanie materiałów / towarów na zewnątrz</td>
			<td class="lightHeader">Nr bieżący</td>
			<td class="lightHeader">Egz.</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>			
		</tr>
		<tr>
			<td class="lightHeader">Nr magazynowy</td>
			<td class="lightHeader">Data wystawienia</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>__DATE_TODAY</td>			
		</tr>
		<tr>
			<td class="lightHeader">Środek transportu</td>
			<td class="lightHeader">Zamówienie</td>
			<td class="lightHeader">Przeznaczenie</td>
			<td class="lightHeader">Data wysyłki</td>
			<td class="lightHeader">Wysyłka na koszt</td>
			<td class="lightHeader" colspan="2">Numer i data faktury - specyfikacji</td>
		</tr>
		<tr>
			<td>__ORDER_FIRM</td>
			<td>__ORDER_ID</td>
			<td>&nbsp;</td>
			<td>__DATE_SEND</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table></td></tr>
	<tr><td><table class="tContent bordered">
	<tr>
		<td class="darkHeader" rowspan="2" width="10%">Kod towaru - materiału</td>
		<td class="darkHeader" rowspan="2">Nazwa materiału / towaru / opakowania</td>
		<td class="darkHeader" colspan="3">ILOŚĆ</td>
		<td class="darkHeader" rowspan="2" width="5%">Cena</td>
		<td class="darkHeader" rowspan="2" width="5%">Wartość</td>
		<td class="darkHeader" rowspan="2" width="5%">Konto<br/>syntet.</td>
		<td class="darkHeader" rowspan="2" width="5%">Zapas<br/>Ilość</td>
	</tr>
	<tr>
		<td class="darkHeader" width="5%">Zadysponowana</td>
		<td class="darkHeader" width="5%">J.m.</td>
		<td class="darkHeader" width="5%">Wydana</td></tr>
	__POSITIONS
	</table></td></tr>
	<tr><td><table class="tFooter bordered">
	<tr>
		<td class="lightHeader">Wystawił</td>
		<td class="lightHeader">Zatwierdził</td>
		<td class="darkHeader" colspan="3">WYMIENIONE ILOŚCI</td>
		<td class="lightHeader">Ewidencja ilościowo-wartościowa</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="text-align: center;">Wydał<br/><br/><br/><br/></td>
		<td style="text-align: center;">data<br/><br/><br/><br/></td>
		<td style="text-align: center;">Odebrał<br/><br/><br/><br/></td>
		<td>&nbsp;</td>		
	</tr>
	</table></td></tr>
</table>
</body>
</html>