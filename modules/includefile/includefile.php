<?php
class includefile extends view {
	public $optionValue;
	public function showModule() {
		if(file_exists($this->data->localPath.$this->data->templatePath.$this->optionValue)) {
			$content = file_get_contents($this->data->localPath.$this->data->templatePath.$this->optionValue);
			$parsedContent = $this->parseContent($content);
			if($parsedContent!=null)
				return $parsedContent;
			else return $content;
		} else return null;
	}
}