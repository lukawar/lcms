<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=produkty">Produkty</a></li>
			<li><a href="#?action=produkty&option=addProduct">Dodaj nowy produkt</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-suitcase"></i>
					<span>Nowy produkt</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Wpisz dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="box-content" id="accordion">
				<h3>Wersja PL</h3>
				<div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Nazwa (pl)</label>
						<div class="col-sm-5">
							<input type="text" class="form-control"  name="PRO_NAME" required="required"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Krótki opis</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_SHORT"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Opis</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_LONG"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Szczegóły</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DETAILS"></textarea>
						</div>
					</div>
					
				</div>
				<h3>Wersja EN</h3>
				<div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Nazwa (en)</label>
						<div class="col-sm-5">
							<input type="text" class="form-control"  name="PRO_NAME_EN"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Krótki opis (en)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_SHORT_EN"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Opis (en)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_LONG_EN"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Szczegóły (en)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DETAILS_EN"></textarea>
						</div>
					</div>
					
				</div>
				<h3>Wersja FR</h3>
				<div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Nazwa (fr)</label>
						<div class="col-sm-5">
							<input type="text" class="form-control"  name="PRO_NAME_FR"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Krótki opis (fr)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_SHORT_FR"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Opis (fr)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DESC_LONG_FR"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Szczegóły (fr)</label>
						<div class="col-sm-8">
							<textarea class="form-control"  name="PRO_DETAILS_FR"></textarea>
						</div>
					</div>
					
				</div>
				
			</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Kategoria</label>
					<div class="col-sm-5">
						__CATEGORIES
					</div>
				</div>			
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5">
						<select name="PRO_STATUS">
							<option value="nowy">nowy</option>
							<option value="aktywny">aktywny</option>
							<option value="nieaktywny">nieaktywny</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="produkty">
				<input type="hidden" name="option" value="addProductSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" class="btn btn-secondary cancelUrl" url="?action=produkty">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	var icons = {
		header: "ui-icon-circle-arrow-e",
		activeHeader: "ui-icon-circle-arrow-s"
	};
	$("#accordion").accordion({icons: icons });
	WinMove();
});
</script>