<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=produkty">Produkty</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-suitcase"></i>
					<span>Lista kategorii</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="optionsBar"><a href="?action=produkty&option=addCategory" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nową kategorię</a></div>
				<div id="contentPlace">__TABLE_CATEGORY</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-suitcase"></i>
					<span>Lista produktów</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="optionsBar"><a href="?action=produkty&option=addProduct" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy produkt</a></div>
				<div id="contentPlace">__TABLE_PRODUCT</div>
			</div>
		</div>
	</div>
</div>