<section class="sidebar-search-view" id="search-module">
	<h3 class="block_title block_title_narr">SZUKAJ</h3>
	<form name="search" action="index.php" method="post">
		<p><input type="string" class="form-control search-input" name="searchString" placeholder="szukaj ..." value="__SEARCH_STRING" required></p>
		<p><button class="btn btn-success" type="submit">szukaj</button></p>
		<input type="hidden" name="action" value="category">
		<input type="hidden" name="option" value="searchProduct">
	</form>
	<p><a href="__SORT_PRICE_GROW" class="sortUrl" by="price" type="asc">cena rosnąco</a> | <a href="__SORT_PRICE_LOW" class="sortUrl" by="price" type="desc">cena malejąco</a></p>
	<p><a href="__SORT_NAME_GROW" class="sortUrl" by="name" type="asc">A-Z</a> | <a href="__SORT_NAME_LOW" class="sortUrl" by="name" type="desc">Z-A</a></p>
</section>