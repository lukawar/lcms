<h3 class="block_title">__CAT_NAME</h3>
<div class="row single-product">
	<div class="col-md-6" id="productImage">__PRO_FOTO</div>
	
	
	<div class="col-md-6">
		__PRODUCT_TYPE
		<h3>__PRO_NAME</h3>
		<p class="__FLAG_CLASS koszt-zakupu">koszt zakupu <strong>__PRO_POINTS</strong> pkt. <strong>__ZLOTOWKA</strong></p>
		__PRODUCT_NEW_POINTS
		<p class="cart-adding-containter"><input type="number" id="productCount" width="20px" value="1" min="1"> <button type="button" id="addToCart" class="btn btn-primary" product="__PRO_ID"><i class="material-icons">shopping_cart</i> do koszyka</button></p>
		<hr>
		<p class="chest-container" id="chest-button">__CHEST</p>
	</div>
</div>
<div class="row">__PRO_THUMBNAILS</div>

<p>&nbsp;</p>
<div class="row">
	<div class="col-md-12"><p>__PRO_DESC_LONG</p></div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>

<div class="modal fade" id="modalWindow" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="modalLabel">Koszyk</h5>
      </div>
      <div class="modal-body" id="modalBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary buttonUrl" url="__CART_PATH">Zobacz koszyk</button> <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>