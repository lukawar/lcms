<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=produkty">Produkty</a></li>
			<li><a href="#?action=produkty&option=editProduct">edycja</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-suitcase"></i>
					<span>Edycja</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Wpisz dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
					<div class="box-content" id="accordion">
					<h3>Wersja PL</h3>
					<div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Nazwa (pl)</label>
							<div class="col-sm-5">
								<input type="text" class="form-control"  name="PRO_NAME" value="__PRO_NAME" required="required"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Krótki opis</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_SHORT">__PRO_DESC_SHORT</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Opis</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_LONG">__PRO_DESC_LONG</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Szczegóły</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DETAILS">__PRO_DETAILS</textarea>
							</div>
						</div>
						
					</div>
					<h3>Wersja EN</h3>
					<div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Nazwa (en)</label>
							<div class="col-sm-5">
								<input type="text" class="form-control"  name="PRO_NAME_EN" value="__PRO_EN_NAME"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Krótki opis (en)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_SHORT_EN">__PRO_EN_DESC_SHORT</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Opis (en)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_LONG_EN">__PRO_EN_DESC_LONG</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Szczegóły (en)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DETAILS_EN">__PRO_EN_DETAILS</textarea>
							</div>
						</div>
						
					</div>
					<h3>Wersja FR</h3>
					<div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Nazwa (fr)</label>
							<div class="col-sm-5">
								<input type="text" class="form-control"  name="PRO_NAME_FR" value="__PRO_FR_NAME"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Krótki opis (fr)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_SHORT_FR">__PRO_FR_DESC_SHORT</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Opis (fr)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DESC_LONG_FR">__PRO_FR_DESC_LONG</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Szczegóły (fr)</label>
							<div class="col-sm-8">
								<textarea class="form-control"  name="PRO_DETAILS_FR">__PRO_FR_DETAILS</textarea>
							</div>
						</div>
						
					</div>
					
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Alias</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="PRO_ALIAS" required="required" value="__PRO_ALIAS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Kategoria</label>
					<div class="col-sm-5">
						__CATEGORIES
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5">
						__STATUS
					</div>
				</div>
				
				<input type="hidden" name="action" value="produkty">
				<input type="hidden" name="option" value="editProductSave">
				<input type="hidden" name="PRO_ID" value="__PRO_ID">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" class="btn btn-secondary cancelUrl" url="?action=produkty">Zamknij</button>
					</div>
				</div>
				</form>	
				<h4 class="page-header">Pliki i załączniki</h4>
				<form name="adminFiles" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group" style="display: none;">
					<label class="col-sm-3 control-label">Obraz</label>
					<div class="col-sm-9">__FILE_NAME</div>
					<div class="col-sm-3"></div>
					<div class="col-sm-9"><input type="file" name="PRO_FILENAME_"></div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Załączniki</label>
					<div class="col-sm-9">
						__FILES
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-3 control-label">Dodaj nowy</label>
					<div class="col-sm-9"><input type="file" name="PRO_FILENAME"></div>
				</div>
				
				<input type="hidden" name="action" value="produkty">
				<input type="hidden" name="option" value="filesSave">
				<input type="hidden" name="PRO_ID" value="__PRO_ID">
				<input type="hidden" name="PRO_CAT_ID" value="__PRO_CAT_ID">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" class="btn btn-secondary cancelUrl" url="?action=produkty">Zamknij</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	var icons = {
		header: "ui-icon-circle-arrow-e",
		activeHeader: "ui-icon-circle-arrow-s"
	};
	$("#accordion").accordion({icons: icons });
	WinMove();
});
</script>