<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=produkty">Produkty</a></li>
			<li><a href="#?action=produkty&option=editCategory"></a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-suitcase"></i>
					<span>Edycja</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Wpisz dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa (pl)</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="CAT_NAME" required="required" value="__CAT_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa (en)</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="CAT_NAME_EN" value="__CAT_EN_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa (fr)</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="CAT_NAME_FR" value="__CAT_FR_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Alias</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="CAT_ALIAS" required="required" value="__CAT_ALIAS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nadkategoria</label>
					<div class="col-sm-9">__CAT</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5">
						__CAT_STATUS
					</div>
				</div>
				
				<input type="hidden" name="action" value="produkty">
				<input type="hidden" name="option" value="editCategorySave">
				<input type="hidden" name="id" value="__CAT_ID">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" class="btn btn-secondary" id="cancelUrl" url="?action=produkty">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>