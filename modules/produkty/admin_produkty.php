<?php
class produkty extends View implements IModule {
	public $statusList = array('active'=>'aktywny', 'inactive'=>'ukryty', 'deleted'=>'usunięty');
	public $imgPath = 'files/product/';
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			return $this->getData(null);
		} else return $this->goAway();
	}
	
	public function getData($type) {
		$this->db->queryString('select * from '.__BP.'products');
			$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'category', 'CAT_ID', 'CAT_ID');
			
			if($type=='promocje') {
				$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID');
				$this->db->where('and', 'PPR_STATUS<>4');
			}
			
			if($type=='nowosci') {
				$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID');
				$this->db->where('and', 'PNE_STATUS<>3');
			}
			
			if($type=='bb') 
				$this->db->where('and', 'PRO_FROM="bb"');
				
			$products = $this->db->getDataFetch();
			
			if($products)	{
				$table = new table('dataTable', 'table table-striped stripe hover');
				$tableHeader = array('lp.', 'Kategoria', 'Nazwa', 'Kod', 'Kod BB', 'Status', '');
				$table->createHeader($tableHeader);
				
				$i = 1;
				foreach($products as $product) {
					$table->addCell($i, 'lp');
					$table->addCell($product['CAT_NAME']);
					$table->addCell($product['PRO_NAME']);
					$table->addCell($product['PRO_CODE']);
					$table->addCell($product['PRO_EAN']);
					$table->addCell($this->statusList[$product['PRO_STATUS']]);
					//$table->addLink('admin.php?action=produkty&option=edit&id='.$product['PRO_ID'], 'edycja',null, null, 'edycja');
					$table->addButton('edit', 'admin.php?action=produkty&option=edit&id='.$product['PRO_ID'], 'primary');
					$table->addRow();
					$i++;
					
				}
				$table = $table->createBody();
			} else $table = $this->noProduct();
			
			$list = array(
				'__GLYPHICON' => 'card_giftcard',
				'__HEAD' => 'LISTA PRODUKTÓW',
				'__CONTENT'=> $table
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('produkty','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> nowy produkt</a>');
			$page->setToolbar();
			return $page->setPage($list);
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('produkty', 'addSave', 'Wypełnij dane');
			$form->addText('Nazwa', 'PRO_NAME', 'required');
			$form->addText('Kod produktu', 'PRO_CODE', 'required');
			$form->addSelect('Kategoria', 'PRO_CAT_ID', $this->getCategory() , null);
			$form->addText('Cena zakupu', 'PRO_PRICE', 'required');
			$form->addText('Punkty', 'PRO_POINTS', 'required');
			$form->addInput('Opis skrócony', 'PRO_DESC_SHORT', null, 'textarea.tpl.php');
			$form->addInput('Pełny opis', 'PRO_DESC_LONG', null, 'textarea.tpl.php');
			$form->addInput('Zdjęcie', 'PRO_FOTO', null, 'file.tpl.php');
			$form->addSelect('Status', 'PRO_STATUS', $this->getStatus() , null);
			
			$form->addElement('<div class="form-group formTitle"><h4> Nowość</h4></div>');
			$form->addCheckbox('Aktywny', 'PNE_ID_ACTIVE', null);
			$form->addInput('Data rozpoczęcia', 'PNE_DATE_FROM', null, 'datepicker.tpl.php');
			$form->addInput('Data zakończnia', 'PNE_DATE_TO', null, 'datepicker.tpl.php');
			$form->addSelect('Typ wyświetlania', 'PNE_STATUS', array('all'=>'ciągły', 'bydate'=>'po dacie'));
			
			$form->addElement('<div class="form-group formTitle"><h4> Promocja</h4></div>');
			$form->addCheckbox('Aktywny', 'PPR_ID_ACTIVE', null);
			$form->addText('Wartość promocji', 'PPR_VALUE', null);
			$form->addSelect('Przeliczanie', 'PPR_TYPE', array('percent'=>'procentowe', 'minus'=>'odejmij od ceny', 'value'=>'wartość'));
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj nowy produkt',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'card_giftcarde',
			);
			
			$page->setSubmit();
			$page->setCancel($this->var->action);
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$insert = array(
				'PRO_NAME' => $this->var->PRO_NAME,
				'PRO_ALIAS' => coder::stripPL($this->var->PRO_NAME),
				'PRO_STATUS' => $this->var->PRO_STATUS,
				'PRO_DESC_LONG' => addslashes($this->var->PRO_DESC_LONG),
				'PRO_DESC_SHORT' => addslashes($this->var->PRO_DESC_SHORT),
				'PRO_CODE' => $this->var->PRO_CODE,
				'PRO_PRICE' => $this->var->PRO_PRICE,
				'PRO_POINTS' => $this->var->PRO_POINTS,
			);
			
			$this->db->queryString(__BP.'products');
			$id = $this->db->insertQuery($insert);
			
			//product news
			if(!empty($this->var->PNE_ID_ACTIVE)) {
				$this->db->queryString(__BP.'products_news');
				$insert = array(
					'PNE_PRO_ID' => $id,
					'PNE_DATE_FROM' => $this->var->PNE_DATE_FROM,
					'PNE_DATE_TO' => $this->var->PNE_DATE_TO,
					'PNE_STATUS' => $this->var->PNE_STATUS,
				);
				$this->db->insertQuery($insert);
			}
			
			//product promotion
			if(!empty($this->var->PPR_ID_ACTIVE)) {
				$this->db->queryString(__BP.'products_promotions');
				$insert = array(
					'PPR_PRO_ID' => $id,
					'PPR_VALUE' => $this->var->PPR_VALUE,
					'PPR_STATUS' => $this->var->P_STATUS,
				);
				$this->db->insertQuery($insert);
			}
			
			//category
			$insert_cat = array(
				'CAT_ID' => $this->var->PRO_CAT_ID,
				'PRO_ID' => $id
			);
			
			$this->db->queryString(__BP.'pro_cat');
			$id = $this->db->insertQuery($insert_cat);
			
			//update product count and generate category tree
			$this->db->queryString('update '.__BP.'category set CAT_COUNT=(select count(pro.PRO_ID) from '.__BP.'pro_cat prc left join '.__BP.'products pro using(PRO_ID) where prc.CAT_ID='.$this->var->PRO_CAT_ID.' and pro.PRO_STATUS=1) where CAT_ID='.$this->var->PRO_CAT_ID);
			
			$this->db->execQuery();
			
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			$model->load('category', 'generateCategoryTree');
			
			//save event 
			eventSaver::add($this->var->admin_id, 'add produkt - '. $this->var->PRO_NAME, 'admin', 'produkty', 'add', $id);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'produkty', $id, 'add', $this->var->PRO_NAME, addslashes($this->var->PRO_DESC_LONG), addslashes($this->var->PRO_DESC_SHORT), $this->var->PRO_POINTS, 'status: '.$this->var->PRO_STATUS);
			
			//header('location: admin.php?action=produkty&option=editProduct&id='.$id);
			header('location: admin.php?action=produkty');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'products');
			$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID');
			$this->db->where('and', 'PRO_ID='.$this->var->id);
			//$this->db->where('and', 'PRO_FROM="bb"');
			$produkt = $this->db->getRow();
			
			if($produkt) {
				//other images
				$this->db->queryString('select * from '.__BP.'pro_foto');
				$this->db->where('and', 'PFO_PRO_ID='.$this->var->id);
				$this->db->where('and', 'PFO_STATUS=1');
				$photos = $this->db->getDataFetch();
				
				$photoContainer = null;
				if($photos) {
					foreach($photos as $photo) 
						$photoContainer.='<div class="col-sm-3" id="photo-'.$photo['PFO_ID'].'"><div><img src="'.$this->data->localPath.$this->imgPath.$produkt['PRO_CODE'].'/thumb2_'.$photo['PFO_FILE'].'" class="img-responsive"></div><div><a class="foto-links" href="'.$photo['PFO_ID'].'">usuń</a></div></div>';
				}
				
				$photoContainer = '<div class="row">'.$photoContainer.'</div>';
				
				if($produkt['PRO_FOTO']!='')
					$filename = '<img src="'.$this->data->localPath.$this->imgPath.$produkt['PRO_CODE'].'/thumb2_'.$produkt['PRO_FOTO'].'">';
				else $filename = 'brak pliku';
				
				$form = new Form('produkty', 'editSave', 'Edycja danych');
				$form->addText('Nazwa', 'PRO_NAME', 'required', $produkt['PRO_NAME']);
				$form->addText('Alias', 'PRO_ALIAS', 'required', $produkt['PRO_ALIAS']);
				$form->addText('Kod produktu', 'PRO_CODE', 'required', $produkt['PRO_CODE']);
				$form->addSelect('Kategoria', 'PRO_CAT_ID', $this->getCategory(), $produkt['CAT_ID']);
				$form->addText('Cena zakupu', 'PRO_PRICE', 'required', $produkt['PRO_PRICE']);
				$form->addText('Punkty', 'PRO_POINTS', 'required', $produkt['PRO_POINTS']);
				$form->addInput('Opis skrócony', 'PRO_DESC_SHORT', stripslashes($produkt['PRO_DESC_SHORT']), 'textarea.tpl.php');
				$form->addInput('Pełny opis', 'PRO_DESC_LONG', stripslashes($produkt['PRO_DESC_LONG']), 'textarea.tpl.php');
				$form->addInput('Zdjęcie', 'PRO_FOTO', $filename, 'file.tpl.php');
				$form->addInput('Dodatkowe zdjęcia', 'PFO_FILE[]', $photoContainer, 'file.tpl.php', null, 'fileAttachment');
				$form->addSelect('Status', 'PRO_STATUS', $this->getStatus(), $produkt['PRO_STATUS']);
				$form->addHidden('PRO_ID', $produkt['PRO_ID']);
				
				if(!empty($produkt['PNE_ID']) and $produkt['PNE_STATUS']!=3) $pNew = 'checked';
				else $pNew = null;
				
				if(!empty($produkt['PPR_ID']) and $produkt['PPR_STATUS']!=4) $pProm = 'checked';
				else $pProm = null;
				
				$form->addElement('<div class="form-group formTitle"><h4> Nowość</h4></div>');
				$form->addCheckbox('Aktywny', 'PNE_ID_ACTIVE', $pNew);
				$form->addInput('Data rozpoczęcia', 'PNE_DATE_FROM', $produkt['PNE_DATE_FROM'], 'datepicker.tpl.php');
				$form->addInput('Data zakończnia', 'PNE_DATE_TO', $produkt['PNE_DATE_TO'], 'datepicker.tpl.php');
				$form->addSelect('Typ wyświetlania', 'PNE_STATUS', array('all'=>'ciągły', 'bydate'=>'po dacie'), $produkt['PNE_STATUS']);
				$form->addHidden('PNE_ID', $produkt['PNE_ID']);
				
				$form->addElement('<div class="form-group formTitle"><h4> Promocja</h4></div>');
				$form->addCheckbox('Aktywny', 'PPR_ID_ACTIVE', $pProm);
				$form->addText('Wartość promocji', 'PPR_VALUE', null, $produkt['PPR_VALUE']);
				$form->addSelect('Przeliczanie', 'PPR_STATUS', array('percent'=>'procentowe', 'minus'=>'odejmij od ceny', 'value'=>'wartość'), $produkt['PPR_STATUS']);
				$form->addHidden('PPR_ID', $produkt['PPR_ID']);
				
				$page = new Page;
				$list = array(
					'__HEAD' => 'Edycja danych produktu',
					'__CONTENT' => $form->setForm(),
					'__GLYPHICON' => 'card_giftcarde',
				);
				
				$page->setSubmit();
				$page->setCancel($this->var->action);
				$page->setToolbar();			
				return $page->setPage($list, 'pageForm.tpl.php');
			} else return $this->noProduct();
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array(
				'PRO_NAME' => $this->var->PRO_NAME,
				'PRO_ALIAS' => $this->var->PRO_ALIAS,
				'PRO_STATUS' => $this->var->PRO_STATUS,
				'PRO_DESC_LONG' => addslashes($this->var->PRO_DESC_LONG),
				'PRO_DESC_SHORT' => addslashes($this->var->PRO_DESC_SHORT),
				'PRO_POINTS' => $this->var->PRO_POINTS,
				'PRO_PRICE' => $this->var->PRO_PRICE,
				'PRO_CODE' => $this->var->PRO_CODE,
			);
			
			if($_FILES['PRO_FOTO']['name']) {
				$update['PRO_FOTO'] = $this->filesSave('PRO_FOTO', $this->data->localPath.$this->imgPath.$this->var->PRO_CODE.'/');
			}
			
			//attachments
    	foreach ($_FILES["PFO_FILE"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
          
          $this->db->queryString(__BP.'pro_foto');
          $tmp_name = $_FILES["PFO_FILE"]["tmp_name"][$key];
          $filename = $_FILES["PFO_FILE"]["name"][$key];
          move_uploaded_file($tmp_name, $this->data->localPath.$this->imgPath.$this->var->PRO_CODE.'/'."$filename");
          
          $this->attachmentSave($filename, $this->data->localPath.$this->imgPath.$this->var->PRO_CODE.'/');
          
          $insert = array('PFO_FILE' => $filename, 'PFO_PRO_ID' => $this->var->PRO_ID);
          $this->db->insertQuery($insert);
        }
      }
			
			$this->db->queryString(__BP.'products');
			$this->db->updateQuery($update, 'PRO_ID', $this->var->PRO_ID);
			
			//product news
			if(!empty($this->var->PNE_ID_ACTIVE)) {
				$this->db->queryString(__BP.'products_news');	
				$data = array(
						'PNE_PRO_ID' => $this->var->PRO_ID,
						'PNE_DATE_FROM' => $this->var->PNE_DATE_FROM,
						'PNE_DATE_TO' => $this->var->PNE_DATE_TO,
						'PNE_STATUS' => $this->var->PNE_STATUS,
				);
				
				if(!empty($this->var->PNE_ID))
					$this->db->updateQuery($data, 'PNE_ID', $this->var->PNE_ID);
				else 
					$this->db->insertQuery($data);
			} else {
				if(!empty($this->var->PNE_ID)) {
					$this->db->queryString(__BP.'products_news');
					$this->db->updateQuery(array('PNE_STATUS'=>3), 'PNE_ID', $this->var->PNE_ID);
				}
			}
			
			//product promotion
			if(!empty($this->var->PPR_ID_ACTIVE)) {
				$this->db->queryString(__BP.'products_promotions');
				$data = array(
					'PPR_PRO_ID' => $this->var->PRO_ID,
					'PPR_VALUE' => $this->var->PPR_VALUE,
					'PPR_STATUS' => $this->var->PPR_STATUS,
				);
				if(!empty($this->var->PPR_ID)) 
					$this->db->updateQuery($data, 'PPR_ID', $this->var->PPR_ID);
				else $this->db->insertQuery($data);
			} else {
				if(!empty($this->var->PPR_ID)) {
					$this->db->queryString(__BP.'products_promotions');
					$this->db->updateQuery(array('PPR_STATUS'=>4), 'PPR_ID', $this->var->PPR_ID);
				}
			}	
			
			//category
			$this->db->queryString('delete from '.__BP.'pro_cat where PRO_ID='.$this->var->PRO_ID);
			$this->db->execQuery();
			
			$insert_cat = array(
				'CAT_ID' => $this->var->PRO_CAT_ID,
				'PRO_ID' => $this->var->PRO_ID
			);
			
			$this->db->queryString(__BP.'pro_cat');
			$id = $this->db->insertQuery($insert_cat);
			
			//update product count and generate category tree
			$this->db->queryString('update '.__BP.'category set CAT_COUNT=(select count(pro.PRO_ID) from '.__BP.'pro_cat prc left join '.__BP.'products pro using(PRO_ID) where prc.CAT_ID='.$this->var->PRO_CAT_ID.' and pro.PRO_STATUS=1) where CAT_ID='.$this->var->PRO_CAT_ID);
			$this->db->execQuery();
			
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			$model->load('category', 'generateCategoryTree');
			
			//save event 
			eventSaver::add($this->var->admin_id, 'edit produkt - '. $this->var->PRO_NAME, 'admin', 'produkty', 'edit', $this->var->PRO_ID);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'produkty', $this->var->PRO_ID, 'edit', $this->var->PRO_NAME, addslashes($this->var->PRO_DESC_LONG), addslashes($this->var->PRO_DESC_SHORT), $this->var->PRO_POINTS, 'status: '.$this->var->PRO_STATUS);
			
			header('location: admin.php?action=produkty');
		} else return $this->goAway();
	}
	
	public function filesSave($file, $path) {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {

			if($_FILES[$file]['name']) {
				if(!file_exists($path))
					mkdir($path, 0755);
	  		$name = $_FILES[$file]['name'];
	  		$filename = pathinfo($_FILES[$file]['name'], PATHINFO_FILENAME);
	  		$image_file = new ImageLoader(PRODUCT_FULL_X,PRODUCT_FULL_Y, 'full_'.$filename, $file,$path);
	  		$image = $image_file->load();
	  		
	  		$image_file = new ImageLoader(PRODUCT_THUMB_2_X,PRODUCT_THUMB_2_Y, 'thumb1_'.$filename, $file, $path, false);
	  		$image_file->load();
	  		
	  		$image_file = new ImageLoader(PRODUCT_THUMB_1_X,PRODUCT_THUMB_1_Y, 'thumb2_'.$filename, $file, $path, false);
	  		$image_file->load();
	      
	      return $name;
			}
		} else return $this->goAway();
	}
	
	public function attachmentSave($file, $path) {
		if(!file_exists($path))
			mkdir($path, 0755);
		$image_file = new ImageLoader(PRODUCT_FULL_X,PRODUCT_FULL_Y, $file, 'full_'.$file, $this->data->localPath.$path);
		$image = $image_file->loadByImagick();
		
		$image_file = new ImageLoader(PRODUCT_THUMB_2_X,PRODUCT_THUMB_2_Y, $file, 'thumb1_'.$file, $this->data->localPath.$path, false);
		$image_file->loadByImagick();
		
		$image_file = new ImageLoader(PRODUCT_THUMB_1_X,PRODUCT_THUMB_1_Y, $file, 'thumb2_'.$file, $this->data->localPath.$path, false);
		$image_file->loadByImagick();

	}
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	public function delPhoto() {
		if(isset($this->var->photo) and !empty($this->var->photo)) {
			$this->db->queryString(__BP.'pro_foto');
			$this->db->updateQuery(['PFO_STATUS' => 2],'PFO_ID', $this->var->photo);
			return true;
		} else return false;
	}
	
	private function getStatus() {
		$status = array(
			'active' => 'aktywny',
			'inactive' => 'nieaktywny',
			'deleted' => 'usunięty',
		);
		return $status;		
	}
	
	private function getCategory() {
		$this->db->queryString('select CAT_ID, CAT_NAME from '.__BP.'category');
		$this->db->where('and', 'CAT_STATUS=1');
		$categories = $this->db->getData();
		/*if($sub)
			$this->db->where('and', 'CAT_PARENT=0');*/
		
		if($categories) {
			$category = array();

			foreach($categories as $cat) 
				$category[$cat['CAT_ID']] = $cat['CAT_NAME'];
		}
		return $category;
	}
	
	public function getForMenu() {
		$menu_module = $this->var->menu_module;
		$menu_option = $this->var->menu_option;
		
		if($menu_module=='produkty')
				$selected = " checked";
			else $selected = '';
		$line="<input type='radio' name='menu' value='produkty:none:1:Produkty:produkty'".$selected."> - produkty<br/>";
		
		return $line;
	}

}

function produkty($option=null) {
	$produkty = new produkty;
	if($option!=null and method_exists($produkty, $option))
		return $produkty->$option();
	else 
		return $produkty->showModule();
}