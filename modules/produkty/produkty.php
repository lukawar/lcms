<?php
class produkty extends View implements IModule {
	public $path = 'modules/produkty/templates/';
	public $imgPath = 'files/product/';
	
	public function showModule() {
		
	}
	
	public function view() {
		$line = null;
		if(isset($this->var->id) and $this->var->id!=null and isset($this->var->cat) and $this->var->cat!=null) {
			$this->db->queryString('select PRO_ID,PRO_FOTO,PRO_CODE,PRO_NAME,PRO_POINTS,PRO_DESC_LONG,CAT_NAME,PPR_STATUS,PPR_VALUE,PNE_STATUS, CHE_ID from '.__BP.'products');
			$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'category', 'CAT_ID', 'CAT_ID');
			$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
			$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID', 'PNE_STATUS<>3');
			$this->db->join('left', __BP.'chest', 'CHE_PRO_ID', 'PRO_ID', 'CHE_USR_ID='.$this->var->user_id.' and CHE_STATUS=1');
			$this->db->where('and', 'CAT_STATUS=1');
			$this->db->where('and', 'PRO_STATUS=1');
			$this->db->where('and', 'PRO_ALIAS="'.$this->var->cat.'"');
			$this->db->where('and', 'CAT_ID="'.(int)$this->var->id.'"');
			
			$pro = $this->db->getRow();
			//$this->db->query();
			$productThumbs = null;
			if($pro) {
				if($pro['PRO_FOTO']!=null) {
					$imagePath = '<a data-toggle="lightbox" data-gallery="gallery" href="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/full_'.$pro['PRO_FOTO'].'"><img src="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb1_'.$pro['PRO_FOTO'].'" class="img-fluid"></a>';
					
					//product photos
					$this->db->queryString('select * from '.__BP.'pro_foto');
					$this->db->where('and','PFO_PRO_ID='.$pro['PRO_ID']);
					$this->db->where('and','PFO_STATUS=1');
					
					$thumbs = $this->db->getDataFetch();
					if($thumbs) {
						$productThumbs.='<div class="col-md-2"><a href="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/full_'.$pro['PRO_FOTO'].'" data-gallery="gallery" thumb="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb1_'.$pro['PRO_FOTO'].'" class="thumbElement"><img src="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb2_'.$pro['PRO_FOTO'].'" class="img-responsive"></div>';
						foreach($thumbs as $thumb)
							$productThumbs.='<div class="col-md-2"><a href="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/full_'.$thumb['PFO_FILE'].'" data-gallery="gallery" thumb="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb1_'.$thumb['PFO_FILE'].'" class="thumbElement"><img src="'.$this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb2_'.$thumb['PFO_FILE'].'" class="img-responsive"></div>';
					}
				} else 
					$imagePath = '<img src="'.$this->data->localPath.$this->imgPath.'noproduct_1.jpg">';
					
				$promPoints = 0;
				$flagClass = null;
				$productType = null;
				$productNewPoints = null;
				
				//promotions
				if($pro['PPR_STATUS']!=null) {
					switch ($pro['PPR_STATUS']) {
				    case 'percent':$promPoints = round($pro['PRO_POINTS'] - (($pro['PRO_POINTS']*$pro['PPR_VALUE'])/100));break;
				    case 'minus':$promPoints = round($pro['PRO_POINTS'] - $pro['PPR_VALUE']);break;
				    case 'value':$promPoints = round($pro['PPR_VALUE']);break;
					}
					$productType = '<h5 class="product-promotion"><span>PROMOCJA</span></h5>';
					$productNewPoints = '<h5 class="product-promotion"><span>PROMOCJA</span> '.$promPoints.' pkt + 1 zł</h5>';
					$flagClass = 'striked';
				}
				
				//new
				if($pro['PNE_STATUS']!=null) {
					$productType.= '<h5 class="product-new"><span>NOWOŚĆ</span></h5>';
				}
				
				$chest = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
				
				$template = file_get_contents($this->data->localPath.$this->path.'produkt.tpl.php');
				$placeholders = array(
					'__PRO_ID' => $pro['PRO_ID'],
					'__PRO_NAME' => $pro['PRO_NAME'],
					'__PRO_POINTS' => $pro['PRO_POINTS'],
					'__PRO_FOTO' => $imagePath,
					'__PRO_DESC_LONG' => $pro['PRO_DESC_LONG'],
					'__CAT_NAME' => $pro['CAT_NAME'],
					'__CART_PATH' => helper::href(array('cart','check')),
					'__PRO_THUMBNAILS' => $productThumbs,
					'__ZLOTOWKA' => $this->lang->zlotowka,
					'__PRODUCT_TYPE' => $productType,
					'__FLAG_CLASS' => $flagClass,
					'__PRODUCT_NEW_POINTS' => $productNewPoints,
					'__CHEST' => $chest->load('chest', 'content', ['product_id' => $pro['PRO_ID'], 'chest_id' => $pro['CHE_ID']]),
				);
				
				return Template::parse($placeholders, $template);
			} else return $this->noProduct();
		}
	}
	
	public function setProducts_blocked_sdkfjslkdjlsgnsldigspdgisdpgsdgpsdgsdpoksodofs() {
		$this->db->queryString('select * from '.__BP.'products where PRO_CHECK=0');
		$p = $this->db->getRow();
		if($p) {
			$imageName = null;
			if(file_exists($this->imgPath.$p['PRO_CODE'].'/')) {
				$handle = opendir($this->imgPath.$p['PRO_CODE'].'/');
				unset($catalogs);
				$filen = null;
				
				while($catalog = readdir($handle)) {
				  if ($catalog != '.' AND $catalog !='..')
				    $catalogs[] = $catalog;
				}
			
				if(count($catalogs)>0) {
					$i = 0;
					foreach($catalogs as $file) {
						//$filen.= "<img src='".$this->imgPath.$p['PRO_CODE'].'/'.$file."'>";
						$this->filesSave($file, $this->imgPath.$p['PRO_CODE'].'/');
						if($i>0) {
							$this->db->queryString(__BP.'pro_foto');
							$this->db->insertQuery(array('PFO_PRO_ID'=>$p['PRO_ID'], 'PFO_FILE'=>$file));
						} else $imageName = $file;
						$i++;						
					}
				}
				
			}
			
			$this->db->queryString(__BP.'products');
			$this->db->updateQuery(array('PRO_FOTO' => $imageName, 'PRO_CHECK' => 1, 'PRO_ALIAS' => coder::stripPL($p['PRO_NAME'])),'PRO_ID', $p['PRO_ID']);
		}
	}
	
	public function setProducts_blocked_bb_sdjfhsdhfsu() {
		$temp = 'files/bb/';
		$this->db->queryString('select * from '.__BP.'products where PRO_CHECK=0');
		$p = $this->db->getRow();
		if($p) {
			$imageName = null;
			if($p['PRO_CODE']!='') {
				$i = 0;
				echo $p['PRO_CODE'].'</br>';
				mkdir($this->imgPath.$p['PRO_CODE'], 0707);
				foreach (glob($temp.$p['PRO_EAN'].'*.*') as $filename) {
					echo $filename . "<br/>";
					$this->filesSave(basename($filename), 'files/bb/', $this->imgPath.$p['PRO_CODE'].'/');
					if($i>0) {
						$this->db->queryString(__BP.'pro_foto');
						$this->db->insertQuery(array('PFO_PRO_ID'=>$p['PRO_ID'], 'PFO_FILE'=>basename($filename)));
					} else $imageName = $filename;
					$i++;	
				}
			}
			$this->db->queryString(__BP.'products');
			$this->db->updateQuery(array('PRO_FOTO' => $imageName, 'PRO_CHECK' => 1, 'PRO_ALIAS' => coder::stripPL($p['PRO_NAME'])),'PRO_ID', $p['PRO_ID']);
		}
	}
	
	public function filesSave($file, $path, $newPath) {
		//$name = $_FILES[$file]['name'];
		//$filename = pathinfo($_FILES[$file]['name'], PATHINFO_FILENAME);
		$image_file = new ImageLoader(PRODUCT_FULL_X,PRODUCT_FULL_Y, $file, 'full_'.$file, $this->data->localPath.$path);
		$image = $image_file->loadByImagick($newPath);
		
		$image_file = new ImageLoader(PRODUCT_THUMB_2_X,PRODUCT_THUMB_2_Y, $file, 'thumb1_'.$file, $this->data->localPath.$path, false);
		$image_file->loadByImagick($newPath);
		
		$image_file = new ImageLoader(PRODUCT_THUMB_1_X,PRODUCT_THUMB_1_Y, $file, 'thumb2_'.$file, $this->data->localPath.$path, false);
		$image_file->loadByImagick($newPath);

	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	public function offerPoints() {
		$maxPoints = 3000 + $this->var->user_points;
		
		$this->db->queryString('select PRO_ID,PRO_NAME, PRO_ALIAS, PRO_POINTS, CAT_ID, PRO_FOTO, PRO_DESC_SHORT, PRO_CODE, PPR_STATUS, PPR_VALUE,PNE_STATUS  from '.__BP.'products');
		$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
		$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
		$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID', 'PNE_STATUS<>3');
		$this->db->where('and', 'PRO_STATUS=1');
		$this->db->where('and', 'PRO_POINTS<='.$this->var->user_points);
		//$this->db->where('and', 'PRO_ID in (100, 102,101, 103, 104, 105, 106)'); //test
		$this->db->limit(10);
		$this->db->orderBy('rand()');
		//$this->db->prepareQuery();$this->db->query();
		
		$offer = $this->db->getDataFetch();
		
		if($offer) {
			$result = null;
		
			foreach($offer as $o) {
				$promPoints = null;
				$flagClass = null;
				$productType = null;
				
				//promotions
				if($o['PPR_STATUS']!=null) {
					switch ($o['PPR_STATUS']) {
				    case 'percent':$promPoints = round($o['PRO_POINTS'] - (($o['PRO_POINTS']*$o['PPR_VALUE'])/100));break;
				    case 'minus':$promPoints = round($o['PRO_POINTS'] - $o['PPR_VALUE']);break;
				    case 'value':$promPoints = round($o['PPR_VALUE']);break;
					}
					$productType = '<h5 class="product-promotion"><span>PROMOCJA</span> '.$promPoints.' pkt + 1 zł</h5>';
					$flagClass = 'striked';
				}
				
				//new
				if($o['PNE_STATUS']!=null) {
					$productType.= '<h5 class="product-new"><span>NOWOŚĆ</span></h5>';
				}
				
				if(file_exists($this->data->localPath.$this->imgPath.$o['PRO_CODE'].'/thumb2_'.$o['PRO_FOTO']))
					$image = $this->data->localPath.$this->imgPath.$o['PRO_CODE'].'/thumb2_'.$o['PRO_FOTO'];
				else $image = $this->data->localPath.$this->imgPath.'noproduct_1.jpg';
				
				$data = array(
					'__LINK' => helper::href(array('produkty', 'view', $o['CAT_ID'], $o['PRO_ALIAS'])),
					'__NAME' => $o['PRO_NAME'],
					'__ID' => $o['PRO_ID'],
					'__PHOTO' => $image,
					'__POINTS' => $o['PRO_POINTS'],
					'__DESC' => $o['PRO_DESC_SHORT'],
					'__FLAG_CLASS' => $flagClass,
					'__PRODUCT_TYPE' => $productType,
				);
				$result.= Template::parseFile($data, $this->data->localPath.$this->path.'offer.tpl.php');
			} return Template::parseFile(['__OFFER'=>$result], $this->data->localPath.$this->path.'offerContainer.tpl.php');
		} else return null;
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------*/
	
	public function filterSearch() {
		if(!isset($this->var->option))
			$this->var->option = null;
			
		if(!isset($this->var->id))
			$this->var->id = null;
			
		if(!isset($this->var->cat))
			$this->var->cat = null;
		
		$list = array(
			'__SEARCH_STRING' => null,
			'__SORT_PRICE_GROW' => helper::href(array($this->var->action, $this->var->option, $this->var->id, $this->var->cat)),
			'__SORT_PRICE_LOW' => helper::href(array($this->var->action, $this->var->option, $this->var->id, $this->var->cat)),
			'__SORT_NAME_GROW' => helper::href(array($this->var->action, $this->var->option, $this->var->id, $this->var->cat)),
			'__SORT_NAME_LOW' => helper::href(array($this->var->action, $this->var->option, $this->var->id, $this->var->cat)),
		);
		return Template::parseFile($list,$this->data->localPath.$this->path.'search.tpl.php');
	}
	
	public function setSortType() {
		if(isset($this->var->by) and $this->var->by!=null and isset($this->var->type) and $this->var->type!=null) {
			if($this->var->by=='price')
				$_SESSION['sort_by'] = 'PRO_PRICE';
			if($this->var->by=='name')
				$_SESSION['sort_by'] = 'PRO_NAME';
			$_SESSION['sort_type'] = $this->var->type;
			return 'sorted';
		} else return false;
	}
}

function produkty($option=null) {
	$produkty = new produkty;
	if($option!=null and $option!='lang')
		return $produkty->getPage($option);
  elseif($option=='lang')
    return $produkty->getPage();
	else 
		return $produkty->getMenu();
}