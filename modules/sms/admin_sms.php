<?php
class sms extends View implements IModule {
	public $path = 'modules/sms/templates/';
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = null;
		
			$list = array(
				'__GLYPHICON' => 'phone_android',
				'__HEAD' => 'SMS',
				'__CONTENT'=> $table
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('sms','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> wyślij</a>');
			$page->setToolbar();
			return $page->setPage($list);	
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('sms', 'send', 'Wypełnij dane');
			$form->addText('Nr odbiorcy', 'SMS_NUMBER', 'required');
			$form->addInput('Treść', 'SMS_TEXT', null, 'textarea.tpl.php');
			$page = new Page;
			$list = array(
				'__HEAD' => 'WYŚLIJ SMS',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'phone_android',
			);
			
			$page->setSubmit();
			$page->setCancel($this->var->action);
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function send() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			require_once($this->data->localPath.'system/lib/SerwerSMS/vendor/autoload.php');
		
				try{
				  $serwersms = new SerwerSMS\SerwerSMS('expansja','eXpBb2o16_ssmany!');

				  $messages[] = array(
				      'phone' => $this->var->SMS_NUMBER,
				      'text' => strip_tags($this->var->SMS_TEXT)
				  );
				  
				  $result = $serwersms->messages->sendPersonalized(
				            $messages,
				            'Bea Bonus',
				            array(
				                    'test' => false,
				                    'details' => true
				            )
				  );

				  $list =  'Skolejkowano: '.$result->queued.'<br />';
				  $list.= 'Niewysłano: '.$result->unsent.'<br />';

				  foreach($result->items as $sms){
				    $list.= 'ID: '.$sms->id.'<br />';
				    $list.= 'NUMER: '.$sms->phone.'<br />';
				    $list.= 'STATUS: '.$sms->status.'<br />';
				    $list.= 'CZĘŚCI: '.$sms->parts.'<br />';
				    $list.= 'WIADOMOŚĆ: '.$sms->text.'<br />';
				  }

				} catch(Exception $e){
				  $list = 'ERROR: '.$e->getMessage();
				}
		
			$liste = array(
				'__GLYPHICON' => 'phone_android',
				'__HEAD' => 'SMS',
				'__CONTENT'=> $list
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('sms'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> powrót</a>');
			$page->setToolbar();
			return $page->setPage($liste);			
			
		} else return $this->goAway();
	}
}

function sms($option=null) {
	$sms = new sms;
	if($option!=null and method_exists($sms, $option))
		return $sms->$option();
	else 
		return $sms->showModule();
}