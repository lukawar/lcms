<?php
class sms extends View {
	public function showModule() {
		return null;	
	}
	
	public function testmodule() {
		require_once($this->data->localPath.'system/lib/SerwerSMS/vendor/autoload.php');
		
		try{
		  $serwersms = new SerwerSMS\SerwerSMS('webapi_expansja','Prometeusz2');

		  $messages[] = array(
		      'phone' => '+48607385979',
		      'text' => 'Wiadomość Łukasz'
		  );
		  $messages[] = array(
		      'phone' => '+48501144941',
		      'text' => 'Bartek - daj znać, czy wiadomosc dotarła'
		  );
		  
		  $messages[] = array(
		      'phone' => '+48604571638',
		      'text' => 'Ewa - daj znać Łukaszowi, czy wiadomosc dotarła'
		  );
		  
		  $messages[] = array(
		      'phone' => '+48783106169',
		      'text' => 'Aga - daj znać Łukaszowi, czy wiadomosc dotarła'
		  );
		  
		  $messages[] = array(
		      'phone' => '+48602795178',
		      'text' => 'Aga - daj znać Łukaszowi, czy wiadomosc dotarła'
		  );

		  $result = $serwersms->messages->sendPersonalized(
		            $messages,
		            'Bea Bonus',
		            array(
		                    'test' => false,
		                    'details' => true
		            )
		  );
		  
		   /*$result = $serwersms->messages->sendSms(
            array(
                    '+48607385979',
            ),
            'Test FULL message',
            'Bea Bonus',
            array(
                    'test' => false,
                    'details' => true
            )
  		);*/

		  echo 'Skolejkowano: '.$result->queued.'<br />';
		  echo 'Niewysłano: '.$result->unsent.'<br />';

		  foreach($result->items as $sms){
		    echo 'ID: '.$sms->id.'<br />';
		    echo 'NUMER: '.$sms->phone.'<br />';
		    echo 'STATUS: '.$sms->status.'<br />';
		    echo 'CZĘŚCI: '.$sms->parts.'<br />';
		    echo 'WIADOMOŚĆ: '.$sms->text.'<br />';
		  }

		} catch(Exception $e){
		  echo 'ERROR: '.$e->getMessage();
		}
		
	}
	
	public function sendImportInfo($sms) {
		require_once($this->data->localPath.'system/lib/SerwerSMS/vendor/autoload.php');
		try{
			$this->db->queryString('select max(IIN_ID) as import from '.__BP.'import_info');
			$this->db->where('and', 'IIN_STATUS=1');
			$campaign = $this->db->getRow();
			
			$today = date('Y-m-d');
			$this->db->tableName(__BP.'sms');
			foreach($sms as $info) {
				$text = 'Witaj '.$info['name'].'! Stan Twojego konta w programie Bea Bonus na dzien '.$today.' wynosi '.$info['points'].' pkt. Sprawdzaj aktualne promocje, zyskuj wiecej i szybciej.';
				$messages[] = array(
			      'phone' => $info['phone'],
			      'text' => $text
			  );
			  
			  $this->db->insertRow(['SMS_CAMPAIGN' => $campaign['import'], 'SMS_USR_ID' => $info['id'], 'SMS_NUMBER' => $info['phone'], 'SMS_TEXT' => $text]);
			}
		  $serwersms = new SerwerSMS\SerwerSMS('expansja','eXpBb2o16_ssmany!');
		  
		  $result = $serwersms->messages->sendPersonalized(
		            $messages,
		            'Bea Bonus',
		            array(
		                    'test' => false,
		                    'details' => true
		            )
		  );


		} catch(Exception $e){
		  $list = 'ERROR: '.$e->getMessage();
		}
	}
	
}

function sms() {
	$sms = new sms;
	return $sms->showModule();
}