<?php
$module_info['title'] = 'Administratorzy';
$module_info['description'] = 'Zarządzanie kontami administratorów';
$module_info['author'] = 'Expansja';

$menu['title'][0] = 'Konta administratorów';
$menu['action'][0] = 'administrator';
$menu['icon'][0] = 'person';
$menu['order'][0] = '4';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;