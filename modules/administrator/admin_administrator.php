<?php
class administrator extends View implements IModule {
	public $path = 'modules/administrator/templates/';

	public function checkAdmin() {
		if(U_TYPE=='base') {
			if(!empty($this->var->l_login) and !empty($this->var->l_pass)) {
				$_SESSION['s_l_login'] = $this->var->s_l_login = $this->var->l_login;
				$_SESSION['s_l_pass'] = $this->var->s_l_pass = coder::hashString($this->var->l_pass);
				
				//var_dump($this->var);
				//die($this->setRedirectLink());
				//header('location: '.$this->setRedirectLink());
				header('location: admin.php');
			}
			
			if(!empty($this->var->s_l_login) and !empty($this->var->s_l_pass)) {				
				$this->db->queryString("select * from ".__BP."administrator");
				$this->db->where('and', "ADM_LOGIN='".$this->var->s_l_login."'");
				$this->db->where('and', "ADM_PASS='".$this->var->s_l_pass."'");
				$this->db->where('and', "ADM_ACTIVE=1");
				
				$admin = $this->db->getRow();
				//die($this->db->query());
				
				if($admin['ADM_ID']!='') {
					$this->var->admin_id = $_SESSION['admin_id'] = $admin['ADM_ID'];
					$this->var->admin_data = $_SESSION['admin_data'] = $admin['ADM_NAME'].' '.$admin['ADM_SURNAME'];
					$this->var->admin_role= $_SESSION['admin_role'] = $admin['ADM_ROLE'];
					$this->var->admin_agency = $_SESSION['admin_agency'] = $admin['ADM_AGENCY'];
					$this->var->admin_alias = $_SESSION['admin_alias'] = $admin['ADM_ALIAS'];
					$this->var->admin_from = $_SESSION['admin_from'] = $admin['ADM_FROM'];
					$this->var->admin_avatar = $_SESSION['admin_avatar'] = $admin['ADM_AVATAR'];
					$checkLogin['logged'] = 'true';
					
					//var_dump($this->var);
					
					if(isset($this->var->loginForm) and $this->var->loginForm=='setLogin') {
						eventSaver::add($this->var->admin_id, 'login', 'admin', 'administrator', 'login', $this->var->admin_id);
						$this->var->loginForm = null;
					}
					
					return $checkLogin;
				} else {
					$checkLogin['logged'] = 'false';
					$checkLogin['info'] = 'Błędne dane logowania';
					$checkLogin['container'] = '#loginInfo';
					//TODO: add bad login save info
					
					return $checkLogin;
				}
			} else return false;
		} elseif(U_TYPE=='file') {
				//die($var->s_l_pass." ".__U_LOGIN);
				if($this->var->s_l_login==__U_LOGIN and $this->var->s_l_pass==__U_PASS) {
					$this->var->admin_id = $_SESSION['admin_id'] = 1;
					$this->var->admin_data = $_SESSION['admin_data'] = 'sysadmin';
					$this->var->admin_role = $_SESSION['admin_role'] = 'admin';
					$this->var->admin_agency = $_SESSION['admin_agency'] = null;
					$this->var->admin_alias = $_SESSION['admin_alias'] = null;
					$this->var->admin_from = $_SESSION['admin_from'] = null;
					return true;
				} else return false;
			}
	}
	
	public function dropdown() {
		$list = array(
			'__AVATAR' => 'templates/backend/devoops/img/avatar.png',
			'__ADMIN_DATA' => $this->var->admin_data,
		);
		
		return Template::parse($list, file_get_contents($this->path.'dropdown.tpl.php'));
	}
	
	public function showModule() {
		if($this->var->admin_role=='admin') {
			$this->db->queryString("select ADM_SEND_MAIL, ADM_AGENCY, ADM_NAME, ADM_SURNAME, ADM_MAIL, ADM_ROLE, ADM_ACTIVE, ADM_FROM, ADM_ID, group_concat(ADR_ROLE_ID SEPARATOR ', ')as privileges from ".__BP."administrator left join ".__BP."admin_roles on ADM_ID=ADR_ADM_ID group by ADM_ID order by ADM_ID");
			$admins = $this->db->GetDataFetch();
			
			$i = 1;
			if($admins) {
				$table = new table('dataTable', 'table table-striped stripe hover');
				$tableHeader = array('lp.', 'Dane','Mail', 'Konto', 'Uprawnienia', 'Typ','From', 'Status','');
				$table->createHeader($tableHeader);
				
				foreach($admins as $admin) {
					$table->addCell($i, 'lp');
					$table->addCell($admin['ADM_NAME']." ".$admin['ADM_SURNAME']);
					$table->addCell($admin['ADM_MAIL']);
					//$table->addCell($admin['ADM_SEND_MAIL']);
					$table->addCell($admin['ADM_ROLE']);
					$table->addCell($admin['privileges']);
					$table->addCell($admin['ADM_AGENCY']);
					$table->addCell($admin['ADM_FROM']);
					$table->addCell(($admin['ADM_ACTIVE']=='active') ? 'aktywny' : 'nieaktywny');
					$table->addButton('edit', 'admin.php?action=administrator&option=editAdmin&id='.$admin['ADM_ID'], 'primary');
					$table->addRow();
					$i++;
				}
				$tableBody = $table->createBody();
			} else $tableBody = $this->noData();
			
			$list = array(
				'__CONTENT'	=>$tableBody,
				'__HEAD' => 'Konta administratorów'
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('administrator','addAdmin'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj konto</a>');
			$page->setToolbar();
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function addAdmin() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('administrator', 'addSave', 'Wypełnij dane');
			$form->addText('Imię', 'ADM_NAME', 'required');
			$form->addText('Nazwisko', 'ADM_SURNAME', 'required');
			$form->addText('E-mail', 'ADM_MAIL', 'required');
			$form->addText('Login', 'ADM_LOGIN', 'required');
			$form->addInput('Hasło', 'ADM_PASS', null, 'password.tpl.php', 'required');
			$form->addSelect('Uprawnienia', 'ADM_ROLE', array('user'=>'user', 'admin'=>'admin') , null);
			$form->addSelect('From', 'ADM_FROM', array('bb'=>'bb', 'ex'=>'ex') , null);
			$form->addSelect('Powiadomienia', 'ADM_SEND_MAIL', array('nie'=>'nie', 'tak'=>'tak') , null);
			$form->addText('Typ', 'ADM_AGENCY');
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj nowe konto',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'group',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=administrator" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		$this->db->queryString(__BP.'administrator');
		$admin_alias = coder::hashString(microtime());

		$add_data = array(
			'ADM_NAME'=>$this->var->ADM_NAME,
			'ADM_ALIAS'=>$admin_alias,
			'ADM_SURNAME'=>$this->var->ADM_SURNAME,
			'ADM_LOGIN'=>$this->var->ADM_LOGIN,
			'ADM_AGENCY'=>$this->var->ADM_AGENCY,
			'ADM_MAIL'=>$this->var->ADM_MAIL,
			'ADM_PASS'=>coder::hashString($this->var->ADM_PASS),
			'ADM_ROLE'=>$this->var->ADM_ROLE,
			'ADM_FROM'=>$this->var->ADM_FROM,
			'ADM_SEND_MAIL'=>$this->var->ADM_SEND_MAIL,
		);
		
		//send data login if is selected 'tak'
		if($this->var->ADM_SEND_MAIL=='tak') {
			$list = array(
				'__LINK'	=> SC_ROOT_URL.'admin',
				'__LOGIN' => $this->var->ADM_LOGIN,
				'__PASS' => $this->var->ADM_PASS,
			);
			$text = Template::parseFile($list, $this->data->localPath.$this->path.'userInfo.tpl.php');
			$mailer = new Mailer;
			$mailer->subject = 'Nowe konto administracyjne';
			$mailer->mailText = $text;
			$mailer->sendMail($this->var->ADM_MAIL, $this->var->ADM_NAME.' '.$this->var->ADM_SURNAME);
		}
		
		$admin = $this->db->InsertQuery($add_data);
		
		header('location: admin.php?action=administrator');
	}
	
	public function editAdmin() {
		if($this->var->admin_role=='admin') {
			$this->db->queryString('select * from '.__BP.'administrator');
			$this->db->where('and',  'ADM_ID='.$this->var->id);
			$admin = $this->db->getRow();
			
			$form = new Form('administrator', 'editSave', 'Dane użytkownika');
			$form->addText('Imię', 'ADM_NAME', 'required', $admin['ADM_NAME']);
			$form->addText('Nazwisko', 'ADM_SURNAME', 'required', $admin['ADM_SURNAME']);
			$form->addText('E-mail', 'ADM_MAIL', 'required', $admin['ADM_MAIL']);
			$form->addText('Login', 'ADM_LOGIN', 'required', $admin['ADM_LOGIN']);
			$form->addInput('Hasło', 'ADM_PASS', null, 'password.tpl.php', 'required');
			$form->addSelect('Uprawnienia', 'ADM_ROLE', array('user'=>'user', 'admin'=>'admin'), $admin['ADM_ROLE']);
			$form->addSelect('From', 'ADM_FROM', array('bb'=>'bb', 'ex'=>'ex'), $admin['ADM_FROM']);
			$form->addSelect('Powiadomienia', 'ADM_SEND_MAIL', array('nie'=>'nie', 'tak'=>'tak'), $admin['ADM_SEND_MAIL']);
			$form->addText('Typ', 'ADM_AGENCY', null, $admin['ADM_AGENCY']);
			$form->addElement('<div class="form-group formTitle"><h4>Uprawnienia</h4></div>');
			$form->addElement($this->getPositions($admin['ADM_ID']));
			$form->addHidden('ADM_ID', $this->var->id);
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Edycja danych konta',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'group',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=administrator" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');

		} else return $this->goAway();
	}
	
	public function editSave() {	
		$this->db->queryString(__BP.'administrator');
		$update_data = array(
			'ADM_NAME'=>$this->var->ADM_NAME,
			'ADM_SURNAME'=>$this->var->ADM_SURNAME,
			'ADM_MAIL'=>$this->var->ADM_MAIL,
			'ADM_SEND_MAIL'=>$this->var->ADM_SEND_MAIL,
			'ADM_AGENCY'=>$this->var->ADM_AGENCY,
			'ADM_LOGIN'=>$this->var->ADM_LOGIN,
			'ADM_ROLE'=>$this->var->ADM_ROLE,
			'ADM_FROM'=>$this->var->ADM_FROM,
		);
		if(!empty($this->var->ADM_PASS))
			$update_data['ADM_PASS'] = coder::hashString($this->var->ADM_PASS);
			
		$this->db->UpdateQuery($update_data, 'ADM_ID', $this->var->ADM_ID);
		
		$this->editSaveRoles();
		
		header('location: admin.php?action=administrator');
	}
	
	public function editSaveRoles() {
		if(is_array($this->var->role)) {
			//delete prevoius data
			$this->db->queryString('delete from '.__BP.'admin_roles where ADR_ADM_ID='.$this->var->ADM_ID);
			$this->db->execQuery();
			
			//insert new roles
			foreach($this->var->role as $role=>$value) {
				$r = explode(',',$role);
				$insert = array('ADR_ADM_ID'=>$this->var->ADM_ID, 'ADR_NAME'=>$r[0], 'ADR_ROLE_ID'=>$r[1]);
				$this->db->queryString(__BP.'admin_roles');
				$this->db->insertQuery($insert);

				unset($insert);
			}
		}
	}
	
	private function getPositions($id=null) {
		$result = null;
		if($id) {
			$this->db->queryString('select ADR_ROLE_ID from '.__BP.'admin_roles');
			$this->db->where('and', 'ADR_ADM_ID='.$id);
			$roles = $this->db->getDataFetch();
			
			if($roles) {
				foreach($roles as $role)
					$role_list[] = $role['ADR_ROLE_ID'];
			} else $role_list = array();
		} else $role_list = array();
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists($this->data->localPath.$this->data->modulesPath.$module."/config.php"))  {
		    include($this->data->localPath.$this->data->modulesPath.$module."/config.php");
		    if(isset($menu) and isset($menu['action'][0])) {
		    	$list = array(
							'__CH_NAME' => "role[".$menu['title'][0].','.$menu['action'][0]."]",
							'__CH_CHECKED' => (in_array($menu['action'][0],$role_list)) ? 'checked' : '',
							'__CH_TEXT' => $menu['title'][0],
						);
						
					$result.= Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					unset($list);
		    }
		  }
		}

		return $result;
	}
	
	//widget for dasboard
	public function dashboard() {
		$list = array(
			'__MODULE_TITLE'=>'Konta administratorów',
			'__MODULE_ICON'=>'fa fa-user',
			'__MODULE_CONTENT'=>"<a href='admin.php?action=administrator'>Lista</a>",
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
	
	public function rewriteUsers() {
		$this->db->queryString('select * from '.__BP.'users_temp where UTE_ID not in (select USR_ID from '.__BP.'users) limit 30');
		$users_temp = $this->db->getDataFetch();
		
		if($users_temp) {
			foreach($users_temp as $ut) {
				$ph = explode(' ', $ut['UTE_PH']);
				$this->db->queryString('select ADM_ID from '.__BP."administrator where ADM_SURNAME='".$ph[1]."'");
				$phid = $this->db->getRow();
				if($phid)
					$id = $phid['ADM_ID'];
				else $id = null;
				
				$insert = array(
					'USR_ID' => $ut['UTE_ID'],
					'USR_ALIAS' => coder::stripPL($ut['UTE_NAME']),
					'USR_NAME' => $ut['UTE_NAME'],
					'USR_LOGIN' => $ut['UTE_LOGIN'],
					'USR_PASS' => coder::hashString($ut['UTE_PASS']),
					'USR_NIP' => $ut['UTE_NIP'],
					'USR_PH' => $id,
				);
				
				$this->db->queryString(__BP.'users');
				$this->db->insertQuery($insert);
				var_dump($insert);
				unset($insert);
			}
		}
	}
	
	public function getPh() {
		$this->db->queryString('select ADM_ID, ADM_NAME, ADM_SURNAME from '.__BP.'administrator');
		$this->db->where('and', 'ADM_AGENCY="ph"');
		$this->db->where('and', 'ADM_ACTIVE=1');
		$phs = $this->db->getDataFetch();
		
		$result = array();
		$result[0] = 'wybierz ph';
		if($phs) {
			foreach($phs as $ph)
				$result[$ph['ADM_ID']] = $ph['ADM_NAME'].' '.$ph['ADM_SURNAME'];
				
			return $result;
		} else return null;
	}
}

function administrator($option=null) {
	$admin = new administrator;
	if($option!=null and method_exists($admin, $option))
		return $admin->$option();
	else 
		return $admin->showModule();
}