<li class="dropdown">
	<a href="#" class="dropdown-toggle account" data-toggle="dropdown">
		<div class="avatar">
			<img src="__AVATAR" class="img-rounded" alt="avatar" />
		</div>
		<i class="fa fa-angle-down pull-right"></i>
		<div class="user-mini pull-right">
			<span class="welcome">Zalogowany</span>
			<span>__ADMIN_DATA</span>
		</div>
	</a>
	<ul class="dropdown-menu">
		<li>
			<a href="?action=options&option=profile">
				<i class="fa fa-user"></i>
				<span class="hidden-sm text">Profil</span>
			</a>
		</li>
		<li>
			<a href="?action=options">
				<i class="fa fa-cog"></i>
				<span class="hidden-sm text">Ustawienia</span>
			</a>
		</li>
		<li>
			<a href="logout.php">
				<i class="fa fa-power-off"></i>
				<span class="hidden-sm text">Wyloguj</span>
			</a>
		</li>
	</ul>
</li>