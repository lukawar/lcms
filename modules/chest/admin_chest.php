<?php
class chest extends View implements IModule {
	public $path = 'modules/chest/templates/';
	
	public function showModule() {
		return null;
	}
	
	public function getUserChest($data) {
		$user_points = $data['points'];
		$id = $data['id'];
		$this->db->queryString('select * from '.__BP.'chest');
		$this->db->join('left', __BP.'products','PRO_ID', 'CHE_PRO_ID');
		$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
		$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
		$this->db->join('left', __BP.'category', 'CAT_ID', 'CAT_ID');
		$this->db->where('and', 'PRO_STATUS=1');
		$this->db->where('and', 'CHE_STATUS=1');
		$this->db->where('and', 'CAT_STATUS=1');
		$this->db->where('and', 'CHE_USR_ID='.(int)$id);
		$chest_list = $this->db->getDataFetch();
		//var_dump($this->var);
		
		if($chest_list) {
						
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Bonus', 'Punkty', '');
			$table->createHeader($tableHeader);
			$i = 1;
			foreach($chest_list as $chest) {
				$prom = null;
				$points = $chest['PRO_POINTS'];
				
				if($chest['PPR_STATUS']!=null) {
					switch ($chest['PPR_STATUS']) {
				    case 'percent':$points = round($chest['PRO_POINTS'] - (($chest['PRO_POINTS']*$chest['PPR_VALUE'])/100));break;
				    case 'minus':$points = round($chest['PRO_POINTS'] - $chest['PPR_VALUE']);break;
				    case 'value':$points = round($chest['PPR_VALUE']);break;
					}
					$prom = '<span class="product-promotion"> PROMOCJA</span>';
				}
				
				$table->addCell($i.'.', 'lp');
				$table->addLink(helper::href(array('produkty', 'view', $chest['CAT_ID'], $chest['PRO_ALIAS'])), $chest['PRO_NAME'].$prom);
				$table->addLink(helper::href(array('produkty', 'view', $chest['CAT_ID'], $chest['PRO_ALIAS'])), $points. ' pkt');
				
				
				if($points<=$user_points)
					$table->addCell('w zasięgu ');
				else { 
					$p = (int)$points - (int)$user_points;
					$table->addCell('brakuje '.$p.' pkt');
				}
				
				$table->addRow();
				$i++;
			} 
			return $table->createBody();
		} else return ' - brak pozycji - ';//return $this->lang->chest_is_empty;
	}
	
}

function chest($option=null) {
	$chest = new chest;
	if($option!=null and method_exists($chest, $option))
		return $chest->$option();
	else 
		return $chest->showModule();
}