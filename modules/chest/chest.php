<?php
class chest extends View implements IModule {
	public $path = 'modules/chest/templates/';
	
	public function showModule() {
		return null;
	}
	
	public function getUserChest() {
		$this->db->queryString('select * from '.__BP.'chest');
		$this->db->join('left', __BP.'products','PRO_ID', 'CHE_PRO_ID');
		$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
		$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
		$this->db->join('left', __BP.'category', 'CAT_ID', 'CAT_ID');
		$this->db->where('and', 'PRO_STATUS=1');
		$this->db->where('and', 'CHE_STATUS=1');
		$this->db->where('and', 'CAT_STATUS=1');
		$this->db->where('and', 'CHE_USR_ID='.(int)$this->var->user_id);
		$chest_list = $this->db->getDataFetch();
		//var_dump($this->var);
		
		if($chest_list) {
			$cart_temp = array();
			if(isset($this->var->cart)) {
				$cart = (object)$this->var->cart;
				
				if(isset($cart->products) and count($cart->products)>0)
					foreach($cart->products as $c)
						$cart_temp[] = $c['id'];
			}
			
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Bonus', 'Punkty', '', '');
			$table->createHeader($tableHeader);
			$i = 1;
			foreach($chest_list as $chest) {
				$prom = null;
				$points = $chest['PRO_POINTS'];
				
				if($chest['PPR_STATUS']!=null) {
					switch ($chest['PPR_STATUS']) {
				    case 'percent':$points = round($chest['PRO_POINTS'] - (($chest['PRO_POINTS']*$chest['PPR_VALUE'])/100));break;
				    case 'minus':$points = round($chest['PRO_POINTS'] - $chest['PPR_VALUE']);break;
				    case 'value':$points = round($chest['PPR_VALUE']);break;
					}
					$prom = '<span class="product-promotion"> PROMOCJA</span>';
				}
				
				$table->addCell($i.'.', 'lp');
				$table->addLink(helper::href(array('produkty', 'view', $chest['CAT_ID'], $chest['PRO_ALIAS'])), $chest['PRO_NAME'].$prom);
				$table->addLink(helper::href(array('produkty', 'view', $chest['CAT_ID'], $chest['PRO_ALIAS'])), $points. ' pkt');
				
				if(is_array($cart_temp) and in_array($chest['PRO_ID'],$cart_temp)) 
					$table->addCell($this->lang->cart_in);
				else {
					if($points<=$this->var->user_points)
						$table->addCell('<button type="button" class="btn btn-success btn-sm addToCart" product="'.$chest['PRO_ID'].'" gotocart="true"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> '.$this->lang->chest_to_cart.'</button>');
					else { 
						$p = (int)$points - (int)$this->var->user_points;
						$table->addCell($this->lang->cart_too_little.$p.' pkt');
					}
				}	
				$table->addCell('<button type="button" class="btn btn-danger btn-sm chestPosDel" product="'.$chest['CHE_ID'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> '.$this->lang->chest_del.'</button>');
				$table->addRow();
				$i++;
			} 
			return $table->createBody();
		} else return $this->lang->chest_is_empty;
	}
	
	public function content($data) {
		if(is_array($data)) {
			if(!is_null($data['chest_id']))
				return $this->buttonChest('inside', $data);
			else return $this->buttonChest('outside', $data);
		} else return null;
	}
	
	public function add() {
		if(isset($this->var->product) and !is_null($this->var->product)) {
			$insert = array(
				'CHE_USR_ID' => $this->var->user_id,
				'CHE_PRO_ID' => $this->var->product,
			);
			
			$this->db->queryString(__BP.'chest');
			$chest = $this->db->insertQuery($insert);
			
			return $this->buttonChest('inside', ['product_id' => $this->var->product, 'chest_id' => $chest]);
		} else return 'false';
	}

	public function del() {
		if(isset($this->var->product) and !is_null($this->var->product)) {			
			$this->db->queryString('update '.__BP.'chest set CHE_STATUS=2');
			$this->db->where('and', 'CHE_ID='.(int)$this->var->product);
			$this->db->where('and', 'CHE_USR_ID='.(int)$this->var->user_id);
			$this->db->execQuery();
			
			return $this->getUserChest();
		} else return 'false';
	}
	
	private function buttonChest($in,$data) {
		$chestContent['outside'] = array('icon' =>'bookmark_border', 'action' => 'addToChest', 'text' => 'chest_add');
		$chestContent['inside'] = array('icon' =>'bookmark', 'action' => 'inChest', 'text' => 'chest_in');
		$list = array(
			'__CHEST_ACTION' => $chestContent[$in]['action'],
			'__PRODUCT_ID' => $data['product_id'],
			'__CHEST_ID' => $data['chest_id'],
			'__CHEST_TEXT' => $this->lang->$chestContent[$in]['text'],
			'__ICON' => $chestContent[$in]['icon'],
		);
		return Template::parseFile($list,$this->data->localPath.$this->path.'button.tpl.php');
	}
	
}

function chest($option=null) {
	$chest = new chest;
	if($option!=null and method_exists($chest, $option))
		return $chest->$option();
	else 
		return $chest->showModule();
}