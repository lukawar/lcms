<?php
$module_info['title'] = 'Artykuły';
$module_info['description'] = 'Artykuły tekstowe - tworzenie i zarządzanie';
$module_info['author'] = 'Expansja';

$menu['title'][0] = 'Artykuły';
$menu['action'][0] = 'articles';
$menu['icon'][0] = 'mode_edit';
$menu['order'][0] = '5';
$menu['show'][0] = true;
$menu['menu'][0] = true;
$menu['dashboard'][0] = false;