<?php
class articles extends View {
	public $path = 'modules/articles/templates/'; //path to templates for admin
	public $artPath = 'files/articles/'; //path to articles
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Nazwa', 'Alias','Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			
			$this->db->queryString('select * from '.__BP.'articles');
			$this->db->where('and', 'ART_PARENT=0');
			$this->db->orderBy('ART_ORDER');
			$articles = $this->db->getDataFetch();
			
			if($articles) {
				foreach($articles as $art) {
					$table->addCell($i, 'lp');
					$table->addCell($art['ART_NAME']);
					$table->addCell($art['ART_ALIAS']);
					$table->addCell($art['ART_STATUS']);
					//$table->addLink('?action=articles&option=add&id='.$art['ART_ID'], 'dodaj&nbsp;podstronę');
					$table->addLink('?action=articles&option=edit&id='.$art['ART_ID'], 'edycja');
					$table->addRow();
					$i++;
					
					//check for childs
					//NOPE
					/*$this->db->queryString('select * from '.__BP.'articles where ART_PARENT='.$art['ART_ID'].' order by ART_ORDER');
					$ch_articles = $this->db->getDataFetch();
					
					if($ch_articles) {
						$chi = 1;
						foreach($ch_articles as $chart) {
							$table->addCell(null);
							$table->addCell($chi.'. '.$chart['ART_NAME']);
							$table->addCell($chart['ART_ALIAS']);
							$table->addCell($chart['ART_STATUS']);
							$table->addCell(null);
							$table->addLink('?action=articles&option=edit&id='.$chart['ART_ID'], 'edycja');
							$table->addRow();
							$chi++;
						}
						
					}*/
				}
				$tableBody = $table->createBody();
			} else $tableBody = $this->noData();
			
			$list = array(
				'__GLYPHICON' => 'mode_edit',
				'__HEAD' => 'ARTYKUŁY',
				'__CONTENT'=> $tableBody
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('articles','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj nowy</a>');
			$page->setToolbar();
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('articles', 'addSave', 'Wypełnij dane');
			$form->addText('Nazwa', 'ART_NAME', 'required');
			$form->addInput('Tekst', 'ART_CONTENT', null, 'textarea.tpl.php');
			$form->addSelect('Status', 'ART_STATUS', array('nowy'=>'nowy', 'opublikowany'=>'opublikowany', 'ukryty'=>'ukryty') , null);
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj nowy artykuł tekstowy',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'mode_edit',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=articles" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->ART_NAME) {
				$this->db->queryString(__BP.'articles');
				$article_name = $this->checkFile(coder::stripPL($this->var->ART_NAME));
				$insert = array(
					//'ART_PARENT' => $this->var->ART_PARENT,
					'ART_NAME' => $this->var->ART_NAME,
					'ART_ALIAS' => $article_name,
					'ART_CONTENT' => addslashes($this->var->ART_CONTENT),
					'ART_STATUS' => $this->var->ART_STATUS,
					'ART_ADM_ID' => $this->var->admin_id,
				);
				$article_id = $this->db->insertQuery($insert);
				
				file_put_contents($this->artPath.$article_name.'.php', $this->var->ART_CONTENT);
				
				//save event 
				eventSaver::add($this->var->admin_id, 'new article - '. $this->var->ART_NAME, 'admin', 'articles', 'add', $article_id);
				//save backup content - the old one
				//$this->setBackup($article_name.'.php', $this->var->ART_CONTENT); 
				
				//save backup
				eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'articles', $article_id, 'add', $this->var->ART_NAME, $article_name, addslashes($this->var->ART_CONTENT));
			}
			header('location: admin.php?action=articles');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {	
			$this->db->queryString('select * from '.__BP.'articles');
			$this->db->where('and', 'ART_ID='.$this->var->id);
			$art = $this->db->getRow();
			
			$form = new Form('articles', 'editSave', 'Edycja');
			$form->addText('Nazwa', 'ART_NAME', 'required', $art['ART_NAME']);
			$form->addText('Alias', 'ART_ALIAS', 'required', $art['ART_ALIAS']);
			$form->addInput('Tekst', 'ART_CONTENT', $art['ART_CONTENT'], 'textarea.tpl.php');
			$form->addSelect('Status', 'ART_STATUS', array('nowy'=>'nowy', 'opublikowany'=>'opublikowany', 'ukryty'=>'ukryty') , $art['ART_STATUS']);
			$form->addHidden('ART_ID', $art['ART_ID']);
			$page = new Page;
			$list = array(
				'__HEAD' => 'Edycja artykułu tekstowego',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'mode_edit',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=articles" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->ART_NAME) {
				$this->db->queryString(__BP.'articles');

				$update = array(
					//'ART_PARENT' => $this->var->ART_PARENT,
					'ART_NAME' => $this->var->ART_NAME,
					'ART_ALIAS' => $this->var->ART_ALIAS,
					'ART_CONTENT' => addslashes($this->var->ART_CONTENT),
					'ART_STATUS' => $this->var->ART_STATUS,
				);
				$this->db->updateQuery($update, 'ART_ID', $this->var->ART_ID);
				
				file_put_contents($this->artPath.$this->var->ART_ALIAS.'.php', $this->var->ART_CONTENT);
				
				//save backup content - the old one
				//$this->setBackup($this->var->ART_NAME.'.php', $this->var->ART_CONTENT); 
				
				//save event 
				eventSaver::add($this->var->admin_id, 'edit article - '.$this->var->ART_NAME, 'admin', 'articles', 'edit', $this->var->ART_ID);
				
				//backup save
				eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'articles', $this->var->ART_ID, 'edit', $this->var->ART_NAME,  $this->var->ART_ALIAS, addslashes($this->var->ART_CONTENT));
			}
			header('location: admin.php?action=articles');
		} else return $this->goAway();
	}
	
	private function setBackup($file, $content) {
		$path = $this->artPath."backup/".date('Ymd_His_');
		file_put_contents($path.$file, $content);
	}
	
	private function checkFile($file) {
		if(file_exists($this->artPath.$file.'.php')) {
			$file.='_';
			return $this->checkFile($file);
		} else return $file;
	}
	
	private function getStatus($id=null) {
		$status = array('nowy', 'opublikowany', 'nieopublikowany');
		$select = new select('ART_STATUS', 'ART_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	private function getParents($id=null) {
		$this->db->queryString('select ART_ID, ART_NAME from '.__BP.'articles where ART_PARENT=0');
		$articles = $this->db->getDataFetch();
		
		$select = new select('ART_PARENT', 'ART_PARENT');
		
		$select->addNode(' - brak - ', 0);
		foreach($articles as $art) {
			if($id==$art['ART_ID'])
				$select->addNode($art['ART_NAME'], $art['ART_ID'], 'selected');
			else $select->addNode($art['ART_NAME'], $art['ART_ID']);
		}
		return $select->create();
	}
	
	 public function getForMenu() {
	 	/**/
		$menu_module = $this->var->menu_module;
		$menu_option = $this->var->menu_option;
		/**/
		
		$this->db->queryString('select ART_ID, ART_NAME, ART_ALIAS from '.__BP.'articles where ART_STATUS in (1,2)');
		$articles = $this->db->getDataFetch();
				
		foreach($articles as $article) {
			if($menu_module=='articles' and $menu_option==$article['ART_ALIAS'])
				$selected = " checked";
			else $selected = '';
				
			$line.="<input type='radio' name='menu' value='articles:".$article['ART_ALIAS'].":".$article['ART_ID'].":".$article['ART_NAME'].":text'".$selected."> - ".$article['ART_NAME']."<br/>";
		}
		
		return $line;
	}
}

function articles($option=null) {
	$articles = new articles;	
	if($option!=null and method_exists($articles, $option)) {	
		return $articles->$option();
	} else {
		return $articles->showModule();
	}
}