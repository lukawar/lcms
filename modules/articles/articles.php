<?php
class articles extends View implements IModule {
	public $artPath = 'files/articles/'; //path to articles
	
	public function showModule() {
		if(isset($this->var->option)) {
			return $this->getArticle($this->var->option);
		} else return null;
	}
	
	public function getArticleFile($name) {
		if(file_exists($this->data->localPath.$this->artPath.$name.'.php'))
			return file_get_contents($this->data->localPath.$this->artPath.$name.'.php');
		else return $this->pageError();
	}
	
	public function getArticle($name) {
		$this->db->queryString('select ART_NAME, ART_CONTENT from '.__BP.'articles');
		$this->db->where('and', 'ART_STATUS=2');
		$this->db->where('and', 'ART_ALIAS="'.$name.'"');
		$article = $this->db->getRow();
		
		if($article)
			return '<h3 class="block_title block_title_narr">'.$article['ART_NAME'].'</h3><p>&nbsp;</p>'.$article['ART_CONTENT'];
		else return $this->pageError();
	}
}

function articles($name=null) {
	$articles = new articles;
	if($name!=null)
		return $articles->getArticle($name);
	else 
		return $articles->showModules();
}