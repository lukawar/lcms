<?php
class dashboard extends View implements IModule {
	public $path = 'modules/dashboard/templates/';
	
	public function showModule() {
		$this->db->queryString("select * from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id." order by ADD_ORDER");
		$cads = $this->db->getDataFetch();
		
		if($cads) {
			$result = null;
			$module = new Module('admin_');
			foreach($cads as $c) {
				$result.= $module->load($c['ADD_ACTION'],'dashboard');
			}
			
		} else $result = "<p align='center'>Brak zdefiniowanych widgetów. Możesz zdefiniować je w <a href='?action=options'>tym miejscu</a>.</p>";
		
		$list = array(
				'__GLYPHICON' => 'tune',
				'__HEAD' => 'DASHBOARD',
				'__CONTENT'=> $result
			);
			
		$page = new Page;
		$page->setButton('<a href="'.helper::href(array('options'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj widget</a>');
		$page->setToolbar();
		return $page->setPage($list);
	}
}

function dashboard($option=null) {
	$dashboard = new dashboard;
	if($option!=null and method_exists($dashboard, $option))
		return $dashboard->$option();
	else 
		return $dashboard->showModule();
}