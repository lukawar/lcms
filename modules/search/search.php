<?php
class Search extends View {
	//public $artPath = 'articles/'; //path to articles
	public $content;
	
	public function getSearch() {
		//search in articles
		$this->db->queryString("select * from ".__BP."articles where ART_STATUS='opublikowany' and (ART_NAME like ('%".$this->var->searchString."%') or ART_CONTENT like ('%".$this->var->searchString."%'))");
		$articles = $this->db->getDataFetch();
		
		$line = "<h2>Wyniki wyszukiwania</h2><h3>Artykuły</h3>";
		if($articles) {
			foreach($articles as $art) {
				$line.="<p class='cal_header'>".$art['ART_NAME']."</p><p>".substr(strip_tags($art['ART_CONTENT']), 0, 200)."</p><p align='right'><a href='?action=articles&option=".$art['ART_ALIAS']."' class='more'>czytaj całość &raquo;</a></p><div class='cal_spacer'></div><p>&nbsp;</p>";
			}
			
		} else $line.= "<p style='font-style: italic;text-align: center;'> - brak wyników - </p>";
		
		//search in news
		$this->db->queryString("select * from ".__BP."news where NEW_STATUS in (1,2) and (NEW_NAME like ('%".$this->var->searchString."%') or NEW_DESC_LONG like ('%".$this->var->searchString."%'))");
		$news = $this->db->getDataFetch();
		
		$line.= "<h3>Aktualności</h3>";
		if($news) {
			foreach($news as $new) {
				$line.="<p class='cal_header'>".$new['NEW_NAME']." <span>(".$new['NEW_DATE_DISPLAY'].")</span></p><p>".substr(strip_tags($new['NEW_DESC_LONG']), 0, 200)."</p><p align='right'><a href='?action=news&option=show&id=".$new['NEW_ID']."' class='more'>czytaj całość &raquo;</a></p><div class='cal_spacer'></div><p>&nbsp;</p>";
			}
			
		} else $line.= "<p style='font-style: italic;text-align: center;'> - brak wyników - </p>";
		
		//search in calendarium
		/*$cal_data = new Connector("select * from ".__BP."calendarium where CAL_ACTIVE='active' and CAL_TYPE='kalendarium' and (CAL_NAME like ('%".$this->var->string."%') or CAL_TEXT like ('%".$this->var->string."%'))");
		//$news_data->query();
		$cals = $cal_data->getDataFetch();
		
		$line.= "<h4>Kalendarium</h4>";
		if($cals) {
			foreach($cals as $cal) {
				$line.="<p class='cal_header'>".$cal['CAL_NAME']." <span>(".$cal['CAL_DATE'].")</span></p><p>".$cal['CAL_TEXT']."</p><div class='cal_spacer'></div><p>&nbsp;</p>";
			}
			
		} else $line.= "<p style='font-style: italic;text-align: center;'> - brak wyników - </p>";*/
		
		return $line;
	}
	
	public function getTitle() {
		return "Wyniki szukania";
	}
}

function search($option=null) {
	$search = new Search;
	
	if($option!=null and method_exists($search, $option)) {	
		return $search->$option();
	} else 
		return $search->getSearch();
}
?>