<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=gallery">Galeria</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-th"></i>
					<span>Kategorie galerii</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar"><a href="?action=gallery&option=addCategory" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nową kategorię</a></div>
				<div id="contentPlace">__TABLE_CATEGORY</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-th"></i>
					<span>Pozycje galerii</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar">filtruj <select name="f_cat" id="f_cat" onChange="catFilter()">__SELECT_CATEGORY</select> &nbsp;&nbsp;&nbsp;&nbsp;<a href="?action=gallery&option=addGallery" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy plik</a>&nbsp; &nbsp;<a href="?action=gallery&option=viewTiles" class="btn btn-primary btn-label-left"><span><i class="fa fa-th"></i></span>widok siatki</a> </div>
				<div id="contentPlace">__TABLE_GALLERY</div>
			</div>
		</div>
	</div>
</div>

<script>
function catFilter() {
	//var filter = document.getElementById('f_cat').value;
	window.location.href='?action=gallery&cat='+document.getElementById('f_cat').value;
}

document.getElementById('f_cat').value = '__SELECT_VALUE';
</script>