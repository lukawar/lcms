<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=gallery">Galeria</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-th"></i>
					<span>Pozycje galerii</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar">filtruj __SELECT_CATEGORY &nbsp;&nbsp;&nbsp;&nbsp;<a href="?action=gallery&option=addGallery" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy plik</a>&nbsp; &nbsp;<a href="?action=gallery" class="btn btn-primary btn-label-left"><span><i class="fa fa-list"></i></span>widok szczegółowy</a> </div>
				<div id="contentPlace">__GALLERY_TILES</div>
			</div>
		</div>
	</div>
</div>

<script>
function catFilter() {
	//var filter = document.getElementById('f_cat').value;
	window.location.href='?action=gallery&option=viewTiles&cat='+document.getElementById('GCA_ID').value;
}

//document.getElementById('GCA_ID').value = '__SELECT_VALUE';
</script>