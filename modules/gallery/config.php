<?php
$module_info['title'] = 'Galerie';
$module_info['description'] = 'Galerie - tworzenie i zarządzanie';
$module_info['author'] = 'Expansja';

$menu['title'][0] = 'Galerie';
$menu['action'][0] = 'gallery';
$menu['icon'][0] = 'photo_library';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;