<?php
class options extends View implements IModule {
	public $path = 'modules/options/templates/';
	public $avatarPath = 'files/avatar/';
	
	public function showModule() {
		$list = array(
				'__GLYPHICON' => 'settings',
				'__HEAD' => 'USTAWIENIA KONTA',
				'__CONTENT'=> $this->getModulesForDashboard()
			);
		
		$page = new Page;
		$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
		$page->setButton('<button type="button" url="?action=dashboard" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
		$page->setToolbar();			
		return $page->setPage($list, 'pageForm.tpl.php');
	}
	
	public function profile() {
		$this->db->queryString('select * from '.__BP.'administrator');
		//$this->db->join('left', __BP.'client_config', 'CCO_ADM_ID', 'ADM_ID');
		$this->db->where('and', 'ADM_ID='.$this->var->admin_id);
		$admin = $this->db->getRow(); 
		
		$form = new Form('options', 'profileSave', 'Dane osobowe', 'multipleForm.tpl.php');
		$form->addText('Imię', 'ADM_NAME', 'required',$admin['ADM_NAME']);
		$form->addText('Nazwisko', 'ADM_SURNAME', 'required',$admin['ADM_SURNAME']);
		$form->addText('E-mail', 'ADM_MAIL', 'required',$admin['ADM_MAIL']);
		$form->addSubmit('Zapisz dane');
		
		$formLogin = new Form('options', 'passwordChange', 'Dane logowania', 'multipleForm.tpl.php');
		$formLogin->addText('Login', 'ADM_LOGIN', 'required',$admin['ADM_LOGIN']);
		$formLogin->addInput('Hasło', 'ADM_PASS1', null, 'password.tpl.php', 'required');
		$formLogin->addInput('Hasło ponownie', 'ADM_PASS2', null, 'password.tpl.php', 'required');
		$formLogin->addSubmit('Zapisz dane');		
		
		$formAvatar = new Form('options', 'avatarChange', 'Wygląd profilu', 'multipleForm.tpl.php');
		$formAvatar->addElement($this->getAvatars());
		$formAvatar->addInput('Dodaj nowe zdjęcie', 'NEW_ADM_AVATAR', null, 'file.tpl.php');
		
		$formAvatar->addSubmit('Zapisz dane');
		
		$page = new Page;
		$list = array(
			'__HEAD' => 'EDYCJA PROFILU',
			'__CONTENT' => $form->setForm().$formLogin->setForm().$formAvatar->setForm(),
			'__GLYPHICON' => 'settings',
		);
		
		return $page->setPage($list, 'default.tpl.php');
	}
	
	private function getAvatars() {	
		$image[] = $this->data->localPath.$this->avatarPath.'bb-big.png';
		$image[] = $this->data->localPath.$this->avatarPath.'expansja.png';
		
		if(!in_array($this->var->admin_avatar, $image))
			$image[] = $this->var->admin_avatar;
		
		$avatar_list = null;
		foreach($image as $i) {
			$checked = null;
			if($this->var->admin_avatar==$i)
				$checked = ' checked="checked"';
			$avatar_list.= '<div class="col-sm-6 col-md-3"><p align="center"><img src="'.$i.'" class="img-responsive" width="100px"><br/><input type="radio" name="avatar_image" value="'.$i.'"'.$checked.'></p></div>';
		}
			
		return '<div class="form-group labeled">'.$avatar_list.'</div>';
	}
	
	public function avatarChange() {
		if($_FILES['NEW_ADM_AVATAR']['name']) {
			$update['ADM_AVATAR'] = $this->filesSave('NEW_ADM_AVATAR');
		} else {
			$update['ADM_AVATAR'] = $this->var->avatar_image;
		}
		$this->db->queryString(__BP.'administrator');
		$this->db->updateQuery($update, 'ADM_ID', $this->var->admin_id);
		
		header('location: admin.php?action=dashboard');
	}
	
	private function filesSave($file) {
		if($_FILES[$file]['name']) {
  		$filename = md5(microtime());
  		$image_file = new ImageLoader(90,90, $filename, $file, $this->data->localPath.$this->avatarPath);
  		$image = $image_file->load();
      
      return $this->data->localPath.$this->avatarPath.$filename.$image;
		}
	}
	
	public function getModulesForDashboard() {
		$handle = opendir($this->data->localPath.$this->data->modulesPath);
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		unset($list);
		
		foreach($catalogs as $module) {
			unset($menu);
			unset($dashboard);
		  if(file_exists($this->data->localPath.$this->data->modulesPath.$module.'/config.php'))  {
		    include($this->data->localPath.$this->data->modulesPath.$module.'/config.php');
		    if(isset($menu) and $menu['dashboard'][0]==true) {
		    	$list[$menu['action'][0]] = array(
							'action' => $menu['action'][0],
							'title' => $menu['title'][0],
						);
		    }
		  }
		}
		
		//get modules for list
		$this->db->queryString("select ADD_ACTION, ADD_ORDER from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id);
		$cad = $this->db->getDataFetch();
		if($cad) {
			foreach($cad as $c)
				$cad_list[$c['ADD_ACTION']] = $c['ADD_ORDER'];
		} else $cad_list = array();
		
		//get privileges for admin
		$this->db->queryString("select ADR_ROLE_ID from ".__BP."admin_roles where ADR_ADM_ID=".$this->var->admin_id);
		$car = $this->db->getDataFetch();
		if($car) {
			foreach($car as $cr)
				$car_list[] = $cr['ADR_ROLE_ID'];
		} else $car_list = array();
		
		if(isset($list) and is_array($list)) {
			$form = new Form('options', 'moduleSave', 'Wybierz widgety');
			$i = 1;

			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Moduł', 'Włączony', 'Pozycja', '');
			$table->createHeader($tableHeader);
			
			foreach($list as $l) {
				if(in_array($l['action'], $car_list) or $this->var->admin_role=='admin') {
					
					if(array_key_exists($l['action'], $cad_list))
						$bold = $cad_list[$l['action']];
					else $bold = null;
					
					$table->addCell($i, 'lp');
					$table->addCell($l['title'], $bold!=null ? 'td_bold' : '');
					$table->addCell($bold!=null ? 'tak' : 'nie', $bold!=null ? 'td_bold' : '');
					$table->addCell($bold!=null ? "<input type='text' name='m_order[".$l['action']."]' value='".$bold."'>" : '');
					$table->addCell($bold!=null ? "<input type='checkbox' name='m_action[".$l['action']."]' checked='checked'>" : "<input type='checkbox' name='m_action[".$l['action']."]'>");
					$table->addRow();
					$i++;
				}
			}
			$form->addElement($table->createBody());
			
			return $form->setForm();
		} else return $this->noData();
	}
	
	public function moduleSave() {
		$this->db->queryString("delete from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id);
		$this->db->execQuery();
		
		if($this->var->m_action) {
			foreach($this->var->m_action as $key=>$value) {
				$list = array(
					'ADD_ADM_ID'=>$this->var->admin_id,
					'ADD_ACTION'=>$key,
					'ADD_ORDER'=>$this->var->m_order[$key]
				);
				$this->db->queryString(__BP.'admin_dashboard');
				$this->db->insertQuery($list);
				unset($list);
			}
		}
		header("location: admin.php");
	}
	
	public function profileSave() {
		if($this->var->ADM_NAME) {
			$update = array(
				'ADM_NAME'=>$this->var->ADM_NAME,
				'ADM_SURNAME'=>$this->var->ADM_SURNAME,
				'ADM_MAIL'=>$this->var->ADM_MAIL,
			);
			$this->db->queryString(__BP.'administrator');
			$this->db->updateQuery($update, 'ADM_ID', $this->var->admin_id);	
		}
		
		header("location: admin.php?action=dashboard");
	}
	
	public function passwordChange() {
		if($this->var->ADM_PASS1 and $this->var->ADM_PASS2) {
			$update = array('ADM_PASS'=>coder::hashString($this->var->ADM_PASS1), 'ADM_LOGIN' => $this->var->ADM_LOGIN);
			
			$this->db->queryString(__BP.'administrator');
			$this->db->updateQuery($update, 'ADM_ID', $this->var->admin_id);
			header("location: logout.php?redirect=admin");
		}
	}
	
	public function emailSave() {
			$update = array(
				'CCO_EMAIL' => $this->var->CCO_EMAIL,
				'CCO_HOST' => $this->var->CCO_HOST,
				'CCO_USER' => $this->var->CCO_USER,
				'CCO_PASS' => $this->var->CCO_PASS,
				'CCO_SMTP_AUTH' => $this->var->CCO_SMTP_AUTH,
				'CCO_SSL' => $this->var->CCO_SSL,
				'CCO_PORT' => $this->var->CCO_PORT,
				'CCO_FROM_NAME' => $this->var->CCO_FROM_NAME,
				'CCO_REPLY_TO' => $this->var->CCO_REPLY_TO,
			);
			
			//save config data to file
			file_put_contents('files/data/'.$this->var->admin_alias.'/config.php', '<?php $data='."'".json_encode($update)."';");
			
			$this->db->queryString(__BP.'client_config');
			$this->db->updateQuery($update, 'CCO_ADM_ID', $this->var->admin_id);
			
			header("location: ?action=options&option=profile");
	}

	
	public function getInstalledModules() {
		$line = null;
		$handle = opendir($this->data->localPath.$this->data->modulesPath);
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists($this->data->localPath.$this->data->modulesPath.$module.'/config.php'))  {
		    include($this->data->localPath.$this->data->modulesPath.$module.'/config.php');
		    
		    if(isset($menu)) {
		    	$list = array(
							'__AUTHOR' => $module_info['author'],
							'__TEXT' => $module_info['description'],
							'__CLASS_ICON' => $menu['icon'][0],
							'__TITLE' => $module_info['title'],
							'__DASHBOARD' => null,
						);
					 
					 //$result[$menu['order'][0]] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
		    }
		  }
		}
		
		ksort($result);
		foreach($result as $key=>$value)
			$line.=$value;
		return $line;
	}
	
	public function avatar(){
		return $this->var->admin_avatar;
	}
	
	public function adminName(){
		return $this->var->admin_data;
	}
	
	public function system() {
		if($this->var->admin_role=='admin') {			
			$list = array(
				'__GLYPHICON' => 'settings',
				'__HEAD' => 'KONFIGURACJA',
				'__CONTENT'=> $this->getInstalledModules(),
			);
		
			$page = new Page;
			$page->setButton('<a class="btn btn-warning" target="_blank" href="'.$this->data->localPath.$this->path.'info.php"><span class="glyphicon glyphicon-cloud" aria-hidden="true"></span> konfiguracja serwera</a>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
}

function options($option=null) {
	$options = new options;
	if($option!=null and method_exists($options, $option))
		return $options->$option();
	else 
		return $options->showModule();
}