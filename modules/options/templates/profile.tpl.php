<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=options&option=profile">Profil</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-user"></i>
					<span>Dane konta</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Dane osobowe</h4>
				<form name="adminEdit" action="index.php" method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">Imię</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="ADM_NAME"  value="__ADM_NAME" required/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwisko</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="ADM_SURNAME"  value="__ADM_SURNAME" required/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Email</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="ADM_MAIL" value="__ADM_MAIL" required/>
					</div>
				</div>
				
				<input type="hidden" name="action" value="options">
				<input type="hidden" name="option" value="profileSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
					</div>
				</div>
				</form>
				
				<h4 class="page-header">Zmiana hasła</h4>
				<form name="passwordChange" id="passwordChange" action="index.php" method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">Login</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="ADM_LOGIN" id="ADM_LOGIN" required value="__ADM_LOGIN"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Hasło</label>
					<div class="col-sm-5">
						<input type="password" class="form-control" name="ADM_PASS1" id="ADM_PASS1" required/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Powtórz hasło</label>
					<div class="col-sm-5">
						<input type="password" class="form-control" name="ADM_PASS2" id="ADM_PASS2" required/><span class="spanInfo"></span>
					</div>
				</div>
				
				<input type="hidden" name="action" value="options">
				<input type="hidden" name="option" value="passwordChange">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
					</div>
				</div>
				</form>
				
				<h4 class="page-header">Dane konta pocztowego</h4>
				<form name="smtpData" id="smtpData" action="index.php" method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">Konto wysyłające</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_EMAIL" id="CCO_EMAIL" required value="__CCO_EMAIL"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa hosta</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_HOST" id="CCO_HOST" required value="__CCO_HOST"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa użytkownika</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_USER" id="CCO_USER" required value="__CCO_USER"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Hasło</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_PASS" id="CCO_PASS" required value="__CCO_PASS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Autoryzacja SMTP</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_SMTP_AUTH" id="CCO_SMTP_AUTH" required value="__CCO_SMTP_AUTH"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Używaj SSL</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_SSL" id="CCO_SSL" required value="__CCO_SSL"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Port</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_PORT" id="CCO_PORT" required value="__CCO_PORT"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Pole nadawca</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_FROM_NAME" id="CCO_FROM_NAME" required value="__CCO_FROM_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Pole REPLY-TO</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="CCO_REPLY_TO" id="CCO_REPLY_TO" required value="__CCO_REPLY_TO"/>
					</div>
				</div>
				<input type="hidden" name="action" value="options">
				<input type="hidden" name="option" value="emailSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
					</div>
				</div>
				</form>
					
			</div>
		</div>
	</div>
</div>
<script>
$('#passwordChange').submit(function() {
	if($('#ADM_PASS1').val()!=$('#ADM_PASS2').val()) {
		$('.spanInfo').html('Hasła są różne!');
		return false;
	} else return true;
});
</script>