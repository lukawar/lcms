<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=options">Ustawienia konta</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Dostępne moduły wyświetlane na stronie startowej</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					<form name="module_table" action="" method="post">
						__MODULE_TABLE
						<div class="form-group">
							<div class="col-sm-9">
								<button type="submit" class="btn btn-primary">Zapisz</button>
							</div><br/>
						</div>
						<input type="hidden" name="action" value="options">
						<input type="hidden" name="option" value="moduleSave">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>