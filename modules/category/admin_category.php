<?php
class category extends View {
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'category');
			$this->db->where('and', 'CAT_PARENT=0');
			$categories = $this->db->getDataFetch();
			
			if($categories) {
				$tableCategory = new table('dataTable', 'table table-striped stripe hover');
				$tableCategoryHeader = array('lp.', 'Nazwa', 'Alias', 'Produkty', 'Status', '');
				$tableCategory->createHeader($tableCategoryHeader);
				
				$i = 1;
			
				foreach($categories as $cat) {
					$tableCategory->addCell($i, 'lp');
					$tableCategory->addCell($cat['CAT_NAME']);
					$tableCategory->addCell($cat['CAT_ALIAS']);
					$tableCategory->addCell($cat['CAT_COUNT']);
					$tableCategory->addCell($cat['CAT_STATUS']);
					//$tableCategory->addLink('admin.php?action=produkty&option=addCategory&id='.$cat['CAT_ID'], 'podkategoria',null, null, 'podkategoria');
					$tableCategory->addLink('admin.php?action=category&option=edit&id='.$cat['CAT_ID'], 'edycja',null, null, 'edycja');
				//	$tableCategory->addLink('admin.php?action=category&option=delete&id='.$cat['CAT_ID'], 'usuń',null, null, 'edycja');
					$tableCategory->addRow();
					$i++;
					
					/*no subcategories
					$this->db->queryString('select * from '.__BP.'category');
					$this->db->where('and', 'CAT_PARENT='.$cat['CAT_ID']);
					$subcategories = $this->db->getDataFetch();
					if($subcategories) {
						$is = 1;
						foreach($subcategories as $scat) {
							$tableCategory->addCell();
							$tableCategory->addCell($is.'. '.$scat['CAT_NAME']);
							$tableCategory->addCell($scat['CAT_ALIAS']);
							$tableCategory->addCell($scat['CAT_STATUS']);
							$tableCategory->addCell();
							$tableCategory->addLink('admin.php?action=produkty&option=editCategory&id='.$scat['CAT_ID'], 'edycja',null, null, 'edycja');
							$tableCategory->addRow();
							$is++;
						}		
					}*/
				}
				$tableCategory = $tableCategory->createBody();
			} else $tableCategory = $this->noCategory();
			
			$list = array(
				'__HEAD' => 'Kategorie produktów',
				'__CONTENT' => $tableCategory,
				'__GLYPHICON' => 'storage',
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('category','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj kategorię</a>');
			$page->setToolbar();
			return $page->setPage($list);
		} else return $this->goAway();	
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('category', 'addSave', 'Wypełnij dane');
			$form->addText('Nazwa', 'CAT_NAME', 'required');
			
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj kategorię produktu',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'storage',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=category" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$insert = array(
				'CAT_NAME' => $this->var->CAT_NAME,
				'CAT_ALIAS' => coder::stripPL($this->var->CAT_NAME),
				//'CAT_STATUS' => $this->var->CAT_STATUS,
				//'CAT_PARENT' => $this->var->PRO_CAT_ID,
			);
			
			$this->db->queryString(__BP.'category');
			$category = $this->db->insertQuery($insert);
			
			$this->generateCategoryTree();
			
			//save event 
			eventSaver::add($this->var->admin_id, 'add category - '. $this->var->CAT_NAME, 'admin', 'category', 'add', $category);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'category', $category, 'add', $this->var->CAT_NAME);
			
			header('location: admin.php?action=category');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'category where CAT_ID='.$this->var->id);
			$cat = $this->db->getRow();

			if($cat) {
				$form = new Form('category', 'editSave', 'Edycja pozycji');
				$form->addText('Nazwa', 'CAT_NAME', 'required', $cat['CAT_NAME']);
				$form->addText('Alias', 'CAT_ALIAS', 'required', $cat['CAT_ALIAS']);
				$form->addSelect('Status', 'CAT_STATUS', $this->getStatus(), $cat['CAT_STATUS']);
				$form->addHidden('id', $cat['CAT_ID']);
				
				$page = new Page;
				$list = array(
					'__HEAD' => 'Dodaj kategorię produktu',
					'__CONTENT' => $form->setForm(),
					'__GLYPHICON' => 'storage',
				);
				
				$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
				$page->setButton('<button type="button" url="?action=category" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
				$page->setToolbar();			
				return $page->setPage($list, 'pageForm.tpl.php');
			} else return $this->noData();
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array(
				'CAT_NAME' => $this->var->CAT_NAME,
				'CAT_ALIAS' => $this->var->CAT_ALIAS,
				'CAT_STATUS' => $this->var->CAT_STATUS,
				//'CAT_PARENT' => $this->var->PRO_CAT_ID,
			);
			
			$this->db->queryString(__BP.'category');
			$this->db->updateQuery($update, 'CAT_ID', $this->var->id);
			
			$this->generateCategoryTree();
			
			//save event 
			eventSaver::add($this->var->admin_id, 'edit category - '. $this->var->CAT_NAME, 'admin', 'category', 'edit', $this->var->id);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'category', $category, 'edit', $this->var->CAT_NAME, 'status: '.$this->var->CAT_STATUS);
			
			header('location: admin.php?action=category');
		} else return $this->goAway();
	}
	
	public function generateCategoryTree() {
		$catPath = $this->data->localPath.'files/generated/';
		$template = '<li><a href="%s">%s</a><span>%s</span></li>';
		$line = null;
		
		$this->db->queryString('select * from '.__BP.'category');
		$this->db->where('and', 'CAT_STATUS=1');
		$this->db->where('and', 'CAT_PARENT=0');
		$this->db->orderBy('CAT_ORDER');
		$categories = $this->db->getDataFetch();
		$i = 1;
		foreach($categories as $cat) 
			$line.= sprintf($template, helper::href(array('category', 'view', $cat['CAT_ALIAS'])),$cat['CAT_NAME'], $cat['CAT_COUNT']);
		
		file_put_contents($catPath.'category.php', $line);		
	}
	
	private function getStatus() {
		$status = array(
			'active' => 'aktywny',
			'inactive' => 'nieaktywny',
		);
		return $status;		
	}
}

function category($option=null) {
	$category = new category;
	if($option!=null and method_exists($category, $option))
		return $category->$option();
	else 
		return $category->showModule();
}