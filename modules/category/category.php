<?php
class category extends View {
	public $path = 'modules/category/templates/';
	public $imgPath = 'files/product/';
	public $forPage = 18;
	
	public function showModule() {
		return file_get_contents($this->data->localPath.'files/generated/category.php');
	}
	
	public function searchProduct(){
		if(isset($this->var->searchString) and $this->var->searchString!=null) {
			header('location: '.helper::href(['category','search',$this->var->searchString]));
		}	else header('location: homepage.html');
	}
	
	public function search() {
		if(isset($this->var->id) and $this->var->id!=null) {
			return $this->view($this->var->id, $this->lang->search_title);
		} else return $this->noProduct();
	}
	
	public function view($search=null, $title=null) {
		$line = null;
		if(isset($this->var->id) and $this->var->id!=null) {
			$this->db->queryString('select SQL_CALC_FOUND_ROWS *, PRO_ID,PPR_STATUS,PRO_POINTS,PPR_VALUE,CAT_ID,PRO_ALIAS,PRO_FOTO,PRO_CODE,PNE_STATUS,PRO_NAME,PRO_DESC_SHORT,CAT_NAME from '.__BP.'products');
			$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
			$this->db->join('left', __BP.'category', 'CAT_ID', 'CAT_ID');
			$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID', 'PPR_STATUS<>4');
			$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID', 'PNE_STATUS<>3');
			$this->db->where('and', 'CAT_STATUS=1');
			$this->db->where('and', 'PRO_STATUS=1');
			
			if(!is_null($search))
				$this->db->where('and', 'PRO_NAME like "%'.$this->var->id.'%"');
			else $this->db->where('and', 'CAT_ALIAS="'.$this->var->id.'"');
			
			//sort
			if(isset($this->var->sort_by) and !is_null($this->var->sort_by) and isset($this->var->sort_type) and !is_null($this->var->sort_type))
				$this->db->orderBy($this->var->sort_by.' '.$this->var->sort_type);
			
			if(isset($this->var->cat) and (int)$this->var->cat>0) {
				$limit = ((int)$this->var->cat - 1) * $this->forPage;
				$page = (int)$this->var->cat;
			} else { 
				$limit = 0;
				$page = 1;
			}
			
			//paging
			$this->db->limit($limit,$this->forPage);
			
			$products = $this->db->getDataFetch();
			
			$all = $this->db->getFoundRows();
			
			//add category alias to redirect
			$_SESSION['last_category']	= $this->var->id;
			
			if($products) {	
				$text_cart_too_little = $this->lang->cart_too_little;
				$text_cart_add = $this->lang->cart_add_to;
			
				$template = file_get_contents($this->data->localPath.$this->path.'category.tpl.php');
				$placeholders = array(
					'__PRO_ID', 
					'__PRO_NAME', 
					'__PRO_POINTS', 
					'__PRO_FOTO', 
					'__PRO_DESC_SHORT', 
					'__PRO_LINK', 
					'__BUTTON',
					'__FLAG_CLASS',
					'__PRODUCT_TYPE'
				);
				
				foreach($products as $pro) {
					$promPoints = null;
					$flagClass = null;
					$productType = null;
					
					$price = $pro['PRO_POINTS'];
					
					//promotions
					if($pro['PPR_STATUS']!=null) {
						switch ($pro['PPR_STATUS']) {
					    case 'percent':$promPoints = round($pro['PRO_POINTS'] - (($pro['PRO_POINTS']*$pro['PPR_VALUE'])/100));break;
					    case 'minus':$promPoints = round($pro['PRO_POINTS'] - $pro['PPR_VALUE']);break;
					    case 'value':$promPoints = round($pro['PPR_VALUE']);break;
						}
						$productType = '<h5 class="product-promotion"><span>PROMOCJA</span> '.$promPoints.' pkt + 1 zł</h5>';
						$flagClass = 'striked';
						$price = $promPoints;
					}
					
					//new
					if($pro['PNE_STATUS']!=null) {
						$productType.= '<h5 class="product-new"><span>NOWOŚĆ</span></h5>';
					}
					
					$link = helper::href(array('produkty', 'view', $pro['CAT_ID'], $pro['PRO_ALIAS']));
					if($this->var->user_points>=$price)
						$button = '<button class="buttonUrl enough" url="'.$link.'">'.$text_cart_add.'</button>';
					else {
						$points = (int)$price - (int)$this->var->user_points;
						$button = '<button class="not_enough">'.$text_cart_too_little.$points.' pkt</button>';
					}
					
					if($pro['PRO_FOTO']!=null)
						$imagePath = $this->data->localPath.$this->imgPath.$pro['PRO_CODE'].'/thumb2_'.$pro['PRO_FOTO'];
					else $imagePath = $this->data->localPath.$this->imgPath.'noproduct_2.jpg';
					
					$data = array(
						$pro['PRO_ID'], 
						$pro['PRO_NAME'], 
						$pro['PRO_POINTS'], 
						$imagePath, 
						$pro['PRO_DESC_SHORT'], 
						$link, 
						$button,
						$flagClass,
						$productType
					);
					$line.=str_replace($placeholders, $data, $template);
				}
				
				$line = '<div class="row">'.$line.'</div>';
				
				if(is_null($title))
					$title = $pro['CAT_NAME'];
					
				//paging numbers & links
				if($all>(int)$this->forPage) {
					$pagerElements = null;
					
					$elements = ceil((int)$all/(int)$this->forPage);
					
					for($i=1;$i<=$elements;$i++) {
						$class = null;
						if($i==$page)
							$class = 'active';
						$pagerElements.='<li class="page-item '.$class.'"><a class="page-link" href="'.helper::href(array($this->var->action, $this->var->option, $this->var->id, $i)).'">'.$i.' </a></li>';
					}
					
					$prev = helper::href(array($this->var->action, $this->var->option, $this->var->id, $page-1));
					$next = helper::href(array($this->var->action, $this->var->option, $this->var->id, $page+1));
					$prevClass = null;
					$nextClass = null;
					
					if($limit<1) {
						$prev = null;
						$prevClass = 'disabled';
					} 
					
					if($page>=$elements) {
						$next = null;
						$nextClass = 'disabled';
					}
					
					$pagerTemplate = array(
						'__PREV' => $prev,
						'__NEXT' => $next,
						'__CLASS_PREV' => $prevClass,
						'__CLASS_NEXT' => $nextClass,
						'__NAV' => $pagerElements,
					);
					
					//echo 'all:'.$all.' page: '.$page.' limit: '.$limit;
					
					$pager = Template::parseFile($pagerTemplate, $this->data->localPath.$this->path.'pager.tpl.php');
				} else $pager = null;
					
				return '<h3 class="block_title">'.$title.'</h3>'.$pager.$line.$pager;
			} else return $this->noProduct();
		}
	}
}