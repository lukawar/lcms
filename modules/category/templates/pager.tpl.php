<div class="row">
	<nav aria-label="Page navigation col-xs-12">
		<ul class="pagination pagination-sm">
			<li class="page-item __CLASS_PREV"><a href="__PREV" aria-label="Poprzedni" class="page-link"><span aria-hidden="true">&laquo;</span><span class="sr-only">Poprzedni</span></a></li>
			__NAV
			<li class="page-item __CLASS_NEXT"><a href="__NEXT" aria-label="Next" class="page-link"><span aria-hidden="true">&raquo;</span><span class="sr-only">Następny</span></a></a></li>
		</ul>
	</nav>
</div>