<section class="product-item col-xs-12 col-sm-6 col-lg-4" id="list-__PRO_ID">
	<figure><a href="__PRO_LINK" title="__PRO_NAME"><img src="__PRO_FOTO" alt=""></a></figure>
	<div class="points-flag	__FLAG_CLASS">
		<span class="pr-points txt-med">__PRO_POINTS</span>
		<span class="pr-flag-unit">PKT</span>
		<span class="pr-add-pln txt-med"> + 1 zł</span>
	</div>
	<div class="product-info">
		__PRODUCT_TYPE
		<h3 class="prod-list-title">__PRO_NAME</h3>
		<p class="prod-list-desc">__PRO_DESC_SHORT</p>
	</div>
	<div class="button-centerer">__BUTTON</div>
</section>