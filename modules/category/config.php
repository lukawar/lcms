<?php
$module_info['title'] = 'Kategorie produktów';
$module_info['description'] = 'Kategorie produktów - aktualizacja i zarządzanie';
$module_info['author'] = 'expansja';

$menu['title'][0] = 'Kategorie';
$menu['action'][0] = 'category';
$menu['icon'][0] = 'storage';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;