<div class="item"><section class="product-item col-xs-12 col-sm-12 col-lg-12">
	<figure><a href="__PRODUCT_HREF" title="__PRODUCT_NAME"><img src="__PRODUCT_THUMB" alt=""></a></figure>
	<div class="points-flag	">
		<span class="pr-points txt-med">__PRODUCT_PKT</span>
		<span class="pr-flag-unit">PKT</span>
		<span class="pr-add-pln txt-med"> + 1 zł</span>
	</div>
	<div class="product-info">
		<h5 class="product-new"><span>NOWOŚĆ</span></h5>
		<h3 class="prod-list-title">__PRODUCT_NAME</h3>
		<p class="prod-list-desc"></p><p>__PRODUCT_DESCR</p><p></p>
	</div>
	<div class="button-centerer">__BUTTON</div>
</section></div>