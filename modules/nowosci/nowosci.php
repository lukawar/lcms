<?php
class nowosci extends View implements IModuleHomepage {
	public $path = 'modules/nowosci/templates/';
	public $imgPath = 'files/product/';
	
	public function showModule() {
		return null;
	}
	
	public function homepage() {
		return $this->getProducts();
	}
	
	public function getProducts() {
		$this->db->queryString('select * from '.__BP.'products');
		$this->db->join('left', __BP.'products_news', 'PNE_PRO_ID', 'PRO_ID');
		$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
		$this->db->where('and', 'PRO_STATUS=1');
		$this->db->where('and', 'PNE_STATUS<>3');
		$this->db->limit(10);
		
		$news = $this->db->getDataFetch();
		//$this->db->prepareQuery();$this->db->query();
		if($news) {
			$result = null;
			$text_cart_too_little = $this->lang->cart_too_little;
			$text_cart_add = $this->lang->cart_add_to;
			
			foreach($news as $n) {
				$link = helper::href(array('produkty', 'view', $n['CAT_ID'], $n['PRO_ALIAS']));
				if($this->var->user_points>=$n['PRO_POINTS'])
					$button = '<button class="buttonUrl enough" url="'.$link.'">'.$text_cart_add.'</button>';
				else {
					$points = (int)$n['PRO_POINTS'] - (int)$this->var->user_points;
					$button = '<button class="not_enough">'.$text_cart_too_little.$points.' pkt</button>';
				}
				
				$data = array(
					'__PRODUCT_HREF' => helper::href(array('produkty', 'view', $n['CAT_ID'], $n['PRO_ALIAS'])),
					'__PRODUCT_NAME' => $n['PRO_NAME'],
					'__PRODUCT_PKT' => $n['PRO_POINTS'],
					'__PRODUCT_DESCR' => $n['PRO_DESC_SHORT'],
					'__PRODUCT_THUMB' => $this->data->localPath.$this->imgPath.$n['PRO_CODE'].'/thumb2_'.$n['PRO_FOTO'],
					'__BUTTON' => $button,
				);
				$result.= Template::parseFile($data, $this->data->localPath.$this->path.'default.tpl.php');
				unset($data);
			}
			return $result;
		} else return null;
	}
}