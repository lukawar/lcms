<?php
class nowosci extends View implements IModule {
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			return $model->load('produkty', 'getData', 'nowosci');
		} else return $this->goAway();
	}
}

function nowosci($option=null) {
	$nowosci = new nowosci;
	if($option!=null and method_exists($nowosci, $option))
		return $nowosci->$option();
	else 
		return $nowosci->showModule();
}