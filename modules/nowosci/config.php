<?php
$module_info['title'] = 'Nowości';
$module_info['description'] = 'Nowości produktów - aktualizacja i zarządzanie';
$module_info['author'] = 'expansja';

$menu['title'][0] = 'Nowości';
$menu['action'][0] = 'nowosci';
$menu['icon'][0] = 'whatshot';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;