<?php
class import extends View  {
	private $path = 'files/json/';
	
	//get data from api, create backup ad insert data
	public function getData() {
		if($this->var->key==SC_API_KEY) {
		//if(1) {
			//create ctatalog (month-year) if not exists
			$this->generatedCatalog = date('mY').'/';
			
			$catalog = $this->data->localPath.$this->path.$this->generatedCatalog;
			$fileName = date('Ymd_His').'_import.php';
			
			if(!file_exists($catalog))
				if (!mkdir($catalog, 0777, true))
	    		die('Failed to create folders...');
	    		
			//get date from and to
			$today = date('Y-m-d');
			
			$weekCalc = $this->getStartAndEndDate();
			
			//die('week: '.$currentWeek.' date: '.$weekCalc['week_start'].' '.$weekCalc['week_end'].'<br/>');

			$data = new getJSON;
			$this->json =  $data->getData('?Od='.$weekCalc['week_start'].'&Do='.$weekCalc['week_end']);
			$parsed = json_decode($this->json);
			
			//save file to backup
			$this->saveJSONToFile($catalog, $fileName);
			
			//save to import_info
			$list = array(
				'IIN_DATE_FROM' => $weekCalc['week_start'],
				'IIN_DATE_TO' => $weekCalc['week_end'],
				'IIN_IMPORT_ID' => $parsed->import_id,
				'IIN_FILE' => $catalog.$fileName,
			);
			$this->db->queryString(__BP.'import_info');
			$importId = $this->db->insertQuery($list);
	
			//save to user_points
			$this->db->tableName(__BP.'user_points');
			foreach($parsed->users as $user) {
				foreach($user->products as $product)	{
					$row = array(
						'UPO_USR_NIP' => $user->user_nip,
						'UPO_PRO_ID' => $product->product_id,
						'UPO_IMPORT_VALUE' => $product->product_value,
						'UPO_PRO_COUNT' => $product->product_count,
						'UPO_ORDER_DATE' => $product->order_date,
						'UPO_IMPORT_ID' => $importId,
						'UPO_IMPORT_DATE' => $today,
					);
					$this->db->insertRow($row);
				}
			}
		}
	}
	
	//reclaculate all points, get USR_ID by nip
	public function recalculateImport() {
		if($this->var->key==SC_API_KEY) {
		//if(1) {
			//positions from points
			$this->db->queryString('select * from '.__BP.'user_points');
			//not recalculated
			//$this->db->where('and', 'UPO_STATUS=1');
			//where user = 0
			$this->db->where('and', 'UPO_USR_ID=0');
			$points = $this->db->getDataFetch();
			
			//promotions for products
			$pDate = $this->getStartAndEndDate();
			
			$this->db->queryString('select * from '.__BP.'products_import_prom');
			$this->db->where('and', 'PIP_TYPE<>4');
			$promotions = $this->db->getDataFetch();
			
			$promList = array();
			
			if($promotions) {
				foreach($promotions as $prom) {
					$promList[$prom['PIP_PRODUCT']] = array('value' => $prom['PIP_VALUE'], 'type' => $prom['PIP_TYPE'], 'date_from' => $prom['PIP_DATE_FROM'], 'date_to' => $prom['PIP_DATE_TO']);
				}
			}
						
			if($points) {
				foreach($points as $point) {
					$calculatedPoints = $point['UPO_IMPORT_VALUE'];
					
					//promotions
					if(array_key_exists($point['UPO_PRO_ID'], $promList)) {
						if($point['UPO_ORDER_DATE']>=$promList[$point['UPO_PRO_ID']]['date_from'] and $point['UPO_ORDER_DATE']<=$promList[$point['UPO_PRO_ID']]['date_to']) {
							switch ($promList[$point['UPO_PRO_ID']]['type']) {
						    case 'percent':$calculatedPoints = $calculatedPoints + (($calculatedPoints*$promList[$point['UPO_PRO_ID']]['value'])/100);break;
						    case 'plus':$calculatedPoints = $calculatedPoints + $promList[$point['UPO_PRO_ID']]['value'];break;
						    case 'value':$calculatedPoints = $promList[$point['UPO_PRO_ID']]['value'];break;
							}
						}
					}
					
					$this->db->queryString('update '.__BP.'user_points set UPO_USR_ID=(select USR_ID from '.__BP.'users where USR_NIP=UPO_USR_NIP and USR_STATUS=2), UPO_VALUE='.$calculatedPoints.', UPO_STATUS=2 where UPO_ID='.$point['UPO_ID']);
					$this->db->execQuery();
				}
			}
		}
	}
	
	//recalculate all ACTIVE clients points
	public function recalculateClients() {
		if($this->var->key==SC_API_KEY) {
		//if(1) {
			//set all old recalculaions to 'stored' 
			$this->db->queryString('update '.__BP.'user_recalculate set URE_STATUS=2');
			$this->db->execQuery();
			
			$this->db->queryString('select USR_ID from '.__BP.'users');
			$this->db->where('and', 'USR_STATUS in (1,2)');
			$this->db->where('and', "USR_NIP<>''");
			//$this->db->where('and', 'USR_REPORT=1');
			
			$users = $this->db->getDataFetch();
			if($users) {
				$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
				foreach($users as $user)
					$model->load('user', 'recalculateUserPoints', $user['USR_ID']);
			}
		}
	}
	
	public function sendInfoBySms() {
		if($this->var->key==SC_API_KEY) {
			//gather clients to send info
			$this->db->queryString('select USR_ID, USR_PHONE, USR_POINTS, USR_PERSON_NAME from '.__BP.'users');
			$this->db->where('and', "USR_PHONE<>''");
			$this->db->where('and', 'USR_STATUS=2');
			//only for test
			//$this->db->where('and', 'USR_REPORT=0');
			
			$users = $this->db->getDataFetch();
			
			if($users) {
				$sms = array();
				foreach($users as $user) {
					$sms[] = array('id'=>$user['USR_ID'], 'phone' => $user['USR_PHONE'], 'points' => $user['USR_POINTS'], 'name' => $user['USR_PERSON_NAME']);
				}
				
				$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
				$model->load('sms', 'sendImportInfo', $sms);
			}
		}
	}
	
	private function getStartAndEndDate() {
		//automatic data week -1
		/** **/
		$currentWeek = date('W', strtotime(date('Y-m-d')));
	  $dto = new DateTime();
	  $dto->setISODate(date('Y'), (int)$currentWeek-1);
	  $ret['week_start'] = $dto->format('Ymd');
	  $ret['week_start_full'] = $dto->format('Y-m-d');
	  $dto->modify('+6 days');
	  $ret['week_end'] = $dto->format('Ymd');
	  $ret['week_end_full'] = $dto->format('Y-m-d');
	  /** **/
	  //or custom data 
	  /** */
	  $ret['week_start'] = $dto->format('20161001');
	  $ret['week_start_full'] = $dto->format('2016-10-01');
	  $ret['week_end'] = $dto->format('20161023');
	  $ret['week_end_full'] = $dto->format('2016-10-23');
	  /** **/
	  return $ret;
	}
	
	private function saveJSONToFile($catalog, $fileName) {
		file_put_contents($catalog.$fileName, $this->json);
	}
}

function import($option=null) {
	$import = new import;
	if($option!=null and method_exists($import, $option))
		return $import->$option();
	else 
		return $import->showModule();
}