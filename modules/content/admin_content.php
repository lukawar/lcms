<?php
class content extends View implements IModule {
	public function showModule() {
		return $this->load();
	}
	public function load() {
		if($this->var->action!='index') {
			$loader = $this->var->action;
			if(file_exists($this->data->localPath.$this->data->modulesPath.$this->var->action."/admin_".$this->var->action.".php")) {
				require_once $this->data->localPath.$this->data->modulesPath.$this->var->action."/admin_".$this->var->action.".php";
				if(!isset($this->var->option))
					$this->var->option = null;
				return $loader($this->var->option);
			} else return $this->loadStartPage();
		} else return $this->loadStartPage();
	}
	
	private function loadStartPage() {
		$module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		return $module->load('dashboard','view');
	}
}

function content() {
	$result = new content;
	return $result->load();
}