<?php
class content extends View implements IModule {
	public $artPath = 'files/pages/';
	
	public function showModule() {
		if($this->var->user_status=='new' and $this->var->action!='user' and $this->var->action!='articles') {
			$module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			return $module->load('user','panel');
		} else {
			if($this->var->action!='homepage') {
				$module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
				if(!isset($this->var->option))
					$this->var->option = null;
				return $module->load($this->var->action,$this->var->option);
				//return 'name: '.$this->var->action.$this->var->option;
			} else return $this->loadStartPage();
		}
	} 
	
	private function loadStartPage() {
		$module = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
		return $module->load('category','view');
	}
}

function content() {
	$content = new Content;
	return $content->showModule();
}