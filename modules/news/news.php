<?php
class news extends View implements IModule {
	public $path = 'modules/news/templates/'; //path to templates for admin
	public $newsPath = 'files/news/'; //path to articles
	public $backgroundDefaultFront = 'templates/frontend/bb/img/logo_beabonus.png';
	
	public function homepage() {
		$today = date('Y-m-d');
		$line = null;
		
		$this->db->queryString('select * from '.__BP.'news');
		$this->db->where('and', 'NEW_STATUS=2');
		$this->db->where('and', "NEW_DATE_START<='$today'");
		$this->db->where('and', "NEW_DATE_END>='$today'");
		$this->db->orderBy('NEW_DATE_DISPLAY DESC');
		$this->db->limit(3);
		
		$news = $this->db->getDataFetch();
		if($news) {
			foreach($news as $new) {
				$list = array(
					'__TITLE' => $new['NEW_NAME'],
					'__TEXT' => stripslashes($new['NEW_DESC_SHORT']),
					'__LINK'=>helper::href(array('news', 'show', $new['NEW_ALIAS'])),
					'__IMAGE' => $this->data->localPath.$this->newsPath.'b/'.$new['NEW_BACKGROUND'],
				);
				$line.= Template::parseFile($list, $this->data->localPath.$this->path.'item.tpl.php');
			}
			return $line;
		} else return null;
	}
	
	public function showModule($name=null) {
		$this->db->queryString('select * from '.__BP.'news');
		$this->db->where('and', 'NEW_STATUS=2');
		$this->db->orderBy('NEW_DATE_DISPLAY desc');
		
		$news = $this->db->getDataFetch();
		
		$newData = null;
		
		foreach($news as $new) {
			if($new['NEW_BACKGROUND']!='')
				$background = $this->data->localPath.$this->newsPath.'m/'.$new['NEW_BACKGROUND'];
			else $background = $this->backgroundDefaultFront;
			
			$list = array(
					'__DISPLAY_DATE'=>$new['NEW_DATE_DISPLAY'],
					'__ALIAS'=>$new['NEW_ALIAS'],
					'__TITLE'=>$new['NEW_NAME'],
					'__CONTENT'=>stripslashes($new['NEW_DESC_SHORT']),
					'__BACKGROUND'=>$background,
					'__LINK'=>helper::href(array('news', 'show', $new['NEW_ALIAS'])),
				);
				
			$newData.= Template::parseFile($list, $this->path.'all.tpl.php');
		}
		
		return Template::parseFile(array('__CONTENT'=>$newData), $this->path.'list-container.tpl.php');//$newData;
	}
	

	public function show() {
		if(isset($this->var->id) and !empty($this->var->id)) {
			$this->db->queryString('select * from '.__BP.'news');
			$this->db->where('and', 'NEW_STATUS=2');
			$this->db->where('and', 'NEW_ALIAS="'.$this->var->id.'"');
			$new = $this->db->getRow();
			if($new) {
				if($new['NEW_BACKGROUND']!='')
					$background = '<img src="'.$this->data->localPath.$this->newsPath.'b/'.$new['NEW_BACKGROUND'].'" class="img-full">';
				else $background = "<div style='height: 200px;'>&nbsp;</div>";//'<img src="'.$this->backgroundDefaultFront.'">';
				
				$list = array(
						'__DISPLAY_DATE'=>$new['NEW_DATE_DISPLAY'],
						'__ALIAS'=>$new['NEW_ALIAS'],
						'__TITLE'=>$new['NEW_NAME'],
						'__TEXT'=>stripslashes($new['NEW_DESC_LONG']),
						'__IMAGE'=>$background,
						'__LINK'=>helper::href(array('news')),
						//'__CURRENT' => $current
				);
					
				return Template::parseFile($list, $this->data->localPath.$this->path.'one.tpl.php');
			} else return $this->noData();
		} else return $this->noData();
	}

}

function news($name=null) {
	$news = new news;
	if($name!=null) {
		if(method_exists($news, $name))
			return $news->getModule($name);
		else
			return $news->getList($name);
	} else 
		return $news->showModule();
}