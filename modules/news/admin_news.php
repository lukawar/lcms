<?php
class news extends View implements iModule {
	public $path = 'modules/news/templates/'; //path to templates for admin
	public $files = 'files/news/';
	
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('dataTable', 'table table-striped stripe hover');
			$tableHeader = array('lp.', 'Nazwa', 'Data dodania', 'Data wyświetlana', 'Data startu', 'Data zakończenia', 'Status', '', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
							
			$this->db->queryString('select * from '.__BP.'news');
			$this->db->where('and', 'NEW_STATUS<>4');
			$news = $this->db->getDataFetch();
				
			if($news)	{
				foreach($news as $n) {
					$table->addCell($i, 'lp');
					$table->addCell($n['NEW_NAME']);
					$table->addCell($n['NEW_DATE_ADD']);
					$table->addCell($n['NEW_DATE_DISPLAY']);
					$table->addCell($n['NEW_DATE_START']);
					$table->addCell($n['NEW_DATE_END']);
					$table->addCell($n['NEW_STATUS']);
					$table->addButton('edit', 'admin.php?action=news&option=edit&id='.$n['NEW_ID'], 'primary');
					//$table->addLink('admin.php?action=news&option=edit&id='.$n['NEW_ID'], 'edycja',null, null, 'edycja');
					$table->addButton('delete', 'javascript:dialog_del("admin.php?action=news&option=delete&id='.$n['NEW_ID'].'")', 'danger');
					//$table->addLink('javascript:dialog_del("admin.php?action=news&option=delete&id='.$n['NEW_ID'].'")', 'usuń',null, null, 'usuń');
					$table->addRow();
					$i++;
						
				}
				$tableBody = $table->createBody();		
			} else $tableBody = $this->noData();
			
			$list = array(
				'__GLYPHICON' => 'free_breakfast',
				'__HEAD' => 'AKTUALNOŚCI',
				'__CONTENT'=> $tableBody
			);
			
			$page = new Page;
			$page->setButton('<a href="'.helper::href(array('news','add'), null, 'full').'" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> dodaj wpis</a>');
			$page->setToolbar();
			return $page->setPage($list);
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$form = new Form('news', 'addSave', 'Wypełnij dane');
			$form->addText('Nazwa', 'NEW_NAME', 'required');
			$form->addInput('Wyświetlana data', 'NEW_DATE_DISPLAY', null, 'datepicker.tpl.php');
			$form->addInput('Data rozpoczęcia', 'NEW_DATE_START', null, 'datepicker.tpl.php');
			$form->addInput('Data zakończenia', 'NEW_DATE_END', null, 'datepicker.tpl.php');
			$form->addSelect('Status', 'NEW_STATUS', array('nowy'=>'nowy', 'aktywny'=>'aktywny', 'nieaktywny'=>'nieaktywny') , null);
			$form->addInput('Tekst skrócony', 'NEW_DESC_SHORT', null, 'textarea.tpl.php');
			$form->addInput('Tekst pełny', 'NEW_DESC_LONG', null, 'textarea.tpl.php');
			//$form->addInput('Zdjęcie', 'NEW_BACKGROUND', null, 'file.tpl.php');
			$page = new Page;
			$list = array(
				'__HEAD' => 'Dodaj nowego newsa',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'free_breakfast',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=news" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$insert = array(
				'NEW_NAME' => $this->var->NEW_NAME,
				'NEW_ALIAS' => coder::stripPL($this->var->NEW_NAME),
				'NEW_DESC_LONG' => addslashes($this->var->NEW_DESC_LONG),
				'NEW_DESC_SHORT' => addslashes($this->var->NEW_DESC_SHORT),
				'NEW_DATE_DISPLAY' => $this->var->NEW_DATE_DISPLAY,
				'NEW_DATE_START' => $this->var->NEW_DATE_START,
				'NEW_DATE_END' => $this->var->NEW_DATE_END,
				'NEW_STATUS' => $this->var->NEW_STATUS,
				'NEW_ADM_ID' => $this->var->admin_id,
			);
			
			$this->db->queryString(__BP.'news');
			$id = $this->db->insertQuery($insert);
			
			//save event 
			eventSaver::add($this->var->admin_id, 'add news - '. $this->var->NEW_NAME, 'admin', 'news', 'add', $id);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'news', $id, 'add', $this->var->NEW_NAME, $this->var->NEW_DESC_LONG, $this->var->NEW_DESC_SHORT, $this->var->NEW_DATE_DISPLAY.';'.$this->var->NEW_DATE_START.';'.$this->var->NEW_DATE_END);
			
			header('location: admin.php?action=news&option=edit&id='.$id);
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'news');
			$this->db->where('and', 'NEW_ID='.$this->var->id);
			$news = $this->db->getRow();
			
			if($news['NEW_BACKGROUND']!=null)
				$filename = '<img src="'.$this->data->localPath.$this->files.$news['NEW_BACKGROUND'].'" width="300">';
			else $filename = 'brak pliku';
			
			$form = new Form('news', 'editSave', 'Edycja newsa');
			$form->addText('Nazwa', 'NEW_NAME', 'required', $news['NEW_NAME']);
			$form->addText('Alias', 'NEW_ALIAS', 'required', $news['NEW_ALIAS']);
			$form->addInput('Wyświetlana data', 'NEW_DATE_DISPLAY', $news['NEW_DATE_DISPLAY'], 'datepicker.tpl.php');
			$form->addInput('Data rozpoczęcia', 'NEW_DATE_START', $news['NEW_DATE_START'], 'datepicker.tpl.php');
			$form->addInput('Data zakończenia', 'NEW_DATE_END', $news['NEW_DATE_END'], 'datepicker.tpl.php');
			$form->addSelect('Status', 'NEW_STATUS', array('nowy'=>'nowy', 'aktywny'=>'aktywny', 'nieaktywny'=>'nieaktywny') , $news['NEW_STATUS']);
			$form->addInput('Tekst skrócony', 'NEW_DESC_SHORT', $news['NEW_DESC_SHORT'], 'textarea.tpl.php');
			$form->addInput('Tekst pełny', 'NEW_DESC_LONG', $news['NEW_DESC_LONG'], 'textarea.tpl.php');
			$form->addInput('Zdjęcie', 'NEW_BACKGROUND', $filename, 'file.tpl.php');
			$form->addHidden('NEW_ID', $news['NEW_ID']);
			$page = new Page;
			$list = array(
				'__HEAD' => 'Edycja newsa',
				'__CONTENT' => $form->setForm(),
				'__GLYPHICON' => 'free_breakfast',
			);
			
			$page->setButton('<button type="submit" class="btn btn-success submitBtn"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> zapisz</button>');
			$page->setButton('<button type="button" url="?action=news" class="btn btn-danger cancelBtn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> anuluj</button>');
			$page->setToolbar();			
			return $page->setPage($list, 'pageForm.tpl.php');
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array(
				'NEW_NAME' => $this->var->NEW_NAME,
				'NEW_ALIAS' => $this->var->NEW_ALIAS,
				'NEW_DESC_LONG' => addslashes($this->var->NEW_DESC_LONG),
				'NEW_DESC_SHORT' => addslashes($this->var->NEW_DESC_SHORT),
				'NEW_DATE_DISPLAY' => $this->var->NEW_DATE_DISPLAY,
				'NEW_DATE_START' => $this->var->NEW_DATE_START,
				'NEW_DATE_END' => $this->var->NEW_DATE_END,
				'NEW_STATUS' => $this->var->NEW_STATUS,
			);
			
			
			if($_FILES['NEW_BACKGROUND']['name']) {
				$update['NEW_BACKGROUND'] = $this->filesSave('NEW_BACKGROUND', $this->files, $this->var->NEW_ID);
			}
			
			$this->db->queryString(__BP.'news');
			$this->db->updateQuery($update, 'NEW_ID', $this->var->NEW_ID);
			
			//save event 
			eventSaver::add($this->var->admin_id, 'edit news - '. $this->var->NEW_NAME, 'admin', 'news', 'edit', $this->var->NEW_ID);
			
			//save backup
			eventSaver::setBackup($this->var->admin_id, $this->data->typeSite, 'news', $this->var->NEW_ID, 'edit', $this->var->NEW_NAME, $this->var->NEW_DESC_LONG, $this->var->NEW_DESC_SHORT, $this->var->NEW_DATE_DISPLAY.';'.$this->var->NEW_DATE_START.';'.$this->var->NEW_DATE_END);
			
			header('location: admin.php?action=news');
		} else return $this->goAway();
	}
	
	public function delete() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'news');
			$this->db->updateQuery(array('NEW_STATUS'=>4), 'NEW_ID', $this->var->id);
			
			header('location: admin.php?action=news');
		} else return $this->goAway();
	}
	
	private function getStatus($id=null) {
		$status = array('nowy', 'aktywny', 'nieaktywny');
		$select = new select('NEW_STATUS', 'NEW_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();		
	}
	
	private function getType($id=null) {
		$type = array('news', 'blog');
		$select = new select('NEW_TYPE', 'NEW_TYPE');
		foreach($type as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();		
	}
	
	private function createNewsSelect($id) {
		$type = array('aktualności'=>1, 'blog'=>2);
		
		$line = "<option value='0'> - wszystkie - </option>";
		foreach($type as $t=>$value) {
			if($value==$id)
				$line.= "<option value='".$value."' selected> - ".$t." - </option>";
			else $line.= "<option value='".$value."'> - ".$t." - </option>";
		}
		
		return "<select name='TYPE_ID' id='TYPE_ID'>".$line.'</select>';
	}
	
	public function dashboard() {
		$this->db->queryString("select * from ".__BP."news where NEW_STATUS=2 order by NEW_DATE_DISPLAY limit 10");
		$news = $this->db->getDataFetch();
		
		if($news) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.','Nazwa', 'Data');
			$table->createHeader($tableHeader);
			$i = 1;
			foreach($news as $new) {
				$link = 'admin.php?action=news&option=edit&id='.$new['NEW_ID'];
				$table->addLink($link, $i, null,null, 'Szczegóły');
				$table->addLink($link, $new['NEW_NAME'], null, null, 'Szczegóły');
				$table->addLink($link, $new['NEW_DATE_DISPLAY'], null, null, 'Szczegóły');
				$table->addRow();
				$i++;
			}
			$tabList = $table->createBody()."<a href='?action=news'>Pełna lista <i class='fa fa-arrow-right'></i></a>";
		} else $tabList = $this->noData();
		$list = array(
			'__MODULE_TITLE'=>'Aktualności - 10 ostatnich',
			'__MODULE_ICON'=>'fa fa-coffee',
			'__MODULE_CONTENT'=>$tabList,
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
	
	public function filesSave($file, $path, $id) {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			//obraz główny TEXT
			if($_FILES[$file]['name']) {
				
				//$selFolder = $this->var->PRO_ID;
				//if(!file_exists($this->filePath."{$id}"))
  			//	mkdir($this->filePath.$id, 0777);
	  		$filename = md5(microtime());
	  		
	  		//$name = $_FILES[$file]['name'];
	  		//$filename = pathinfo($_FILES[$file]['name'], PATHINFO_FILENAME);
	  		$image_file = new ImageLoader(__NEWS_B_X,__NEWS_B_Y, $filename, $file, $this->data->localPath.$path.'b/');
	  		$image = $image_file->load();
	  		
	  		$image_file = new ImageLoader(__NEWS_M_X,__NEWS_M_Y, $filename, $file, $this->data->localPath.$path.'m/');
	  		$image_file->load();
	  		
	  		$image_file = new ImageLoader(__NEWS_S_X,__NEWS_S_Y, $filename, $file, $this->data->localPath.$path.'s/');
	  		$image_file->load();
	      
	      return $filename.$image;
			}
		} else return $this->goAway();
	}
}

function news($option=null) {
	$news = new News;
	if($option!=null and method_exists($news, $option))
		return $news->$option();
	else 
		return $news->showModule();
}