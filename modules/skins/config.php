<?php
$module_info['title'] = 'Skórki';
$module_info['description'] = 'Zarządzanie skórkami strony';
$module_info['author'] = 'Expansja';

$menu['order'][] = '2';
$menu['title'][0] = 'Skórki';
$menu['action'][0] = 'skins';
$menu['icon'][0] = 'extension';

$menu['show'][0] = true;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;