<?php
class promocje extends View implements IModule {
	public function showModule() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			return $model->load('produkty', 'getData', 'promocje');
		} else return $this->goAway();
	}
}

function promocje($option=null) {
	$promocje = new promocje;
	if($option!=null and method_exists($promocje, $option))
		return $promocje->$option();
	else 
		return $promocje->showModule();
}