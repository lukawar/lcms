<?php
class promocje extends View implements IModuleHomepage {
	public $path = 'modules/promocje/templates/';
	public $imgPath = 'files/product/';

	public function showModule() {
		return null;
	}
	
	public function homepage() {
		return $this->getProducts();
	}
	
	public function getProducts() {
		$this->db->queryString('select PRO_POINTS,PPR_VALUE,PPR_STATUS,CAT_ID,PRO_ALIAS,PRO_NAME,PRO_DESC_SHORT,PRO_CODE,PRO_FOTO from '.__BP.'products');
		$this->db->join('left', __BP.'products_promotions', 'PPR_PRO_ID', 'PRO_ID');
		$this->db->join('left', __BP.'pro_cat', 'PRO_ID', 'PRO_ID');
		$this->db->where('and', 'PRO_STATUS=1');
		$this->db->where('and', 'PPR_STATUS<>4');
		$this->db->limit(10);
		
		$news = $this->db->getDataFetch();
		//$this->db->prepareQuery();$this->db->query();
		if($news) {
			$result = null;
			$text_cart_too_little = $this->lang->cart_too_little;
			$text_cart_add = $this->lang->cart_add_to;
			
			foreach($news as $n) {
				$promPoints =0;
				switch ($n['PPR_STATUS']) {
			    case 'percent':$promPoints = round($n['PRO_POINTS'] - (($n['PRO_POINTS']*$n['PPR_VALUE'])/100));break;
			    case 'minus':$promPoints = round($n['PRO_POINTS'] - $n['PPR_VALUE']);break;
			    case 'value':$promPoints = round($n['PPR_VALUE']);break;
				}
				
				$link = helper::href(array('produkty', 'view', $n['CAT_ID'], $n['PRO_ALIAS']));
				if($this->var->user_points>=$n['PRO_POINTS'])
					$button = '<button class="buttonUrl enough" url="'.$link.'">'.$text_cart_add.'</button>';
				else {
					$points = (int)$promPoints - (int)$this->var->user_points;
					$button = '<button class="not_enough">'.$text_cart_too_little.$points.' pkt</button>';
				}
				
				$data = array(
					'__PRODUCT_HREF' => helper::href(array('produkty', 'view', $n['CAT_ID'], $n['PRO_ALIAS'])),
					'__PRODUCT_NAME' => $n['PRO_NAME'],
					'__PRODUCT_PKT' => $n['PRO_POINTS'],
					'__PRODUCT_DESCR' => $n['PRO_DESC_SHORT'],
					'__PRODUCT_THUMB' => $this->data->localPath.$this->imgPath.$n['PRO_CODE'].'/thumb2_'.$n['PRO_FOTO'],
					'__PRODUCT_PROMOTION' => (int)$promPoints,
					'__BUTTON' => $button,
				);
				$result.= Template::parseFile($data, $this->data->localPath.$this->path.'default.tpl.php');
				unset($data);
			}
			return $result;
		} else return null;
	}
}