<?php
class report extends View implements IModule {
	public $path = 'modules/report/templates/';
	
	public function showModule() {
		return null;
	}
	
	private function initialize($title) {
		require_once($this->data->localPath.'system/lib/PHPExcel/vendor/autoload.php');
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator($this->var->admin_data)
									 ->setLastModifiedBy($this->var->admin_data)
									 ->setTitle($title)
									 ->setSubject($title)
									 ->setDescription('')
									 ->setKeywords('')
									 ->setCategory('');
		return $objPHPExcel;
	}
	
	private function output($objPHPExcel, $filename) {
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
	
	public function user() {
		$objPHPExcel = $this->initialize('Uczestnicy');

		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'Lp')
		            ->setCellValue('B1', 'Nazwa')
		            ->setCellValue('C1', 'Nip')
		            ->setCellValue('D1', 'Dane')
		            ->setCellValue('E1', 'Telefon')
		            ->setCellValue('F1', 'Email')
		            ->setCellValue('G1', 'Ph')
		            ->setCellValue('H1', 'Ostatnie logowanie')
		            ->setCellValue('I1', 'Punkty')
		            ->setCellValue('J1', 'Posiadane')
		            ->setCellValue('K1', 'Promocyjne')
		            ->setCellValue('L1', 'Wydane')
		            ->setCellValue('M1', 'Status');
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);
		
		$this->db->queryString('select *, max(ULO_DATE) as login_date from '.__BP.'users');
		$this->db->join('left', __BP.'administrator', 'USR_PH', 'ADM_ID');
		$this->db->join('left', __BP.'user_recalculate', 'URE_USR_ID', 'USR_ID', 'URE_STATUS=1');
		$this->db->join('left', __BP.'user_login', 'ULO_USR_ID', 'USR_ID');
		if($this->var->REP_STATUS!='all')		
			$this->db->where('and', "USR_STATUS='".$this->var->REP_STATUS."'");
		if(isset($this->var->REP_PH) and $this->var->REP_PH!=0)		
			$this->db->where('and', 'USR_PH='.$this->var->REP_PH);
		//ph filter
		if($this->var->admin_agency=='ph')
			$this->db->where('and', 'USR_PH='.$this->var->admin_id);
			
		if(isset($this->var->REP_DATE_FROM) and $this->var->REP_DATE_FROM!='')
			$this->db->where('and', "USR_DATE_ADD>='".$this->var->REP_DATE_FROM."'");
			
		if(isset($this->var->REP_DATE_TO) and $this->var->REP_DATE_TO!='')
			$this->db->where('and', "USR_DATE_ADD<='".$this->var->REP_DATE_TO."'");
			
		//without test accounts
		if($this->var->admin_agency!='A')
			$this->db->where('and', 'USR_REPORT=1');
			
		$this->db->groupBy('USR_ID');
		
		//$this->db->prepareQuery();$this->db->query();
		$users = $this->db->getDataFetch();
		//die();
		
		if($users) {
			$model = new Model($this->data->typeSite, $this->data->localPath, $this->data->modulesPath);
			$status = $model->load('user', 'getStatus');
			$i = 1;
			
			foreach($users as $user) {
				//$objPHPExcel->setActiveSheetIndex(0)
				$objPHPExcel->getActiveSheet()
										->setCellValueByColumnAndRow(0, $i+1, $i)
										->setCellValueByColumnAndRow(1, $i+1, $user['USR_NAME'])
										->setCellValueByColumnAndRow(2, $i+1, $user['USR_NIP'])
										->setCellValueByColumnAndRow(3, $i+1, $user['USR_PERSON_NAME'].' '.$user['USR_PERSON_SURNAME'].', '.$user['USR_STREET'].' '.$user['USR_NUMBER'].', '.$user['USR_POSTCODE'].', '.$user['USR_CITY'])
										->setCellValueByColumnAndRow(4, $i+1, $user['USR_PHONE'])
										->setCellValueByColumnAndRow(5, $i+1, $user['USR_EMAIL'])
										->setCellValueByColumnAndRow(6, $i+1, $user['ADM_NAME'].' '.$user['ADM_SURNAME'])
										->setCellValueByColumnAndRow(7, $i+1, $user['login_date'])
										->setCellValueByColumnAndRow(8, $i+1, $user['URE_POINTS_IMPORT'])
										->setCellValueByColumnAndRow(9, $i+1, $user['URE_POINTS'])
										->setCellValueByColumnAndRow(10, $i+1, $user['URE_POINTS_PROMOTIONS'])
										->setCellValueByColumnAndRow(11, $i+1, $user['URE_POINTS_ORDERS'])
										->setCellValueByColumnAndRow(12, $i+1, $status[$user['USR_STATUS']]);
										
				$i++;
			}
		}	
		
		
		$objPHPExcel->getActiveSheet()->setTitle('Uczestnicy');
		
		$this->output($objPHPExcel, 'uczestnicy.xls');
	}
	
}

function report($option=null) {
	$report = new report;
	if($option!=null and method_exists($report, $option))
		return $report->$option();
	else 
		return $report->showModule();
}