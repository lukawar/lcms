<?php
$module_info['title'] = 'Raporty';
$module_info['description'] = 'Moduł generowania raportów';
$module_info['author'] = 'Expansja';

$menu['title'][0] = 'Raporty';
$menu['action'][0] = 'report';
$menu['icon'][0] = 'date_range';

$menu['show'][0] = false;
$menu['menu'][0] = false;
$menu['dashboard'][0] = false;