<?php
class redirect extends View {
	public $path = 'modules/redirect/templates/'; //path to templates for admin
	
	public function getList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {		
			//campanies
			$this->db->queryString('select * from '.__BP.'redirect_campanies');
			$this->db->where('and', 'RCA_STATUS=1');
			$campanies = $this->db->getDataFetch();
			
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Kampania', 'Nazwa', 'Data wysłania');
			$table->createHeader($tableHeader);
			
			$i = 1; 
			foreach($campanies as $cam) {
				$table->addCell($i, 'lp');

				$table->addLink('request.php?action=reques&option=show&id='.$cam['RCA_ID'], $cam['RCA_MAI_ID'],'light', null, 'pokaż');
				$table->addLink('request.php?action=reques&option=show&id='.$cam['RCA_ID'], $cam['RCA_TITLE'],'light', null, 'pokaż');
				$table->addLink('request.php?action=reques&option=show&id='.$cam['RCA_ID'], $cam['RCA_DATE_SEND'],'light', null, 'pokaż');
				
				$table->addRow();
				$i++;
				
			}
			$table_companies = $table->createBody();
			
			$list = array('__TABLE_CAMPANIES'=>$table_companies, '__TABLE_RAPORT'=>$this->show());
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
			
		} else return $this->goAway();
	}
	
	public function show() {
		$this->db->queryString('select * from '.__BP.'redirect_campanies');
		if($this->var->id==null)
			$this->db->orderBy('RCA_DATE_SEND DESC');
		else $this->db->where('and', 'RCA_MAI_ID='.$this->var->id);
		$campany = $this->db->getRow();
		
		if($campany) {
			//sended count
			$this->db->queryString('select count(MOS_NR) as MOS_COUNT from '.__BP.'mailing_out_send');
			$this->db->where('and', 'MOS_NR='.$campany['RCA_MAI_ID']);
			$send_ = $this->db->getRow();
			if($send_)
				$send = $send_['MOS_COUNT'];
			else $send = 0;
			
			//sended count
			$this->db->queryString('select count(MOF_NR) as MOF_COUNT from '.__BP.'mailing_out_false');
			$this->db->where('and', 'MOF_NR='.$campany['RCA_MAI_ID']);
			$false_ = $this->db->getRow();
			if($false_)
				$false = $false_['MOF_COUNT'];
			else $false = 0;
			
			$all = $send + $false;
			
			$false_p = number_format(($false/$all)*100, 2, ',', ' ');
			$send_p = number_format(($send/$all)*100, 2, ',', ' ');
			
			//wyświetlenia maila
			$this->db->queryString('select count(RED_CLI_ID) as RED_COUNT from '.__BP.'redirect');
			$this->db->where('and', 'RED_MAILING_ID='.$campany['RCA_MAI_ID']);
			$this->db->where('and', 'RED_TYPE=2');
			$viewed_ = $this->db->getRow();
			if($viewed_) {
				$view = $viewed_['RED_COUNT'];
				//unikalne
				$this->db->queryString('select count(distinct(RED_CLI_ID)) as RED_COUNT_U from '.__BP.'redirect');
				$this->db->where('and', 'RED_MAILING_ID='.$campany['RCA_MAI_ID']);
				$this->db->where('and', 'RED_TYPE=2');
				//$this->db->groupBy('RED_CLI_ID');
				$viewed_single_ = $this->db->getRow();
				$view_single = $viewed_single_['RED_COUNT_U'];
				$view_single_p = number_format(($view_single/$all)*100, 2, ',', ' ');
			} else {
				$view = 0;
				$view_single = 0;
			}
			
			$line = '<h4 class="page-header">Raport wysyłki</h4>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Tytuł mailingu: </div><div class="col-sm-8">'.$campany['RCA_TITLE'].'</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Data wysyłki: </div><div class="col-sm-8">'.$campany['RCA_DATE_SEND'].'</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Numer: </div><div class="col-sm-8">'.$campany['RCA_MAI_ID'].'</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Ilość wiadomości: </div><div class="col-sm-8">'.$all.'</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Ilość wysłanych maili: </div><div class="col-sm-8">'.$send.' ('.$send_p.'%)</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Ilość niewysłanych maili: </div><div class="col-sm-8">'.$false.' ('.$false_p.'%)</div></div>';
			
			$line.= '<p>&nbsp;</p><p>&nbsp;</p><h4 class="page-header">Raport przeglądania - OR (~)</h4>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Ilość wyświetleń ogółem: </div><div class="col-sm-8"><b>'.$view.'</b>/'.$all.'</div></div>';
			$line.= '<div class="form-group"><div class="col-sm-3 text-right">Ilość unikalnych wyświetleń: </div><div class="col-sm-8"><b>'.$view_single.'</b>/'.$all.' ('.$view_single_p.'%)</div></div>';
			$line.= '<p>&nbsp;</p><p>&nbsp;</p><h4 class="page-header">Raport klikalności linków (ogółem / unikalne / wysłanych maili / CTR)</h4>';
			
			$this->db->queryString('select RDA_ALIAS, RDA_LINK from '.__BP.'redirect_data');
			$this->db->where('and', 'RDA_TYPE=1');
			$this->db->where('and', 'RDA_MAI_ID='.$campany['RCA_MAI_ID']);
			$redirect_data = $this->db->getDataFetch();
			
			$rd_all = 0;
			$rd_single = 0;
			
			foreach($redirect_data as $rd) {
				$this->db->queryString('select count(RED_CLI_ID) as COUNT_ID from '.__BP.'redirect');
				$this->db->where('and', 'RED_MAILING_ID='.$campany['RCA_MAI_ID']);
				$this->db->where('and', 'RED_LINK_REQUEST="'.$rd['RDA_ALIAS'].'"');
				
				$rd_all_ = $this->db->getRow();
				if($rd_all_) {
					$rd_all = $rd_all_['COUNT_ID'];
					$this->db->queryString('select count(distinct(RED_CLI_ID)) as COUNT_ID_SINGLE from '.__BP.'redirect');
					$this->db->where('and', 'RED_MAILING_ID='.$campany['RCA_MAI_ID']);
					$this->db->where('and', 'RED_LINK_REQUEST="'.$rd['RDA_ALIAS'].'"');
					
					$rd_single_ = $this->db->getRow();
					//$this->db->query();
					$rd_single = $rd_single_['COUNT_ID_SINGLE'];
					$rd_p =  number_format(($rd_single/$all)*100, 2, ',', ' ');
				} else {
					$rd_all = 0;
					$rd_single = 0;
					$rd_p = 0;
				}
				$line.= '<div class="form-group"><div class="col-sm-8 text-right">'.$rd['RDA_LINK'].'</div><div class="col-sm-4"><b>'.$rd_all.'</b> / '.$rd_single.' / '.$all.' / '.$rd_p.'%</div></div>';
			}
		}
		return $line;
	}

}

function redirect($option=null) {
	$redirect = new redirect;
	if($option!=null and method_exists($redirect, $option))
		return $redirect->$option();
	else 
		return $redirect->getList();
}