<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=redirect">Mailingi</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa envelope-o"></i>
					<span>Raporty mailingowe</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					<h4 class="page-header">Wysłane kampanie</h4>
					__TABLE_CAMPANIES
					
					<p>&nbsp;</p>
					<div id="table_raport">__TABLE_RAPORT</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.text-right {font-weight: bold; text-align: right;}
.form-group {padding-top: 15px;}
</style>