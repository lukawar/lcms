<div class="section-top-back top-back2">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<h2>BĄDŹ NA BIEŻĄCO</h2>
				<h4>Zapisz się  do newslettera</h4>
			</div>
		</div>
	</div>
</div>

<div class="page-section">
	<div class="container">
		<div class="grain-section hide-on-small-and-down"></div>
		<div class="row">
			<div class="col s12 m8 l8">
				<div class="form-container">
					<form name="newsletter" id="newsletter" action="index.php?action=newsletter&option=addAddress" method="post">
						<input type="hidden" name="action" value="newsletter">
						<input type="hidden" name="option" value="addAddress">
						<p><input type="email" name="email" placeholder="WPISZ ADRES EMAIL" required="required"> <button class="btn waves-effect waves-light" type="submit" name="action" id="send">WYŚLIJ<i class="material-icons right">send</i></button></p>
						<p class="small"><input type="checkbox" id="check1" name="check1"/>
      				<label for="check1" id="label-check1">Wyrażam zgodę na przechowywanie, wykorzystywanie i przetwarzanie przez Nordkalk sp. z o.o. moich danych osobowych w celach promocyjnych, informacyjnych, reklamowych i marketingowych zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych    ( t.j. Dz..U. z 2014 r. poz.1182) ze zmianami) oraz zgody na otrzymywanie informacji promocyjnych, informacyjnych, reklamowych i marketingowych o usługach i produktach Nordkalk sp. z o.o. na podany adres e-mail zgodnie z ustawą z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (t.j.  Dz.U. z 2013 r. poz. 1422 ze zmianami ).</label></p>
						
						<p class="small"><input type="checkbox" id="check2" name="check2"/>
      				<label for="check2" id="label-check2">Wyrażam zgodę na przechowywanie, wykorzystywanie i przetwarzanie moich danych osobowych przez Nordkalk sp. z o.o. w celach marketingu bezpośredniego przy użyciu telekomunikacyjnych urządzeń końcowych i automatycznych systemów wywołujących zgodnie z ustawą z 16 lipca 2004 r. Prawo Telekomunikacyjne ( t.j. Dz.U. z 2014 r. poz.243 ze zmianami )</label></p>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){	
	$("form").submit(function( event ) {
		var response = true;
		$('#label-check1, #label-check2').removeClass('notValid');
		if($('#check2').is(':checked')) {
			
		}  else {
			$('#label-check2').addClass('notValid');
			response = false;
		}
		
		if($('#check1').is(':checked')) {
			
		}  else {
			$('#label-check1').addClass('notValid');
			response = false
		}
		
  	return response;
   
  	//$("span").text( "Not valid!" ).show().fadeOut( 1000 );
  	
	});
});
</script>