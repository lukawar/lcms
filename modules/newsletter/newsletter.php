<?php
class newsletter extends View {
	public $path = 'modules/newsletter/templates/';
	public function getForm() {
		$list = array();
		return Template::parse($list, file_get_contents($this->path.'form.tpl.php'));
	}
	
	public function addAddress() {
		if($this->var->email) {
			$this->db->queryString(__BP.'newsletter_user');
			$insert = array('NUS_NAME' => $this->var->email);
			$this->db->insertQuery($insert);
			
			header('location: newsletter-save.html');
		}
	}
	
	public function save() {
		$list = array();
		return Template::parse($list, file_get_contents($this->path.'save.tpl.php'));
	}
}

function newsletter($option=null) {
	$newsletter = new newsletter;
	if($option!=null and method_exists($newsletter, $option))
		return $newsletter->$option();
	else
		return $newsletter->getForm();
}