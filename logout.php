<?php
session_start();
$_SESSION = array();
session_destroy();
if(isset($_GET['redirect'])) {
	if(file_exists($_GET['redirect'].'.php'))
		$redirect = $_GET['redirect'].'.php';
	else $redirect = 'index.php';
} else $redirect = 'index.php';
	

header('location: '.$redirect);