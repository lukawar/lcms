<?php
$config_template = array(
	'index'=>'index.tpl.php',
	'templates'=>'index_tmce.tpl.php',
	'articles'=>'index_tmce.tpl.php',
	'faq'=>'index_tmce.tpl.php',
	'dashboard'=>'lightBox.tpl.php',
	'gallery'=>'load.tpl.php',
	'recipients'=>'fancyBox.tpl.php',
	'campaign'=>'lightBox.tpl.php',
);

$login_require = array(
	'login' => 'login.tpl.php',
	'action' => 'administrator',
	'option' => 'checkAdmin'
);

$config_page = array(
	'pages_path' => 'templates/backend/default/pages/',
	'forms_path' => 'templates/backend/default/forms/',
);

$config_file = array(
	'alias'=>'default',
	'name'=>'lightAdmin  v2.0',
	'description'=>'Backend',
	'author'=>'expansja.pl',
	'date_add'=>'2016-08-02',
	'thumbnail'=>'templates/backend/default/img/thumbnail.png',
);