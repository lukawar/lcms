<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>ZALOGUJ SIĘ</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="templates/backend/devoops/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="templates/backend/devoops/css/login.css" type="text/css" media="screen" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<script>
		function setFocus() {
			document.getElementById('l_login').focus(); 
		}
	</script>
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="setFocus();">
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Panel administracyjny</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Strona główna</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	<div class="container">
		<form class="form-signin" name="login" method="post" action="login.php" role="form">
			<h2 class="form-signin-heading">Zaloguj się</h2>
			<input type="text" name="l_login" id="l_login" class="form-control" placeholder="Login" required autofocus>
			<input type="password" name="l_pass" ide="l_pass" class="form-control" placeholder="Hasło" required>
			<label class="checkbox">
          <input type="checkbox" value="remember-me"> zapamiętaj
        </label>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj</button>
		</form>
	</div>
	<div id="footer">
    <div class="container">
    	<ul class="nav navbar-nav">
      	<li><a href=""></a></li>
      </ul>
    </div>
  </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="templates/backend/devoops/js/bootstrap.min.js"></script>
</body>
</html>