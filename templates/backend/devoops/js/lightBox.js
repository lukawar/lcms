$(document).ready(function() {	
	//$('#contentPlace').on("click",'.light' ,function(event) {
	$('.light').click(function(event) {
		event.preventDefault();
		//var boxWidth = 0;
		var href = $(this).attr('href');
		var classInfo = $(this).attr('class');
		
		var wrapContent;
		
		if($(this).attr('title')=='' || $(this).attr('title')==undefined)
			var title = '';
		else var title = $(this).attr('title');
		
		//wraps
		var wrapContainer = '<div id="infoMaster"><div id="lightLoad"><img src="templates/backend/devoops/img/devoops_getdata.gif"></div></div>';
		var wrapHeader = '<div id="infoPanel" class="'+classInfo+'"><div id="infoContainer"><div id="infoHeader"><div id="infoTitle">'+title+'<div id="infoControls"><button id="infoClose" class="infoClose"></button></div></div></div><div id="infoContent">';
		var wrapFooter = '</div></div></div>';
		var wrapFalse = 'brak danych';
		
		$(wrapContainer).hide().appendTo("body").fadeIn(800);		
	  //$('body').append(wrapContainer);
		//showLoad();
		
		$.ajax({
  		url: href,
  		cache: false
		}).done(function(html) {
  		wrapContent = html;
			closeLoad();
  		$('body').append(wrapHeader+wrapContent+wrapFooter);
 
  		var infoPanelWidth = Math.floor($('#infoPanel').innerWidth()/2);
  		
  		var styles = {
      	marginLeft: '-'+infoPanelWidth+'px',
      	top: '10px',
      	/*visibility: 'visible'*/
    	};
 			$('#infoPanel').css(styles);
 			//$("#infoPanel").show();
    	/*$('#infoPanel').animate({
		    top: '20px'
		  }, 800, function() {
		   
		  });*/
		  $("#infoPanel, #infoPanel img, #infoPanel.zoom").show( "scale", 
         {percent: 100, direction: "both"}, 2000 );
		   
  		/*$('#infoPanel').css(styles);*/
  		var sh = window.innerHeight;
			var lh = $('#infoPanel').innerHeight();
			//console.log(sh+" / "+lh);
			if(sh<lh) {
				sh = sh - 30;	
				$('#infoContent').css('height',sh+'px');
			}
		}).fail(function(html) {
  		$('body').append(wrapHeader+wrapFalse+wrapFooter);
		});
	});
	
	$(document).on("click",'.infoClose' ,function() {
		lightClose();
	});
	
	$(document).on("click",'#infoMaster' ,function() {
		lightClose();
	});
	
	function lightClose() {
		$('#infoPanel').remove();
		$('#infoMaster').fadeOut(200, function() {
	    $('#infoMaster').remove();
  	});
		return false;
	}
	
	function showWindow() {
		
	}
	
	function showLoad() {
		var wrapLoad = "<div id='lightLoad'><img src='/templates/img/load.gif'></div>";
		$('body').append(wrapLoad);
	}
	
	function closeLoad() {
		$('#lightLoad').remove();
	}
	
	$(document).keydown(function(event) {
  	if(event.which==27)
  		lightClose();
	});

	//Message Box
	var lightMessage = $('#lightMessage');
	if(lightMessage.length) {
		$('body').append('<div id="infoMaster"></div>');
		$('#lightMessage').wrap('<div id="infoPanel" class="messageBox"><div id="infoContent"></div></div>');
		$('#lightMessage').append('<button type="button" class="infoClose">OK</button>');
	}
});

function changeRes() {
	var sh = window.innerHeight;
	var lh = $('#infoPanel').innerHeight();
	var shr;
	if(sh<lh) {
		shr = sh - 30;
		$('#infoContent').css('height',sh+'px');
	}
}

$(window).resize(function () {
	changeRes();
});

(function($) {
	$.fn.lightClose = function() {
		$('#infoPanel').remove();
		$('#infoMaster').fadeOut(200, function() {
	    $('#infoMaster').remove();
  	});
	}
	
	$.fn.messageText = function(info) {
		$('body').append('<div id="messageText"> '+info+' </div>');
		setTimeout("jQuery('#messageText').remove();", 2000);
	}
})(jQuery);