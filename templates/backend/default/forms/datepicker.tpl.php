<div class="form-group labeled">
	<label for="__NAME" class="col-sm-3 control-label">__LABEL</label>
	<div class="col-sm-9">
		<div class="input-group date" data-provide="datepicker">
		    <input type="text" class="form-control_o" id="__NAME" name="__NAME" value="__VALUE" __REQUIRED>
		    <div class="input-group-addon">
		        <span class="glyphicon glyphicon-th"></span>
		    </div>
		</div>
	</div>
</div>