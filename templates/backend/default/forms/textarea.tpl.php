<div class="form-group labeled">
	<label for="__NAME" class="col-sm-3 control-label">__LABEL</label>
	<div class="col-sm-9">
		<textarea class="form-control" name="__NAME" id="__NAME">__VALUE</textarea>
	</div>
</div>