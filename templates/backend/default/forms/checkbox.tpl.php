<div class="form-group labeled">
	<label for="__NAME" class="col-sm-3 control-label">__LABEL</label>
	<div class="col-sm-9">
		<div class="checkbox">
      <label>
        <input type="checkbox" name="__NAME" id="__NAME" __CHECKED __REQUIRE>
      </label>
    </div>
	</div>
</div>