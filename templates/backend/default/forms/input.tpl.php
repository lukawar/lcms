<div class="form-group labeled">
	<label for="__NAME" class="col-sm-12 col-md-3 control-label">__LABEL</label>
	<div class="col-sm-12 col-md-9">
		<input class="form-control" type="text" id="__NAME" name="__NAME" __VALUE __REQUIRED>
	</div>
</div>