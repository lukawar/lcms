<div class="form-group labeled">
	<label for="__NAME" class="col-sm-3 control-label">__LABEL</label>
	<div class="col-sm-9">
		__VALUE
		<div id="inputFileContainer"><input class="form-control __CLASS" type="file" id="__NAME" name="__NAME"></div>
	</div>
</div>