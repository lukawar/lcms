<!DOCTYPE html>
<html lang="pl">
<head>
	<title>Panel administracyjny</title>
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href="templates/backend/default/css/admin.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="templates/backend/default/css/login.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	
</head>
<body>
<div class="backgroundFull loginBackground">
	<div class="loginPanel">
		<div class="elementsContainer">
			<div class="elements">
				<div class="bbContainer"><img src="templates/backend/default/img/bb-big.png"><p>&nbsp;</p><h3>PANEL ADMINISTRACYJNY</h3></div>
				<div class="loginContainer">
					<form method="post" name="adminLogin" action="admin.php" class="form-horizontal">
						<div class="form-group"><div class="col-sm-12"><input type="text" name="l_login" placeholder="login" class="form-control"></div></div>
						<div class="form-group"><div class="col-sm-12"><input type="password" name="l_pass" placeholder="hasło" class="form-control"></div></div>
						<div class="form-group"><div class="col-sm-12"><button type="submit" name="l_submit" class="btn btn-default">Zaloguj</button></div></div>
						<input type="hidden" name="action" value="administrator">
						<input type="hidden" name="option" value="checkAdmin">
						<input type="hidden" name="loginForm" value="setLogin">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>