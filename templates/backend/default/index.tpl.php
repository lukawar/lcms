<!DOCTYPE html>
<html lang="pl">
<head>
	<title>Panel administracyjny</title>
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700&subset=latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin-ext" rel="stylesheet">
	<link href="templates/backend/default/css/admin.css" type="text/css" rel="stylesheet" media="screen,projection,print"/>
	<!--<link href="https://cask.scotch.io/bootstrap-4.0-flex.css" type="text/css" rel="stylesheet" media="screen,projection, print"/>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
	
	<script type="text/javascript" src="templates/backend/default/js/datatables.js"></script>
	<script src="templates/backend/default/js/script.js"></script>
	<script type="text/javascript" src="templates/default/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="templates/default/js/loadEditor.js"></script>	
	<script type="text/javascript" src="templates/default/js/datepicker.js"></script>	
	<script type="text/javascript" src="templates/default/js/treeview.js"></script>	
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-85891768-1', 'auto');
	  ga('send', 'pageview');

	</script>
</head>
<body>
<div id="backgroundsContainer">
	<div class="bcLayer1"></div>
	<div class="bcLayer2"></div>
</div>
<div id="panelContentFlex" class="container">
	<div class="spacer50 row rownm"><div class="col-md-12"></div></div>
	<div id="panelContainerInner" class="normal">
		
		<!--top menu container-->
		<div id="topContainer" class="row rownm">
			<div id="topContainerLeft" class="col-xs-6 col-sm-6 col-md-2"><a href="admin.php"><div class="nameContainer">BEABONUS</div></a></div>
			<div id="topContainerRight" class="col-xs-6 col-md-10">
				<div class="dropdown dropdown-right showOnL">
					<button class="btn btn-menu" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
				  </button>
						
				  <ul class="dropdown-menu drop-user" aria-labelledby="dLabel">
				  	<li><a href="admin.php?action=options&option=profile"><i class="material-icons">settings</i> profil</a></li>
				  	<li><a href="admin.php?action=options&option=settings"><i class="material-icons">settings</i> opcje</a></li>
				    <li class="danger"><a href="logout.php?redirect=admin"><i class="material-icons">power_settings_new</i> wyloguj</a></li>
				    
				  </ul>
				</div>
				
				<div class="dropdown-right showOnS">
					<button class="btn btn-menu" type="button" id="mobileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <i class="material-icons">menu</i>
				  </button>
				</div>
				
			</div>
		</div>
		
		<!--header-->
		<div id="headContainer" class="row rownm">
			<div id="headContainerUser" class="col-md-2">
				<div id="headContainerCloakB"></div>
				<div id="headContainerAvatar"><img src="{/options:avatar/}" vspace="5" hspace="5"></div>
				<div id="headContainerName">{/options:adminName/}</div>
			</div>
			<div class="col-md-10"></div>
			<div class="headBoxLeft col-md-2"></div><div class="headBoxRight col-md-10"></div>
		</div>
		
		<!--content-->
		<div id="contentContainer" class="row rownm">
			<div id="contentMenuLeft" class="col-md-2"><div id="menuLeft">{/admin_menu/}</div></div>
			<div id="contentPanel" class="col-xs-12 col-md-10">{/content/}</div>
		</div>
		
	</div>
	<div class="spacer50 row rownm"><div class="col-md-12"></div></div>
</div>

<!--modal-->
<div class="modal fade" id="modalWindow" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="modalLabel"></h5>
      </div>
      <div class="modal-body_p toPrint" id="modalBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" onclick="setPrint()">Wydruk</button> <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
      </div>
    </div>
  </div>
</div>
<script>
function setPrint() {
    window.print();
}
</script>
</body>
</html>