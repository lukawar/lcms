$(document).ready(function() {
	//initialize tables
	$('#dataTable').DataTable({
		 fixedHeader: true
	});
	
	//focused inputs
	$('.labeled input').focus(function() {
		$(this).parent().parent().addClass('active');
	});
	$('.labeled input').focusout(function() {
		$(this).parent().parent().removeClass('active');
	});
	
	//buttons action
	$("#mobileMenu").click(function(event) {
		$('#menuLeft').toggleClass('show');
	})
	
	$(".cancelBtn").click(function(event) {
    event.preventDefault();
    document.location.href = $(this).attr('url');
	});
	
	$.fn.datepicker.defaults.format = "yyyy-mm-dd";
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
	});
	
	$(document).on("click",'.userDetails' ,function(event) {
		event.preventDefault();
		$.post('ajax.php', {action: 'user', option: 'details', id: $(this).attr('href')})
		.done(function(response){
			var data = jQuery.parseJSON(response);
			$('#modalBody').html(data.responseText);
			$('#modalLabel').html(data.modalLabel);
			$('#modalWindow').modal('show');
		});
	});
	
	/*$(document).on("click",'.pageMaximize' ,function(event) {
		event.preventDefault();
		$('body').addClass('modal-open');
	});*/
	
	//attachments
	var lesson_att_line = '<p><input type="file" name="PFO_FILE[]" class="form-control fileAttachment"></p>';
	$(document).on('change','.fileAttachment', function() {
     //$('#inputFileContainer').append(lesson_att_line);
     $(this).parent().append(lesson_att_line);
     //console.log('file add');
  });
  
  $(document).on("click",'.foto-links' ,function(event) {
		event.preventDefault();
		if (confirm("Czy na pewno chcesz usunąć?")) {
			var id = $(this).attr('href');
			$.post('ajax.php', {action: 'produkty', option: 'delPhoto', photo: id})
			.done(function(repsonse){
				$('#photo-'+id).remove();
			});
			
		}
	});
	
	//order functions ex
	$(document).on('change','#OST_STATUS_ex' ,function(event) {
		var status = $(this).val();
		if(status==='sended') {$('#OST_SEND_CODE_ex').prop('required',true);$('#OST_SEND_FIRM_ex').prop('required',true);$('#OST_SEND_DATE_ex').prop('required',true);} 
		else {$('#OST_SEND_CODE_ex').prop('required',false);$('#OST_SEND_FIRM_ex').prop('required',false);$('#OST_SEND_DATE_ex').prop('required',false);}
	});
	
	//order functions bb
	$(document).on('change','#OST_STATUS_bb' ,function(event) {
		var status = $(this).val();
		if(status==='sended') {$('#OST_SEND_CODE_bb').prop('required',true);$('#OST_SEND_FIRM_bb').prop('required',true);$('#OST_SEND_DATE_bb').prop('required',true);} 
		else {$('#OST_SEND_CODE_bb').prop('required',false);$('#OST_SEND_FIRM_bb').prop('required',false);$('#OST_SEND_DATE_bb').prop('required',false);}
	});
});

//initialize fixed page headers
$(function(){
	var shrinkHeader = 50;
	var shrinkMenuLeft = 310;
	var shrinkToolbar = 310;
	var menuHeight = $('#menuLeft').height() + 60;
	var h = window.innerHeight;
	
	$(window).scroll(function() {
    var scroll = getCurrentScroll();
    if(scroll >= shrinkHeader) $('#panelContainerInner').removeClass('normal').addClass('shrink');
    	else $('#panelContainerInner').removeClass('shrink').addClass('normal');
    	
    if(h>menuHeight) {
	    if(scroll >= shrinkMenuLeft) $('#menuLeft').addClass('shrink');
	    	else $('#menuLeft').removeClass('shrink'); 
	  }
    
    if(scroll >= shrinkToolbar) $('#pageToolbar').addClass('shrink');
    	else $('#pageToolbar').removeClass('shrink');
  });

	function getCurrentScroll() {return window.pageYOffset || document.documentElement.scrollTop;}
});

//DIALOGS
function dialog_del(plik) {
  if (!confirm("Czy na pewno chcesz usunąć tę pozycję?"))
  return ;
  window.location=plik;
}