<!DOCTYPE html>
<html lang="pl">
<head>
	<title>Panel administracyjny</title>
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Anton&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href="templates/backend/default/css/admin.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="https://cask.scotch.io/bootstrap-4.0-flex.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="templates/backend/default/js/script.js"></script>
	
</head>
<body>
<div id="backgroundsContainer">
	<div class="bcLayer1"></div>
	<div class="bcLayer2"></div>
</div>
<div id="panelContainer">
	<div id="panelContainerInner" class="normal">
	
		<!--top menu container-->
		<div id="topContainer">
			<div id="topContainerLeft"></div>
			<div id="topContainerRight"></div>
		</div>
		
		<!--header-->
		<div id="headContainer">
			<div id="headContainerCloakB"></div>
		</div>
		
		<!--content-->
		<div id="contentContainer">
			<div id="contentMenuLeft"> - {/admin_menu/} - </div>
			<div id="content">{/content/}</div>
			<div>
				<p><a href="logout.php?redirect=admin">wyloguj się</a></p>
			</div>
		</div>
		
	</div>
</div>
</body>
</html>