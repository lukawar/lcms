{/includefile:header.tpl.php/}
<body>
	<div id="mn" class="main-nav">
		<div class="container">
		<div class="row">
			
			<nav class="navbar	">

			{/includefile:top-menu.tpl.php/}
			{/user:usrBox/}
			</nav>
			
		</div>
		</div>
	</div>

  <header>
 		<div id="slick-news">{/news:homepage/}</div>
	</header>


	<div class="container contents">
	  <div class="row">
	    <div class="col-sm-12 col-md-9">
			<main>
				<h3 class="block_title">NOWOŚCI</h3>
				<div class="slick-homepage">
					{/nowosci:homepage/}
				</div>
				<p>&nbsp;</p>
				<h3 class="block_title">PROMOCJE</h3>
				<div class="slick-homepage">
					{/promocje:homepage/}
				</div>
			  
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
			</main>

	    </div>

		<aside class="col-sm-12 col-md-3 sidebar"> 
		{/produkty:filterSearch/}
			<section id="bonus_block">
			<h3 class="block_title block_title_narr">Katalog bonusów</h3>
				<ul class="categories">
					{/category/}
				</ul>
			</section>
		{/produkty:offerPoints/}
		</aside>

	  </div>
	</div>
{/includefile:footer.tpl.php/}
</body>
</html>