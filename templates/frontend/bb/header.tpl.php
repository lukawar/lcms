<!doctype html>
<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="test">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Beabeza</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="templates/frontend/bb/css/bootstrap.min.css">
    <link rel="stylesheet" href="templates/frontend/bb/css/style.css">
    <link rel="stylesheet" href="templates/frontend/bb/css/owl.carousel.css">
    <link rel="stylesheet" href="templates/frontend/bb/css/slick-theme.css">
    <link rel="stylesheet" href="templates/default/css/ekko-lightbox.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="templates/frontend/bb/js/slick.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="templates/default/js/ekko-lightbox.js"></script>
    <script src="templates/frontend/bb/js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Ubuntu:300,400,500,700&subset=latin-ext" rel="stylesheet">
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85891768-1', 'auto');
  ga('send', 'pageview');

</script>
  </head>