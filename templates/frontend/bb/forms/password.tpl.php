<div class="row form-group labeled">
	<label for="__NAME" class="col-sm-2 control-label">__LABEL</label>
	<div class="col-sm-10">
		<input class="form-control" type="password" id="__NAME" name="__NAME" __VALUE __REQUIRED>
	</div>
</div>