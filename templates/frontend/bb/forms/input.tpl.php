<div class="row form-group labeled">
	<label for="__NAME" class="col-xs-4 col-sm-2 control-label">__LABEL</label>
	<div class="col-xs-8 col-sm-10">
		<input class="form-control" type="text" id="__NAME" name="__NAME" __VALUE __REQUIRED>
	</div>
</div>