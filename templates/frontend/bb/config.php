<?php
$config_template = array(
	'homepage'=>'homepage.tpl.php',
);

$login_require = array(
	'login' => 'login.tpl.php',
	'action' => 'user',
	'option' => 'checkLogin'
);

$config_page = array();

$config_file = array(
	'alias'=>'bb',
	'name'=>'BeaBeleza  v1.0',
	'description'=>'Podstawowa skórka',
	'author'=>'expansja.pl',
	'date_add'=>'2016-08-02',
	'thumbnail'=>'templates/frontend/bb/img/thumbnail.jpg',
);