$(document).ready(function() {

	var menuElement  = document.querySelector(".main-nav");
	var navbarElement = $('.navbar-collapse');

	window.addEventListener('scroll', function(e) {
		mpos = menuElement.getBoundingClientRect();
		// console.log("klasy", menuElement.className +'_'+ mpos.top +' _ '+ ' \ ', mpos.top);
		if (pageYOffset >344) {

			//przy skrollowaniu można też dodać regółę dla mobila, niech nagłówek się zmniejszy do minimalnej potrzebnej wysokości
			menuElement.classList.add("main-nav-scrolled");
			
			//menuElement.className += menuElement.className.length <= 9 ? ' main-nav-scrolled' : '';
		} else {
			menuElement.classList.remove("main-nav-scrolled");
		}
	});

	


//$('.dropdown-toggle').dropdown('toggle');

	var isMobile = window.matchMedia("only screen and (max-width: 992px)").matches;
	$( window ).resize(function() {
		isMobile = window.matchMedia("only screen and (max-width: 992px)").matches;
	});
	// console.log("is mobile: " + isMobile);

	//copy sidebar menu to main in mobile
	if (isMobile) {
		var bonus_block = document.querySelector('#bonus_block');

		var navbar_collapse = document.querySelector('#bs-example-navbar-collapse-1');
		navbar_collapse.appendChild(bonus_block);

	} 

	function navbar_helper(){
		console.log("to remowe cls");
		isMobile ? navbarElement.removeClass('in') : '';
	}


	navbar_helper();

	$(document).on('click', '.thumbElement', function(event) {
		event.preventDefault();
		var data = '<a href="'+$(this).attr('href')+'"><img src="'+$(this).attr('thumb')+'"></a>';
		$('#productImage').html(data);
	});
	
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
	});
	
	$('#slick-news').slick({ 
    slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		dots: true,
		autoplaySpeed: 4000,
  });
	
	 $('#slick-sidebar').slick({ 
    	slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
  });
  
  $('.slick-homepage').slick({
    slidesToShow: isMobile? 1 : 3,
	  slidesToScroll: 1,
	  autoplay: false,
	  dots: true,
	  autoplaySpeed: 4000,
  });
  
	
	$('#userPanel a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show')
	});

	$(document).on("click",'.buttonUrl' ,function(event) {
		window.location = $(this).attr('url');
	});
	
	$(document).on("click",'.buttonUrlConfirm' ,function(event) {
		if (!confirm("Czy na pewno chcesz usunąć tę pozycję?"))
  	return ;
		window.location = $(this).attr('url');
	});
	
	//-------------------------------------------------------------------------------------
	
	$('#addToCart, .addToCart').click(function() {
		$(this).prop("disabled", true);
		var count = $('#productCount').val();
		
		if(count<0 || count==null)
			count = 1;
		
		$.post('get.php', {action: 'cart', option: 'add', id: $(this).attr('product'), count: count})
		.done(function(response){
			if($(this).attr('gotocart')=='true')
				window.location = 'cart-check.html';
			var data = jQuery.parseJSON(response);
			if(data.result===true) {
				$('#cartPositions').html(data.cartContent);
				$('#userPoints').html(data.pointsAll);
				$('#cartData').html(data.countAll + ' szt. / ' + data.valueAll + ' pkt.');
			}
			$('#modalBody').html(data.responseText);
			$('#addToCart').prop("disabled", false);
			$('#modalWindow').modal('show');
		});
	});
	
	$(document).on('change','.cart-count' ,function(event) {
		$(this).prop("disabled", true);
		var productCount = $(this).val();
		$.post('get.php', {action: 'cart', option: 'change', id: $(this).attr('product'), pCount: productCount})
		.done(function(response){
			var data = jQuery.parseJSON(response);
			if(data.response==true) {
				$('#userPoints').html(data.pointsAll);
				$('#cartData').html(data.countAll + ' szt. / ' + data.valueAll + ' pkt.');
				//$('#modalBody').html(data.responseText);
				$('#cartPositions').html(data.cartContent);
				//$('#modalWindow').modal('show');
			} else {
				$('#cartPositions').html(data.cartContent);
				$('#modalBody').html(data.responseText);
				$('#modalWindow').modal('show');
			}
		});
		$(this).prop("disabled", false);
	});
	
	$(document).on("click",'.cartPosDel' ,function(event) {
		event.preventDefault();
		$(this).prop("disabled", true);
		$.post('get.php', {action: 'cart', option: 'delete', id: $(this).attr('product')})
		.done(function(response){
			var data = jQuery.parseJSON(response);
			$('#userPoints').html(data.pointsAll);
			$('#cartData').html(data.countAll + ' szt. / ' + data.valueAll + ' pkt.');
			$('#modalBody').html(data.responseText);
			$('#cartPositions').html(data.cartContent);
			$('#modalWindow').modal('show');
		});
	});
	
	$(document).on("click",'#userAddAddress' ,function(event) {
		$(this).prop("disabled", true);
		event.preventDefault();
		$.post('get.php', {action: 'user', option: 'formSendAddress'})
		.done(function(response){
			$('#orderContent').html(response);
			$('#userAddAddress').prop("disabled", false);
		});
	});
	
	$(document).on("click",'.orderDetails' ,function(event) {
		event.preventDefault();
		$.post('get.php', {action: 'order', option: 'details', id: $(this).attr('href')})
		.done(function(response){
			var data = jQuery.parseJSON(response);
			$('#modalBody').html(data.responseText);
			$('#modalLabel').html(data.modalLabel);
			$('#modalWindow').modal('show');
		});
	});
	
	$(document).on("click",'.sortUrl' ,function(event) {
		event.preventDefault();
		var by = $(this).attr('by');
		var type = $(this).attr('type');
		var urlHref = $(this).attr('href');
		
		$.post('get.php', {action: 'produkty', option: 'setSortType', by: by, type: type})
		.done(function(response){
			if(response==='sorted')
				window.location = urlHref;
		});
	});
	
	//add to chest
	$(document).on("click",'#addToChest' ,function(event) {
		event.preventDefault();
		var product = $(this).attr('product');
		
		$.post('get.php', {action: 'chest', option: 'add', product: product})
		.done(function(response){
			if(response!='false')
				$('#chest-button').html(response);
		});
	});
	
	//delete from chest
	$(document).on("click",'.chestPosDel' ,function(event) {
		if (confirm("Czy na pewno chcesz usunąć tę pozycję?")) {
			event.preventDefault();
			var product = $(this).attr('product');
			
			$.post('get.php', {action: 'chest', option: 'del', product: product})
			.done(function(response){
				if(response!='false')
					$('#przechowalnia').html(response);
			});
		}
	});
	
});

function dialog_del(plik) {
  if (!confirm("Czy na pewno chcesz usunąć tę pozycję?"))
  return ;
  window.location=plik;
}