<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="makieta">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bea Beleza / Bea Benefit</title>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="templates/frontend/bb/css/bootstrap.css">
        <link rel="stylesheet" href="templates/frontend/bb/css/style.css">
    </head>
    <body>


<div class="wrapper">
	<div class="container logpage">
		<div class="row">
			<div class="col-sm-12 col-md-7 col-lg-5 bblogo">
				<img src="templates/frontend/bb/img/beabeleza_logo.png" alt="logo Bea Beleza">
			</div>
		</div>
	  <div class="row">
		    <div class="col-sm-12 col-md-7 col-lg-5">
		        <h2>Witamy serdecznie w Bea&nbsp;Bonus,</h2>
		        <p>programie lojalnościowym dla salonów fryzjerskich. Jeśli chcesz do nas dołączyć, skontaktuj się z <a href="http://www.beabeleza.pl/Kontakt#kontakt" target="_blank">Przedstawicielem Handlowym Bea Beleza Polska</a>.</p>
			
			<div class="formblock">
				<h2 class="log-head">Zaloguj się</h2>
				<p>Użyj loginu oraz hasła z pakietu startowego,
				który otrzymałeś od Twojego Przedstawiciela Handlowego</p>
				<div id="loginInfo"></div>
				<form name="loginForm" method="post" action="index.php">
	  			<div class="form-group">
						<input type="text" class="form-control" name="u_login" placeholder="login / email">
						<input type="password" class="form-control" id="mainPass" name="u_password" placeholder="hasło">
						
						<input type="submit" class="loginInput" value="zaloguj">
	  			</div>
	  			<input type="hidden" name="loginForm" value="login">
				</form>
				
			</div>
		    <img class="bok" src="templates/frontend/bb/img/bok-mock.png" alt="bok">
		  	</div>
	    </div>
	  </div>
</div>
</body>
</html>