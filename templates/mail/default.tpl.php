<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>BeaBonus</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<style>
		html, body {
			height: 100%;
			padding: 0;
			margin: 0;
			background: #f2f2f2;
		}
		body, div, p, a {
			font-family: 'Verdana';
			color: #333;
			text-decoration: none;
			font-size: 14px;
		}
		
		h3 {
			font-family: 'Verdana';
			color: #333;
			font-size: 16px;
		}
		
		a {color: #333;}
		span {
			color: red;
			font-size: 7pt;
		}
		
		hr {
			background-color: #09b0f0;
			height: 1px;
			border:0;
		}
		
		.dotted {
			height: 1px;
			margin: 15px 0;
			border-bottom: 1px dashed #dedede;

		}
		
		.table-striped {
			width: 100%;
			border-collapse: collapse;
		}
		
		.table-striped th {border-top: 1px solid #dedede; background: #f1f1f1;text-align: left;}
		
		.table-striped th, .table-striped td {
			border-top: 1px solid #dedede;
			padding: 7px;
			font-size: 14px;
		}
		
		.alignright {
			text-align: right!important;
		}
		
		.bolded {
			font-weight: bold;
		}
		</style>
	</head>
	<body style="margin: 0; padding: 0; border: 0 none; font: inherit; font-size: 13px; color: #333; vertical-align: baseline; background-color: #f2f2f2; background-repeat: no-repeat; background-attachment: scroll; background-position: 0 0; background-image: none; line-height: 20px; overflow: auto; font-family: Lato,Roboto,Helvetica,Arial,sans-serif; font-weight: 300;">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td style="padding: 20px; border-bottom: 1px solid #c0c0c0;"><img src="cid:logo.png" border="0" align="left" width="70px"/></td></tr>
		<tr><td style="background: #fff;padding: 40px 20px">__CONTENT</td></tr>
		<tr><td style="padding: 20px;border-top: 1px solid #c0c0c0;"><a style="color: #a6a6a6"" href="http://www.beabeleza.pl/">www.beabeleza.pl</a></td></tr>
		<tr><td style="padding: 20px;border-top: 1px solid #c0c0c0;text-align: center; font-size: 10px;color: #a6a6a6">telefon: <strong> +48 61/8411752</strong>  &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;  e-mail: <a href="mailto:bonus@beabeleza.pl" style="font-size: 10px;color: #a6a6a6;"><strong>bonus@beabeleza.pl</strong></a></td></tr>
		</table>
	</body>
</html>